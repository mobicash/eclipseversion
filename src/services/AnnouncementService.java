package services;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.conn.ConnectTimeoutException;

import utils.ConnectUtils;
import utils.Transaction;
import utils.Vars;
import android.app.Service;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.IBinder;
import android.preference.PreferenceManager;
import ez.pay.project.Globals;

public class AnnouncementService extends Service {

	SharedPreferences prefs;
	SharedPreferences.Editor editor;
	String orderId;
	Transaction trans;
	String announcement, description;
	Vars vars;

	@Override
	public void onCreate() {
		// TODO Auto-generated method stub
		super.onCreate();

	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		// TODO Auto-generated method stub

		// TODO Auto-generated method stub
		prefs = PreferenceManager.getDefaultSharedPreferences(this);
		editor = prefs.edit();
		vars = new Vars(this);
		new AnnouncementAsyncTask().execute();

		return super.onStartCommand(intent, flags, startId);

	}

	@Override
	public void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
	}

	@Override
	public IBinder onBind(Intent intent) {
		// TODO Auto-generated method stub
		return null;
	}

	public void retrofitRequest(String orderId) {

	}

	public class AnnouncementAsyncTask extends AsyncTask<String, Void, String> {
		String json;
		public String theResult = null;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();

		}

		@Override
		protected String doInBackground(String... arg) {

			StringBuilder builder = new StringBuilder();
			ConnectUtils connector = new ConnectUtils();
			HttpClient client = connector.getNewHttpClient();

			String urlString = null;

			// LOAD SERVER PREF

			urlString = vars.server
					+ "/bio-api/android/androidNewBroadcast.php";

			HttpGet httpGet = new HttpGet(urlString);

			try {
				HttpResponse response = client.execute(httpGet);
				StatusLine statusLine = response.getStatusLine();
				int statusCode = statusLine.getStatusCode();
				if (statusCode == 200) {
					HttpEntity entity = response.getEntity();
					InputStream content = entity.getContent();
					BufferedReader reader = new BufferedReader(
							new InputStreamReader(content));
					String line;
					while ((line = reader.readLine()) != null) {
						builder.append(line);
					}
					json = builder.toString();

				} else {

				}
			} catch (ConnectTimeoutException e) {

				e.printStackTrace();
			} catch (ClientProtocolException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}

			return json;
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);

			try {

				trans = new Transaction(result);

				// IF OUTPUT WAS RECEIVED FROM SERVER
				if (trans.getMessage() != null) {

					if (trans.getResult().equals("Success")) {
						announcement = trans.getMessage();
						description = trans.getExtra1();

						String appVersion = Globals.version;
						String currentVersion = trans.getExtra2();

						editor.putString("version", currentVersion);

						editor.putString("announcement", announcement);
						editor.putString("description", description);
						editor.commit();
					} else {

					}
				} else {

				}

			} catch (Exception e) {

			}

		}
	}

}
