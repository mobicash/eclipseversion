package services;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.UUID;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothServerSocket;
import android.bluetooth.BluetoothSocket;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import ez.pay.project.Dstv;

/**
 * ����ദ�����й��������ú͹����������������豸 ����һ���߳�,������������
 * һ���߳����������豸��һ���߳�����ִ�����ݴ�������ʱ
 * */
public class BluetoothService {

	private static final String TAG = "BluetoothService";
	private static final boolean D = true;

	// �ڴ����������׽��ּ�¼SDP������
	private static final String NAME = "BTPrinter";
	// �����Ӧ�ó�������һ�����ص�UUID
	private static final UUID MY_UUID = UUID
			.fromString("00001101-0000-1000-8000-00805F9B34FB");

	private final BluetoothAdapter mAdapter;
	private final Handler mHandler;
	private AcceptThread mAcceptThread;
	private ConnectThread mConnectThread;
	private ConnectedThread mConnectedThread;
	private int mState;
	public static byte[] BluetoothRecvBuf = new byte[1024];
	public static int BluetoothRecvBufLen = 0;
	public static boolean BluetoothRecvOK = false;
	// ��ǰ����״̬�ĳ���
	public static final int STATE_NONE = 0; // Ĭ�ϵ�,ʲô��û��
	public static final int STATE_LISTEN = 1; // �������ӵļ���
	public static final int STATE_CONNECTING = 2;// ����һ����������
	public static final int STATE_CONNECTED = 3; // ���ӵ�һ��Զ���豸

	/**
	 * ���캯����׼��һ���µ�BTPrinter�Ự ontext UI������� handler
	 * �����������Ϣ���ͻ�UI�
	 */
	public BluetoothService(Context context, Handler handler) {
		mAdapter = BluetoothAdapter.getDefaultAdapter();
		mState = STATE_NONE;
		mHandler = handler;
	}

	/**
	 * ���õ�ǰ״̬������ state ���嵱ǰ������״̬
	 */
	private synchronized void setState(int state) {
		if (D)
			Log.d(TAG, "setState()" + mState + "->" + state);
		mState = state;

		// ����״̬Handler��UI���Ը���
		mHandler.obtainMessage(Dstv.MESSAGE_STATE_CHANGE, state, -1)
				.sendToTarget();
	}

	/**
	 * ���ص�ǰ������״̬
	 */
	public synchronized int getState() {
		return mState;
	}

	/**
	 * ���������ر���������(������)ģʽ��ʼAcceptThread �Ự,���ûonResume()
	 */

	public synchronized void start() {
		if (D)
			Log.d(TAG, "start");

		// ȡ���κ��߳���ͼ����
		if (mConnectThread != null) {
			mConnectThread.cancel();
			mConnectThread = null;
		}
		// ������һ������ʱȡ���κ��߳�
		if (mConnectedThread != null) {
			mConnectedThread.cancel();
			mConnectedThread = null;
		}

		// �����߳�������BluetoothServerSocket
		if (mAcceptThread == null) {
			mAcceptThread = new AcceptThread();
			mAcceptThread.start();
		}
		setState(STATE_LISTEN);
	}

	/**
	 * ����ConnectThread�������ӵ�һ��Զ���豸�� device BluetoothDevice������
	 */
	public synchronized void connect(BluetoothDevice device) {
		if (D)
			Log.d(TAG, "connect to:" + device);

		// ȡ���κ��߳���ͼ����
		if (mState == STATE_CONNECTING) {
			if (mConnectThread != null) {
				mConnectThread.cancel();
				mConnectThread = null;
			}

		}
		// ��������һ������ʱȡ���κ��߳�
		if (mConnectedThread != null) {
			mConnectedThread.cancel();
			mConnectedThread = null;
		}
		// �����߳���������豸
		mConnectThread = new ConnectThread(device);
		mConnectThread.start();
		setState(STATE_CONNECTING);

	}

	/**
	 * ����ConnectedThread��ʼ����һ����������
	 * 
	 * @param socket
	 *            BluetoothSocket�ϵ�����
	 * @param device
	 *            ���BluetoothDevice�Ѿ�����
	 */
	public synchronized void connected(BluetoothSocket socket,
			BluetoothDevice device) {
		if (D)
			Log.d(TAG, "Connected");
		// ȡ���߳��������
		if (mConnectThread != null) {
			mConnectThread.cancel();
			mConnectThread = null;
		}
		// ��������һ������ȡ���κ��߳�
		if (mConnectedThread != null) {
			mConnectedThread.cancel();
			mConnectedThread = null;
		}
		// ȡ�������߳�,��Ϊֻ�������ӵ�һ���豸
		if (mAcceptThread != null) {
			mAcceptThread.cancel();
			mAcceptThread = null;
		}
		// �����߳����������Ӻ�ִ�д���
		mConnectedThread = new ConnectedThread(socket);
		mConnectedThread.start();

		// ���������豸�����ƻص�UI�
		Message msg = mHandler.obtainMessage(Dstv.MESSAGE_DEVICE_NAME);
		Bundle bundle = new Bundle();
		bundle.putString(Dstv.DEVICE_NAME, device.getName());
		msg.setData(bundle);
		mHandler.sendMessage(msg);

		setState(STATE_CONNECTED);
	}

	/**
	 * ֹͣ���е��߳�
	 */
	public synchronized void stop() {
		if (D)
			Log.d(TAG, "stop");
		setState(STATE_NONE);
		if (mConnectThread != null) {
			mConnectThread.cancel();
			mConnectThread = null;
		}
		if (mConnectedThread != null) {
			mConnectedThread.cancel();
			mConnectedThread = null;

		}
		if (mAcceptThread != null) {
			mAcceptThread.cancel();
			mAcceptThread = null;
		}
	}

	/**
	 * ��һ����ͬ����ConnectedThread��ʽ��д
	 * 
	 * @param out
	 *            д���ֽ�
	 */
	public void write(byte[] out, int off, int len) {
		// ������ʱ����
		// public void write(byte[] out) {
		ConnectedThread r;
		// ConnectedThreadͬ���ĸ���
		synchronized (this) {
			if (mState != STATE_CONNECTED)
				return;
			r = mConnectedThread;
		}
		// ִ��д��ͬ��
		r.write(out);
	}

	/**
	 * ����ConnectThread�������ӵ�һ��Զ���豸
	 * 
	 * @param device
	 *            BluetoothDevice������
	 */

	/**
	 * ��������ʧ�ܲ�֪ͨUI�
	 */
	private void connectionFailed() {
		setState(STATE_LISTEN);
		// һ��ʧ����Ϣ���ͻػ

		Message msg = mHandler.obtainMessage(Dstv.MESSAGE_TOAST);
		Bundle bundle = new Bundle();
		bundle.putString(Dstv.TOAST, "�������ӵ��豸��");
		msg.setData(bundle);
		mHandler.sendMessage(msg);

	}

	/**
	 * ��ʾ������Ӷ�ʧ��֪ͨUI���
	 */
	private void connectionLost() {
		// һ��ʧ����Ϣ���͸�Activity
		Message msg = mHandler.obtainMessage(Dstv.MESSAGE_TOAST);
		Bundle bundle = new Bundle();
		bundle.putString(Dstv.TOAST, "�ѶϿ����ӡ�������ӣ�");
		msg.setData(bundle);
		mHandler.sendMessage(msg);
	}

	/**
	 * ����߳����м������������,����һ���������˿ͻ���,������,ֱ��һ�����ӱ�����(����,ֱ�
	 * ���ȡ��)��
	 */
	private class AcceptThread extends Thread {
		// ���ط�����Socket
		private final BluetoothServerSocket mmServerSocket;

		public AcceptThread() {
			BluetoothServerSocket tmp = null;
			// ����һ���µļ���������Socket
			try {
				tmp = mAdapter
						.listenUsingRfcommWithServiceRecord(NAME, MY_UUID);
			} catch (IOException e) {
				// TODO �Զ����ɵ�catch��
				Log.e(TAG, "listen() failed", e);
			}
			mmServerSocket = tmp;

		}

		public void run() {
			if (D)
				Log.d(TAG, "BEGIN mAcceptThread" + this);
			setName("AcceptThread");
			BluetoothSocket socket = null;

			// ����ǲ����ӵ���������Socket

			while (mState != STATE_CONNECTED) {
				try {
					// һ����������,ֻ�᷵�سɹ����ӻ�һ������
					socket = mmServerSocket.accept();
				} catch (IOException e) {
					Log.e(TAG, "accept() failed", e);
					break;
				}

				// ������ӱ�����
				if (socket != null) {
					synchronized (BluetoothService.this) {
						switch (mState) {
						case STATE_LISTEN:
						case STATE_CONNECTING:
							// �������,�������ӵ��̡߳�
							connected(socket, socket.getRemoteDevice());
							break;
						case STATE_NONE:
						case STATE_CONNECTED:
							// Ҫôû��׼���û��Ѿ�����,��ֹ�µ� socket.
							try {
								socket.close();
							} catch (IOException e) {
								Log.e(TAG, "Could not close unwanted socket", e);
							}
							break;
						}
					}
				}
			}
			if (D)
				Log.i(TAG, "END mAcceptThread");
		}

		public void cancel() {
			if (D)
				Log.d(TAG, "cancel " + this);
			try {
				mmServerSocket.close();
			} catch (IOException e) {
				Log.e(TAG, "close() of server failed", e);
			}

		}

	}

	/**
	 * ����ConnectedThread��ʼ����һ���������� socket BluetoothSocket�ϵ�����
	 * device װ�õ�BluetoothDevice�Ѿ�����
	 */

	/**
	 * ����߳�������ͼʹһ����������һ���豸��������ֱͨҪô���ӳɹ�Ҫôʧ�ܡ�
	 */
	private class ConnectThread extends Thread {
		private final BluetoothSocket mmSocket;
		private final BluetoothDevice mmDevice;

		public ConnectThread(BluetoothDevice device) {
			mmDevice = device;
			BluetoothSocket tmp = null;
			// Ϊһ�����ӵ�BluetoothDevice�õ�һ��BluetoothSocket
			try {
				tmp = device.createRfcommSocketToServiceRecord(MY_UUID);
			} catch (IOException e) {
				Log.e(TAG, "create failed", e);
			}
			mmSocket = tmp;
		}

		public void run() {
			Log.i(TAG, "BEFIN mConnectThrread");
			setName("ConnectThread");

			mAdapter.cancelDiscovery();

			// ��BluetoothSocket������

			try {
				// һ���������÷��سɹ����ӻ�һ������
				mmSocket.connect();
			} catch (IOException e) {

				connectionFailed();
				// �ر�socket
				try {
					mmSocket.close();
				} catch (IOException e2) {
					Log.e(TAG,
							"unable to close() socket during connection failure",
							e2);
				}
				// ������������������ģʽ
				BluetoothService.this.start();
				return;
			}
			// ����ConnectThread
			synchronized (BluetoothService.this) {
				mConnectThread = null;

			}
			// �������ӵ��߳�
			connected(mmSocket, mmDevice);

		}

		public void cancel() {
			try {
				mmSocket.close();
			} catch (IOException e) {
				Log.e(TAG, "close() of connect socket failed", e);
			}
		}
	}

	/**
	 * ����߳�����������һ��Զ�̵��豸�� ���������д���ʹ����Ĵ��䡣
	 */
	private class ConnectedThread extends Thread {
		private final BluetoothSocket mmSocket;
		private final InputStream mmInStream;
		private final OutputStream mmOutStream;

		public ConnectedThread(BluetoothSocket socket) {
			Log.d(TAG, "create ConnectedThread");
			mmSocket = socket;
			InputStream tmpIn = null;
			OutputStream tmpOut = null;

			// BluetoothSocket����������
			try {
				tmpIn = socket.getInputStream();
				tmpOut = socket.getOutputStream();
			} catch (IOException e) {
				Log.e(TAG, "temp sockets not created", e);
			}

			mmInStream = tmpIn;
			mmOutStream = tmpOut;
		}

		public void run() {
			Log.i(TAG, "BEGIN mConnectedThread");
			int bytes, i;
			while (true) {
				try {
					byte[] buffer = new byte[256];
					// ��InputStream���Ķ�
					bytes = mmInStream.read(buffer);
					if (bytes > 0) {
						// ���ͻ�õ��ֽڵ�UI�� Activity
						// System.out.println(bytes+""+"-----");
						mHandler.obtainMessage(Dstv.MESSAGE_READ, bytes, -1,
								buffer).sendToTarget();

						String Hex = "";

						for (i = 0; i < bytes; i++) {
							BluetoothRecvBuf[BluetoothRecvBufLen++] = buffer[i];
							// Hex = Hex + buffer[i]+""+" ";
							System.out.println(buffer[i] + "::");
						}

						System.out.println("�յ���:" + Hex);
						if (BluetoothRecvBuf[BluetoothRecvBufLen - 1] == 0x03) {
							System.out.println("rec OK");
							BluetoothRecvOK = true;

						} else {
							System.out.println("END ERROR");
						}

					} else {
						Log.e(TAG, "disconnected");
						connectionLost();

						if (mState != STATE_NONE) {
							Log.e(TAG, "disconnected");
							// ������������������ģʽ
							BluetoothService.this.start();
						}
						break;
					}
				} catch (IOException e) {
					Log.e(TAG, "disconnected", e);
					connectionLost();

					if (mState != STATE_NONE) {

						BluetoothService.this.start();
					}
					break;
				}
			}
		}

		/**
		 * OutStreamд������
		 * 
		 * @param buffer
		 *            д���ֽ�
		 */
		public void write(byte[] buffer) {
			try {
				mmOutStream.write(buffer, 0, buffer.length);

				// ���������͵���Ϣ�ص�UI��Activity
				mHandler.obtainMessage(Dstv.MESSAGE_WRITE, -1, -1, buffer)
						.sendToTarget();
			} catch (IOException e) {
				Log.e(TAG, "Exception during write", e);
			}
		}

		public void cancel() {
			try {
				mmSocket.close();
			} catch (IOException e) {
				Log.e(TAG, "close() of connect socket failed", e);
			}
		}

	}

}
