package utils;

import models.Student;

import org.json.JSONException;
import org.json.JSONObject;

public class StudentParser {

	public Student getStudent(String json) {
		try {
			JSONObject jsonObject = new JSONObject(json);
			Student student = new Student();

			student.setResult(jsonObject.getString("result"));

			String result = jsonObject.getString("result");

			if (!result.equals("FAILED")) {
				student.setMessage(jsonObject.getString("message"));
				student.setError(jsonObject.getString("error"));
				student.setRegNumber(jsonObject.getString("regNumber"));
				student.setStudentName(jsonObject.getString("studentName"));
				student.setSchoolName(jsonObject.getString("schoolName"));
				student.setSchoolCode(jsonObject.getString("schoolCode"));
				student.setSchoolFees(jsonObject.getString("schoolFees"));
			} else {
				student.setError(jsonObject.getString("error"));
			}

			return student;
		} catch (JSONException e) {
			// solve the exception here ....
			e.printStackTrace();
			return null;
		}
	}
}
