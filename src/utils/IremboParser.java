package utils;

import models.Irembo;

import org.json.JSONException;
import org.json.JSONObject;

import ez.pay.project.Globals;

public class IremboParser {
	Irembo irembo = new Irembo();

	public Irembo getIrembo(String json) {
		try {
			JSONObject jsonObject = new JSONObject(json);

			irembo.setMessage(jsonObject.getString("message"));
			irembo.setResult(jsonObject.getString("result"));
			irembo.setError(jsonObject.getString("error"));

			if (jsonObject.getString("result").equals("Success")) {

				irembo.setBiller_code(jsonObject.getString("biller_code"));
				irembo.setBill_number(jsonObject.getString("bill_number"));
				irembo.setDescription(jsonObject.getString("description"));
				irembo.setCustome_r_name(jsonObject.getString("custome_r_name"));
				irembo.setCreation_date(jsonObject.getString("creation_date"));
				irembo.setAmount(jsonObject.getString("amount"));
				irembo.setCustomer_id_number(jsonObject
						.getString("customer_id_number"));
				irembo.setMobile(jsonObject.getString("mobile"));
				irembo.setRra_account_name(jsonObject
						.getString("rra_account_name"));
				irembo.setRra_account_number(jsonObject
						.getString("rra_account_number"));

			}

			return irembo;
		} catch (JSONException e) {

			irembo.setMessage(Globals.InternetErrorMessage);
			irembo.setResult("failed");
			irembo.setError(Globals.InternetErrorMessage);

			e.printStackTrace();
			return null;
		}
	}
}
