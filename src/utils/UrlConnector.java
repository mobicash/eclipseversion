package utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import android.util.Log;

public class UrlConnector {

	StringBuilder builder = new StringBuilder();
	HttpClient client = new DefaultHttpClient();
	List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();

	String imei;

	String serverAddress = "";
	String serverResult;

	// CONSTRUCTOR
	public UrlConnector(String serverAddress,
			List<NameValuePair> nameValuePairs, Vars vars) {

		this.serverAddress = serverAddress;
		this.nameValuePairs = nameValuePairs;
		NameValuePair nv = new BasicNameValuePair("imei", vars.imei);
		this.nameValuePairs.add(nv);
	}

	public String connectt() {

		try {

			ConnectUtils connector = new ConnectUtils();
			HttpClient httpclient = connector.getNewHttpClient();

			HttpPost httppost = new HttpPost(serverAddress);

			UrlEncodedFormEntity obj = new UrlEncodedFormEntity(nameValuePairs);
			obj.setChunked(true);
			httppost.setEntity(obj);

			String length = String.valueOf(httppost.getEntity()
					.getContentLength());

			HttpResponse response = httpclient.execute(httppost);
			HttpEntity entity = response.getEntity();
			InputStream content = entity.getContent();

			BufferedReader reader = new BufferedReader(new InputStreamReader(
					content));
			String line;
			while ((line = reader.readLine()) != null) {
				builder.append(line);
			}
			httpclient.getConnectionManager().shutdown();

		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		serverResult = builder.toString();
		return serverResult;
	}

	public void log(String msg) {
		Log.v("URL CONNECTOR", msg);
	}

}
