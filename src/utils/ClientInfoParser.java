package utils;

import org.json.JSONException;
import org.json.JSONObject;

import ez.pay.project.ClientInfo;

//json parsing done here ...

public class ClientInfoParser {

	public ClientInfo getParsedResults(String json) {
		ClientInfo clientInfo = new ClientInfo();
		try {
			if (json != null && !json.isEmpty()) {
				JSONObject reader = new JSONObject(json);
				clientInfo.setMessage(reader.getString("message"));
				clientInfo.setResult(reader.getString("result"));

				JSONObject details = reader.getJSONObject("details");
				clientInfo.setClientNumber(details.getString("clientnumber"));
				clientInfo.setClientID(details.getString("clientID"));
				clientInfo.setClientName(details.getString("clientname"));
				clientInfo.setClientPhoto(details.getString("clientphoto"));
			} else {

				setClientInfoToNull(clientInfo);
			}

			return clientInfo;
		} catch (JSONException e) {
			setClientInfoToNull(clientInfo);
			e.printStackTrace();
			return null;
		}
	}

	public void setClientInfoToNull(ClientInfo client) {
		client.setMessage(null);
		client.setResult(null);
		client.setClientNumber(null);
		client.setClientID(null);
		client.setClientName(null);
		client.setClientPhoto(null);

	}
}
