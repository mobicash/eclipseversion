package utils;

import java.util.ArrayList;
import java.util.List;

import models.AgentLog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class AgentLogParser {

	List<AgentLog> agentLogList = new ArrayList<AgentLog>();

	public List<AgentLog> getAgentLogsList(String json) {
		try {
			JSONObject reader = new JSONObject(json);
			String result = reader.getString("result");
			String error = reader.getString("error");
			JSONArray detailsArray = reader.getJSONArray("details");

			for (int i = 0; i < detailsArray.length(); i++) {
				AgentLog agentLog = new AgentLog();
				JSONObject transObject = detailsArray.getJSONObject(i);
				String transferId = transObject.getString("id");
				String date = transObject.getString("date");
				String amount = transObject.getString("amount");
				String status = transObject.getString("status");
				String description = transObject.getString("description");
				JSONObject transferTypeObject = transObject
						.getJSONObject("transferType");
				String transferName = transferTypeObject.getString("name");
				
				
				String name="";
				if(transObject.has("member")){
					JSONObject memberObject = transObject.getJSONObject("member");
					 name = memberObject.getString("name");
				}

				

				agentLog.setResult(result);
				agentLog.setError(error);
				agentLog.setTransferId(transferId);
				agentLog.setDate(date);
				agentLog.setAmount(amount);
				agentLog.setStatus(status);
				agentLog.setDescription(description);
				agentLog.setTransferName(transferName);
				agentLog.setName(name);

				agentLogList.add(agentLog);
			}

			return agentLogList;
		} catch (JSONException e) {
			// solve the exception here ....
			e.printStackTrace();
			return null;
		}
	}

}
