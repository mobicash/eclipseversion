package utils;

import org.json.JSONException;
import org.json.JSONObject;

import ez.pay.project.ElectricityInfo;

//json parsing done here ...

public class ElectricityParser {

	ElectricityInfo electricityInfo;
	JSONObject jsonObject = new JSONObject();

	public ElectricityInfo showResults(String json) {

		try {
			if (json != null && !json.isEmpty()) {
				jsonObject = new JSONObject(json);

				electricityInfo = new ElectricityInfo(
						jsonObject.getString("name"),
						jsonObject.getString("meter_no"),
						jsonObject.getString("amount"),
						jsonObject.getString("error"),
						jsonObject.getString("result"));

			} else {
				setElectricityInfoToNull();
			}

			return electricityInfo;
		} catch (JSONException e) {

			setElectricityInfoToNull();

			e.printStackTrace();
			return null;
		}
	}

	private void setElectricityInfoToNull() {
		electricityInfo = new ElectricityInfo(null, null, null, null, null);

	}
}
