package utils;

import org.json.JSONException;
import org.json.JSONObject;

import ez.pay.project.PayElectricityInfo;

//json parsing done here ...

public class PayElectricityParser {

	PayElectricityInfo payElectricityInfo;
	JSONObject jsonObject = new JSONObject();

	public PayElectricityInfo showResults(String json) {

		try {
			if (json != null && !json.isEmpty()) {
				jsonObject = new JSONObject(json);

				payElectricityInfo = new PayElectricityInfo();
				payElectricityInfo.setMeterNumber(jsonObject
						.getString("meter_no"));
				payElectricityInfo.setName(jsonObject.getString("name"));
				payElectricityInfo.setTokenNo(jsonObject.getString("token_no"));
				payElectricityInfo.setUnits(jsonObject.getString("units"));
				payElectricityInfo.setReceitNo(jsonObject
						.getString("receipt_no"));
				payElectricityInfo.setAmount(jsonObject.getString("amount"));
				payElectricityInfo.setVat(jsonObject.getString("vat"));
				payElectricityInfo
						.setTendered(jsonObject.getString("tendered"));
				payElectricityInfo.setTotal(jsonObject.getString("total"));
				payElectricityInfo.setArrears(jsonObject.getString("arrears"));
				payElectricityInfo.setResult(jsonObject.getString("result"));
				
				if(jsonObject.has("message"))
				payElectricityInfo.setMessage(jsonObject.getString("message"));
				
				payElectricityInfo.setError(jsonObject.getString("error"));
				payElectricityInfo.setDate(jsonObject.getString("datetime"));

			} else {
				setPayElectricityInfoToNull();
			}

			return payElectricityInfo;
		} catch (JSONException e) {

			setPayElectricityInfoToNull();

			e.printStackTrace();
			return null;
		}
	}

	private void setPayElectricityInfoToNull() {
		payElectricityInfo = new PayElectricityInfo();
		payElectricityInfo.setMeterNumber(null);
		payElectricityInfo.setName(null);
		payElectricityInfo.setTokenNo(null);
		payElectricityInfo.setUnits(null);
		payElectricityInfo.setReceitNo(null);
		payElectricityInfo.setAmount(null);
		payElectricityInfo.setVat(null);
		payElectricityInfo.setTendered(null);
		payElectricityInfo.setTotal(null);
		payElectricityInfo.setArrears(null);
		payElectricityInfo.setResult(null);
		payElectricityInfo.setMessage(null);
		payElectricityInfo.setError(null);
		payElectricityInfo.setDate(null);

	}
}
