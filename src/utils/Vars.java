package utils;

import org.json.JSONObject;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.preference.PreferenceManager;
import android.telephony.TelephonyManager;

import com.google.gson.Gson;

import ez.pay.project.Alerter;


public class Vars {
	
	// JSON OBJECT TO HOLD REPLY
	public	JSONObject json = new JSONObject();
	public	Alerter alerter;
		// PROGRESS BAR
	public	ProgressDialog pd;
	public String imei;
		// APP VARS
	public	String agentPhone;
	public	String server;
	public	SharedPreferences prefs;
		//TRANS OBJECT
	public Transaction trans;
	public Editor editor;
	
	public Gson gson;
	
	public Context context;
	
	//CHECKS INTERNET CONNECTS
	public ConnectionDetector cd;
	
	public Vars(Context context){
		alerter = new Alerter(context);
		prefs = PreferenceManager.getDefaultSharedPreferences(context);
		server = prefs.getString("server", null);		
		agentPhone = prefs.getString("agentPhone", null);
		//imei = prefs.getString("imei", null);
		editor = prefs.edit();
		gson = new Gson();
		this.context = context;
		cd = new ConnectionDetector(context);
		//INIT IMEI
		TelephonyManager tm = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
		imei = tm.getDeviceId();
		
		if(imei == null){
			imei = "NA-"+agentPhone;
		}
		
	}
	

}
