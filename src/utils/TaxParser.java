package utils;

import org.json.JSONException;
import org.json.JSONObject;

import ez.pay.project.Tax;

public class TaxParser {

	Tax tax = new Tax();

	public Tax getTax(String json) {

		try {

			if (json != null && !json.isEmpty()) {
				JSONObject reader = new JSONObject(json);

				if (reader.has("result")) {
					if (reader.getString("result") != null) {
						tax.setResult(reader.getString("result"));
					}
				}

				if (reader.has("error")) {
					if (reader.getString("error") != null) {
						tax.setError(reader.getString("error"));
					}
				}

				if (reader.has("bank_name")) {
					if (reader.getString("bank_name") != null) {
						tax.setBankName(reader.getString("bank_name"));
					}
				}

				if (reader.has("RRA_REF")) {
					if (reader.getString("RRA_REF") != null) {
						tax.setRraRefId(reader.getString("RRA_REF"));
					}
				}

				if (reader.has("TIN")) {
					if (reader.getString("TIN") != null) {
						tax.setTin(reader.getString("TIN"));
					}
				}

				if (reader.has("TAX_PAYER_NAME")) {
					if (reader.getString("TAX_PAYER_NAME") != null) {
						tax.setTaxPayerName(reader.getString("TAX_PAYER_NAME"));
					}
				}

				if (reader.has("TAX_TYPE_DESC")) {
					if (reader.getString("TAX_TYPE_DESC") != null) {
						tax.setTaxTypeDescription(reader
								.getString("TAX_TYPE_DESC"));
					}
				}

				if (reader.has("TAX_CENTRE_NO")) {
					if (reader.getString("TAX_CENTRE_NO") != null) {
						tax.setTaxCenterNo(reader.getString("TAX_CENTRE_NO"));
					}
				}

				if (reader.has("TAX_TYPE_NO")) {
					if (reader.getString("TAX_TYPE_NO") != null) {
						tax.setTaxTypeNo(reader.getString("TAX_TYPE_NO"));
					}
				}

				if (reader.has("ASSESS_NO")) {
					if (reader.getString("ASSESS_NO") != null) {
						tax.setAssessNo(reader.getString("ASSESS_NO"));
					}
				}

				if (reader.has("RRA_ORIGIN_NO")) {
					if (reader.getString("RRA_ORIGIN_NO") != null) {
						tax.setRraOriginNo(reader.getString("RRA_ORIGIN_NO"));
					}
				}

				if (reader.has("AMOUNT_TO_PAY")) {
					if (reader.getString("AMOUNT_TO_PAY") != null) {
						tax.setAmountToPay(reader.getString("AMOUNT_TO_PAY"));
					}
				}

				if (reader.has("DEC_DATE")) {
					if (reader.getString("DEC_DATE") != null) {
						tax.setDecDate(reader.getString("DEC_DATE"));
					}
				}

				if (reader.has("DEC_ID")) {
					if (reader.getString("DEC_ID") != null) {
						tax.setDecId(reader.getString("DEC_ID"));
					}
				}

			} else {
				setTaxToNull();
			}
			return tax;
		} catch (JSONException e) {
			setTaxToNull();
			e.printStackTrace();
			return null;
		}
	}

	private void setTaxToNull() {
		// TODO Auto-generated method stub

		tax.setResult(null);
		tax.setError(null);
		tax.setBankName(null);
		tax.setRraRefId(null);
		tax.setTin(null);
		tax.setTaxPayerName(null);
		tax.setTaxTypeDescription(null);
		tax.setTaxCenterNo(null);
		tax.setTaxTypeNo(null);
		tax.setAssessNo(null);
		tax.setRraOriginNo(null);
		tax.setAmountToPay(null);
		tax.setDecDate(null);
		tax.setDecId(null);

	}

}
