package utils;

import models.Result;

import org.json.JSONException;
import org.json.JSONObject;

public class ResultParser {

	public Result getResult(String json) {
		try {
			JSONObject reader = new JSONObject(json);
			String result = reader.getString("result");
			String message = reader.getString("message");

			Result resultObject = new Result();
			resultObject.setMessage(message);
			resultObject.setResult(result);

			return resultObject;
		} catch (JSONException e) {
			// solve the exception here ....
			e.printStackTrace();
			return null;
		}
	}

}
