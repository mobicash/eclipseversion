package utils;

import org.json.JSONObject;

public class Transaction {

	private String transactionType;
	private String result;
	private String error;
	private String agent;
	private String client;
	// private String clientId;
	private String clientId;
	private String message;
	private String transactionid;
	private String confNum;
	private String extra1;
	private String extra2;
	private String extra3;
	private String extra4;
	private String smsCode;
	private String commissions;
	private String dateTime;
	private String details;

	public String getExtra3() {
		return extra3;
	}

	public void setExtra3(String extra3) {
		this.extra3 = extra3;
	}

	public String getExtra2() {
		return extra2;
	}

	public void setExtra2(String extra2) {
		this.extra2 = extra2;
	}

	public String getDateTime() {
		return dateTime;
	}

	public void setDateTime(String dateTime) {
		this.dateTime = dateTime;
	}

	public String getCommissions() {
		return commissions;
	}

	public void setCommissions(String commissions) {
		this.commissions = commissions;
	}

	public String getExtra4() {
		return extra4;
	}

	public void setExtra4(String extra4) {
		this.extra4 = extra4;
	}

	public String getDetails() {
		return details;
	}

	public void setDetails(String details) {
		this.details = details;
	}

	JSONObject json = new JSONObject();

	public Transaction() {

	}

	public Transaction(String jsonString) {
		try {

			// if (jsonString != null && !jsonString.isEmpty()) {

			json = new JSONObject(jsonString);

			if (json.has("transactionType")) {
				if (json.getString("transactionType") != null) {

					setTransactionType(json.getString("transactionType"));
				}
			}

			if (json.has("result")) {
				if (json.getString("result") != null) {
					setResult(json.getString("result"));
				}
			}

			if (json.has("error")) {
				if (json.getString("error") != null) {
					setError(json.getString("error"));
				}
			}

			if (json.has("agent")) {
				if (json.getString("agent") != null
						&& !json.getString("agent").isEmpty()) {
					setAgent(json.getString("agent"));
				}
			}

			if (json.has("client")) {
				if (json.getString("client") != null) {

					setClient(json.getString("client"));
				}
			}

			if (json.has("message")) {
				if (json.getString("message") != null) {
					setMessage(json.getString("message"));
				}
			}

			if (json.has("transactionid")) {
				if (json.getString("transactionid") != null) {
					setTransactionid(json.getString("transactionid"));
				}
			}

			if (json.has("confNum")) {
				if (json.getString("confNum") != null) {
					setConfNum(json.getString("confNum"));
				}
			}

			if (json.has("extra1")) {
				if (json.getString("extra1") != null) {
					setExtra1(json.getString("extra1"));
				}
			}

			if (json.has("extra2")) {
				if (json.getString("extra2") != null) {
					setExtra2(json.getString("extra2"));
				}
			}

			if (json.has("extra3")) {
				if (json.getString("extra3") != null) {

					setExtra3(json.getString("extra3"));
				}
			}

			if (json.has("smsCode")) {
				if (json.getString("smsCode") != null) {
					setSmsCode(json.getString("smsCode"));
				}
			}

			if (json.has("clientId")) {
				if (json.getString("clientId") != "null"
						&& !json.getString("clientId").isEmpty()) {

					setClientId(json.getString("clientId"));

				}
			}

			if (json.has("commissions")) {
				if (json.getString("commissions") != "null"
						&& !json.getString("commissions").isEmpty()) {

					setCommissions(json.getString("commissions"));

				}
			}

			if (json.has("datetime")) {
				if (json.getString("datetime") != "null") {

					setDateTime(json.getString("datetime"));

				}
			}

			if (json.has("details")) {
				if (json.getString("details") != "null") {

					setDetails(json.getString("details"));

				}
			}

			if (json.has("extra4")) {
				if (json.getString("extra4") != "null") {

					setExtra4(json.getString("extra4"));

				}
			}

			// } else {
			// setTransactionToNull();
			// }

		} catch (Exception e) {
			// setTransactionToNull();
			e.printStackTrace();
		}
	}

	private void setTransactionToNull() {
		// TODO Auto-generated method stub
		setTransactionType(null);
		setResult(null);
		setError(null);
		setAgent(null);
		setClient(null);
		setMessage(null);
		setTransactionid(null);
		setConfNum(null);
		setExtra1(null);
		setExtra2(null);
		setSmsCode(null);
		setClientId(null);
	}

	public String getTransactionType() {
		return transactionType;
	}

	public void setTransactionType(String transactionType) {
		this.transactionType = transactionType;
	}

	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}

	public String getAgent() {
		return agent;
	}

	public void setAgent(String agent) {
		this.agent = agent;
	}

	public String getClient() {
		return client;
	}

	public void setClient(String client) {
		this.client = client;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getTransactionid() {
		return transactionid;
	}

	public void setTransactionid(String transactionid) {
		this.transactionid = transactionid;
	}

	public String getConfNum() {
		return confNum;
	}

	public void setConfNum(String confNum) {
		this.confNum = confNum;
	}

	public String getExtra1() {
		return extra1;
	}

	public void setExtra1(String extra1) {
		this.extra1 = extra1;
	}

	public String getSmsCode() {
		return smsCode;
	}

	public void setSmsCode(String smsCode) {
		this.smsCode = smsCode;
	}

	public JSONObject getJson() {
		return json;
	}

	public void setJson(JSONObject json) {
		this.json = json;
	}

	public String getClientId() {
		return clientId;
	}

	public void setClientId(String clientId) {
		this.clientId = clientId;
	}

	public void log(String msg) {
		System.out.println(msg);
	}
}
