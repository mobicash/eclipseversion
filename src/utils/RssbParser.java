package utils;

import models.Rssb;

import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

public class RssbParser {

	public Rssb getRssb(String json) {
		try {

			JSONObject jsonObject = new JSONObject(json);

			Rssb rssb = new Rssb();

			rssb.setResult(jsonObject.getString("result"));

			rssb.setMessage(jsonObject.getString("message"));

			JSONObject detailsObject = jsonObject.getJSONObject("details");

			if (detailsObject.has("Errorcode"))
				rssb.setErrorCode(detailsObject.getString("Errorcode"));

			if (detailsObject.has("reply"))
				rssb.setReply(detailsObject.getString("reply"));

			if (detailsObject.has("name"))
				rssb.setName(detailsObject.getString("name"));

			if (detailsObject.has("householdNID"))
				rssb.setHouseholdNID(detailsObject.getString("householdNID"));

			if (detailsObject.has("totalPremium"))
				rssb.setTotalPremium(detailsObject.getString("totalPremium"));

			if (detailsObject.has("alreadyPaid"))
				rssb.setAlreadyPaid(detailsObject.getString("alreadyPaid"));

			if (detailsObject.has("eligibility"))
				rssb.setEligibility(detailsObject.getString("eligibility"));

			if (detailsObject.has("houseHoldCategory"))
				rssb.setHouseHoldCategory(detailsObject
						.getString("houseHoldCategory"));

			if (detailsObject.has("numberOfMembers"))
				rssb.setNumberOfMembers(detailsObject
						.getString("numberOfMembers"));

			if (detailsObject.has("provincename"))
				rssb.setProvince(detailsObject.getString("provincename"));

			if (detailsObject.has("districtname"))
				rssb.setDistrict(detailsObject.getString("districtname"));

			if (detailsObject.has("invoice"))
				rssb.setInvoice(detailsObject.getString("invoice"));

			return rssb;
		} catch (JSONException e) {

			e.printStackTrace();
			Log.e("error", e.toString());
			return null;
		}
	}
}
