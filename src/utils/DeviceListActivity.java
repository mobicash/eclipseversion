package utils;

import java.util.Set;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import ez.pay.project.R;
public class DeviceListActivity extends Activity {
    
    private static final String TAG = "DeviceListActivity";
    private static final boolean D = true;

    // ���ض������ͼ
    public static String EXTRA_DEVICE_ADDRESS = "device_address";

    // ��Ա�ֶ�
    private BluetoothAdapter mBtAdapter;
    private ArrayAdapter<String> mPairedDevicesArrayAdapter;
    private ArrayAdapter<String> mNewDevicesArrayAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // �����������
        requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
        setContentView(R.layout.device_list);

        // 
        setResult(Activity.RESULT_CANCELED);

        //��ʼ����ť��ִ�з����豸
        Button scanButton = (Button) findViewById(R.id.button_scan);
        scanButton.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                doDiscovery();
                v.setVisibility(View.GONE);
            }
        });
        //�����Ѿ�����豸�Ͷ��·��ֵ��豸��ʼ������������
        mPairedDevicesArrayAdapter = new ArrayAdapter<String>(this, R.layout.device_name);
        mNewDevicesArrayAdapter = new ArrayAdapter<String>(this, R.layout.device_name);

        //�ҵ��������б���ͼ����豸

        ListView pairedListView = (ListView) findViewById(R.id.paired_devices);
        pairedListView.setAdapter(mPairedDevicesArrayAdapter);
        pairedListView.setOnItemClickListener(mDeviceClickListener);

        // �ҵ�������Ϊ�·��ֵ��豸���б�
        ListView newDevicesListView = (ListView) findViewById(R.id.new_devices);
        newDevicesListView.setAdapter(mNewDevicesArrayAdapter);
        newDevicesListView.setOnItemClickListener(mDeviceClickListener);

        // ���豸������ע��㲥
        IntentFilter filter = new IntentFilter(BluetoothDevice.ACTION_FOUND);
        this.registerReceiver(mReceiver, filter);

        //�������Ѿ������ע��㲥
        filter = new IntentFilter(BluetoothAdapter.ACTION_DISCOVERY_FINISHED);
        this.registerReceiver(mReceiver, filter);

        // �ñ��ص�����������
        mBtAdapter = BluetoothAdapter.getDefaultAdapter();

        // ��һ��Ŀǰ����豸
        Set<BluetoothDevice> pairedDevices = mBtAdapter.getBondedDevices();

        // ���������豸,���ÿһ��ArrayAdapter
        if (pairedDevices.size() > 0) {
            findViewById(R.id.title_paired_devices).setVisibility(View.VISIBLE);
            for (BluetoothDevice device : pairedDevices) {
                mPairedDevicesArrayAdapter.add(device.getName() + "\n" + device.getAddress());
            }
        } else {
            String noDevices = getResources().getText(R.string.none_paired).toString();
            mPairedDevicesArrayAdapter.add(noDevices);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        // ȷ��û�з���
        if (mBtAdapter != null) {
            mBtAdapter.cancelDiscovery();
        }

        // ע���㲥����
        this.unregisterReceiver(mReceiver);
    }

    /**
     * BluetoothAdapter��ʼ�����豸
     */
    private void doDiscovery() {
        if (D) Log.d(TAG, "doDiscovery()");

        // �ڱ�������ʾɨ���
        setProgressBarIndeterminateVisibility(true);
        setTitle(R.string.scanning);

        //�����豸����Ļ
        findViewById(R.id.title_new_devices).setVisibility(View.VISIBLE);

        // ����Ѿ����־�ֹͣ��
        if (mBtAdapter.isDiscovering()) {
            mBtAdapter.cancelDiscovery();
        }

        // �����BluetoothAdapter����
        mBtAdapter.startDiscovery();
    }

    // ����ļ�����ListViews�������豸��
    private OnItemClickListener mDeviceClickListener = new OnItemClickListener() {
        public void onItemClick(AdapterView<?> av, View v, int arg2, long arg3) {
            // ȡ������
            mBtAdapter.cancelDiscovery();

            //��ȡ�豸��MAC��ַ
            String info = ((TextView) v).getText().toString();
            String address = info.substring(info.length() - 17);

            //�������intent���а���MAC��ַ
            Intent intent = new Intent();
            intent.putExtra(EXTRA_DEVICE_ADDRESS, address);

            //���� ������ Activity
            setResult(Activity.RESULT_OK, intent);
            finish();
        }
    };

    // ������������ˣ�BroadcastReceiver���������豸�͸��ı���
    private final BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();

            // ������һ���豸
            if (BluetoothDevice.ACTION_FOUND.equals(action)) {
                //�õ�BluetoothDevice�������ͼ
                BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                // ������Ѿ���Թ�,��������Ϊ�����Ѿ���Ե�
                if (device.getBondState() != BluetoothDevice.BOND_BONDED) {
                    mNewDevicesArrayAdapter.add(device.getName() + "\n" + device.getAddress());
                }
            //��������ɺ�,�ı�Activity������
            } else if (BluetoothAdapter.ACTION_DISCOVERY_FINISHED.equals(action)) {
                setProgressBarIndeterminateVisibility(false);
                setTitle(R.string.select_device);
                if (mNewDevicesArrayAdapter.getCount() == 0) {
                    String noDevices = getResources().getText(R.string.none_found).toString();
                    mNewDevicesArrayAdapter.add(noDevices);
                }
            }
        }
    };

}
