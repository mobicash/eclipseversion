package utils;

import java.util.ArrayList;
import java.util.List;

import models.Area;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class AreaParser {

	List<Area> areaList = new ArrayList<Area>();

	public List<Area> getAreaList(String json) {
		try {
			JSONObject jObject = new JSONObject(json);
			JSONArray jArray = jObject.getJSONArray("return");

			for (int i = 0; i < jArray.length(); i++) {
				Area area = new Area();
				JSONObject jArrayObject = jArray.getJSONObject(i);
				area.setId(jArrayObject.getString("id"));
				area.setValue(jArrayObject.getString("value"));
				area.setDefaultValue(jArrayObject.getString("defaultValue"));
				areaList.add(area);
			}

			return areaList;
		} catch (JSONException e) {
			// solve the exception here ....
			e.printStackTrace();
			return null;
		}
	}

}
