package utils;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

import android.content.ContentResolver;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Environment;
import android.util.Log;
import dbStuff.Personsugarorm;
import ez.pay.project.Base64;
import ez.pay.project.R;

public class GenUtils {

	public static String getPicString(Personsugarorm person, String photoType,
			Vars vars) {
		Bitmap facePic = null;
		ByteArrayOutputStream bao1 = new ByteArrayOutputStream();

		String ba1 = "error";

		ContentResolver cr = vars.context.getContentResolver();

		File f = null;
		if (photoType.equalsIgnoreCase("face")) {
			f = new File(Environment.getExternalStorageDirectory(),
					person.phoneNum + ".jpg");
		} else if (photoType.equalsIgnoreCase("id")) {
			f = new File(Environment.getExternalStorageDirectory(),
					person.phoneNum + "_photoid.jpg");
		}

		if (f != null) {

			Uri mUri = Uri.fromFile(f);
			log("loading image uri is:" + mUri.toString());
			try {
				facePic = android.provider.MediaStore.Images.Media.getBitmap(
						cr, mUri);
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();

				facePic = BitmapFactory.decodeResource(
						vars.context.getResources(), R.drawable.faceblankblank);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			// facePic = decodeFile(f);

			if (facePic != null) {

				facePic.compress(Bitmap.CompressFormat.JPEG, 30, bao1);

				byte[] imagearray1 = bao1.toByteArray();

				log("IMAGE ARRAY LENGTH:" + imagearray1.length);

				ba1 = Base64.encodeBytes(imagearray1);

			}

		}

		return ba1;
	}

	public static void printParams(List<String> params) {
		for (String p : params) {
			System.out.println("PARAM:" + p);
		}
	}

	static void log(String msg) {
		Log.e("GEN UTILS:", msg);
	}

}
