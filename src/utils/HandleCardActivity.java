package utils;



import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.Toast;
import ez.pay.project.R;

public class HandleCardActivity extends Activity {
	  /** 
     * Called when the activity is first created. 
     * */
	 
	
	 private EditText cardId;
	 private EditText password;
	 private EditText sectorNumber;
	 private EditText chunk1;
	 private EditText chunk2;
	 private EditText chunk3;
	 private EditText chunk4;
	 private Button read;
	 private Button write;
	 private Button beep;
	 private RadioGroup group;
	 private RadioButton KeyA;
	 private RadioButton KeyB;
	 private CheckBox m_checkBox1;
	 private CheckBox m_checkBox2;
	 private CheckBox m_checkBox3;
	 private CheckBox m_checkBox4;
	 byte mode;
	 byte block;
	 HandleCard handleCard=new HandleCard();
	 

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState); 
		 MyApplication.getInstance().addActivity(this);
		setContentView(R.layout.handlecard);
		cardId=(EditText) findViewById(R.id.cardID);
		password=(EditText) findViewById(R.id.password);
	    sectorNumber=(EditText) findViewById(R.id.quID );
	    chunk1=(EditText) findViewById(R.id.chunk1);
	    chunk2=(EditText) findViewById(R.id.chunk2);
	    chunk3=(EditText) findViewById(R.id.chunk3);
	    chunk4=(EditText) findViewById(R.id.chunk4);
	    read=(Button) findViewById(R.id.read);
	    write=(Button) findViewById(R.id.write);
	    beep=(Button) findViewById(R.id.beep);
	    group=(RadioGroup) findViewById(R.id.key);
	    KeyA=(RadioButton) findViewById(R.id.keyA);
	    KeyB=(RadioButton) findViewById(R.id.KeyB);
	    m_checkBox1=(CheckBox) findViewById(R.id.checkbox1);
	    m_checkBox2=(CheckBox) findViewById(R.id.checkbox2);
	    m_checkBox3=(CheckBox) findViewById(R.id.checkbox3);
	    m_checkBox4=(CheckBox) findViewById(R.id.checkbox4);
	    sectorNumber.setText("0");
	    password.setText("FFFFFFFFFFFF");

	    
	    group.setOnCheckedChangeListener(new OnCheckedChangeListener()
	    {
   
			@Override
			public void onCheckedChanged(RadioGroup group, int checkedId) {
				// TODO Auto-generated method stub
				if(KeyA.getId()==checkedId)
				{
					mode=00;
				}
				if(KeyB.getId()==checkedId)
				{
					mode=01;
				}
			}
	    	
	    }
	    );
	    
	  
	   
	    read.setOnClickListener(new OnClickListener() 
	    {
 
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method 
				boolean checkbox[] = new boolean[4];
				int i;
				int trycount = 0;
				String Data="";
				if (m_checkBox1.isChecked()) checkbox[0]= true; else checkbox[0] = false;
				if (m_checkBox2.isChecked()) checkbox[1]= true; else checkbox[1] = false;
				if (m_checkBox3.isChecked()) checkbox[2]= true; else checkbox[2] = false;
				if (m_checkBox4.isChecked()) checkbox[3]= true; else checkbox[3] = false;
				   long CardSnr=handleCard.RF_Request();
				   if(CardSnr==0)
				   {
					   Toast.makeText(HandleCardActivity.this,"Ѱ��ʧ�ܣ�", 300).show();
				   }
				   else
				   {
					    
					cardId.setText(CardSnr+"");
					int  m_sectorNumber=Integer.parseInt(sectorNumber.getText()+"");
					
					String m_Password=password.getText().toString();
					
					byte[] password=HandleCard.toByteArray(m_Password);
           	         boolean ret;
           	
					block=(byte) (m_sectorNumber*4);
					ret = handleCard.RF_AuthM1Card(block,mode,CardSnr,password);
					if(ret==true)
					{
						for (i=0;i<4;i++)
						{
							if (checkbox[i] == true)
							{							
								block=(byte) (m_sectorNumber*4+i);
								for (trycount = 0;trycount<3;trycount++)
								{
									Data = handleCard.RF_read(block);
									
									if (!Data.equals(""))
									{
										switch (i)
										{
											case 0: chunk1.setText(Data); break;
											case 1: chunk2.setText(Data); break;
											case 2: chunk3.setText(Data); break;
											case 3: chunk4.setText(Data); break;
											
										}
										break;
									} else
									{
										if (trycount==3)
										{
											Toast.makeText(HandleCardActivity.this,m_sectorNumber+"��"+i+"�����ʧ�ܣ�", 300).show();
										}
									}
								}
							}
						}
						handleCard.RF_Beep();
					} else
					{						
						Toast.makeText(HandleCardActivity.this,"������֤ʧ�ܣ�", 300).show();
					}
			}
	    	
	    }
	    }
	    );
	    
	    
	    write.setOnClickListener(new OnClickListener() 
	    {

			@Override
			public void onClick(View v) {  
				boolean result=true;
				// TODO Auto-generated method stub
				String w_chunk1=chunk1.getText().toString();
				String w_chunk2=chunk2.getText().toString(); 
				String w_chunk3=chunk3.getText().toString();
				String w_chunk4=chunk4.getText().toString();
				
				
				boolean checkbox[] = new boolean[4];
				int i;
				if (m_checkBox1.isChecked()) checkbox[0]= true; else checkbox[0] = false;
				if (m_checkBox2.isChecked()) checkbox[1]= true; else checkbox[1] = false;
				if (m_checkBox3.isChecked()) checkbox[2]= true; else checkbox[2] = false;
				if (m_checkBox4.isChecked()) checkbox[3]= true; else checkbox[3] = false;
				   long CardSnr=handleCard.RF_Request();
				   if(CardSnr==0)
				   {
					   Toast.makeText(HandleCardActivity.this,"Ѱ��ʧ�ܣ�", 300).show();
				   }
				   else
				   {
					   
					   cardId.setText(CardSnr+"");
						int  m_sectorNumber=Integer.parseInt(sectorNumber.getText()+"");
						
						String m_Password=password.getText().toString();
						
						byte[] password=HandleCard.toByteArray(m_Password);
						System.out.println("password"+HandleCard.byteArrayToHexString(password,12," "));
	           	         boolean ret;
	           	      
						block=(byte) (m_sectorNumber*4);
						ret = handleCard.RF_AuthM1Card(block,mode,CardSnr,password);
						if(ret==true)
						{
							for (i=0;i<4;i++)
							{
								if (checkbox[i] == true)
								{	
									block=(byte) (m_sectorNumber*4+i);
									for (int trycount = 0;trycount<3;trycount++)
									{
										if (i == 0 && block == 0)
							            {
							            	Toast.makeText(HandleCardActivity.this,"0�����ĵ�0�鲻��д���ݣ�", 300).show();
							            } else
										{											
											switch (i)
											{
												case 0: result=  handleCard.RF_Write(block,HandleCard.toByteArray(w_chunk1)); break;
												case 1: result=  handleCard.RF_Write(block,HandleCard.toByteArray(w_chunk2)); break;
												case 2: result=  handleCard.RF_Write(block,HandleCard.toByteArray(w_chunk3)); break;
												case 3: result=  handleCard.RF_Write(block,HandleCard.toByteArray(w_chunk4)); break;												
											}
											if (result == false)
											{
												Toast.makeText(HandleCardActivity.this,"д��ʧ�ܣ�", 300).show();
												break;												
											}
										}
									}
								}
							}
							if (result == true)
							{
								Toast.makeText(HandleCardActivity.this,"д���ɹ���", 300).show();
								handleCard.RF_Beep();
							}
						} else
						{						
							Toast.makeText(HandleCardActivity.this,"������֤ʧ�ܣ�", 300).show();
						}
				}
		    	
		    }
		    }
		    );
	    beep.setOnClickListener(new OnClickListener() 
	    {
 
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method 
				handleCard.RF_Beep();
			}
	    });
	}
	
	    @Override
	    public boolean onCreateOptionsMenu(Menu menu)
	    {
	    	 //��Menu��Դ�г�ʼ��Menu ����true�򵯳�Ĭ��menu��false�򲻵���Menu�����Զ���   
	    	       MenuInflater inflater = getMenuInflater();   
	    	       inflater.inflate(R.menu.activity, menu);   
	               return true;  
	    }
	    @Override
	    public boolean onOptionsItemSelected(MenuItem item)
	    {
	    	switch(item.getItemId())
	    	{
	    	case R.id.scan:
	    		Intent intent = new Intent();
	             setResult(Activity.RESULT_OK, intent);
	             finish();
                return true;
	    		
	    	case R.id.eixt:    
		       dialog(); 
		       return true;
	    	}  
	    	
	    	return false;
	    }
	    public void dialog() { 
	        AlertDialog.Builder builder = new Builder(this); 
	        builder.setMessage("ȷ��Ҫ�˳���?"); 
	        builder.setTitle("��ʾ"); 
	        builder.setPositiveButton("ȷ��", 
	                new android.content.DialogInterface.OnClickListener() { 
	                    @Override 
	                    public void onClick(DialogInterface dialog, int which) { 
	                        dialog.dismiss();
	                        MyApplication.getInstance().exit();  
	                    } 
	                }); 
	        builder.setNegativeButton("ȡ��", 
	                new android.content.DialogInterface.OnClickListener() { 
	                    @Override 
	                    public void onClick(DialogInterface dialog, int which) { 
	                        dialog.dismiss(); 
	                    } 
	                }); 
	        builder.create().show(); 
	    } 
	
}
