/**
 * 
 */
package utils;

import java.util.Date;

import services.BluetoothService;
import ez.pay.project.Dstv;

/**
 * @author Administrator
 *
 */
public class HandleCard {
	 
	 public byte [] rf_buf=new byte[255];
	 public static int rf_len=0;
	 public static int rf_command_state;
	 byte[] cardsnr= new byte[4];
	 
	
	  
		public boolean RecvTimeOut(final int timeout)
	    {
			Date dt= new Date();    
			Long time= dt.getTime();
			Long time1 =0L;
			int i;
	    	while (!BluetoothService.BluetoothRecvOK)
	    	{
				dt= new Date(); 
				time1 =dt.getTime();
				//System.out.println("use = "+(time1-time)+""+" timeout = "+timeout+"");
				if(time1-time>timeout)
	 			{			
					System.out.println("time out.....");    
					break;      
				}   
	    	}
	    	if (BluetoothService.BluetoothRecvOK)
	    	{
	    		System.out.println("BluetoothRecvOK....."); 
	    		for (i=0;i<BluetoothService.BluetoothRecvBufLen-1;i++)
	    		{
	    			rf_buf[i] = BluetoothService.BluetoothRecvBuf[i];
	    			System.out.println("rf_buf"+i+":"+rf_buf[i]);
	    		}
	    		rf_len = BluetoothService.BluetoothRecvBufLen;
	    	} else
	    	{
	    		rf_len = 0;
	    	} 
	    	return BluetoothService.BluetoothRecvOK;  
	    }
	      
	    public static String byteArrayToHexString(byte[] array, int len,String Spac) {
	        StringBuffer hexString = new StringBuffer();
	        int i=0;
	        for (byte b : array) 
	        {
	          int intVal = b & 0xff;
	          if (intVal < 0x10)
	            hexString.append("0");
	          hexString.append(Integer.toHexString(intVal));
	          hexString.append(Spac);
	          i++;
	          if (i>len) break;
	        }
	        return hexString.toString();    
	      }
	 
	    public static byte[] toByteArray(String hexString) 
	    {
			
			final byte[] byteArray = new byte[hexString.length() / 2];
			int k = 0;
			for (int i = 0; i < byteArray.length; i++) {
				//��Ϊ��16���ƣ����ֻ��ռ��4λ��ת�����ֽ���Ҫ����16���Ƶ��ַ�����λ����
				byte high = (byte) (Character.digit(hexString.charAt(k), 16) & 0xff);
				byte low = (byte) (Character.digit(hexString.charAt(k + 1), 16) & 0xff);
				byteArray[i] = (byte) (high << 4 | low);
				k += 2;
			}
			return byteArray;
		}
	    public void InitRecvBuf()
	    {
	    	BluetoothService.BluetoothRecvBufLen = 0;
	    	BluetoothService.BluetoothRecvOK = false;
	    	rf_command_state = 0;
	    	rf_len = 0;    	
	    }

	    public boolean CheckBuf()
	    {
	    	int datalen;
	    	int i; 
	    	byte state,bcc,check;
	    	
	    	if (rf_buf[0] == 2)
	    	{
	    		String Hex= "";
				
	    		for (i=0;i<rf_len;i++)
	    		{
	    			Hex = Hex + rf_buf[i]+""+" ";
	    			//System.out.println(buffer[i]+"::");
	    		}
	    		System.out.println("CheckBuf = "+byteArrayToHexString(rf_buf,rf_len," ")); 
	    		
	    		datalen = rf_buf[2];
	    		System.out.println("datalen = "+datalen + "");
	    		state = rf_buf[4];
	    		System.out.println("state="+state);
	    		bcc = rf_buf[rf_len-2];
	    		check =0;
				for (i=0;i<datalen+2;i++)
				{
					System.out.println(rf_buf[i+1] + "");
					check ^= rf_buf[i+1];
				}
				if (bcc == check)
				{    					
					System.out.println("BCC OK");
					if (state == 0)
					{
						System.out.println("state OK");
						return true;
					}
				} else
				{
					System.out.println("BCC ERROR bcc="+ bcc+""+" check="+check+"");
				}
	    		}
	    	return false;
	    }
	    
	    public static long bytesToUint(byte[] array) {
	        return ((long) (array[3] & 0xff))
	             | ((long) (array[2] & 0xff)) << 8
	             | ((long) (array[1] & 0xff)) << 16
	             | ((long) (array[0] & 0xff)) << 24;
	    }

	    public static byte[] uintToBytes( long n )
	    {
	        byte[] b = new byte[4];
	        b[3] = (byte) (n & 0xff);
	        b[2] = (byte) (n >> 8 & 0xff);
	        b[1] = (byte) (n >> 16 & 0xff);
	        b[0] = (byte) (n >> 24 & 0xff);
	      
	        return b;
	    }
	    
	    //Ѱ��
	    public long RF_Request()
	    {
	    	
	    	//byte sendbuf[] = {0x02,0x00,0x04,(byte) 0xA7,0x31,0x4E,0x20,(byte)0xFC,0x03};
	    	byte sendbuf[] = new byte[32];
	    	byte sendbcc = 0;
	    	int i;
	    	/*String SendStr = new String(sendbuf);
	    	byte[] sendbyte = SendStr.getBytes();*/
	    	InitRecvBuf();
	    	
	    	sendbuf[0] = 0x02;
	    	sendbuf[1] = 0x00;
	    	sendbuf[2] = 0x04;
	    	sendbuf[3] = (byte)0xA7;
	    	sendbuf[4] = 0x31;
	    	sendbuf[5] = 00;
	    	sendbuf[6] = 0x64;  ;
	    	for (i=1;i<=6;i++) sendbcc ^=sendbuf[i];
	    	sendbuf[7] = (byte) sendbcc;
	    	sendbuf[8] = 0x03;
	    	System.out.println("sendbuf = "+byteArrayToHexString(sendbuf,9," "));
	    	
	    	
	    	Dstv.mService.write(sendbuf,0,9);
	    	if (RecvTimeOut(2000) == false) return 0;
	    	if (CheckBuf() == false) return 0;
	    	cardsnr[0] = rf_buf[6];
	    	cardsnr[1] = rf_buf[7];
	    	cardsnr[2] = rf_buf[8];
	    	cardsnr[3] = rf_buf[9];
	    	System.out.println(".......= "+bytesToUint(cardsnr)+"");
	    	return bytesToUint(cardsnr); 
	    } 
	    //������֤
		public boolean RF_AuthM1Card(byte block,byte mode,long cardsnr,byte[] password)
	    {
			byte sendbuf[] = new byte[32];
	    	byte cardbyte[] = new byte[4];
	        byte sendbcc=0;
	        int i;
	        
	        InitRecvBuf();
	        
	        System.out.println(cardsnr+",,,,,,,,");
	        cardbyte = uintToBytes(cardsnr);
	        
	    	sendbuf[0] = 0x02;
	    	sendbuf[1] = 0x00;
	    	sendbuf[2] = 0x0E;
	    	sendbuf[3] = (byte)0xA7;
	    	sendbuf[4] = 0x32;
	    	sendbuf[5] = mode;
	    	sendbuf[6] = block;  	
	    	sendbuf[7] = cardbyte[0];  
	    	sendbuf[8] = cardbyte[1];
	    	sendbuf[9] = cardbyte[2];
	    	sendbuf[10] = cardbyte[3];
	    	sendbuf[11] = password[0];
	    	sendbuf[12] = password[1];
	    	sendbuf[13] = password[2];
	    	sendbuf[14] = password[3];
	    	sendbuf[15] = password[4];
	    	sendbuf[16] = password[5];
	    	
	    	
	    	for (i=1;i<=16;i++) sendbcc ^=sendbuf[i];
	    	sendbuf[17] = (byte) sendbcc;
	    	sendbuf[18] = 0x03;
	    	System.out.println("sendbuf = "+byteArrayToHexString(sendbuf,18," "));
	    	
	    	
	    	Dstv.mService.write(sendbuf,0,sendbuf.length);
	    	System.out.println("RecvTimeOut(2000)="+RecvTimeOut(2000)); 
	    	//System.out.println("CheckBuf()="+CheckBuf());
	    	if (RecvTimeOut(2000) == false) return false;
	    	if (CheckBuf() == false) return false;
	    	return true; 
	    }
		//������
		public String RF_read(byte block)
		{
			int i;
			byte sendbcc=0;
			String result = "";
			byte sendbuf[] = new byte[32];
			byte data[] = new byte[16];
			
			InitRecvBuf();
			sendbuf[0] = 0x02;
	    	sendbuf[1] = 0x00;
	    	sendbuf[2] = 0x03;
	    	sendbuf[3] = (byte)0xA7;
	    	sendbuf[4] = 0x33;
	    	sendbuf[5] = block;
	    	for (i=1;i<=5;i++) sendbcc ^=sendbuf[i];
	    	sendbuf[6] = (byte) sendbcc;
	    	sendbuf[7] = 0x03;
	    	System.out.println("sendbuf = "+byteArrayToHexString(sendbuf,8," "));
	    		    	
	    	Dstv.mService.write(sendbuf,0,8);
	    	System.out.println("RecvTimeOut(200)="+RecvTimeOut(200)); 
	    	//System.out.println("CheckBuf()="+CheckBuf());
	    	if (RecvTimeOut(200) == false) return "";
	    	if (CheckBuf() == false) return "";
	    	
	    	for (i=0;i<16;i++)
	    	{
	    		data[i] = rf_buf[i+5];
	    	}    	
	    	result = byteArrayToHexString(data,16,"").toUpperCase();
	    	
			return result;
		}
		//д����
		 public boolean RF_Write(byte block, byte[] wbuf)
	    {
			int i;
			byte sendbcc=0;
			
			byte sendbuf[] = new byte[32];
			
			InitRecvBuf();
			sendbuf[0] = 0x02;
			sendbuf[1] = 0x00;
			sendbuf[2] = 0x13;
			sendbuf[3] = (byte) 0xA7;
			sendbuf[4] = 0x34;
			sendbuf[5]=block;
			
			for (i=0;i<16;i++)
			{
				sendbuf[i+6] = wbuf[i];
			}	 
			
			for (i=1;i<=21;i++) sendbcc ^=sendbuf[i];
			sendbuf[22] = (byte) sendbcc;
			sendbuf[23] = 0x03;
			System.out.println("sendbuf = "+byteArrayToHexString(sendbuf,24," "));
			
			Dstv.mService.write(sendbuf,0,24);
			System.out.println("RecvTimeOut(200)="+RecvTimeOut(200)); 
			//System.out.println("CheckBuf()="+CheckBuf());
			if (RecvTimeOut(200) == false) return false;
			if (CheckBuf() == false) return false;
			return true;
	    }
		 
	    public boolean MF_Halt()
	    {
	    	byte request[] = {0x02,0x00,0x04,(byte) 0xA7,0x38,0x4E,0x20,(byte)0xF5,0x03};
	    	boolean ret;
	    	InitRecvBuf();
	    	Dstv.mService.write(request,0,request.length);
	    	ret = RecvTimeOut(4000);
	    	if (ret==false) 
			{
			System.out.println("RecvTimeOut= ERR");
			return true;
			} else
			{
				System.out.println("RecvTimeOut= OK");
			}
	    	ret = CheckBuf();
	    	if (ret==false) 
			{
				System.out.println("CheckBuf= ERR");
				return ret;
			}
	    	return ret;
	    }
	    public void RF_Beep()
	    {
	    	byte send[] = {0x02,0x00,0x02,(byte) 0xA7,(byte) 0x99,0x3c,0x03};
	    	Dstv.mService.write(send,0,7);
			
	    }
}
