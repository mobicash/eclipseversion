package utils;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import ez.pay.project.Bouquet;

//json parsing done here ...

public class BouquetParser {

	List<Bouquet> bList = new ArrayList<Bouquet>();
	Bouquet bouquet;

	public List<Bouquet> showResults(String json) {

		try {
			if (json != null && !json.isEmpty()) {
				JSONObject reader = new JSONObject(json);

				JSONObject bouquetObject = reader.getJSONObject("bouquets");
				JSONArray bouquetArray = bouquetObject.getJSONArray("bouquet");
				for (int i = 0; i < bouquetArray.length(); i++) {
					bouquet = new Bouquet();
					JSONObject c = bouquetArray.getJSONObject(i);
					String id = c.getString("id");
					String name = c.getString("name");
					String price = c.getString("price");

					bouquet.setId(Integer.parseInt(id));
					bouquet.setName(name);
					bouquet.setPrice(Double.parseDouble(price));
					bList.add(bouquet);
				}
			} else {
				setBoquetToNull();
			}

			return bList;
		} catch (JSONException e) {

			setBoquetToNull();

			e.printStackTrace();
			return null;
		}
	}

	private void setBoquetToNull() {
		bouquet.setId(0);
		bouquet.setName("Error");
		bouquet.setPrice(0);
		bList.add(bouquet);

	}
}
