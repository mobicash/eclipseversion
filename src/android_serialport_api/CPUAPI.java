package android_serialport_api;

import android.os.SystemClock;
import android.util.Log;

public class CPUAPI {
	private static final byte[] SWITCH_COMMAND = "D&C00040104".getBytes();
	private static final byte[] CONFIGURATION_READER_MODE = "c05060102\r\n"
			.getBytes();
	private static final byte[] CONFIGURATION_PROTOCOL_MODE = "c05060c1001\r\n"
			.getBytes();
	private static final byte[] SET_CHECK_CODE = "c05060c04c1\r\n".getBytes();
	private static final byte[] FIND = "f26\r\n".getBytes();
	private static final String COLLISION_SELECT_CARD = "f9320\r\n";
	private static final String SELECT = "f9370";
	private static final byte[] RESET = "fE051\r\n".getBytes();
	private static final byte[] GET_CHALLENGE = "f0a010084000008\r\n".getBytes();

	
	private static final String ENTER = "\r\n";
	
	private static final String NO_RESPONSE = "No response from card.";

	private byte[] mBuffer = new byte[1024];


	private boolean switchStatus() {
		sendCommand(SWITCH_COMMAND);
		Log.i("whw", "SWITCH_COMMAND hex=" + new String(SWITCH_COMMAND));
		SystemClock.sleep(200);
		SerialPortManager.switchRFID = true;
		return true;
	}

	/**
	 * Configuration reader mode
	 * @return
	 */
	public boolean configurationReaderMode() {
		int length = receive(CONFIGURATION_READER_MODE, mBuffer);
		String receiveData = new String(mBuffer, 0, length);
		Log.i("whw", "configurationReaderMode   str=" + receiveData);
		if (length > 0
				&& receiveData.startsWith("RF carrier on! ISO/IEC14443 TYPE A, 106KBPS.")) {
			return true;
		}
		return false;
	}

	/**
	 * According to the card type card protocol configuration mode (subcarrier rate modulation)
	 * @return
	 */
	public boolean configurationProtocolMode() {
		int length = receive(CONFIGURATION_PROTOCOL_MODE, mBuffer);
		String receiveData = new String(mBuffer, 0, length);
		Log.i("whw", "configurationProtocolMode   str=" + receiveData);
		if (length > 0 && receiveData.startsWith("0x01")) {
			return true;
		}
		return false;
	}

	/**
	 * Set the reader check code
	 * @return
	 */
	public boolean setCheckCode() {
		int length = receive(SET_CHECK_CODE, mBuffer);
		String receiveData = new String(mBuffer, 0, length);
		Log.i("whw", "setCheckCode   str=" + receiveData);
		if (length > 0 && receiveData.startsWith("0xc1")) {
			return true;
		}
		return false;
	}

	/**
	 * DSFID(data storage format identifier):1byte   UID:8bytes
	 * @return 
	 */
	public boolean findCard() {
		int length = receive(FIND, mBuffer);
		String receiveData = new String(mBuffer, 0, length).trim();
		Log.i("whw", "findCard   str=" + receiveData);
		if (length > 0) {
			if(!receiveData.startsWith(NO_RESPONSE) && receiveData.equals("0400")){
				return true;
			}
		}
		return false;
	}
	
	public String collisionSecectCard(){
		int length = receive(COLLISION_SELECT_CARD.getBytes(), mBuffer);
		String receiveData = new String(mBuffer, 0, length).trim();
		Log.i("whw", "CollisionSecectCard   str=" + receiveData);
		if (length > 0) {
			if(!receiveData.startsWith(NO_RESPONSE)){
				return receiveData;
			}
		}
		return "";
	}
	
	public boolean selectCard(String cardNum){
		byte[] command = (SELECT+cardNum+ENTER).getBytes();
		int length = receive(command, mBuffer);
		String receiveData = new String(mBuffer, 0, length).trim();
		Log.i("whw", "selectCard   str=" + receiveData);
		if (length > 0) {
			if(!receiveData.startsWith(NO_RESPONSE)){
				return "28".equals(receiveData);
			}
		}
		return false;
	}
	
	public void reset(){
		int length = receive(RESET, mBuffer);
		String receiveData = new String(mBuffer, 0, length).trim();
		Log.i("whw", "reset   str=" + receiveData);
		if (length > 0) {
			if(!receiveData.startsWith(NO_RESPONSE)){
			}
		}
	}
	
	public void getChallenge(){
		int length = receive(GET_CHALLENGE, mBuffer);
		String receiveData = new String(mBuffer, 0, length).trim();
		Log.i("whw", "getChallenge   str=" + receiveData);
		if (length > 0) {
			if(!receiveData.startsWith(NO_RESPONSE)){
			}
		}
	}
	

	private void sendCommand(byte[] command) {
		SerialPortManager.getInstance().write(command);
	}
	
	private int receive(byte[] command, byte[] buffer) {
		int length = -1;
		if (!SerialPortManager.switchRFID) {
			switchStatus();
		}
		SerialPortManager.getInstance().write(command);
		Log.i("whw", "command hex=" + new String(command));
		length = SerialPortManager.getInstance().read(buffer, 150, 10);
		return length;
	}
}
