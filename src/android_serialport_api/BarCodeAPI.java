package android_serialport_api;

import android.os.SystemClock;
import android.util.Log;

import com.authentication.utils.DataUtils;

public class BarCodeAPI {
	private static final byte[] OPEN_COMMAND = "D&C0004010B".getBytes();
	private static final byte[] CLOSE_COMMAND = "D&C0004010C".getBytes();

	private static final byte[] WAKEUP = { 0x00 };

	private static final byte[] RESTORE_COMMAND = { 0x04, (byte) 0xC8, 0x04,
			0x01, (byte) 0xFF, 0x2F };
	private static final byte[] SET_HOST_COMMAND = { 0x07, (byte) 0xc6, 0x04,
			0x01, 0x0d, (byte) 0x8a, 0x08, (byte) 0xfe, (byte) 0x8f };

	private static final byte[] START_DECODE = { 0x04, (byte) 0xe4, 0x04, 0x01,
			(byte) 0xff, 0x13 };

	private static final byte[] STOP_DECODE = { 0x04, (byte) 0xe5, 0x04, 0x01,
			(byte) 0xff, 0x12 };

	private static final byte[] LED_ON = { 0x05, (byte) 0xe7, 0x04, 0x00, 0x01,
			(byte) 0xfe, 0x79 };

	private static final byte[] BEEP = { 0x05, (byte) 0xe6, 0x04, 0x00, 0x00,
			(byte) 0xfe, 0x7a };

	private static final byte[] DECODE_DATA_PACKET_FORMAT = { 0x07,
			(byte) 0xc6, 0x04, 0x01, 0x0d, (byte) 0xee, 0x01, (byte) 0xfe, 0x32 };
	
	private static  byte[] SET_ENABLE_DISABLE = { 0x07,
		(byte) 0xc6, 0x04, 0x01, 0x0d};
	
	private static  byte[] SET_ENABLE_DISABLE2 = { 0x08,
		(byte) 0xc6, 0x04, 0x01, 0x0d};

	private static final byte[] CMD_ACK = { 0x04, (byte) 0xd0, 0x04, 0x00,
			(byte) 0xff, 0x28 };

	private static final byte[] CMD_NAK = { 0x05, (byte) 0xd1, 0x04, 0x00,
			0x00, 0x06, (byte) 0xff, 0x20 };

	private static final byte[] buffer = new byte[1024];

	private boolean isOpen = false;

	public boolean isOpen() {
		return isOpen;
	}

	public void open() {
		SerialPortManager.getInstance().write(OPEN_COMMAND);
		int length = SerialPortManager.getInstance().read(buffer, 100, 100);
		Log.i("whw", "open length=" + length + "  buffer[0]=" + buffer[0]);
		if (length == 1) {
			isOpen = true;
			SystemClock.sleep(20);
		}
	}

	public void close() {
		SerialPortManager.getInstance().write(CLOSE_COMMAND);
		isOpen = false;
	}

	public void restore() {
		SerialPortManager.getInstance().write(WAKEUP);
		SystemClock.sleep(20);
		SerialPortManager.getInstance().write(RESTORE_COMMAND);
		int length = SerialPortManager.getInstance().read(buffer, 100, 100);
		print("restore");
		SystemClock.sleep(20);
	}

	public boolean setHost() {
		for (int i = 0; i < 3; i++) {
			SerialPortManager.getInstance().write(WAKEUP);
			SystemClock.sleep(200);
			SerialPortManager.getInstance().write(SET_HOST_COMMAND);
			int length = SerialPortManager.getInstance().read(buffer, 100, 100);
			Log.i("whw", "setHost length=" + length);
			if (length == 0) {
				sendNAK();
			} else if (length == 6) {
				return true;
			}
			print("setHost");
		}
		return false;
	}

	/**
	 * Begin decoding
	 * @param inBuffer  inBuffer[0]:code type,inBuffer[1]~inBuffer[length-1] is decoded data
	 * @return  data length
	 */
	public int startDecode(byte[] inBuffer) {
		for (int i = 0; i < 4; i++) {
			Log.i("whw", "for *********************** i=" + i);
			SerialPortManager.getInstance().write(WAKEUP);
			SystemClock.sleep(20);
			SerialPortManager.getInstance().write(START_DECODE);
			int length1 = SerialPortManager.getInstance()
					.read(buffer, 100, 500);
			Log.i("whw", "startDecode length=" + length1);
			print("startDecode");
			if (length1 > 6 && buffer[0] == 0x04) {
				int packetLength = length1 - 6;
				int dataLength = 0;
				if (packetLength > 0) {
					boolean isContinue = true;
					boolean isFirstPacket = true;
					while(isContinue){
						byte[] packet = new byte[packetLength];
						if(isFirstPacket){
							System.arraycopy(buffer, 6, packet, 0, packetLength);
							isFirstPacket = false;
						}else{
							System.arraycopy(buffer, 0, packet, 0, packetLength);
						}

						Log.i("whw", "packet hex=" + DataUtils.toHexString(packet));
						dataLength += getDecodeData(packet, inBuffer,dataLength);
						sendACK();
						packetLength = SerialPortManager.getInstance()
								.read(buffer, 100, 50);
						if(packetLength<=0){
							isContinue = false;
						}
					}

				}
				return dataLength;
			} else if (length1 == 0) {
				continue;
			}else if (length1 == 6) {

				break;
			} else if (length1 == 7) {
				break;
			}
		}
		return 0;
	}

	/**
	 * Stop decoding
	 * @return
	 */
	public boolean stopDecode() {
		SerialPortManager.getInstance().write(WAKEUP);
		SystemClock.sleep(20);
		SerialPortManager.getInstance().write(STOP_DECODE);
		int length = SerialPortManager.getInstance().read(buffer, 100, 100);
		Log.i("whw", "stopDecode length=" + length);
		print("stopDecode");
		//length==6 is ack response
		if(length == 6){
			return true;
		}else{
			return false;
		}
	}

	/**
	 * Length(1byte) Opcode(1byte) Message Source(1byte) Status(1byte) Bar Code
	 * Type(1byte) Decode Data(n bytes) Checksum(2byte)
	 * 
	 * @param data
	 * @return
	 */
	private int getDecodeData(byte[] data, byte[] inBuffer,int dstPos) {
		if (data != null && data.length > 0) {
			Log.i("whw", "receive=" + DataUtils.toHexString(data));
			System.arraycopy(data, 5, inBuffer, dstPos, data.length - 7);
			return data.length - 7;
		}
		return 0;

	}

	/**
	 * After specifying the format of the decoded data generated
	 * @return
	 */
	public boolean decodeDataPacketFormat() {
		for (int i = 0; i < 3; i++) {
			SerialPortManager.getInstance().write(WAKEUP);
			SystemClock.sleep(100);
			SerialPortManager.getInstance().write(DECODE_DATA_PACKET_FORMAT);
			int length = SerialPortManager.getInstance().read(buffer, 100, 100);
			Log.i("whw", "DECODE_DATA_PACKET_FORMAT length=" + length);
			SystemClock.sleep(500);

			if (length == 0) {
				sendNAK();
			} else if (length == 6) {
				return true;
			}
		}
		return false;
	}
	
	/**
	 * Enable/Disable PDF417
	 * @param Enable PDF417(01h)   Disable PDF417(00h)
	 */
	public void setEnablePDF417(int enable){
		byte[] command = new byte[9];
		System.arraycopy(SET_ENABLE_DISABLE, 0, command, 0, SET_ENABLE_DISABLE.length);
		command[5] = 0x0f;
		command[6] = (byte) (enable);
		int sum = 0x10000;
		for (int i = 0; i < command.length-2; i++) {
			sum -= (command[i]&0xff);
		}
		byte[] shot2byte = DataUtils.short2byte((short)sum);
		Log.i("whw", "sum hex="+Integer.toHexString(sum));
		command[7] = shot2byte[0];
		command[8] = shot2byte[1];
		
		for (int i = 0; i < 3; i++) {
			SerialPortManager.getInstance().write(WAKEUP);
			SystemClock.sleep(20);
			SerialPortManager.getInstance().write(command);
			int length = SerialPortManager.getInstance().read(buffer, 100, 100);
			Log.i("whw", "setEnablePDF417 length=" + length);
			print("setEnablePDF417");
			if (length == 0) {
				sendNAK();
			} else if (length == 6) {
				return;
			}
		}
	}
	
	/**
	 * Enable/Disable MicroPDF417
	 * @param enable  Enable MicroPDF417(01h)   Disable MicroPDF417(00h)
	 */
	public void setEnableMicroPDF417(int enable){
		byte[] command = new byte[9];
		System.arraycopy(SET_ENABLE_DISABLE, 0, command, 0, SET_ENABLE_DISABLE.length);
		command[5] = (byte) 0xe3;
		command[6] = (byte) (enable);
		int sum = 0x10000;
		for (int i = 0; i < command.length-2; i++) {
			sum -= (command[i]&0xff);
		}
		byte[] shot2byte = DataUtils.short2byte((short)sum);
		Log.i("whw", "sum hex="+Integer.toHexString(sum));
		command[7] = shot2byte[0];
		command[8] = shot2byte[1];
		
		for (int i = 0; i < 3; i++) {
			SerialPortManager.getInstance().write(WAKEUP);
			SystemClock.sleep(20);
			SerialPortManager.getInstance().write(command);
			int length = SerialPortManager.getInstance().read(buffer, 100, 100);
			Log.i("whw", "setEnableMicroPDF417 length=" + length);
			print("setEnableMicroPDF417");
			if (length == 0) {
				sendNAK();
			} else if (length == 6) {
				return;
			}
		}
	}
	
	
	/**
	 * Enable/Disable Data Matrix
	 * @param enable  Enable Data Matrix(01h)   Disable Data Matrix(00h)
	 */
	public void setEnableDataMatrix(int enable){
		byte[] command = new byte[10];
		System.arraycopy(SET_ENABLE_DISABLE2, 0, command, 0, SET_ENABLE_DISABLE2.length);
//		command[5] = (byte) 0xf0;
//		command[6] = (byte) 0x24;
		
		
		//MaxiCode
		command[5] = (byte) 0xf0;
		command[6] = (byte) 0x26;
		Log.i("whw", "MaxiCode--------------");
		
		
		command[7] = (byte) (enable);
		
		
		

		int sum = 0x10000;
		for (int i = 0; i < command.length-2; i++) {
			sum -= (command[i]&0xff);
		}
		byte[] shot2byte = DataUtils.short2byte((short)sum);
		Log.i("whw", "sum hex="+Integer.toHexString(sum));
		command[8] = shot2byte[0];
		command[9] = shot2byte[1];
		
		for (int i = 0; i < 3; i++) {
			SerialPortManager.getInstance().write(WAKEUP);
			SystemClock.sleep(20);
			SerialPortManager.getInstance().write(command);
			int length = SerialPortManager.getInstance().read(buffer, 100, 100);
			Log.i("whw", "setEnableDataMatrix length=" + length);
			print("setEnableDataMatrix");
			if (length == 0) {
				sendNAK();
			} else if (length == 6) {
				return;
			}
		}
	}



	private void sendACK() {
//		SerialPortManager.getInstance().write(WAKEUP);
//		SystemClock.sleep(20);
		SerialPortManager.getInstance().write(CMD_ACK);
	}

	private void sendNAK() {
		SerialPortManager.getInstance().write(WAKEUP);
		SystemClock.sleep(20);
		SerialPortManager.getInstance().write(CMD_NAK);
	}

	private void print(String str) {
//		byte[] temp = new byte[10];
//		System.arraycopy(buffer, 0, temp, 0, temp.length);
//		Log.i("whw", str + " hex=" + DataUtils.toHexString(temp));
	}
}
