package ez.pay.project;

public class Globals {

	public static final String version = "1.0.7";
	public static final String UPDATED = "updated";
	public static final String NOTUPDATED = "not updated";
	public static final String PSP_ID = "89765423";
	// public static final String inOfficeAddre ssTest = "http://192.168.30.32";
	// public static final String inOfficeAddressProd = "http://192.168.30.30";
	public static final String outOfOfficeAddressTest = "http://mcash.ug:8081";
	public static final String outOfOfficeAddressTestSSl = "https://mcash.ug:8082";
	public static final String outOfOfficeAddressProd = "https://mcash.ug";
	public static final String cyclosTest = "http://192.168.1.20";
	public static final String cyclosTestExterior = "http://154.0.130.42";
	public static final String mobicoreStanbic = "https://mobicore.mcash.ug";
	public static final String mobicoreApn = "https://10.208.20.22:8443/";
	public static final String directPrivateApn = "http://privateapn.mcash.ug:18080/";
	public static final String ghanaServer = "http://ghana.mobicashonline.com/";

	public static final String rraInfoServerAdress = "http://interface.mobicashonline.com/bio-api/androidTax/getRefDetails.php";
	public static final String rraPayServerAdress = "http://interface.mobicashonline.com/bio-api/androidTax/payTax.php";
	// MOBICORE SHIT..
	// SOUTH AFRICA ADDRESS
	public static final String southAfrica = "http://test.mobicash.co.za/";
	// BOTSWANA
	public static final String botswanaMobicore = "http://app.mobicash.co.bw/";

	// RWANDA
	public static final String rwanda = "http://test.jomopay.net/bio-api/mobiSeller/";

	static final String DOWNLOAD_APK_LINK = rwanda
			+ "/bio-api/android/apk/rwanda-mini-agent.apk";
	// public static final String rwanda = "http://test.mcash.rw";
	// public static final String rwanda = "http://test.mobicash.co.za";

	// public static final String apnTest = "https://10.208.20.22:8082";
	// public static final String apnProd = "https://10.208.20.22";
	// BOTSWANA
	public static final String botswanaTest = "http://54.186.129.4/";
	// public static final String botswanaTest = "http://54.246.157.220/";
	// public static final String botswanaLive = "http://54.246.257.220/";
	public static final String initalAppApproval = "Your Agent ID is Synced to this Device, If you change devices you will need "
			+ "Call MCash";

	public static final int fingerPrintQuality = 70;

	public static final int timeOut = 30000;

	// SETTING PASSWORD
	public static final String settingsPassword = "mcash123";

	public static final String ENROLLMENT_SENT = "enrollment_sent";

	public final static String appName = "Mcash";

	// ------------------------------------- COUNTRY SERVERS
	final static CharSequence[] servers = { rwanda };

	final static CharSequence[] ugandaServers = { outOfOfficeAddressTest,
			outOfOfficeAddressTestSSl, cyclosTest, mobicoreStanbic,
			mobicoreApn, outOfOfficeAddressProd, cyclosTestExterior,
			directPrivateApn };

	// BOTSWANA SERVERS
	final static CharSequence[] botsServers = { botswanaTest, botswanaMobicore };

	// RWANDA SERVERS
	final static CharSequence[] rwandaServers = { rwanda };

	// SA SERVERS
	final static CharSequence[] sAServers = { southAfrica };

	// GHANA SERVERS
	final static CharSequence[] ghanaServers = { ghanaServer };

	// COUNTRY LIST
	final static CharSequence[] countries = { "Uganda", "Botswana", "Rwanda",
			"South Africa", "Ghana" };

	// RWANDA COUNTRY NAME
	final static String rwandaCountryName = countries[2].toString();;

	// ref id
	public static final String refId = "refId";
	public static final String result = "result";
	public static final String bankName = "bankName";
	public static final String rraRef = "rraRef";
	public static final String tin = "tin";
	public static final String taxPayerName = "taxPayerName";
	public static final String taxTypeDesc = "taxTypeDesc";
	public static final String taxCentreNo = "taxCentreNo";
	public static final String taxTypeNo = "taxTypeNo";
	public static final String assessNo = "assessNo";
	public static final String rraOriginNo = "rraOriginNo";
	public static final String amountToPay = "amountToPay";
	public static final String decId = "decId";
	public static final String decDate = "decDate";
	public static final String pin = "pin";
	public static final String phoneNumber = "phoneNumber";

	public static final String meterNo = "meterNo";

	public static final String message = "message";
	public static final String regNumber = "regNumber";
	public static final String studentName = "studentName";
	public static final String schoolName = "schoolName";
	public static final String schoolCode = "schoolCode";
	public static final String schoolFees = "schoolFees";

	// transfer sender

	public static final String clientNumber = "clientNumber";
	public static final String clientID = "clientID";
	public static final String clientName = "clientName";
	public static final String clientPhoto = "clientPhoto";
	public static final String agentPin = "agentPin";

	// logs

	public static final String agentLogTransactionID = "agentLogTransactionID";
	public static final String agentLogMember = "agentLogMember";
	public static final String agentLogtransactionType = "agentLogtransactionType";
	public static final String agentLogamount = "agentLogamount";
	public static final String agentLogStatus = "agentLogStatus";
	public static final String agentLogDate = "agentLogDate";
	public static final String agentLogDescription = "agentLogDescription";

	public static final String amount = "amount";

	public static final String senderNumber = "senderNumber";
	public static final String senderName = "senderName";

	public static final String receiverNumber = "receiverNumber";
	public static final String receiverName = "receiverName";

	public static final String clientRedirect = "clientRedirect";

	public static final String customerWalletIdErrorMessage = "Please enter wallet ID / MSISDN of customer";
	public static final String connectionErrorMessage = "No connectivity/availability of server";
	public static final String nfcSerialNumberErrorMessage = "Please enter NFC serial";
	
	public static final String invalidNfcSerialNumberErrorMessage = "Invalid NFC serial";
	public static final String isActiveErrorMessage = "Please select if customer is active";

	public static final String tapNfcErrorMessage = "Please tap NFC tag on the reader";

	public static final String InternetErrorMessage = "Internet problem / Ikibazo cya konegisiyo";

	public static final String updateErrorMessage = "Application out of date, do you want to update it / virisiyo ya sisiteme ufite irashaje urashaka gushyiramo inshyashya ?";
	public static final String cancelCashInErrorMessage = "Are you sure you want to cancel Cash In / Urashaka guhagarika Kubitsa ?";
	public static final String cancelBalanceCheckErrorMessage = "Are you sure you want to cancel Balance Check / Urashaka guhagarika kureba amafaranga asigaye ?";

	public static final String cancelCashOutErrorMessage = "Are you sure you want to cancel Cash Out / Urashaka guhagarika kubikuza ?";
	public static final String cancelTransferErrorMessage = "Are you sure you want to cancel Transfer / Urashaka guhagarika Kohereza ?";
	public static final String noInternetDetectedErrorMessage = "No Internet Connection Detected / Nta interineti ihari";

	public static final String cancelAirtimeErrorMessage = "Are you sure you want to cancel Airtime Purchase / Urashaka guhagarika kugura ama inite ?";

	public static final String cancelEtaxErrorMessage = "Are you sure you want to cancel E-Tax / Urashaka guhagarika kwishyura umusoro ?";
	public static final String cancelPaySchoolFeesMessage = "Are you sure you want to cancel school fees payment / Urashaka guhagarika kwishyura ishuri ?";
	public static final String cancelIremboPaymentMessage = "Are you sure you want to cancel the payment / Urashaka guhagarika kwishyura ?";

	public static final String cancelRssbPaymentMessage = "Are you sure you want to cancel the payment / Urashaka guhagarika kwishyura ?";

	public static final String cancelElectricityErrorMessage = "Are you sure you want to cancel Electricity Purchase / Urashaka guhagarika kugura umuriro ?";
	public static final String cancelDstvErrorMessage = "Are you sure you want to cancel DSTV Purchase / Urashaka guhagarika kugura DSTV ?";
	public static final String cancelCanalErrorMessage = "Are you sure you want to cancel CANAL Purchase / Urashaka guhagarika kugura CANAL ?";
	public static final String cancelStartimesErrorMessage = "Are you sure you want to cancel Startimes Purchase / Urashaka guhagarika kugura Startimes ?";
	public static final String cancelHistoricErrorMessage = "Are you sure you want to cancel historic review / Urashaka guhagarika kureba hisitorike ?";
	public static final String cancelCommissionErrorMessage = "Are you sure you want to cancel commission review / Urashaka guhagarika kureba komisiyo ?";

	public static final String yes = "Yes";
	public static final String no = "No";
	public static final String clientCheckNoErrorMessage = "Please enter client phone , ID / Shyiramo telefoni , indangamuntu y' umukiriya";

	public static final String phoneCheckErrorMessage = "Please enter username";

	public static final String payerCheckErrorMessage = "Please enter payer name / Shyiramo izina ry'uje kwishyura";

	public static final String tvMonthErrorMessage = "Please Enter Month to subscribe / Shyiramo amazi y'ifatabuguzi";
	public static final String tvSmartCardErrorMessage = "Please enter Smart card # / Shyiramo nimero y'ikarita";
	public static final String tvBouquetErrorMessage = "Please select Bouquet / Hitamo buke";
	public static final String tvBouquet = "Select Bouquet / Hitamo buke";

	public static final String tvProviderErrorMessage = "Please choose provider / Hitamo ifatabuguzi";

	public static final String enterAmountErrorMessage = "Please enter amount / Shyiramo amafaranga";

	public static final String enterAmountModuloErrorMessage = "Please enter amount divisible by 3000 or 7000 / Shyiramo amafaranga agabanyika na 3000 cg na 7000";

	public static final String enterAgentPinErrorMessage = "Please enter password";
	
	public static final String invalidPasswordErrorMessage = "Invalid password";
	
	
	public static final String enterPSPIDErrorMessage = "Please enter PSP Id ";
	
	public static final String invalidPSPIDErrorMessage = "PSP Id must contain 8 characters ";
	
	public static final String invalidPinErrorMessage = "Invalid agent pin # / uwo mubare w'ibanga ntubaho";
	public static final String amountLessErrorMessage = "Amount must be greater than 100 / Amafaranga agomba kuba ari hejuru yi 100";
	public static final String amountErrorMessage = "Please enter amount/ Shyiramo umubare w'amafaranga ";
	public static final String amountLessThanFiftyErrorMessage = "Amount must be greater than 50 / Amafaranga agomba kuba ari hejuru yi 50";

	public static final int FINGER_RESULT = 77;
	public static final String noFingerCapturedErrorMessage = "No finger captured / Banza ufate urutoki";
	public static final String enterReceiverAccountErrorMessage = "Please Enter receiver phone , ID / Shyiramo telefoni , indangamuntu y'uwo wohereje";
	public static final String selfTransferErrorMessage = "You can not transfer to your account , Do the cash In / Ntiwakwiyoherereza amafaranga , Bitsa";
	public static final String invalidNumberErrorMessage = "Invalid telephone number / Iyo numero ntibaho ";
	public static final String paymentMethodErrorMessage = "Please select payment method / hitamo uburyo bwo kwishyura";

	public static final String airtimePurchaceProgressMessage = "Airtime purchase / Kugura unite";
	public static final String electricityPurchaceProgressMessage = "Electricity purchase / Kugura umuriro";
	public static final String checkVersionProgressMessage = "Checking application version / Kureba veriziyo ya sisitemu";
	public static final String refIdCheckProgressMessage = "Checking / Igenzurwa";

	public static final String waitProgressMessage = "Please wait / ihangane ...";

	public static final String clientElectricityCheckProgressMessage = "Verification / Igenzurwa";

	public static final String meterNumberErrorMessage = "Please enter meter number / Shyiramo nimero za kashipawa";

	public static final String rraRefIdErrorMessage = "Please Enter Ref ID # / Shyiramo nimero yo kwishyura";
	public static final String billNumberErrorMessage = "Please Enter bill number";
	public static final String nationalIDErrorMessage = "Please Enter national ID #";
	public static final String financialYearErrorMessage = "Please select financial year";
	public static final String studentRegNumberErrorMessage = "Please Enter Student registration # / Shyiramo nimero y'umunyeshuri";

	public static final String name = "name";
	public static final String meterNumber = "meterNumber";
	public static final String error = "error";
	public static final String permission = "permission";
	public static final String DEALERNAME = "DEALERNAME";
	public static final String broker = "Broker";
	public static final String independent = "Independent";

	public static final String confirmRRAPayment = "Are you sure you want to pay / Urashaka kwishyura ?";

	public static final String billerCode = "billerCode";
	public static final String billNumber = "billNumber";
	public static final String description = "description";
	public static final String customerName = "customerName";
	public static final String date = "date";
	public static final String customerIdNumber = "customerIdNumber";
	public static final String mobile = "mobile";
	public static final String rraAccountName = "rraAccountName";
	public static final String rraAccountNumber = "rraAccountNumber";

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
