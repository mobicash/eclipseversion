package ez.pay.project;

//Tax Class

public class Tax {
	private String result;
	private String bankName;
	private String rraRefId;
	private String tin;
	private String taxPayerName;
	private String taxTypeDescription;
	private String taxCenterNo;
	private String taxTypeNo;
	private String assessNo;
	private String rraOriginNo;
	private String amountToPay;
	private String decDate;
	private String decId;
	private String error;
	

	public Tax() {

	}

	public Tax(String result, String bankName, String rraRefId, String tin,
			String taxPayerName, String taxTypeDescription, String taxCenterNo,
			String taxTypeNo, String assessNo, String rraOriginNo,
			String amountToPay, String decDate, String decId,String error) {
		super();
		this.result = result;
		this.bankName = bankName;
		this.rraRefId = rraRefId;
		this.tin = tin;
		this.taxPayerName = taxPayerName;
		this.taxTypeDescription = taxTypeDescription;
		this.taxCenterNo = taxCenterNo;
		this.taxTypeNo = taxTypeNo;
		this.assessNo = assessNo;
		this.rraOriginNo = rraOriginNo;
		this.amountToPay = amountToPay;
		this.decDate = decDate;
		this.decId = decId;
		this.error = error;
	}

	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}

	public String getBankName() {
		return bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public String getRraRefId() {
		return rraRefId;
	}

	public void setRraRefId(String rraRefId) {
		this.rraRefId = rraRefId;
	}

	public String getTin() {
		return tin;
	}

	public void setTin(String tin) {
		this.tin = tin;
	}

	public String getTaxPayerName() {
		return taxPayerName;
	}

	public void setTaxPayerName(String taxPayerName) {
		this.taxPayerName = taxPayerName;
	}

	public String getTaxTypeDescription() {
		return taxTypeDescription;
	}

	public void setTaxTypeDescription(String taxTypeDescription) {
		this.taxTypeDescription = taxTypeDescription;
	}

	public String getTaxCenterNo() {
		return taxCenterNo;
	}

	public void setTaxCenterNo(String taxCenterNo) {
		this.taxCenterNo = taxCenterNo;
	}

	public String getTaxTypeNo() {
		return taxTypeNo;
	}

	public void setTaxTypeNo(String taxTypeNo) {
		this.taxTypeNo = taxTypeNo;
	}

	public String getAssessNo() {
		return assessNo;
	}

	public void setAssessNo(String assessNo) {
		this.assessNo = assessNo;
	}

	public String getRraOriginNo() {
		return rraOriginNo;
	}

	public void setRraOriginNo(String rraOriginNo) {
		this.rraOriginNo = rraOriginNo;
	}

	public String getAmountToPay() {
		return amountToPay;
	}

	public void setAmountToPay(String amountToPay) {
		this.amountToPay = amountToPay;
	}

	public String getDecDate() {
		return decDate;
	}

	public void setDecDate(String decDate) {
		this.decDate = decDate;
	}

	public String getDecId() {
		return decId;
	}

	public void setDecId(String decId) {
		this.decId = decId;
	}
	
	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}

}
