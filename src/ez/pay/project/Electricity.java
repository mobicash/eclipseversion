package ez.pay.project;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URLEncoder;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;

import utils.ConnectUtils;
import utils.ElectricityParser;
import utils.Vars;
import android.app.ActionBar;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;

public class Electricity extends BaseActivity {

	public Vars vars;
	Alerter alerter;
	ElectricityInfo electricityInfo;
	ElectricityParser electricityParser;

	// UI VARS

	EditText agentPin;
	EditText eMemterNo, eAmount;
	protected String imei;
	SharedPreferences pref;

	Button bContinue;
	// Connection detector class
	// ConnectionDetector cd;

	ProgressDialog pd;

	String dealer;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.electricity);
		ActionBar bar = getActionBar();
		bar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#f47701")));

		vars = new Vars(this);
		alerter = new Alerter(this);
		pref = PreferenceManager.getDefaultSharedPreferences(this);

		dealer = pref.getString(Globals.permission, null);
		// cd = new ConnectionDetector(this);
		init();
		// hide keyboard
		this.getWindow().setSoftInputMode(
				WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

	}

	private void init() {
		// TODO Auto-generated method stub
		// SET UI VARS
		eMemterNo = (EditText) findViewById(R.id.etMemterNo);
		eAmount = (EditText) findViewById(R.id.etAmount);

	}

	// BUY
	public void next(View v) {
		// if (cd.isConnectingToInternet()) {
		if (eMemterNo.getText().toString() == null
				|| eMemterNo.getText().toString().isEmpty()) {
			alerter.alerterError(Globals.meterNumberErrorMessage);
		} else if (eAmount.getText().toString() == null
				|| eAmount.getText().toString().isEmpty()) {
			alerter.alerterError(Globals.enterAmountErrorMessage);
		} else {
			new CheckMeterNoInfoAsyncTask().execute(eMemterNo.getText()
					.toString(), eAmount.getText().toString());
			// Intent intent = new Intent(Electricity.this,
			// BuyElectricity.class);
			// startActivity(intent);
		}
		// } else {
		// alerter.alerterError(Globals.noInternetDetectedErrorMessage);
		// }

	}

	// CANCEL
	public void cancel(View v) {

		alerter.alerterConfirmCancel(Globals.cancelElectricityErrorMessage);
	}

	public void layoutClick(View v) {
		InputMethodManager inputManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
		inputManager.hideSoftInputFromWindow(
				getCurrentFocus().getWindowToken(),
				InputMethodManager.HIDE_NOT_ALWAYS);
	}

	// -----------------------------------------------------------------------------------------------------
	public class CheckMeterNoInfoAsyncTask extends
			AsyncTask<String, Void, String> {

		public String theResult;
		public String action;

		String meterNo;
		String amount;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			pd = new ProgressDialog(Electricity.this);
			pd.setTitle(Globals.clientElectricityCheckProgressMessage);
			pd.setMessage("please wait / ihangane ...");
			pd.setCancelable(false);
			pd.setIndeterminate(true);
			pd.show();
		}

		@Override
		// protected String doInBackground(Void... arg0) {
		protected String doInBackground(String... params) {

			StringBuilder builder = new StringBuilder();
			ConnectUtils connector = new ConnectUtils();
			HttpClient client = connector.getNewHttpClient();

			String result = null;
			String sendString = null;

			meterNo = params[0];
			amount = params[1];

			try {

				// DEALER
				sendString = vars.server
						+ "/bio-api/androidElectricity/getMeter.php?"
						+ "meter_no="
						+ URLEncoder.encode(meterNo, "UTF-8")
						+ "&amount="
						+ URLEncoder.encode(amount, "UTF-8")
						+ "&dealer="
						+ URLEncoder.encode(dealer, "UTF-8")
						+ "&version="
						+ URLEncoder.encode(Globals.version, "UTF-8")
						+ "&dealer_name="
						+ URLEncoder.encode(
								vars.prefs.getString(Globals.DEALERNAME, null),
								"UTF-8")

				;

			} catch (Exception e) {

			}

			HttpGet httpGet = new HttpGet(sendString);

			try {
				HttpResponse response = client.execute(httpGet);
				StatusLine statusLine = response.getStatusLine();
				int statusCode = statusLine.getStatusCode();
				if (statusCode == 200) {
					HttpEntity entity = response.getEntity();
					InputStream content = entity.getContent();
					BufferedReader reader = new BufferedReader(
							new InputStreamReader(content));
					String line;
					while ((line = reader.readLine()) != null) {
						builder.append(line);
					}
					result = builder.toString();

				} else {

				}
			} catch (Exception e) {
				result = null;
			}

			return result;
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			// REMOVE DIALOG
			if (pd != null) {
				pd.dismiss();
				// b.setEnabled(true);
			}
			electricityParser = new ElectricityParser();
			electricityInfo = electricityParser.showResults(result);

			// IF OUTPUT WAS RECEIVED FROM SERVER
			if (electricityInfo.getResult() != null
					&& !electricityInfo.getResult().isEmpty()) {
				// if (result.contains("ccess")) {
				if (electricityInfo.getResult().contains("SUCCESS")) {
					Intent intent = new Intent(Electricity.this,
							BuyElectricity.class);
					intent.putExtra(Globals.amount, electricityInfo.getAmount());
					intent.putExtra(Globals.meterNumber,
							electricityInfo.getMeterNumber());
					intent.putExtra(Globals.name, electricityInfo.getName());
					startActivity(intent);

				} else {
					// alerter.alerterError(trans.getResult());
					alerter.alerterError(electricityInfo.getError());
				}
			} else {
				alerter.alerterError(Globals.InternetErrorMessage);
			}
		}
	}

	@Override
	protected void onStop() {
		super.onStop();
		if (pd != null)
			pd.dismiss();
	}

}
