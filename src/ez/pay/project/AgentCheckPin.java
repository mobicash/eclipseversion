package ez.pay.project;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.conn.ConnectTimeoutException;

import utils.ConnectUtils;
import utils.Transaction;
import utils.Vars;
import android.app.ActionBar;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;

//import android.support.v4.app.Fragment;

public class AgentCheckPin extends BaseActivity {

	Vars vars;
	EditText agentPin;
	TextView tvClientCheckTitle;
	Transaction trans;
	Alerter alerter;
	protected String imei;
	TextView checkfloat_agentId;
	SharedPreferences pref;
	String goToClass;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.agent_check_pin);

		// action bar styling
		ActionBar bar = getActionBar();
		bar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#f47701")));

		vars = new Vars(this);
		alerter = new Alerter(this);
		pref = PreferenceManager.getDefaultSharedPreferences(this);

		agentPin = (EditText) findViewById(R.id.checkFloat_agtpin);

		tvClientCheckTitle = (TextView) findViewById(R.id.tv_client_check_title);

		Bundle extras;
		if (savedInstanceState == null) {
			extras = getIntent().getExtras();
			if (extras != null) {
				goToClass = extras.getString("goTo");
				if (goToClass.contains("Commission")) {
					tvClientCheckTitle.setText("Commission / Komisiyo");
					this.setTitle("Commission");
				} else {
					this.setTitle("Historic");
					tvClientCheckTitle.setText("Historic / Raporo");
				}
			}
		}

	}

	// CANCEL
	public void cancel(View v) {
		if (goToClass.contains("Commission")) {
			alerter.alerterConfirmCancel(Globals.cancelCommissionErrorMessage);
		} else {
			alerter.alerterConfirmCancel(Globals.cancelHistoricErrorMessage);
		}
	}

	public void checkAgentPin(View v) {
		checkFloat cb = new checkFloat();
		cb.execute();
	}

	// -----------------------------------------------------------------------------------------------------
	public class checkFloat extends AsyncTask<Void, Void, String> {

		public String theResult = "NO CONNECTION MADE";
		public String action;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			vars.pd = new ProgressDialog(AgentCheckPin.this);
			vars.pd.setTitle(Globals.refIdCheckProgressMessage);
			vars.pd.setMessage(Globals.waitProgressMessage);
			vars.pd.setCancelable(false);
			vars.pd.setIndeterminate(true);
			vars.pd.show();
		}

		@Override
		// protected String doInBackground(Void... arg0) {
		protected String doInBackground(Void... arg0) {

			StringBuilder builder = new StringBuilder();
			ConnectUtils connector = new ConnectUtils();
			HttpClient client = connector.getNewHttpClient();

			String result = "FAILED";
			String urlString = null;

			// LOAD SERVER PREF

			try {
				urlString = vars.server
						+ "/bio-api/checkAgent.php?agentNumber="
						+ URLEncoder.encode(
								vars.prefs.getString("agentPhone", null),
								"UTF-8")
						+ "&agentPin="
						+ URLEncoder.encode(agentPin.getText().toString(),
								"UTF-8") + "&version="
						+ URLEncoder.encode(Globals.version, "UTF-8")

						+ "&dealer_name="
						+ URLEncoder.encode(
								vars.prefs.getString(Globals.DEALERNAME, null),
								"UTF-8")

				;

			} catch (UnsupportedEncodingException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

			HttpGet httpGet = new HttpGet(urlString);

			try {
				HttpResponse response = client.execute(httpGet);
				StatusLine statusLine = response.getStatusLine();
				int statusCode = statusLine.getStatusCode();
				if (statusCode == 200) {
					HttpEntity entity = response.getEntity();
					InputStream content = entity.getContent();
					BufferedReader reader = new BufferedReader(
							new InputStreamReader(content));
					String line;
					while ((line = reader.readLine()) != null) {
						builder.append(line);
					}
					result = builder.toString();

				} else {

				}
			} catch (ConnectTimeoutException e) {

				e.printStackTrace();
			} catch (ClientProtocolException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}

			return result;

		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);

			// REMOVE DIALOG
			if (vars.pd != null) {
				vars.pd.dismiss();
				// b.setEnabled(true);
			}

			// vars.trans = new Transaction(result);
			if (result != null) {
				trans = new Transaction(result);

				// IF OUTPUT WAS RECEIVED FROM SERVER
				if (trans.getResult() != null) {
					// if (result.contains("ccess")) {
					if (trans.getResult().contains("ccess")) {

						if (goToClass.contains("Commission")) {
							Intent intent = new Intent(getApplicationContext(),
									CommissionActivity.class);
							intent.putExtra("agentPin", agentPin.getText()
									.toString());

							intent.putExtra("listName", "Commission");

							intent.putExtra("goTo", goToClass);
							startActivity(intent);
						} else {
							Intent intent = new Intent(getApplicationContext(),
									AgentLogMenu.class);
							intent.putExtra("agentPin", agentPin.getText()
									.toString());
							startActivity(intent);

						}

					} else {
						// alerter.alerterError(trans.getResult());
						alerter.alerterError(trans.getError());
					}
				} else {
					alerter.alerterError("INCORRECT REPLY FROM SERVER");
				}
			} else {

				alerter.alerterError("INVALID RESPONSE FROM THE SEVRER!");
			}
		}
	}

	void log(String msg) {
		System.out.println(msg);
	}

	public void layoutClick(View v) {
		InputMethodManager inputManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
		inputManager.hideSoftInputFromWindow(
				getCurrentFocus().getWindowToken(),
				InputMethodManager.HIDE_NOT_ALWAYS);
	}

}
