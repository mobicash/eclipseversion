package ez.pay.project;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URLEncoder;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.conn.ConnectTimeoutException;

import utils.ConnectUtils;
import utils.Transaction;
import utils.Vars;
import android.app.ActionBar;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;
//import android.support.v4.app.Fragment;

public class AgentFloatCheck extends BaseActivity {

	Vars vars;
	EditText agentPin;
	Transaction trans;
	Alerter alerter;
	protected String imei;
	// ConnectionDetector cd;
	SharedPreferences pref;
	ProgressDialog pd;
	String dealer;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.layout_agent_float_check);
		ActionBar bar = getActionBar();
		bar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#f47701")));

		this.getWindow().setSoftInputMode(
				WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

		vars = new Vars(this);
		alerter = new Alerter(this);
		// cd = new ConnectionDetector(this);
		pref = PreferenceManager.getDefaultSharedPreferences(this);
		dealer = pref.getString(Globals.permission, null);

		agentPin = (EditText) findViewById(R.id.checkFloat_agtpin);

	}

	// CANCEL
	public void cancel(View v) {
		InputMethodManager inputManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
		inputManager.hideSoftInputFromWindow(
				getCurrentFocus().getWindowToken(),
				InputMethodManager.HIDE_NOT_ALWAYS);
		alerter.alerterConfirmCancel(Globals.cancelBalanceCheckErrorMessage);
	}

	public void checkFloat(View v) {
		InputMethodManager inputManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
		inputManager.hideSoftInputFromWindow(
				getCurrentFocus().getWindowToken(),
				InputMethodManager.HIDE_NOT_ALWAYS);

		// if (cd.isConnectingToInternet()) {
		// CHECK FIELD VALUES..

		if (agentPin.getText().toString() == null
				|| agentPin.getText().toString().isEmpty()) {
			alerter.alerterError(Globals.enterAgentPinErrorMessage);
		} else if (agentPin.getText().toString().length() < 4) {
			alerter.alerterError(Globals.invalidPinErrorMessage);
		} else {
			checkFloat cb = new checkFloat();
			cb.execute();
		}

		// } else {
		// alerter.alerterError(Globals.noInternetDetectedErrorMessage);
		// }

	}

	// -----------------------------------------------------------------------------------------------------
	public class checkFloat extends AsyncTask<Void, Void, String> {

		public String theResult = "NO CONNECTION MADE";
		public String action;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			pd = new ProgressDialog(AgentFloatCheck.this);
			pd.setTitle("Checking Balance / kureba amafaranga asigaye");
			pd.setMessage(Globals.waitProgressMessage);
			pd.setCancelable(false);
			pd.setIndeterminate(true);
			pd.show();
		}

		@Override
		// protected String doInBackground(Void... arg0) {
		protected String doInBackground(Void... arg0) {

			// // VARS TO BE SENT TO SERVER
			// List<NameValuePair> nameValuePairs = new
			// ArrayList<NameValuePair>(8);
			// // AGENT NUMBER
			// nameValuePairs.add(new BasicNameValuePair("agentNumber",
			// vars.prefs
			// .getString("agentPhone", null)));
			// // AGENT PIN
			// nameValuePairs.add(new BasicNameValuePair("agentPin", agentPin
			// .getText().toString()));
			//
			// // DEALER
			// nameValuePairs.add(new BasicNameValuePair("dealer", "ARED"));
			//
			// // CREATE CONNECT OBJECT
			// UrlConnector conn = new UrlConnector(vars.server
			// + "/bio-api/androidCheckFloat/androidCheckFloat.php",
			// nameValuePairs, vars);
			//
			// theResult = conn.connectt();
			// return theResult;

			StringBuilder builder = new StringBuilder();
			ConnectUtils connector = new ConnectUtils();
			HttpClient client = connector.getNewHttpClient();

			String result = null;
			String sendString  = null;

			// LOAD SERVER PREF
			
			
			
			try {

				// DEALER
				sendString = vars.server
						+ "/bio-api/androidCheckFloat/androidCheckFloat.php?"
						+ "agentNumber="+ URLEncoder.encode(vars.prefs.getString("agentPhone", null), "UTF-8")
						 +"&agentPin="+ URLEncoder.encode(agentPin.getText().toString(), "UTF-8")
						 +"&dealer="+ URLEncoder.encode(dealer, "UTF-8")
						 +"&version="+ URLEncoder.encode(Globals.version, "UTF-8")
						 +"&dealer_name="+ URLEncoder.encode(dealer, "UTF-8")
						;
						
						
			}catch(Exception e){
			
			}

			 
			
			
			
			
			

			HttpGet httpGet = new HttpGet(sendString);

			try {
				HttpResponse response = client.execute(httpGet);
				StatusLine statusLine = response.getStatusLine();
				int statusCode = statusLine.getStatusCode();
				if (statusCode == 200) {
					HttpEntity entity = response.getEntity();
					InputStream content = entity.getContent();
					BufferedReader reader = new BufferedReader(
							new InputStreamReader(content));
					String line;
					while ((line = reader.readLine()) != null) {
						builder.append(line);
					}
					result = builder.toString();

				}
			} catch (ConnectTimeoutException e) {
				e.printStackTrace();
			} catch (ClientProtocolException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}

			return result;

		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			// REMOVE DIALOG
			if (pd != null) {
				pd.dismiss();
				// b.setEnabled(true);
			}

			// vars.trans = new Transaction(result);
			if (result != null) {
				trans = new Transaction(result);

				// IF OUTPUT WAS RECEIVED FROM SERVER
				if (trans.getResult() != null) {
					// if (result.contains("ccess")) {
					if (trans.getResult().contains("ccess")) {

						LayoutInflater li = LayoutInflater
								.from(getApplicationContext());
						View promptsView;
						promptsView = li.inflate(
								R.layout.dialog_success_simple, null);

						TextView msgTxt = (TextView) promptsView
								.findViewById(R.id.success_simple_message);

						msgTxt.setText(trans.getResult());

						AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
								AgentFloatCheck.this);

						// set prompts.xml to alertdENroialog builder
						alertDialogBuilder.setView(promptsView);
						// set dialog message
						alertDialogBuilder.setCancelable(false)
								.setPositiveButton("OK",
										new DialogInterface.OnClickListener() {
											public void onClick(
													DialogInterface dialog,
													int id) {
												Intent i = new Intent();
												i.setClass(
														getApplicationContext(),
														Services.class);
												startActivity(i);
											}
										});

						// create alert dialog
						AlertDialog alertDialog = alertDialogBuilder.create();
						// show it
						alertDialog.show();

					} else {
						// alerter.alerterError(trans.getResult());
						alerter.alerterError(trans.getError());
					}
				} else {
					alerter.alerterError("INCORRECT REPLY FROM SERVER");
				}
			} else {
				alerter.alerterError("INVALID RESPONSE FROM THE SEVRER!");
			}
		}
	}

	void log(String msg) {
		System.out.println(msg);
	}

	public void layoutClick(View v) {
		InputMethodManager inputManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
		inputManager.hideSoftInputFromWindow(
				getCurrentFocus().getWindowToken(),
				InputMethodManager.HIDE_NOT_ALWAYS);
	}

	@Override
	protected void onStop() {
		super.onStop();
		if (pd != null)
			pd.dismiss();
	}

}
