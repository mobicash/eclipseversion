package ez.pay.project;

import SecuGen.FDxSDKPro.JSGFPLib;
import android.app.Activity;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbManager;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

public class VerifyPrint_Activity extends Activity implements
		View.OnClickListener, java.lang.Runnable {

	private static final String TAG = "SecuGen USB";

	private Button mCapture;
	private Button mButtonRegister;
	private Button mButtonMatch;
	private Button mButtonLed;
	private Button mSDKTest;
	private EditText mEditLog;
	private android.widget.TextView mTextViewResult;
	private android.widget.CheckBox mCheckBoxMatched;
	private android.widget.CheckBox mCheckBoxSCEnabled;
	private PendingIntent mPermissionIntent;
	private ImageView mImageViewFingerprint;
	private ImageView mImageViewRegister;
	private ImageView mImageViewVerify;
	private byte[] mRegisterImage;
	private byte[] mVerifyImage;
	private byte[] mRegisterTemplate;
	private byte[] mVerifyTemplate;
	private int[] mMaxTemplateSize;
	private int mImageWidth;
	private int mImageHeight;
	private int[] grayBuffer;
	private Bitmap grayBitmap;
	private boolean mLed;

	private JSGFPLib sgfplib;

	// This broadcast receiver is necessary to get user permissions to access
	// the attached USB device
	private static final String ACTION_USB_PERMISSION = "com.android.example.USB_PERMISSION";
	private final BroadcastReceiver mUsbReceiver = new BroadcastReceiver() {
		public void onReceive(Context context, Intent intent) {
			String action = intent.getAction();
			Log.d(TAG, "Enter mUsbReceiver.onReceive()");
			if (ACTION_USB_PERMISSION.equals(action)) {
				synchronized (this) {
					UsbDevice device = (UsbDevice) intent
							.getParcelableExtra(UsbManager.EXTRA_DEVICE);

					if (intent.getBooleanExtra(
							UsbManager.EXTRA_PERMISSION_GRANTED, false)) {
						if (device != null) {
							Log.d(TAG, "Vendor ID : " + device.getVendorId()
									+ "\n");
							Log.d(TAG, "Product ID: " + device.getProductId()
									+ "\n");
							// debugMessage("Vendor ID : " +
							// device.getVendorId() + "\n");
							// debugMessage("Product ID: " +
							// device.getProductId() + "\n");
						} else {
							Log.d(TAG,
									"mUsbReceiver.onReceive() Device is null");
						}
					} else {
						Log.d(TAG,
								"mUsbReceiver.onReceive() permission denied for device "
										+ device);
					}
				}
			}
		}
	};

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.verify_print_layout);

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.verify_print_, menu);
		return true;
	}

	@Override
	public void onClick(View arg0) {
		long dwTimeStart = 0, dwTimeEnd = 0, dwTimeElapsed = 0;

	}

	@Override
	public void onPause() {
		sgfplib.CloseDevice();
		mRegisterImage = null;
		mVerifyImage = null;
		mRegisterTemplate = null;
		mVerifyTemplate = null;
		super.onPause();
	}

	@Override
	public void onResume() {

	}

	@Override
	public void onDestroy() {
		sgfplib.CloseDevice();
		mRegisterImage = null;
		mVerifyImage = null;
		mRegisterTemplate = null;
		mVerifyTemplate = null;
		sgfplib.Close();
		super.onDestroy();
	}

	@Override
	protected void onStop() {
		// unregisterReceiver(mUsbReceiver);
		// unregisterReceiver(deliveryBroadcastReceiver);
		super.onStop();
	}

	@Override
	public void run() {

		Log.d(TAG, "Enter run()");
		// ByteBuffer buffer = ByteBuffer.allocate(1);
		// UsbRequest request = new UsbRequest();
		// request.initialize(mSGUsbInterface.getConnection(), mEndpointBulk);
		// byte status = -1;
		while (true) {
			// queue a request on the interrupt endpoint
			// request.queue(buffer, 1);
			// send poll status command
			// sendCommand(COMMAND_STATUS);
			// wait for status event
			/*
			 * if (mSGUsbInterface.getConnection().requestWait() == request) {
			 * byte newStatus = buffer.get(0); if (newStatus != status) {
			 * Log.d(TAG, "got status " + newStatus); status = newStatus; if
			 * ((status & COMMAND_FIRE) != 0) { // stop firing
			 * sendCommand(COMMAND_STOP); } } try { Thread.sleep(100); } catch
			 * (InterruptedException e) { } } else { Log.e(TAG,
			 * "requestWait failed, exiting"); break; }
			 */
		}
	}

}
