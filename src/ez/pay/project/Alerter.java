package ez.pay.project;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

public class Alerter {

	// Shared prefs
	SharedPreferences prefs;
	Editor edit;
	TextView serverText;
	AlertDialog levelDialog;

	CharSequence[] serverList = null;

	/**
	 * @param args
	 */
	Context context;

	public Alerter(Context context) {
		this.context = context;
	}

	public String getCurrency() {
		prefs = PreferenceManager.getDefaultSharedPreferences(context);
		String currency = "0";
		if (prefs.getString("country", null).equalsIgnoreCase("Uganda")) {
			currency = "800";
		} else if (prefs.getString("country", null)
				.equalsIgnoreCase("Botswana")) {
			currency = "072";
		}
		return currency;
	}

	// ----------------------------------------------------------------------------
	// ALERT GENERATOR
	// public void alerter(String title, String message,Context context) {
	public void alerter(String title, String message, Context context) {

		// public void onClick(View arg0) {
		// loadEm();
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
				context);

		// set title
		alertDialogBuilder.setTitle(title);

		// set dialog message
		alertDialogBuilder.setMessage(message).setCancelable(false)
				.setPositiveButton("OK", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						// if this button is clicked, close
						// current activity
						// MainActivity.this.finish();

						dialog.cancel();
					}
				});

		// create alert dialog
		AlertDialog alertDialog = alertDialogBuilder.create();

		// show it
		alertDialog.show();
		// }
	}

	// CANCEL DIALOG AGENT FUNCTIONS
	public void alerterConfirmCancel(String message) {
		LayoutInflater li = LayoutInflater.from(context);
		View promptsView;
		promptsView = li.inflate(R.layout.confirm_cancel_dialog, null);

		// TextView accountTxt = (TextView)
		// promptsView.findViewById(R.id.confirm_dialog_acct);
		TextView errorTxt = (TextView) promptsView
				.findViewById(R.id.confirm_cancel_text);

		// errorTxt.setText("Are you sure you want to cancel?");
		errorTxt.setText(message);

		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
				context);

		// set prompts.xml to alertdialog builder
		alertDialogBuilder.setView(promptsView);
		// set dialog message
		alertDialogBuilder
				.setCancelable(false)
				.setPositiveButton(Globals.yes,
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								// dialog.cancel();
								Intent i = new Intent();
								i.setClass(context, Services.class);
								context.startActivity(i);
							}
						})
				.setNegativeButton(Globals.no,
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								dialog.cancel();
							}
						});

		// create alert dialog
		AlertDialog alertDialog = alertDialogBuilder.create();

		// show it
		alertDialog.show();

	}

	// CANCEL DIALOG AGENT FUNCTIONS
	public void alerterConfirmCheckBalanceCancel(String message) {
		LayoutInflater li = LayoutInflater.from(context);
		View promptsView;
		promptsView = li.inflate(R.layout.confirm_cancel_dialog, null);

		// TextView accountTxt = (TextView)
		// promptsView.findViewById(R.id.confirm_dialog_acct);
		TextView errorTxt = (TextView) promptsView
				.findViewById(R.id.confirm_cancel_text);

		// errorTxt.setText("Are you sure you want to cancel?");
		errorTxt.setText(message);

		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
				context);

		// set prompts.xml to alertdialog builder
		alertDialogBuilder.setView(promptsView);
		// set dialog message
		alertDialogBuilder
				.setCancelable(false)
				.setPositiveButton(Globals.yes,
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								// dialog.cancel();
								Intent i = new Intent();
								i.setClass(context, AgentFloatCheck.class);
								context.startActivity(i);
							}
						})
				.setNegativeButton(Globals.no,
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								dialog.cancel();
							}
						});

		// create alert dialog
		AlertDialog alertDialog = alertDialogBuilder.create();

		// show it
		alertDialog.show();

	}

	// CANCEL DIALOG AGENT FUNCTIONS
	public void alerterConfirmCancelEtax(String message) {
		LayoutInflater li = LayoutInflater.from(context);
		View promptsView;
		promptsView = li.inflate(R.layout.confirm_cancel_dialog, null);

		// TextView accountTxt = (TextView)
		// promptsView.findViewById(R.id.confirm_dialog_acct);
		TextView errorTxt = (TextView) promptsView
				.findViewById(R.id.confirm_cancel_text);

		// errorTxt.setText("Are you sure you want to cancel?");
		errorTxt.setText(message);

		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
				context);

		// set prompts.xml to alertdialog builder
		alertDialogBuilder.setView(promptsView);
		// set dialog message
		alertDialogBuilder
				.setCancelable(false)
				.setPositiveButton(Globals.yes,
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								// dialog.cancel();
								Intent i = new Intent();
								i.setClass(context, Services.class);
								context.startActivity(i);
							}
						})
				.setNegativeButton(Globals.no,
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								dialog.cancel();
							}
						});

		// create alert dialog
		AlertDialog alertDialog = alertDialogBuilder.create();

		// show it
		alertDialog.show();

	}

	// CANCEL DIALOG AGENT FUNCTIONS
	public void alerterConfirmCancelElectricity(String message) {
		LayoutInflater li = LayoutInflater.from(context);
		View promptsView;
		promptsView = li.inflate(R.layout.confirm_cancel_dialog, null);

		// TextView accountTxt = (TextView)
		// promptsView.findViewById(R.id.confirm_dialog_acct);
		TextView errorTxt = (TextView) promptsView
				.findViewById(R.id.confirm_cancel_text);

		// errorTxt.setText("Are you sure you want to cancel?");
		errorTxt.setText(message);

		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
				context);

		// set prompts.xml to alertdialog builder
		alertDialogBuilder.setView(promptsView);
		// set dialog message
		alertDialogBuilder
				.setCancelable(false)
				.setPositiveButton(Globals.yes,
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								// dialog.cancel();
								Intent i = new Intent();
								i.setClass(context, Electricity.class);
								context.startActivity(i);
							}
						})
				.setNegativeButton(Globals.no,
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								dialog.cancel();
							}
						});

		// create alert dialog
		AlertDialog alertDialog = alertDialogBuilder.create();

		// show it
		alertDialog.show();

	}

	// CANCEL DIALOG AGENT FUNCTIONS
	public void alerterConfirmCancelDSTV(String message) {
		LayoutInflater li = LayoutInflater.from(context);
		View promptsView;
		promptsView = li.inflate(R.layout.confirm_cancel_dialog, null);

		// TextView accountTxt = (TextView)
		// promptsView.findViewById(R.id.confirm_dialog_acct);
		TextView errorTxt = (TextView) promptsView
				.findViewById(R.id.confirm_cancel_text);

		// errorTxt.setText("Are you sure you want to cancel?");
		errorTxt.setText(message);

		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
				context);

		// set prompts.xml to alertdialog builder
		alertDialogBuilder.setView(promptsView);
		// set dialog message
		alertDialogBuilder
				.setCancelable(false)
				.setPositiveButton(Globals.yes,
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								// dialog.cancel();
								Intent i = new Intent();
								i.setClass(context, Services.class);
								context.startActivity(i);
							}
						})
				.setNegativeButton(Globals.no,
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								dialog.cancel();
							}
						});

		// create alert dialog
		AlertDialog alertDialog = alertDialogBuilder.create();

		// show it
		alertDialog.show();

	}

	public void alerterConfirmCancelCanal(String message) {
		LayoutInflater li = LayoutInflater.from(context);
		View promptsView;
		promptsView = li.inflate(R.layout.confirm_cancel_dialog, null);

		// TextView accountTxt = (TextView)
		// promptsView.findViewById(R.id.confirm_dialog_acct);
		TextView errorTxt = (TextView) promptsView
				.findViewById(R.id.confirm_cancel_text);

		// errorTxt.setText("Are you sure you want to cancel?");
		errorTxt.setText(message);

		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
				context);

		// set prompts.xml to alertdialog builder
		alertDialogBuilder.setView(promptsView);
		// set dialog message
		alertDialogBuilder
				.setCancelable(false)
				.setPositiveButton(Globals.yes,
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								// dialog.cancel();
								Intent i = new Intent();
								i.setClass(context, Services.class);
								context.startActivity(i);
							}
						})
				.setNegativeButton(Globals.no,
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								dialog.cancel();
							}
						});

		// create alert dialog
		AlertDialog alertDialog = alertDialogBuilder.create();

		// show it
		alertDialog.show();

	}

	public void alerterConfirmCancelStartimes(String message) {
		LayoutInflater li = LayoutInflater.from(context);
		View promptsView;
		promptsView = li.inflate(R.layout.confirm_cancel_dialog, null);

		// TextView accountTxt = (TextView)
		// promptsView.findViewById(R.id.confirm_dialog_acct);
		TextView errorTxt = (TextView) promptsView
				.findViewById(R.id.confirm_cancel_text);

		// errorTxt.setText("Are you sure you want to cancel?");
		errorTxt.setText(message);

		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
				context);

		// set prompts.xml to alertdialog builder
		alertDialogBuilder.setView(promptsView);
		// set dialog message
		alertDialogBuilder
				.setCancelable(false)
				.setPositiveButton(Globals.yes,
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								// dialog.cancel();
								Intent i = new Intent();
								i.setClass(context, Services.class);
								context.startActivity(i);
							}
						})
				.setNegativeButton(Globals.no,
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								dialog.cancel();
							}
						});

		// create alert dialog
		AlertDialog alertDialog = alertDialogBuilder.create();

		// show it
		alertDialog.show();

	}

	public void alerterError(String message) {
		LayoutInflater li = LayoutInflater.from(context);
		View promptsView;
		promptsView = li.inflate(R.layout.error_dialog, null);

		// TextView accountTxt = (TextView)
		// promptsView.findViewById(R.id.confirm_dialog_acct);
		TextView errorTxt = (TextView) promptsView
				.findViewById(R.id.error_dialog_error);

		errorTxt.setText(message);

		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
				context);

		// set prompts.xml to alertdialog builder
		alertDialogBuilder.setView(promptsView);
		// set dialog message
		alertDialogBuilder.setCancelable(false).setPositiveButton("OK",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						// dialog.cancel();
						dialog.cancel();
					}
				});
		// .setNegativeButton("Cancel",
		// new DialogInterface.OnClickListener() {
		// public void onClick(DialogInterface dialog,int id) {
		// dialog.cancel();
		// }
		// });

		// create alert dialog
		AlertDialog alertDialog = alertDialogBuilder.create();

		// show it
		alertDialog.show();

	}

	public void alerterSuccessSimpleHere(String message) {
		LayoutInflater li = LayoutInflater.from(context);
		View promptsView;
		promptsView = li.inflate(R.layout.dialog_success_simple, null);

		// TextView accountTxt = (TextView)
		// promptsView.findViewById(R.id.confirm_dialog_acct);
		TextView messageTxt = (TextView) promptsView
				.findViewById(R.id.success_simple_message);

		messageTxt.setText(message);

		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
				context);

		// set prompts.xml to alertdialog builder
		alertDialogBuilder.setView(promptsView);
		// set dialog message
		alertDialogBuilder.setCancelable(false).setPositiveButton("Ok",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {

						dialog.cancel();

					}
				});
		// .setNegativeButton("Cancel",
		// new DialogInterface.OnClickListener() {
		// public void onClick(DialogInterface dialog,int id) {
		// dialog.cancel();
		// }
		// });

		// create alert dialog
		AlertDialog alertDialog = alertDialogBuilder.create();

		// show it
		alertDialog.show();
	}

	public void alerterUpdateError(String message) {
		LayoutInflater li = LayoutInflater.from(context);
		View promptsView;
		promptsView = li.inflate(R.layout.error_dialog, null);

		// TextView accountTxt = (TextView)
		// promptsView.findViewById(R.id.confirm_dialog_acct);
		TextView errorTxt = (TextView) promptsView
				.findViewById(R.id.error_dialog_error);

		errorTxt.setText(message);

		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
				context);

		// set prompts.xml to alertdialog builder
		alertDialogBuilder.setView(promptsView);
		// set dialog message
		alertDialogBuilder
				.setCancelable(false)
				.setPositiveButton(Globals.yes,
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								// dialog.cancel();
								dialog.cancel();
							}
						})
				.setNegativeButton(Globals.no,
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								dialog.cancel();
							}
						});

		// create alert dialog
		AlertDialog alertDialog = alertDialogBuilder.create();

		// show it
		alertDialog.show();

	}

	public void alerterSuccessNoHome(String message) {
		LayoutInflater li = LayoutInflater.from(context);
		View promptsView;
		promptsView = li.inflate(R.layout.dialog_success_simple, null);

		// TextView accountTxt = (TextView)
		// promptsView.findViewById(R.id.confirm_dialog_acct);
		TextView messageTxt = (TextView) promptsView
				.findViewById(R.id.success_simple_message);

		messageTxt.setText(message);

		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
				context);

		// set prompts.xml to alertdialog builder
		alertDialogBuilder.setView(promptsView);
		// set dialog message
		alertDialogBuilder.setCancelable(false).setPositiveButton("OK",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						// dialog.cancel();
						dialog.cancel();
					}
				});

		// create alert dialog
		AlertDialog alertDialog = alertDialogBuilder.create();

		// show it
		alertDialog.show();
	}

	public void showServerChoices(final TextView serverText) {

		prefs = PreferenceManager.getDefaultSharedPreferences(context);
		String country = prefs.getString("country", null);
		edit = prefs.edit();

		if (country.equalsIgnoreCase("Rwanda")) {
			serverList = Globals.rwandaServers;
		} else {
			serverList = Globals.servers;
		}

		AlertDialog.Builder builder = new AlertDialog.Builder(context);
		builder.setTitle("Select Server:");
		builder.setSingleChoiceItems(serverList, -1,
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int item) {
						switch (item) {
						case 0:
							edit.putString("server", (String) serverList[item]);
							edit.commit();
							Toast.makeText(
									context,
									"Server changed to:"
											+ prefs.getString("server", null),
									Toast.LENGTH_LONG).show();
							serverText.setText(prefs.getString("server", null));
							break;

						}
						levelDialog.dismiss();
					}
				});
		levelDialog = builder.create();
		levelDialog.show();
	}

	public void showCountryChoices(final TextView serverText) {

		prefs = PreferenceManager.getDefaultSharedPreferences(context);
		edit = prefs.edit();

		AlertDialog.Builder builder = new AlertDialog.Builder(context);
		builder.setTitle("Select Country:");
		builder.setSingleChoiceItems(Globals.countries, -1,
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int item) {
						switch (item) {
						case 0:
							edit.putString("country",
									(String) Globals.countries[item]);
							edit.putString("currency", "800");
							edit.commit();
							Toast.makeText(
									context,
									"Country changed to:"
											+ prefs.getString("country", null),
									Toast.LENGTH_LONG).show();
							Toast.makeText(
									context,
									"Currency Code:"
											+ prefs.getString("currency", null),
									Toast.LENGTH_LONG).show();
							serverText.setText(prefs.getString("country", null));
							break;
						case 1:
							edit.putString("country",
									(String) Globals.countries[item]);
							edit.putString("currency", "072");
							edit.commit();
							Toast.makeText(
									context,
									"Country changed to:"
											+ prefs.getString("country", null),
									Toast.LENGTH_LONG).show();
							Toast.makeText(
									context,
									"Currency Code:"
											+ prefs.getString("currency", null),
									Toast.LENGTH_LONG).show();
							serverText.setText(prefs.getString("country", null));
							break;
						case 2:
							// RWANDA?
							edit.putString("country",
									(String) Globals.countries[item]);
							edit.putString("currency", "000");
							edit.commit();
							Toast.makeText(
									context,
									"Country changed to:"
											+ prefs.getString("country", null),
									Toast.LENGTH_LONG).show();
							// Toast.makeText(context,
							// "Currency Code:"+prefs.getString("currency",
							// null), Toast.LENGTH_LONG).show();
							serverText.setText(prefs.getString("country", null));
							break;
						case 3:
							// SA?
							edit.putString("country",
									(String) Globals.countries[item]);
							edit.putString("currency", "000");
							edit.commit();
							Toast.makeText(
									context,
									"Country changed to:"
											+ prefs.getString("country", null),
									Toast.LENGTH_LONG).show();
							// Toast.makeText(context,
							// "Currency Code:"+prefs.getString("currency",
							// null), Toast.LENGTH_LONG).show();
							serverText.setText(prefs.getString("country", null));
							break;
						case 4:
							// GHANA?
							edit.putString("country",
									(String) Globals.countries[item]);
							edit.putString("currency", "000");
							edit.commit();
							Toast.makeText(
									context,
									"Country changed to:"
											+ prefs.getString("country", null),
									Toast.LENGTH_LONG).show();
							// Toast.makeText(context,
							// "Currency Code:"+prefs.getString("currency",
							// null), Toast.LENGTH_LONG).show();
							serverText.setText(prefs.getString("country", null));
							break;
						// case 5:
						// edit.putString("server", (String)
						// Globals.servers[item]);
						// edit.commit();
						// Toast.makeText(context,
						// "Server changed to:"+prefs.getString("server", null),
						// Toast.LENGTH_LONG).show();
						// serverText.setText(prefs.getString("server", null));
						// break;

						}
						levelDialog.dismiss();
					}
				});
		levelDialog = builder.create();
		levelDialog.show();
	}

	void log(String msg) {
		System.out.println(msg);
	}

	// CANCEL DIALOG AGENT FUNCTIONS
	public void alerterToElectricity(String message) {
		LayoutInflater li = LayoutInflater.from(context);
		View promptsView;
		promptsView = li.inflate(R.layout.confirm_cancel_dialog, null);

		// TextView accountTxt = (TextView)
		// promptsView.findViewById(R.id.confirm_dialog_acct);
		TextView errorTxt = (TextView) promptsView
				.findViewById(R.id.confirm_cancel_text);

		// errorTxt.setText("Are you sure you want to cancel?");
		errorTxt.setText(message);

		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
				context);

		// set prompts.xml to alertdialog builder
		alertDialogBuilder.setView(promptsView);
		// set dialog message
		alertDialogBuilder
				.setCancelable(false)
				.setPositiveButton(Globals.yes,
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								// dialog.cancel();
								Intent i = new Intent();
								i.setClass(context, Electricity.class);
								context.startActivity(i);
							}
						})
				.setNegativeButton(Globals.no,
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								dialog.cancel();
							}
						});

		// create alert dialog
		AlertDialog alertDialog = alertDialogBuilder.create();

		// show it
		alertDialog.show();

	}

	// CANCEL DIALOG AGENT FUNCTIONS
	public void alerterToDstv(String message) {
		LayoutInflater li = LayoutInflater.from(context);
		View promptsView;
		promptsView = li.inflate(R.layout.confirm_cancel_dialog, null);

		// TextView accountTxt = (TextView)
		// promptsView.findViewById(R.id.confirm_dialog_acct);
		TextView errorTxt = (TextView) promptsView
				.findViewById(R.id.confirm_cancel_text);

		// errorTxt.setText("Are you sure you want to cancel?");
		errorTxt.setText(message);

		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
				context);

		// set prompts.xml to alertdialog builder
		alertDialogBuilder.setView(promptsView);
		// set dialog message
		alertDialogBuilder
				.setCancelable(false)
				.setPositiveButton(Globals.yes,
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								// dialog.cancel();
								Intent i = new Intent();
								i.setClass(context, Dstv.class);
								context.startActivity(i);
							}
						})
				.setNegativeButton(Globals.no,
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								dialog.cancel();
							}
						});

		// create alert dialog
		AlertDialog alertDialog = alertDialogBuilder.create();

		// show it
		alertDialog.show();

	}

	// CANCEL DIALOG AGENT FUNCTIONS
	public void alerterToCanal(String message) {
		LayoutInflater li = LayoutInflater.from(context);
		View promptsView;
		promptsView = li.inflate(R.layout.confirm_cancel_dialog, null);

		// TextView accountTxt = (TextView)
		// promptsView.findViewById(R.id.confirm_dialog_acct);
		TextView errorTxt = (TextView) promptsView
				.findViewById(R.id.confirm_cancel_text);

		// errorTxt.setText("Are you sure you want to cancel?");
		errorTxt.setText(message);

		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
				context);

		// set prompts.xml to alertdialog builder
		alertDialogBuilder.setView(promptsView);
		// set dialog message
		alertDialogBuilder
				.setCancelable(false)
				.setPositiveButton(Globals.yes,
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								// dialog.cancel();
								Intent i = new Intent();
								i.setClass(context, Canal.class);
								context.startActivity(i);
							}
						})
				.setNegativeButton(Globals.no,
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								dialog.cancel();
							}
						});

		// create alert dialog
		AlertDialog alertDialog = alertDialogBuilder.create();

		// show it
		alertDialog.show();

	}

	// CANCEL DIALOG AGENT FUNCTIONS
	public void alerterToStartimes(String message) {
		LayoutInflater li = LayoutInflater.from(context);
		View promptsView;
		promptsView = li.inflate(R.layout.confirm_cancel_dialog, null);

		// TextView accountTxt = (TextView)
		// promptsView.findViewById(R.id.confirm_dialog_acct);
		TextView errorTxt = (TextView) promptsView
				.findViewById(R.id.confirm_cancel_text);

		// errorTxt.setText("Are you sure you want to cancel?");
		errorTxt.setText(message);

		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
				context);

		// set prompts.xml to alertdialog builder
		alertDialogBuilder.setView(promptsView);
		// set dialog message
		alertDialogBuilder
				.setCancelable(false)
				.setPositiveButton(Globals.yes,
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								// dialog.cancel();
								Intent i = new Intent();
								i.setClass(context, Startimes.class);
								context.startActivity(i);
							}
						})
				.setNegativeButton(Globals.no,
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								dialog.cancel();
							}
						});

		// create alert dialog
		AlertDialog alertDialog = alertDialogBuilder.create();

		// show it
		alertDialog.show();

	}

	// CANCEL DIALOG AGENT FUNCTIONS
	public void alerterToRRA(String message) {
		LayoutInflater li = LayoutInflater.from(context);
		View promptsView;
		promptsView = li.inflate(R.layout.confirm_cancel_dialog, null);

		// TextView accountTxt = (TextView)
		// promptsView.findViewById(R.id.confirm_dialog_acct);
		TextView errorTxt = (TextView) promptsView
				.findViewById(R.id.confirm_cancel_text);

		// errorTxt.setText("Are you sure you want to cancel?");
		errorTxt.setText(message);

		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
				context);

		// set prompts.xml to alertdialog builder
		alertDialogBuilder.setView(promptsView);
		// set dialog message
		alertDialogBuilder
				.setCancelable(false)
				.setPositiveButton(Globals.yes,
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								// dialog.cancel();
								Intent i = new Intent();
								i.setClass(context, RRA.class);
								context.startActivity(i);
							}
						})
				.setNegativeButton(Globals.no,
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								dialog.cancel();
							}
						});

		// create alert dialog
		AlertDialog alertDialog = alertDialogBuilder.create();

		// show it
		alertDialog.show();

	}

	// CANCEL DIALOG AGENT FUNCTIONS
	public void alerterToCheckBalance(String message) {
		LayoutInflater li = LayoutInflater.from(context);
		View promptsView;
		promptsView = li.inflate(R.layout.confirm_cancel_dialog, null);

		// TextView accountTxt = (TextView)
		// promptsView.findViewById(R.id.confirm_dialog_acct);
		TextView errorTxt = (TextView) promptsView
				.findViewById(R.id.confirm_cancel_text);

		// errorTxt.setText("Are you sure you want to cancel?");
		errorTxt.setText(message);

		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
				context);

		// set prompts.xml to alertdialog builder
		alertDialogBuilder.setView(promptsView);
		// set dialog message
		alertDialogBuilder
				.setCancelable(false)
				.setPositiveButton(Globals.yes,
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								// dialog.cancel();
								Intent i = new Intent();
								i.setClass(context, AgentFloatCheck.class);
								context.startActivity(i);
							}
						})
				.setNegativeButton(Globals.no,
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								dialog.cancel();
							}
						});

		// create alert dialog
		AlertDialog alertDialog = alertDialogBuilder.create();

		// show it
		alertDialog.show();

	}

	// CANCEL DIALOG AGENT FUNCTIONS
	public void alerterToAirtime(String message) {
		LayoutInflater li = LayoutInflater.from(context);
		View promptsView;
		promptsView = li.inflate(R.layout.confirm_cancel_dialog, null);

		// TextView accountTxt = (TextView)
		// promptsView.findViewById(R.id.confirm_dialog_acct);
		TextView errorTxt = (TextView) promptsView
				.findViewById(R.id.confirm_cancel_text);

		// errorTxt.setText("Are you sure you want to cancel?");
		errorTxt.setText(message);

		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
				context);

		// set prompts.xml to alertdialog builder
		alertDialogBuilder.setView(promptsView);
		// set dialog message
		alertDialogBuilder
				.setCancelable(false)
				.setPositiveButton(Globals.yes,
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								// dialog.cancel();
								Intent i = new Intent();
								i.setClass(context, Airtime.class);
								context.startActivity(i);
							}
						})
				.setNegativeButton(Globals.no,
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								dialog.cancel();
							}
						});

		// create alert dialog
		AlertDialog alertDialog = alertDialogBuilder.create();

		// show it
		alertDialog.show();

	}

}
