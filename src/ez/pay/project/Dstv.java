package ez.pay.project;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.conn.ConnectTimeoutException;

import services.BluetoothService;
import utils.BouquetParser;
import utils.ConnectUtils;
import utils.Transaction;
import utils.Vars;
import adapters.BouquetListAdapter;
import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class Dstv extends BaseActivity implements OnItemClickListener {

	Context context;

	// HOLD APP WIDE VARIABLES
	Vars vars;
	Alerter alerter;
	Transaction trans;
	boolean print;

	// UI VARS
	EditText smartCardNo, monthSubcribe, agentPin;
	EditText custref;
	EditText umeme_verify_customerphone;
	Button bouquetButton;

	// TRANS VARS
	String custType;
	String custrefHolder;
	String nameHolder;
	String balanceHolder;

	TextView name;
	TextView acctNum;
	TextView acctType;
	TextView balance, tvPrice, tvBouquetPriceText, tvConfirmCardNumber,
			tvBouquetID, etPhone;
	LinearLayout step2Layout;
	LinearLayout step1Layout;
	LinearLayout step2AmountSection;
	protected String imei;
	int position = 0;

	private List<Bouquet> posts;

	String listItemText;
	String listItemId;
	AlertDialog alert;
	// ConnectionDetector cd;
	ProgressDialog pd;
	SharedPreferences pref;

	// bluetooth

	private static final String TAG = "BluetoothPrint";
	private static final boolean D = true;

	// ��BluetoothService�����������Ϣ����
	public static final int MESSAGE_STATE_CHANGE = 1;
	public static final int MESSAGE_READ = 2;
	public static final int MESSAGE_WRITE = 3;
	public static final int MESSAGE_DEVICE_NAME = 4;
	public static final int MESSAGE_TOAST = 5;

	// BluetoothService�յ��Ĵ������ؼ�������
	public static final String DEVICE_NAME = "device_name";
	public static final String TOAST = "toast";

	// ����Ĵ���
	private static final int REQUEST_CONNECT_DEVICE = 1;
	private static final int REQUEST_ENABLE_BT = 2;
	private static final int REQUEST_Set = 3;

	// ���ӵ��豸������
	private String mConnectedDeviceName = null;
	// �ַ�����������������Ϣ
	private StringBuffer mOutStringBuffer;
	// ��������������
	private BluetoothAdapter mBluetoothAdapter = null;
	public static BluetoothService mService = null;

	private TextView mTitle;
	private EditText mOutEditText;
	private Button mPrintButton;
	private Button mClearButton;
	private Button mSetButton;
	MyApplication myapplication;
	TextView tvPrinterMessage;
	double totalPrice;

	CheckBox cbPrint;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.dstv_layout);
		ActionBar bar = getActionBar();
		bar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#f47701")));
		print = false;
		vars = new Vars(this);

		alerter = new Alerter(this);
		pref = PreferenceManager.getDefaultSharedPreferences(this);
		myapplication = (MyApplication) getApplication();
		init();

		if (listItemText != null && listItemId != null) {
			Toast.makeText(getApplicationContext(),
					"TV: " + listItemText + " ID: " + listItemId,
					Toast.LENGTH_LONG).show();
		}
		// cd = new ConnectionDetector(this);
		pd = new ProgressDialog(Dstv.this);

		// hide keyboard
		this.getWindow().setSoftInputMode(
				WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

		// �õ����ص�������
		mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

		if (mBluetoothAdapter == null) {
			Toast.makeText(this, "��������ʹ�ã�", Toast.LENGTH_LONG).show();
			finish();
			return;
		}

	}

	@Override
	public void onStart() {
		super.onStart();
		Log.e("Log ", "********onStart*******");
		if (D)
			// Log.e(TAG, "++ ON START ++");
			// �ж������Ƿ������������û�п���������������
			if (!mBluetoothAdapter.isEnabled()) {
				Log.e("printClick ",
						"********onStart !mBluetoothAdapter.isEnabled() *******");
				Intent enableIntent = new Intent(
						BluetoothAdapter.ACTION_REQUEST_ENABLE);
				startActivityForResult(enableIntent, REQUEST_ENABLE_BT);
			} else {
				if (myapplication.getmService() == null) {
					Log.e("printClick ", "********setupChat *******");
					setupChat();
				}

			}
	}

	@Override
	public synchronized void onResume() {
		super.onResume();
		Log.e("Log ", "********onResume*******");
		if (D)
			// Log.e(TAG, "+ ON RESUME +");
			if (myapplication.getmService() != null) {
				if (myapplication.getmService().getState() == BluetoothService.STATE_NONE) {
					Log.e("printClick ",
							"********onResume mService.getState() == BluetoothService.STATE_NONE *******");
					myapplication.getmService().start();
				}

			}

		if (myapplication.getmService().getState() == BluetoothService.STATE_CONNECTED) {
			// ivPrinterIcon.setBackgroundResource(R.drawable.log);
			tvPrinterMessage.setText(getString(R.string.connected_txt));
			tvPrinterMessage.setTextColor(Color.parseColor("#398f14"));
			Log.e("printClick ",
					"********onResume mService.getState() == BluetoothService.STATE_CONNECTED *******");
		} else {
			// ivPrinterIcon.setBackgroundResource(R.drawable.not_log);
			tvPrinterMessage.setText(getString(R.string.not_connected_txt));
			tvPrinterMessage.setTextColor(Color.parseColor("#e34949"));
			Log.e("printClick ",
					"********onResume mService.getState() != BluetoothService.STATE_CONNECTED *******");
		}

	}

	private void setupChat() {
		Log.d(TAG, "SetupChat");

		// ��ʼ��BluetoothServiceִ����������
		myapplication.setmService(new BluetoothService(this, mHandler));

	}

	private void sendMessage(String message) {
		// ������ڳ�������

		// ���ʵ��Ҫ���͵Ķ���
		if (message.length() > 0) {
			// �õ���Ϣ�ֽڲ��Ҹ���BluetoothService
			byte[] send;
			try {
				send = message.getBytes("GB2312");
			} catch (UnsupportedEncodingException e) {
				send = message.getBytes();
			}
			// mService.write(send,10);
			myapplication.getmService().write(send, 0, send.length);
		}
	}

	@Override
	public synchronized void onPause() {
		super.onPause();
		if (D)
			Log.e(TAG, "- ON PAUSE -");
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		// Stop the Bluetooth services
		if (myapplication.getmService() != null)
			myapplication.getmService().stop();
		if (D)
			Log.e(TAG, "--- ON DESTROY ---");
	}

	// ���±������ұ�״̬�Ͷ�д״̬��Handler
	private final Handler mHandler = new Handler() {

		@Override
		public void handleMessage(Message msg) {
			switch (msg.what) {
			case MESSAGE_STATE_CHANGE:
				if (D)
					Log.i(TAG, "MESSAGE_STATE_CHANGE:" + msg.arg1);
				switch (msg.arg1) {
				case BluetoothService.STATE_CONNECTED:
					// mTitle.setText(R.string.title_connected_to);
					// mTitle.append(mConnectedDeviceName);
					// ivPrinterIcon.setBackgroundResource(R.drawable.log);

					tvPrinterMessage.setText(getString(R.string.connected_txt));
					tvPrinterMessage.setTextColor(Color.parseColor("#398f14"));
					Log.e("printClick ",
							"********handleMessage STATE_CONNECTED *******");

					break;
				case BluetoothService.STATE_CONNECTING:
					// mTitle.setText(R.string.title_connecting);
					tvPrinterMessage
							.setText(getString(R.string.connecting_txt));
					tvPrinterMessage.setTextColor(Color.parseColor("#000000"));
					Log.e("printClick ",
							"********handleMessage CONNECTING *******");

					break;
				case BluetoothService.STATE_LISTEN:
				case BluetoothService.STATE_NONE:
					// mTitle.setText(R.string.title_not_connected);
					// ivPrinterIcon.setBackgroundResource(R.drawable.not_log);
					tvPrinterMessage
							.setText(getString(R.string.not_connected_txt));
					tvPrinterMessage.setTextColor(Color.parseColor("#e34949"));

					Log.e("printClick ",
							"********handleMessage NOT_CONNECTED *******");

					break;
				}
				break;
			case MESSAGE_WRITE:
				break;
			case MESSAGE_READ:
				// ����Ч�ֽڻ���������һ���ַ���
				break;
			case MESSAGE_DEVICE_NAME:
				// ���������豸������
				mConnectedDeviceName = msg.getData().getString(DEVICE_NAME);
				// Toast.makeText(getApplicationContext(),
				// "���ӵ�" + mConnectedDeviceName, Toast.LENGTH_SHORT)
				// .show();
				alerter.alerterSuccessSimpleHere(mConnectedDeviceName
						+ " connected / " + mConnectedDeviceName
						+ " iracometse neza ");

				break;
			case MESSAGE_TOAST:
				// Toast.makeText(getApplicationContext(),
				// msg.getData().getString(TOAST), Toast.LENGTH_SHORT)
				// .show();

				alerter.alerterError("Error retry / ikibazo ongera ugerageze ");

				break;

			}

		}
	};

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (D)
			Log.d(TAG, "onActivityResult" + resultCode);
		switch (requestCode) {
		case REQUEST_CONNECT_DEVICE:
			// ��DeviceListActivity����һ���豸����
			if (resultCode == Activity.RESULT_OK) {

				// ��ȡ�豸��MAC��ַ
				String address = data.getExtras().getString(
						DeviceListActivity.EXTRA_DEVICE_ADDRESS);
				// �õ�BLuetoothDevice����
				if (address == null) {
					break;
				}
				// ��ȡ�豸��������
				BluetoothDevice device = mBluetoothAdapter
						.getRemoteDevice(address);
				// �������ӵ��豸
				myapplication.getmService().connect(device);
			}
			break;
		case REQUEST_ENABLE_BT:
			// �����󼤻������Ļر�
			if (resultCode == Activity.RESULT_OK) {
				// �������������õ�,���,����һ������Ự
				// ���������ɹ����������ʼ��UI
				setupChat();

			} else {
				// �û�û���������������
				Log.d(TAG, "BT not enabled");
				// Toast.makeText(this, R.string.bt_not_enabled_leaving,
				// Toast.LENGTH_SHORT).show();
				alerter.alerterError(getString(R.string.bt_not_enabled_leaving));
				finish();
			}

			/*
			 * case REQUEST_Set:
			 * 
			 * if(resultCode==Activity.RESULT_OK) {
			 * 
			 * byte a[]=data.getExtras().getByteArray("data");
			 * mService.write(a,0,a.length); Toast.makeText(this, "���óɹ���",
			 * Toast.LENGTH_SHORT).show(); } break;
			 */
		}
	}

	@Override
	public void onBackPressed() {
		android.os.Process.killProcess(android.os.Process.myPid());
	}

	private void init() {
		// TODO Auto-generated method stub
		smartCardNo = (EditText) findViewById(R.id.etSmartCardNo);
		monthSubcribe = (EditText) findViewById(R.id.etmonthToSubscribe);
		agentPin = (EditText) findViewById(R.id.etAgentPin);

		// CONFRIM VARS
		bouquetButton = (Button) findViewById(R.id.btLoadBouquet);
		tvPrice = (TextView) findViewById(R.id.tvBouquetPrice);
		tvBouquetPriceText = (TextView) findViewById(R.id.tvBouquetPriceText);
		tvBouquetID = (TextView) findViewById(R.id.tvBouquetID);
		tvPrinterMessage = (TextView) findViewById(R.id.tv_printer_message);
		cbPrint = (CheckBox) findViewById(R.id.cb_print);

		etPhone = (TextView) findViewById(R.id.et_phone);

	}

	public void printClick(View v) {
		if (((CheckBox) v).isChecked()) {

			if (myapplication.getmService().getState() != BluetoothService.STATE_CONNECTED) {
				// Toast.makeText(this, R.string.not_connected,
				// Toast.LENGTH_SHORT).show();
				try {

					Log.e("printClick ",
							"********printClick mService.getState() != BluetoothService.STATE_CONNECTED *******");

					Intent serverIntent = new Intent(this,
							DeviceListActivity.class);
					startActivityForResult(serverIntent, REQUEST_CONNECT_DEVICE);

					return;
				} catch (Exception e) {

				}
			}

		}
	}

	// CHECK ACCOUNT
	public void confirm(View v) {
		String bouquet = bouquetButton.getText().toString();
		final String tvBouquetPrice = tvPrice.getText().toString();
		final String smartCardNumber = smartCardNo.getText().toString();
		final String monthToSubscribe = monthSubcribe.getText().toString();
		final String agPin = agentPin.getText().toString();
		final String bouquetId = tvBouquetID.getText().toString();

		if (bouquet.contains("select Bouquet")) {

			alerter.alerterError(Globals.tvBouquetErrorMessage);
		} else if (smartCardNumber.length() <= 0) {

			alerter.alerterError(Globals.tvSmartCardErrorMessage);
		} else if (monthToSubscribe.length() <= 0) {

			alerter.alerterError(Globals.tvMonthErrorMessage);
		} else if (etPhone.getText().toString() == null
				|| etPhone.getText().toString().isEmpty()) {
			alerter.alerterError(Globals.phoneCheckErrorMessage);
		} else if (etPhone.getText().toString().length() != 10) {

			alerter.alerterError(Globals.invalidNumberErrorMessage);

		} else if (agPin.length() <= 0) {

			alerter.alerterError(Globals.enterAgentPinErrorMessage);
		} else if (agPin.length() < 4) {

			alerter.alerterError(Globals.invalidPinErrorMessage);
		} else {

			double doublemonthToSubscribe = Double.valueOf(monthToSubscribe);
			double doubleBouquetPrice = Double.valueOf(tvBouquetPrice);
			totalPrice = doubleBouquetPrice * doublemonthToSubscribe;
			LayoutInflater li = LayoutInflater.from(getApplicationContext());
			View promptsView;
			promptsView = li.inflate(R.layout.dstv_confirm_dialog, null);

			TextView bouquetTxt = (TextView) promptsView
					.findViewById(R.id.confirm_bouquet);
			TextView priceTxt = (TextView) promptsView
					.findViewById(R.id.confirm_total_price);
			TextView monthTxt = (TextView) promptsView
					.findViewById(R.id.confirm_month);
			TextView tvConfirmCardNumber = (TextView) promptsView
					.findViewById(R.id.tvConfirmCardNumber);
			TextView phone = (TextView) promptsView
					.findViewById(R.id.confirm_phone);

			bouquetTxt.setText(bouquet);
			priceTxt.setText(totalPrice + "");
			monthTxt.setText(monthToSubscribe);
			tvConfirmCardNumber.setText(smartCardNumber);
			phone.setText(etPhone.getText().toString());

			AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
					Dstv.this);

			// set prompts.xml to alertdialog builder
			alertDialogBuilder.setView(promptsView);
			// set dialog message
			alertDialogBuilder
					.setCancelable(false)
					.setPositiveButton(Globals.yes,
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog,
										int id) {

									new BuyDstv().execute(smartCardNumber,
											bouquetId, agPin, monthToSubscribe,
											tvBouquetPrice, etPhone.getText()
													.toString());

								}

							})
					.setNegativeButton(Globals.no,
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog,
										int id) {

									dialog.cancel();

								}
							});

			// create alert dialog
			AlertDialog alertDialog = alertDialogBuilder.create();
			// show it
			alertDialog.show();

		}

	}

	public void log(String msg) {
		System.out.println(msg);
	}

	public void loadBouquets(View v) {
		// if (cd.isConnectingToInternet()) {
		new BouquetsFetcher().execute();
		// } else {
		// alerter.alerterError(Globals.noInternetDetectedErrorMessage);
		// }

	}

	private class BouquetsFetcher extends AsyncTask<Void, Void, String> {
		private static final String TAG = "PostFetcher";
		String theResult = "NO CONNECTION MADE";
		List<Bouquet> bouquets = new ArrayList<Bouquet>();

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			pd.setTitle("Loading bouquets / ishakisha ...");
			pd.setMessage(Globals.waitProgressMessage);
			pd.setCancelable(false);
			pd.setIndeterminate(true);
			pd.show();
		}

		@Override
		protected String doInBackground(Void... params) {

			// Create an HTTP client

			StringBuilder builder = new StringBuilder();
			ConnectUtils connector = new ConnectUtils();
			HttpClient client = connector.getNewHttpClient();

			String result = "FAILED";
			String urlString = null;

			// LOAD SERVER PREF

			try {
				urlString = vars.server
						+ "/bio-api/androidDSTV/bouquet.php?broadcaster="
						+ URLEncoder.encode("DSTV", "UTF-8")
						+ "&broadcasterId="
						+ URLEncoder.encode("1", "UTF-8")
						+ "&dealer="
						+ URLEncoder.encode(
								pref.getString(Globals.permission, null),
								"UTF-8")
						+ "&version="
						+ URLEncoder.encode(Globals.version, "UTF-8")

						+ "&dealer_name="
						+ URLEncoder.encode(
								vars.prefs.getString(Globals.DEALERNAME, null),
								"UTF-8")

				;

			} catch (UnsupportedEncodingException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

			HttpGet httpGet = new HttpGet(urlString);

			try {
				HttpResponse response = client.execute(httpGet);
				StatusLine statusLine = response.getStatusLine();
				int statusCode = statusLine.getStatusCode();
				if (statusCode == 200) {
					HttpEntity entity = response.getEntity();
					InputStream content = entity.getContent();
					BufferedReader reader = new BufferedReader(
							new InputStreamReader(content));
					String line;
					while ((line = reader.readLine()) != null) {
						builder.append(line);
					}
					result = builder.toString();

				} else {

				}
			} catch (ConnectTimeoutException e) {

				e.printStackTrace();
			} catch (ClientProtocolException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}

			return result;
		}

		@Override
		protected void onPostExecute(String bouquet) {

			BouquetParser bp = new BouquetParser();
			final List<Bouquet> listBouquet = bp.showResults(bouquet);

			if (listBouquet.get(0).getName() != "Error") {
				AlertDialog.Builder builder = new AlertDialog.Builder(Dstv.this);
				builder.setTitle(Globals.tvBouquet);
				ListView list = new ListView(Dstv.this);
				list.setAdapter(new BouquetListAdapter(Dstv.this, R.layout.row,
						listBouquet));
				builder.setView(list);
				alert = builder.create();
				alert.show();

				list.setOnItemClickListener(Dstv.this);
			} else {
				alerter.alerterError(Globals.InternetErrorMessage);
			}

			pd.dismiss();

		}

	}

	// on menu item click method
	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		// TODO Auto-generated method stub

		Context context = view.getContext();
		// bouquet name
		TextView textViewItem = ((TextView) view.findViewById(R.id.tvText));

		// get the price
		TextView tvBouquetPrice = ((TextView) view.findViewById(R.id.tvPrice));

		// set the price
		tvBouquetPriceText.setVisibility(View.VISIBLE);
		tvPrice.setText(tvBouquetPrice.getTag().toString());
		tvPrice.setVisibility(View.VISIBLE);

		// get the clicked item name
		listItemText = tvBouquetPrice.getText().toString();

		// get the clicked item ID
		listItemId = textViewItem.getTag().toString();
		tvBouquetID.setText(listItemId);
		tvBouquetID.setVisibility(View.GONE);
		// bouquetButton.setText(listItemText);

		Button bouq = (Button) findViewById(R.id.btLoadBouquet);
		bouq.setText(listItemText);
		alert.dismiss();

	}

	// buy DSTV here

	public class BuyDstv extends AsyncTask<String, Void, String> {

		public String theResult = "NO CONNECTION MADE";

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			pd.setTitle("DSTV Purchase / kugura DSTV");
			pd.setMessage(Globals.waitProgressMessage);
			pd.setCancelable(false);
			pd.setIndeterminate(true);
			pd.show();
		}

		@Override
		protected String doInBackground(String... arg) {

			String cardNumber = arg[0];
			String bouquetId = arg[1];
			String agentPin = arg[2];
			String month = arg[3];
			String price = arg[4];
			String phone = arg[5];

			// Toast.makeText(getApplicationContext(),
			// "card #: " + cardNumber + " bouquetId #: " +
			// bouquetId+",agentPin: "+agentPin+" ,"+month,
			// Toast.LENGTH_LONG).show();

			// VARS TO BE SENT TO SERVER

			StringBuilder builder = new StringBuilder();
			ConnectUtils connector = new ConnectUtils();
			HttpClient client = connector.getNewHttpClient();

			String result = "FAILED";
			String urlString = null;

			// LOAD SERVER PREF

			try {

				// DEALER
				urlString = vars.server
						+ "/bio-api/androidDSTV/dstv_api.php?agentNumber="
						+ URLEncoder.encode(
								vars.prefs.getString("agentPhone", null),
								"UTF-8")
						+ "&agentPin="
						+ URLEncoder.encode(agentPin, "UTF-8")

						+ "&bouquetId="
						+ URLEncoder.encode(bouquetId, "UTF-8")

						+ "&price="
						+ URLEncoder.encode(price, "UTF-8")

						+ "&cardNumber="
						+ URLEncoder.encode(cardNumber, "UTF-8")

						+ "&month="
						+ URLEncoder.encode(month, "UTF-8")

						+ "&broadcaster="
						+ URLEncoder.encode("DSTV", "UTF-8")

						+ "&broadcasterId="
						+ URLEncoder.encode("1", "UTF-8")

						+ "&phoneNumber="
						+ URLEncoder.encode(etPhone.getText().toString(),
								"UTF-8")

						+ "&dealer="
						+ URLEncoder.encode(
								pref.getString(Globals.permission, null),
								"UTF-8")

						+ "&version="
						+ URLEncoder.encode(Globals.version, "UTF-8")

						+ "&dealer_name="
						+ URLEncoder.encode(
								vars.prefs.getString(Globals.DEALERNAME, null),
								"UTF-8");

			} catch (UnsupportedEncodingException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

			HttpGet httpGet = new HttpGet(urlString);

			try {
				HttpResponse response = client.execute(httpGet);
				StatusLine statusLine = response.getStatusLine();
				int statusCode = statusLine.getStatusCode();
				if (statusCode == 200) {
					HttpEntity entity = response.getEntity();
					InputStream content = entity.getContent();
					BufferedReader reader = new BufferedReader(
							new InputStreamReader(content));
					String line;
					while ((line = reader.readLine()) != null) {
						builder.append(line);
					}
					result = builder.toString();

				} else {

				}
			} catch (ConnectTimeoutException e) {

				e.printStackTrace();
			} catch (ClientProtocolException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}

			return result;

		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			// REMOVE DIALOG
			if (pd != null) {
				pd.dismiss();
				// b.setEnabled(true);
			}

			trans = new Transaction(result);

			// IF OUTPUT WAS RECEIVED FROM SERVER
			if (trans.getResult() != null && !trans.getResult().isEmpty()) {

				if (trans.getResult().contains("ccess")) {

					LayoutInflater li = LayoutInflater
							.from(getApplicationContext());
					View promptsView;
					promptsView = li.inflate(R.layout.dialog_success_simple,
							null);

					TextView msgTxt = (TextView) promptsView
							.findViewById(R.id.success_simple_message);

					// VERIFICATION RETURN
					//

					// SET DIALOG TEXT
					// IF POST PAID
					if (trans.getConfNum() != null) {

						msgTxt.setText(trans.getMessage());

						if (cbPrint.isChecked()
								&& myapplication.getmService().getState() == BluetoothService.STATE_CONNECTED) {

							String title = "DSTV";

							String cardTitle = "Smart Card No";
							String card = smartCardNo.getText().toString();

							String monthTitle = "Months To Subscribe";
							String month = monthSubcribe.getText().toString();

							String telePhoneTitle = "Telephone #";
							String telePhone = etPhone.getText().toString();

							String priceTitle = "Total Price #";
							String price = Double.toString(totalPrice);

							String thankUMessage = "Thank you for using MCash ";
							String keepEnjoyingMessage = "Keep Enjoying !";
							// String dash = "##############################";
							String star = "******************************";

							int width = 30;

							int padSize = width - title.length();
							int padStart = title.length() + padSize / 2;

							title = String.format("%" + padStart + "s", title);
							title = String.format("%-" + width + "s", title);

							String message = title + "\n" + star + "\n\n\n"
									+ cardTitle + "\n" + card + "\n\n"
									+ monthTitle + "\n" + month + "\n\n"
									+ telePhoneTitle + "\n" + telePhone
									+ "\n\n" + priceTitle + "\n" + price
									+ "\n\n" + star + "\n" + thankUMessage
									+ "\n" + keepEnjoyingMessage
									+ "\n\n\n\n\n\n\n\n\n\n";

							sendMessage(message);

						}

					}

					AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
							Dstv.this);

					// set prompts.xml to alertdENroialog builder
					alertDialogBuilder.setView(promptsView);
					// set dialog message
					alertDialogBuilder.setCancelable(false).setPositiveButton(
							"OK", new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog,
										int id) {

									Intent i = new Intent();
									i.setClass(getApplicationContext(),
											Services.class);
									startActivity(i);

								}
							});

					// create alert dialog
					AlertDialog alertDialog = alertDialogBuilder.create();
					// show it
					alertDialog.show();

				} else {
					// alerter.alerterError(trans.getResult());
					alerter.alerterError(trans.getError());
				}
			} else {
				alerter.alerterError(Globals.InternetErrorMessage);
			}

		}
	}

	public void layoutClick(View v) {
		InputMethodManager inputManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
		inputManager.hideSoftInputFromWindow(
				getCurrentFocus().getWindowToken(),
				InputMethodManager.HIDE_NOT_ALWAYS);
	}

	public void cancel(View v) {
		alerter.alerterConfirmCancelDSTV(Globals.cancelDstvErrorMessage);
	}

	public void airtimeClick(View v) {
		if (!bouquetButton.getText().toString().contains("select Bouquet")
				|| smartCardNo.getText().length() > 0
				|| monthSubcribe.getText().length() > 0
				|| agentPin.getText().length() > 0) {
			alerter.alerterToAirtime(Globals.cancelDstvErrorMessage);
		} else {
			Intent intent = new Intent(Dstv.this, Airtime.class);
			startActivity(intent);
		}

	}

	public void electricityClick(View v) {

		if (!bouquetButton.getText().toString().contains("select Bouquet")
				|| smartCardNo.getText().length() > 0
				|| monthSubcribe.getText().length() > 0
				|| agentPin.getText().length() > 0) {
			alerter.alerterToElectricity(Globals.cancelDstvErrorMessage);
		} else {
			Intent intent = new Intent(Dstv.this, Electricity.class);
			startActivity(intent);
		}

	}

	public void canalClick(View v) {

		if (!bouquetButton.getText().toString().contains("select Bouquet")
				|| smartCardNo.getText().length() > 0
				|| monthSubcribe.getText().length() > 0
				|| agentPin.getText().length() > 0) {
			alerter.alerterToCanal(Globals.cancelDstvErrorMessage);
		} else {
			Intent intent = new Intent(Dstv.this, Canal.class);
			startActivity(intent);
		}

	}

	public void startimesClick(View v) {

		if (!bouquetButton.getText().toString().contains("select Bouquet")
				|| smartCardNo.getText().length() > 0
				|| monthSubcribe.getText().length() > 0
				|| agentPin.getText().length() > 0) {
			alerter.alerterToStartimes(Globals.cancelDstvErrorMessage);
		} else {
			Intent intent = new Intent(Dstv.this, Startimes.class);
			startActivity(intent);
		}

	}

	public void etaxClick(View v) {

		if (!bouquetButton.getText().toString().contains("select Bouquet")
				|| smartCardNo.getText().length() > 0
				|| monthSubcribe.getText().length() > 0
				|| agentPin.getText().length() > 0) {
			alerter.alerterToRRA(Globals.cancelDstvErrorMessage);
		} else {
			Intent intent = new Intent(Dstv.this, RRA.class);
			startActivity(intent);
		}

	}

	public void checkBalanceClick(View v) {

		if (!bouquetButton.getText().toString().contains("select Bouquet")
				|| smartCardNo.getText().length() > 0
				|| monthSubcribe.getText().length() > 0
				|| agentPin.getText().length() > 0) {
			alerter.alerterToCheckBalance(Globals.cancelDstvErrorMessage);
		} else {
			Intent intent = new Intent(Dstv.this, AgentFloatCheck.class);
			startActivity(intent);
		}

	}

	@Override
	protected void onStop() {
		super.onStop();
		if (pd != null)
			pd.dismiss();

	}

}
