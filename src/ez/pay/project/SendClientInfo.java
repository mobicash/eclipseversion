package ez.pay.project;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import models.Area;
import models.Result;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.conn.ConnectTimeoutException;

import utils.AESHelper;
import utils.AreaParser;
import utils.ConnectUtils;
import utils.ResultParser;
import utils.Transaction;
import utils.Vars;
import adapters.AreaAdapter;
import android.app.ActionBar;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class SendClientInfo extends BaseActivity implements
		OnItemSelectedListener {

	Spinner customerTypeSpinner;
	Alerter alerter;
	Vars vars;
	ProgressDialog pd;
	int defaultValue;
	List<Area> areaList = new ArrayList<Area>();
	List<Area> isActiveList = new ArrayList<Area>();
	MyApplication myApplication;
	// String isActive, isActiveValue;
	EditText etCustomerWalletId;
	String customerWalletId;
	// etNfcTagSerial;
	String customerType;

	String uniqueID;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.send_client_info);
		ActionBar bar = getActionBar();
		bar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#f47701")));

		init();

		uniqueID = UUID.randomUUID().toString();

		String uniqueIDArray[] = uniqueID.split("-");
		int size = uniqueIDArray.length;
		String uniqueIDLastPart = uniqueIDArray[size - 1];
		myApplication.setNfcTagSerial(uniqueIDLastPart);

		new CustomerTypeAsyncTask().execute();

		// // ATM Flag
		// Area area1 = new Area();
		// // Item 1
		// area1.setId("01");
		// area1.setDefaultValue("false");
		// area1.setValue("please select");
		// isActiveList.add(area1);
		//
		// // Item 2
		// Area area2 = new Area();
		// area2.setId("1");
		// area2.setDefaultValue("false");
		// area2.setValue("Yes");
		// isActiveList.add(area2);
		//
		// // Item 3
		// Area area3 = new Area();
		// area3.setId("0");
		// area3.setDefaultValue("false");
		// area3.setValue("No");
		// isActiveList.add(area3);
		//
		// isActiveSpinner.setAdapter(new AreaAdapter(SendClientInfo.this,
		// R.layout.spinner_rows, isActiveList, defaultValue));
		// isActiveSpinner.setOnItemSelectedListener(new
		// OnItemSelectedListener() {
		// @Override
		// public void onItemSelected(AdapterView<?> parentView, View v,
		// int position, long id) {
		//
		// isActive = isActiveList.get(position).getId();
		// isActiveValue = isActiveList.get(position).getValue();
		//
		// }
		//
		// @Override
		// public void onNothingSelected(AdapterView<?> parentView) {
		// // your code here
		// }
		//
		// });

	}

	private void init() {
		// TODO Auto-generated method stub
		pd = new ProgressDialog(SendClientInfo.this);
		myApplication = (MyApplication) getApplication();
		customerTypeSpinner = (Spinner) findViewById(R.id.sp_customer_type);
		// isActiveSpinner = (Spinner) findViewById(R.id.sp_is_active);
		etCustomerWalletId = (EditText) findViewById(R.id.et_customer_wallet_id);

		// etNfcTagSerial = (EditText) findViewById(R.id.et_nfc_tag_serial);
		alerter = new Alerter(this);
		vars = new Vars(this);
		defaultValue = 0;
	}

	public class CustomerTypeAsyncTask extends AsyncTask<String, Void, String> {

		public String theResult;
		public String action;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();

		}

		@Override
		// protected String doInBackground(Void... arg0) {
		protected String doInBackground(String... params) {

			StringBuilder builder = new StringBuilder();
			ConnectUtils connector = new ConnectUtils();
			HttpClient client = connector.getNewHttpClient();

			String result = "FAILED";

			String sendString = null;

			try {

				// DEALER
				sendString = server + "getCustomerType.php";

			} catch (Exception e) {

			}

			HttpGet httpGet = new HttpGet(sendString);

			try {
				HttpResponse response = client.execute(httpGet);
				StatusLine statusLine = response.getStatusLine();
				int statusCode = statusLine.getStatusCode();
				if (statusCode == 200) {
					HttpEntity entity = response.getEntity();
					InputStream content = entity.getContent();
					BufferedReader reader = new BufferedReader(
							new InputStreamReader(content));
					String line;
					while ((line = reader.readLine()) != null) {
						builder.append(line);
					}
					result = builder.toString();

				} else {

				}
			} catch (ConnectTimeoutException e) {
				theResult = "timeout";
				// theResult = "timeout";
				e.printStackTrace();
			} catch (ClientProtocolException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}

			return result;

			//

		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);

			try {

				AreaParser areaParser = new AreaParser();
				areaList = areaParser.getAreaList(result);

				if (areaList.get(0).getValue() != null) {

					customerTypeSpinner.setAdapter(new AreaAdapter(
							SendClientInfo.this, R.layout.spinner_rows,
							areaList, defaultValue));
					customerTypeSpinner
							.setOnItemSelectedListener(SendClientInfo.this);

					// myApplication.setAreaList(areaList);

				} else {
					// alerter.alerterError(Globals.InternetErrorMessage);
				}

			} catch (Exception e) {
				// alerter.alerterError(Globals.InternetErrorMessage);
			}

		}
	}

	public class SendClientInfoAsyncTask extends
			AsyncTask<String, Void, String> {

		public String theResult;
		public String action;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();

		}

		@Override
		// protected String doInBackground(Void... arg0) {
		protected String doInBackground(String... params) {

			StringBuilder builder = new StringBuilder();
			ConnectUtils connector = new ConnectUtils();
			HttpClient client = connector.getNewHttpClient();

			String result = "FAILED";

			String sendString = null;

			try {

				// DEALER
				sendString = server
						+ "enrollJordanClient.php?customerType="
						+ URLEncoder.encode(customerType, "UTF-8")
						+ "&phoneNumber="
						+ URLEncoder.encode(customerWalletId, "UTF-8")

						+ "&nfcTagSerial="
						+ URLEncoder.encode(myApplication.getNfcTagSerial(),
								"UTF-8")
						+ "&tagStatus="
						+ URLEncoder.encode("true", "UTF-8")
						+ "&pspID="
						+ URLEncoder.encode(
								preferences.getString("PSPID", null), "UTF-8")
						+ "&nfdTagUid="
						+ URLEncoder.encode(myApplication.getNfdTagUid(),
								"UTF-8");

//				Log.e("url", sendString);

			} catch (Exception e) {

			}

			HttpGet httpGet = new HttpGet(sendString);

			try {
				HttpResponse response = client.execute(httpGet);
				StatusLine statusLine = response.getStatusLine();
				int statusCode = statusLine.getStatusCode();
				if (statusCode == 200) {
					HttpEntity entity = response.getEntity();
					InputStream content = entity.getContent();
					BufferedReader reader = new BufferedReader(
							new InputStreamReader(content));
					String line;
					while ((line = reader.readLine()) != null) {
						builder.append(line);
					}
					result = builder.toString();

				} else {

				}
			} catch (ConnectTimeoutException e) {
				theResult = "timeout";
				// theResult = "timeout";
				e.printStackTrace();
			} catch (ClientProtocolException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}

			return result;

			//

		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);

			try {

				Log.e("response", result);

				// REMOVE DIALOG
				if (pd != null) {
					pd.dismiss();
					// b.setEnabled(true);
				}

				Transaction trans = new Transaction(result);

				// IF OUTPUT WAS RECEIVED FROM SERVER
				if (trans.getResult() != null && !trans.getResult().isEmpty()) {
					// if (result.contains("ccess")) {
					if (trans.getResult().contains("Success")) {

						Date date = new Date();
						long timeMilli = date.getTime();
						System.out
								.println("Time in milliseconds using Date class: "
										+ timeMilli);

						myApplication.setDateTime(timeMilli);
						// myApplication.setNfcTagSerial(etNfcTagSerial.getText()
						// .toString());
						myApplication.setCustomerWalletId(customerWalletId);

						Intent intent = new Intent(SendClientInfo.this,
								WriteClientInfo.class);
						startActivity(intent);
					} else {
						// alerter.alerterError(trans.getResult());
						alerter.alerterError(trans.getMessage());
					}
				} else {
					alerter.alerterError(Globals.InternetErrorMessage);
				}

			} catch (Exception e) {

				// REMOVE DIALOG
				if (pd != null) {
					pd.dismiss();
					// b.setEnabled(true);
				}
				alerter.alerterError(Globals.InternetErrorMessage);
			}

		}
	}

	public class CheckIfCustomerExistAsyncTask extends
			AsyncTask<String, Void, String> {

		public String theResult;
		public String action;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();

			pd.setMessage("please wait ...");
			pd.setCancelable(false);
			pd.setIndeterminate(true);
			pd.show();

		}

		@Override
		// protected String doInBackground(Void... arg0) {
		protected String doInBackground(String... params) {

			StringBuilder builder = new StringBuilder();
			ConnectUtils connector = new ConnectUtils();
			HttpClient client = connector.getNewHttpClient();

			String result = "FAILED";

			String sendString = null;

			try {

				// DEALER
				sendString = server + "getStudentInfo.php?phoneNumber="
						+ customerWalletId;

			} catch (Exception e) {

			}

			HttpGet httpGet = new HttpGet(sendString);

			try {
				HttpResponse response = client.execute(httpGet);
				StatusLine statusLine = response.getStatusLine();
				int statusCode = statusLine.getStatusCode();
				if (statusCode == 200) {
					HttpEntity entity = response.getEntity();
					InputStream content = entity.getContent();
					BufferedReader reader = new BufferedReader(
							new InputStreamReader(content));
					String line;
					while ((line = reader.readLine()) != null) {
						builder.append(line);
					}
					result = builder.toString();

				} else {

				}
			} catch (ConnectTimeoutException e) {
				theResult = "timeout";
				// theResult = "timeout";
				e.printStackTrace();
			} catch (ClientProtocolException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}

			return result;

			//

		}

		@Override
		protected void onPostExecute(String jsong) {
			super.onPostExecute(jsong);

			try {

				Log.e("result", jsong);

				ResultParser resultParser = new ResultParser();

				Result resultHere = resultParser.getResult(jsong);
				Log.e("result", resultHere.getResult());

				if (resultHere.getResult().contains("sucess")) {
					// Log.e("success", "successss+++++");
					// customToast("Customer already exist .");
					new SendClientInfoAsyncTask().execute();
				} else {
					// customToast("Customer already exist .");
					new SendClientInfoAsyncTask().execute();

					// Log.e("error", resultHere.getResult());
					// alerter.alerterError("Customer not registered please try again .");
					// REMOVE DIALOG

					// if (pd != null) {
					// pd.dismiss();
					// // b.setEnabled(true);
					// }

				}

			} catch (Exception e) {
				alerter.alerterError("Customer not registered please check your connectivity .");
				Log.e("error", e.toString());
				// REMOVE DIALOG
				if (pd != null) {
					pd.dismiss();
					// b.setEnabled(true);
				}
			}

		}
	}

	@Override
	public void onItemSelected(AdapterView<?> parent, View view, int position,
			long id) {
		// TODO Auto-generated method stub

		myApplication.setCustomerType(areaList.get(position).getValue()
				.substring(0, 2));

		customerType = areaList.get(position).getValue();

	}

	@Override
	public void onNothingSelected(AdapterView<?> parent) {
		// TODO Auto-generated method stub

	}

	public void nextClick(View v) {

		customerWalletId = "+962" + etCustomerWalletId.getText().toString();

		if (etCustomerWalletId.getText().toString() == null
				|| etCustomerWalletId.getText().toString().isEmpty()) {
			alerter.alerterError(Globals.customerWalletIdErrorMessage);
		} else if (!isNetworkAvailable()) {
			alerter.alerterError(Globals.connectionErrorMessage);
		}

		// else if (etNfcTagSerial.getText().toString() == null
		// || etNfcTagSerial.getText().toString().isEmpty()) {
		// alerter.alerterError(Globals.nfcSerialNumberErrorMessage);
		// }

		// else if (etNfcTagSerial.getText().length() < 16) {
		// alerter.alerterError(Globals.invalidNfcSerialNumberErrorMessage);
		// }

		else {
			confirm();
		}

	}

	public void confirm() {
		LayoutInflater li = LayoutInflater.from(SendClientInfo.this);
		View promptsView;
		promptsView = li
				.inflate(R.layout.confirm_dialog_send_client_info, null);

		TextView tvCustomerType = (TextView) promptsView
				.findViewById(R.id.tv_customer_type);

		tvCustomerType.setText(customerType);

		// TextView tvIsActive = (TextView) promptsView
		// .findViewById(R.id.tv_is_active);
		// tvIsActive.setText(isActiveValue);

		TextView tvCustomerWalletId = (TextView) promptsView
				.findViewById(R.id.tv_customer_wallet_id);
		tvCustomerWalletId.setText(customerWalletId);

		// TextView tvNfcTagSerial = (TextView) promptsView
		// .findViewById(R.id.tv_nfc_tag_serial);
		// tvNfcTagSerial.setText(etNfcTagSerial.getText().toString());

		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
				SendClientInfo.this);

		// set prompts.xml to alertdialog builder
		alertDialogBuilder.setView(promptsView);
		// set dialog message
		alertDialogBuilder
				.setCancelable(false)
				.setPositiveButton(Globals.yes,
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								new CheckIfCustomerExistAsyncTask().execute();
							}
						})
				.setNegativeButton(Globals.no,
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								dialog.cancel();
							}
						});

		// create alert dialog
		AlertDialog alertDialog = alertDialogBuilder.create();
		// show it
		alertDialog.show();
	}

	public String encryption(String strNormalText, String seedValue) {

		String normalTextEnc = "";
		try {
			normalTextEnc = AESHelper.encrypt(seedValue, strNormalText);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return normalTextEnc;
	}

	private boolean isNetworkAvailable() {
		ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo activeNetworkInfo = connectivityManager
				.getActiveNetworkInfo();
		return activeNetworkInfo != null && activeNetworkInfo.isConnected();
	}

	@Override
	protected void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
		// REMOVE DIALOG
		if (pd != null) {
			pd.dismiss();
			// b.setEnabled(true);
		}
	}

	public void customToast(String message) {
		LayoutInflater inflater = getLayoutInflater();
		View toastLayout = inflater.inflate(R.layout.custom_toast,
				(ViewGroup) findViewById(R.id.custom_toast_layout));
		TextView tvCustomToastMessage = (TextView) toastLayout
				.findViewById(R.id.tv_custom_toast_message);
		tvCustomToastMessage.setText(message);

		Toast toast = new Toast(getApplicationContext());
		toast.setDuration(Toast.LENGTH_LONG);
		toast.setView(toastLayout);
		toast.setGravity(Gravity.CENTER, 0, 0);
		toast.show();
	}

}