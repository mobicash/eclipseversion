package ez.pay.project;

import java.util.ArrayList;
import java.util.List;

import models.Area;
import services.BluetoothService;

import com.orm.SugarApp;

public class MyApplication extends SugarApp {

	private String clientId;
	private String agentPin;
	private String account;
	private String custPin;
	private String amount;
	BluetoothService mService = null;

	private String nfcValue;
	private String customerType;
	private String phoneNumber;
	private String nfcTagSerial;
	private String nfdTagUid;
	private Long dateTime;
	private String customerWalletId;

	private boolean isTrue;

	public boolean isTrue() {
		return isTrue;
	}

	public void setTrue(boolean isTrue) {
		this.isTrue = isTrue;
	}

	private List<Area> areaList = new ArrayList<Area>();

	public List<Area> getAreaList() {
		return areaList;
	}

	public void setAreaList(List<Area> areaList) {
		this.areaList = areaList;
	}

	public String getNfcValue() {
		return nfcValue;
	}

	public void setNfcValue(String nfcValue) {
		this.nfcValue = nfcValue;
	}

	public String getCustomerType() {
		return customerType;
	}

	public void setCustomerType(String customerType) {
		this.customerType = customerType;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getNfcTagSerial() {
		return nfcTagSerial;
	}

	public void setNfcTagSerial(String nfcTagSerial) {
		this.nfcTagSerial = nfcTagSerial;
	}

	public String getNfdTagUid() {
		return nfdTagUid;
	}

	public void setNfdTagUid(String nfdTagUid) {
		this.nfdTagUid = nfdTagUid;
	}

	public Long getDateTime() {
		return dateTime;
	}

	public void setDateTime(Long dateTime) {
		this.dateTime = dateTime;
	}

	public String getCustomerWalletId() {
		return customerWalletId;
	}

	public void setCustomerWalletId(String customerWalletId) {
		this.customerWalletId = customerWalletId;
	}

	public BluetoothService getmService() {
		return mService;
	}

	public void setmService(BluetoothService mService) {
		this.mService = mService;
	}

	public String getClientId() {
		return clientId;
	}

	public void setClientId(String clientId) {
		this.clientId = clientId;
	}

	public String getAgentPin() {
		return agentPin;
	}

	public void setAgentPin(String agentPin) {
		this.agentPin = agentPin;
	}

	public String getAccount() {
		return account;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	public String getCustPin() {
		return custPin;
	}

	public void setCustPin(String custPin) {
		this.custPin = custPin;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public void cleanNfcInfo() {

		if (customerType != null)
			customerType = null;

		if (nfcTagSerial != null)
			nfcTagSerial = null;

		if (dateTime != null)
			dateTime = null;

		if (customerWalletId != null)
			customerWalletId = null;
	}

}
