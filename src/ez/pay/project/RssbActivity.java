package ez.pay.project;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import models.Rssb;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.conn.ConnectTimeoutException;

import utils.ConnectUtils;
import utils.RssbParser;
import utils.Vars;
import adapters.SimpleSpinnerAdapter;
import android.app.ActionBar;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.Spinner;

public class RssbActivity extends BaseActivity {

	EditText etNationalId;
	Alerter alerter;
	// ConnectionDetector cd;
	Vars vars;
	ProgressDialog pd;
	SharedPreferences pref;
	Spinner spFinancialYear;
	List<String> financialYearList = new ArrayList<String>();
	int defaultValue = 0;
	String financialYear;
	String PLEASE_SELECT = "please select";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.rssb_activity);
		ActionBar bar = getActionBar();
		bar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#f47701")));
		pref = PreferenceManager.getDefaultSharedPreferences(this);
		initialize();
		// hide keyboard
		this.getWindow().setSoftInputMode(
				WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
	}

	private void initialize() {
		// TODO Auto-generated method stub
		etNationalId = (EditText) findViewById(R.id.et_national_id);
		alerter = new Alerter(this);
		// cd = new ConnectionDetector(this);
		vars = new Vars(this);
		spFinancialYear = (Spinner) findViewById(R.id.sp_financial_year);

		financialYearList.add(PLEASE_SELECT);
		// financialYearList.add("2016 / 2017");
		financialYearList.add("2017 / 2018");
		// financialYearList.add("2018 / 2019");

		SimpleSpinnerAdapter itemAdapter = new SimpleSpinnerAdapter(
				RssbActivity.this, R.layout.simple_spinner_row,
				financialYearList, defaultValue);
		spFinancialYear.setAdapter(itemAdapter);

		spFinancialYear
				.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
					@Override
					public void onItemSelected(AdapterView<?> parentView,
							View v, int position, long id) {

						financialYear = financialYearList.get(position);

					}

					@Override
					public void onNothingSelected(AdapterView<?> parentView) {
						// your code here
					}

				});

	}

	public void check(View v) {

		// hidding the keybord
		InputMethodManager inputManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
		inputManager.hideSoftInputFromWindow(
				getCurrentFocus().getWindowToken(),
				InputMethodManager.HIDE_NOT_ALWAYS);

		String nationalId = etNationalId.getText().toString();

		// if (cd.isConnectingToInternet()) {
		if (nationalId.length() <= 0) {
			alerter.alerterError(Globals.nationalIDErrorMessage);
		} else if (financialYear.equals(PLEASE_SELECT)) {
			alerter.alerterError(Globals.financialYearErrorMessage);
		} else {
			new LoadInfo().execute(nationalId);
		}

	}

	private class LoadInfo extends AsyncTask<String, Void, String> {
		String nationalId;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			pd = new ProgressDialog(RssbActivity.this);
			pd.setTitle(Globals.refIdCheckProgressMessage);
			pd.setMessage(Globals.waitProgressMessage);
			pd.setCancelable(false);
			pd.setIndeterminate(true);
			pd.show();
		}

		@Override
		protected String doInBackground(String... params) {

			// Create an HTTP client
			nationalId = params[0];

			StringBuilder builder = new StringBuilder();
			ConnectUtils connector = new ConnectUtils();
			HttpClient client = connector.getNewHttpClient();

			String result = "FAILED";
			String urlString = null;

			// LOAD SERVER PREF

			try {
				urlString = vars.server
						+ "/bio-api/androidRssb/rssbGetDetails.php?householdNID="
						+ URLEncoder.encode(nationalId, "UTF-8")
						+ "&dealer="
						+ URLEncoder.encode(
								pref.getString(Globals.permission, null),
								"UTF-8") + "&version="
						+ URLEncoder.encode(Globals.version, "UTF-8")

						+ "&dealer_name="
						+ URLEncoder.encode(
								vars.prefs.getString(Globals.DEALERNAME, null),
								"UTF-8")

				;

			} catch (UnsupportedEncodingException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

			HttpGet httpGet = new HttpGet(urlString);

			try {
				HttpResponse response = client.execute(httpGet);
				StatusLine statusLine = response.getStatusLine();
				int statusCode = statusLine.getStatusCode();
				if (statusCode == 200) {
					HttpEntity entity = response.getEntity();
					InputStream content = entity.getContent();
					BufferedReader reader = new BufferedReader(
							new InputStreamReader(content));
					String line;
					while ((line = reader.readLine()) != null) {
						builder.append(line);
					}
					result = builder.toString();

				} else {

				}
			} catch (ConnectTimeoutException e) {

				e.printStackTrace();
			} catch (ClientProtocolException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}

			return result;
		}

		@Override
		protected void onPostExecute(String json) {

			try {

				RssbParser rssbParser = new RssbParser();

				Rssb rssb = rssbParser.getRssb(json);

				if (rssb.getResult().equals("success")) {
					// displayed info
					Intent intent = new Intent(RssbActivity.this,
							RssbPayActivity.class);

					intent.putExtra("Errorcode", rssb.getErrorCode());
					intent.putExtra("reply", rssb.getReply());
					intent.putExtra("name", rssb.getName());
					intent.putExtra("householdNID", rssb.getHouseholdNID());
					intent.putExtra("totalPremium", rssb.getTotalPremium());
					intent.putExtra("alreadyPaid", rssb.getAlreadyPaid());
					intent.putExtra("eligibility", rssb.getEligibility());
					intent.putExtra("houseHoldCategory",
							rssb.getHouseHoldCategory());
					intent.putExtra("numberOfMembers",
							rssb.getNumberOfMembers());
					intent.putExtra("invoice", rssb.getInvoice());
					intent.putExtra("financialYear", financialYear);

					intent.putExtra("province", rssb.getProvince());
					intent.putExtra("district", rssb.getDistrict());
					startActivity(intent);

				} else {
					alerter.alerterError(rssb.getMessage());
				}

				pd.dismiss();

			} catch (Exception e) {

				if (pd != null)
					pd.dismiss();

			}

		}
	}

	public void layoutClick(View v) {
		InputMethodManager inputManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
		inputManager.hideSoftInputFromWindow(
				getCurrentFocus().getWindowToken(),
				InputMethodManager.HIDE_NOT_ALWAYS);
	}

	// CANCEL
	public void cancel(View v) {
		alerter.alerterConfirmCancelEtax(Globals.cancelRssbPaymentMessage);
	}

	@Override
	protected void onStop() {
		super.onStop();
		if (pd != null)
			pd.dismiss();
	}

}
