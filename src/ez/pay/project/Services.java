package ez.pay.project;

import java.util.ArrayList;

import utils.Item;
import utils.Transaction;
import utils.Vars;
import adapters.GridViewAdapter;
import android.app.ActionBar;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.Html;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class Services extends BaseActivity implements OnItemClickListener {

	// grid
	ListView gridView;
	ArrayList<Item> gridArray = new ArrayList<Item>();
	GridViewAdapter customGridAdapter;
	String server;
	SharedPreferences prefs;
	TextView tvAnnimated;
	RelativeLayout rlAnnouncement;
	String announcement;
	Transaction trans;
	Vars vars;
	String description;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.services);

		// action bar styling
		ActionBar bar = getActionBar();
		bar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#f47701")));
		prefs = PreferenceManager.getDefaultSharedPreferences(this);
		server = prefs.getString("server", null);
		vars = new Vars(this);

		tvAnnimated = (TextView) findViewById(R.id.tv_annimated);
		tvAnnimated.setSelected(true);

		rlAnnouncement = (RelativeLayout) findViewById(R.id.rl_announcement);

		gridArray.add(new Item(BitmapFactory.decodeResource(
				this.getResources(), R.drawable.airtime), "Airtime / Inite"));
		gridArray.add(new Item(BitmapFactory.decodeResource(
				this.getResources(), R.drawable.electricity),
				"Electricity / Umuriro"));
		gridArray.add(new Item(BitmapFactory.decodeResource(
				this.getResources(), R.drawable.dstv1), "DSTV"));
		gridArray.add(new Item(BitmapFactory.decodeResource(
				this.getResources(), R.drawable.canal), "Canal"));
		gridArray.add(new Item(BitmapFactory.decodeResource(
				this.getResources(), R.drawable.startimes), "StarTimes"));

		gridArray.add(new Item(BitmapFactory.decodeResource(
				this.getResources(), R.drawable.rra), "RRA"));

		gridArray.add(new Item(BitmapFactory.decodeResource(
				this.getResources(), R.drawable.rssb),
				"RSSB: Pension / Medical"));

		gridArray.add(new Item(BitmapFactory.decodeResource(
				this.getResources(), R.drawable.irembo), "Irembo"));

		gridArray.add(new Item(BitmapFactory.decodeResource(
				this.getResources(), R.drawable.rssb), "Mutuelles de Sante"));

		gridArray.add(new Item(BitmapFactory.decodeResource(
				this.getResources(), R.drawable.pay_school_fees),
				"Pay school fees"));

		// gridArray.add(new Item(BitmapFactory.decodeResource(
		// this.getResources(), R.drawable.contribution_motor),
		// "Contribution moto"));

		gridArray.add(new Item(BitmapFactory.decodeResource(
				this.getResources(), R.drawable.commission), "Commission"));

		gridArray.add(new Item(BitmapFactory.decodeResource(
				this.getResources(), R.drawable.logs), "Historic / Raporo"));

		gridArray.add(new Item(BitmapFactory.decodeResource(
				this.getResources(), R.drawable.balance),
				" Balance / Amafaranga"));

		gridView = (ListView) findViewById(android.R.id.list);
		customGridAdapter = new GridViewAdapter(this, R.layout.row_services,
				gridArray);
		gridView.setAdapter(customGridAdapter);

		gridView.setOnItemClickListener(this);

		// new AnnouncementAsyncTask().execute();

		if (prefs.getString("description", null) != null) {
			tvAnnimated.setText(Html.fromHtml(prefs.getString("announcement",
					null)));
		}

		// if (prefs.getString("version", null) != null) {
		//
		// if (prefs.getString("version", null).equals(Globals.NOTUPDATED)) {
		//
		// alerterUpdate(getString(R.string.out_dated_error_message));
		// }
		//
		// }
		
		
		if (prefs.getString("version", null) != null) {

			if (!prefs.getString("version", null).equals(Globals.version)) {
				alerterUpdate(getString(R.string.out_dated_error_message));
			}

		}

	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		// TODO Auto-generated method stub

		// Toast.makeText(getApplicationContext(),
		// "Click ListItem Number " + position, Toast.LENGTH_LONG).show();

		switch (position) {
		case 0:

			Intent intent0 = new Intent(Services.this, Airtime.class);
			startActivity(intent0);

			break;

		case 1:
			Intent intent1 = new Intent(Services.this, Electricity.class);
			startActivity(intent1);
			break;

		case 2:
			Intent intent2 = new Intent(this, Dstv.class);
			startActivity(intent2);
			break;

		case 3:
			Intent intent3 = new Intent(this, Canal.class);
			startActivity(intent3);
			break;

		case 4:
			Intent intent4 = new Intent(this, Startimes.class);
			startActivity(intent4);
			break;

		case 5:
			Intent intent5 = new Intent(this, RRA.class);
			intent5.putExtra("action", "RRA");
			startActivity(intent5);
			break;

		case 6:
			// Pension
			Intent intent6 = new Intent(this, RRA.class);
			intent6.putExtra("action", "Pension");
			startActivity(intent6);
			break;

		case 7:
			Intent intent7 = new Intent(this, IremboActivity.class);
			startActivity(intent7);
			break;

		case 8:
			// Pay School fees
			Intent intent8 = new Intent(this, RssbActivity.class);
			startActivity(intent8);
			break;

		case 9:
			// Pay School fees
			Intent intent9 = new Intent(this, SchoolActivity.class);
			startActivity(intent9);
			break;

		case 10:
			// commission
			Intent intent10 = new Intent(this, AgentCheckPin.class);
			intent10.putExtra("goTo", "Commission / Komisiyo");
			startActivity(intent10);
			break;
		case 11:
			// historic
			Intent intent11 = new Intent(this, AgentCheckPin.class);
			intent11.putExtra("goTo", "Historic");
			startActivity(intent11);
			break;

		case 12:
			// check agent float
			Intent intent12 = new Intent(Services.this, AgentFloatCheck.class);
			startActivity(intent12);
			break;

		}

	}

	@Override
	public void onBackPressed() {
		Intent intent = new Intent(Intent.ACTION_MAIN);
		intent.addCategory(Intent.CATEGORY_HOME);
		intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		startActivity(intent);
	}

	public void announcementClick(View v) {
		if (prefs.getString("description", null) != null) {
			Intent intent = new Intent(getApplicationContext(),
					Announcement.class);
			intent.putExtra("ANN", prefs.getString("description", null));
			startActivity(intent);
		}

	}

}
