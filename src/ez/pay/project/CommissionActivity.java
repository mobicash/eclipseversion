package ez.pay.project;

//import android.support.v4.app.Fragment;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import models.AgentLog;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.conn.ConnectTimeoutException;

import utils.AgentLogParser;
import utils.ConnectUtils;
import adapters.AgentLogsAdapter;
import android.app.ActionBar;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

public class CommissionActivity extends BaseActivity implements
		OnItemClickListener {
	SharedPreferences preferences;
	String server;
	List<AgentLog> agentLogList = new ArrayList<AgentLog>();
	ProgressBar progressAgentLog;
	String agentPin;
	SharedPreferences pref;
	String goTo, listName, buttonClick, date, fromDate, toDate;
	TextView tvEmptyList;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.commission_activity);

		// action bar styling
		ActionBar bar = getActionBar();
		bar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#f47701")));
		preferences = PreferenceManager.getDefaultSharedPreferences(this);

		Intent intent = getIntent();
		agentPin = intent.getStringExtra("agentPin");
		goTo = intent.getStringExtra("goTo");
		listName = intent.getStringExtra("listName");

		server = preferences.getString("server", null);
		progressAgentLog = (ProgressBar) findViewById(R.id.progress_agent_log);
		pref = PreferenceManager.getDefaultSharedPreferences(this);
		tvEmptyList = (TextView) findViewById(R.id.tvEmptyList);

		if (listName.contains("Commission")) {

			this.setTitle("Commission");
			new ClientLogsAsyncTask().execute(agentPin);

		} else {

			buttonClick = intent.getStringExtra("buttonClick");

			if (buttonClick.contains("singleClick")) {
				date = intent.getStringExtra("date");
			} else {

				fromDate = intent.getStringExtra("fromDate");
				toDate = intent.getStringExtra("toDate");

			}
			this.setTitle("Historic");
			new ClientLogsAsyncTask().execute(agentPin);
		}

	}

	private class ClientLogsAsyncTask extends AsyncTask<String, Void, String> {
		String json = "NO CONNECTION MADE";

		@Override
		protected void onPreExecute() {
			super.onPreExecute();

		}

		@Override
		protected String doInBackground(String... params) {

			// Create an HTTP client
			StringBuilder builder = new StringBuilder();
			ConnectUtils connector = new ConnectUtils();
			HttpClient client = connector.getNewHttpClient();

			String result = "FAILED";
			String stringUrl = null;
			String agentPin = params[0];

			// date

			// LOAD SERVER PREF

			try {

				if (listName.contains("Commission")) {
					stringUrl = server
							+ "/bio-api/androidCheckFloat/commissionLogs.php?agentNumber="
							+ URLEncoder.encode(
									preferences.getString("agentPhone", null),
									"UTF-8") + "&agentPin=" + agentPin

							+ "&version="
							+ URLEncoder.encode(Globals.version, "UTF-8");

				} else {

					if (buttonClick.contains("singleClick")) {

						// test

						stringUrl = server
								+ "/bio-api/androidCheckFloat/DaytransactionLogs.php?agentNumber="
								+ URLEncoder.encode(preferences.getString(
										"agentPhone", null), "UTF-8")
								+ "&agentPin=" + agentPin + "&Date=" + date
								+ "&version="
								+ URLEncoder.encode(Globals.version, "UTF-8");



						// Live
						// stringUrl = server
						// +
						// "/bio-api/androidCheckFloat/dayTransactionLogs.php?agentNumber="
						// + URLEncoder.encode(preferences.getString(
						// "agentPhone", null), "UTF-8")
						// + "&agentPin=" + agentPin + "&Date=" + date;

					} else {

						stringUrl = server
								+ "/bio-api/androidCheckFloat/transactionLogs.php?agentNumber="
								+ URLEncoder.encode(preferences.getString(
										"agentPhone", null), "UTF-8")
								+ "&agentPin=" + agentPin + "&beginDate="
								+ fromDate + "&endDate=" + toDate + "&version="
								+ URLEncoder.encode(Globals.version, "UTF-8");
						// Log.e("url", stringUrl);

					}

				}

			} catch (UnsupportedEncodingException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

			HttpGet httpGet = new HttpGet(stringUrl);

			try {
				HttpResponse response = client.execute(httpGet);
				StatusLine statusLine = response.getStatusLine();
				int statusCode = statusLine.getStatusCode();
				if (statusCode == 200) {
					HttpEntity entity = response.getEntity();
					InputStream content = entity.getContent();
					BufferedReader reader = new BufferedReader(
							new InputStreamReader(content));
					String line;
					while ((line = reader.readLine()) != null) {
						builder.append(line);
					}
					result = builder.toString();

				} else {

				}
			} catch (ConnectTimeoutException e) {

				e.printStackTrace();
			} catch (ClientProtocolException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}

			// return null;
			return result;
		}

		@Override
		protected void onPostExecute(String json) {

			try {
				AgentLogParser agentLogParser = new AgentLogParser();
				agentLogList = agentLogParser.getAgentLogsList(json);

				ListView listOfAgentLogs = (ListView) findViewById(android.R.id.list);

				if (listOfAgentLogs != null) {

					AgentLogsAdapter adapter = new AgentLogsAdapter(
							CommissionActivity.this, R.layout.single_agent_log,
							agentLogList);

					listOfAgentLogs.setAdapter(adapter);
					setListViewHeightBasedOnChildren(listOfAgentLogs);
					listOfAgentLogs
							.setOnItemClickListener(CommissionActivity.this);

				}

				progressAgentLog.setVisibility(View.GONE);

			} catch (Exception e) {
				tvEmptyList.setVisibility(View.VISIBLE);
				progressAgentLog.setVisibility(View.GONE);
			}

		}

	}

	public static void setListViewHeightBasedOnChildren(ListView listView) {
		// 获�?�ListView对应的Adapter
		ListAdapter listAdapter = listView.getAdapter();
		if (listAdapter == null) {
			return;
		}

		int totalHeight = 40;
		for (int i = 0, len = listAdapter.getCount(); i < len; i++) { // listAdapter.getCount()返回数�?�项的数目
			View listItem = listAdapter.getView(i, null, listView);
			listItem.measure(0, 0); // 计算�?项View 的宽高
			totalHeight += listItem.getMeasuredHeight(); // 统计所有�?项的总高度
		}

		ViewGroup.LayoutParams params = listView.getLayoutParams();
		params.height = totalHeight
				+ (listView.getDividerHeight() * (listAdapter.getCount() - 1));
		// listView.getDividerHeight()获�?��?项间分隔符�?�用的高度
		// params.height最�?�得到整个ListView完整显示需�?的高度
		listView.setLayoutParams(params);
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		// TODO Auto-generated method stub

		AgentLog agentLog = agentLogList.get(position);

		Intent intent = new Intent(CommissionActivity.this,
				SingleAgentLog.class);
		intent.putExtra(Globals.agentLogTransactionID, agentLog.getTransferId());
		intent.putExtra(Globals.agentLogMember, agentLog.getName());
		intent.putExtra(Globals.agentLogtransactionType,
				agentLog.getTransferName());
		intent.putExtra(Globals.agentLogDescription, agentLog.getDescription());
		intent.putExtra(Globals.agentLogamount, agentLog.getAmount());
		intent.putExtra(Globals.agentLogStatus, agentLog.getStatus());
		intent.putExtra(Globals.agentLogDate, agentLog.getDate());
		intent.putExtra("listName", listName);
		startActivity(intent);

	}
}
