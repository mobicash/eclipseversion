package ez.pay.project;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;

import services.BluetoothService;
import utils.ConnectUtils;
import utils.PayElectricityParser;
import utils.Vars;
import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class BuyElectricity extends BaseActivity {

	public Vars vars;
	Alerter alerter;
	PayElectricityInfo payElectricityInfo;
	PayElectricityParser payElectricityParser;

	// UI VARS
	EditText agentPin, customerPhone, electricityAmount;
	TextView tName, tMeterNumber;
	protected String imei;

	Button bContinue;

	String name;
	String meterNumber;
	String amount;

	SharedPreferences pref;
	ProgressDialog pd;

	String dealer;
	CheckBox cbPrint;

	// Connection detector class
	// ConnectionDetector cd;

	// bluetooth

	private static final String TAG = "BluetoothPrint";
	private static final boolean D = true;

	// ��BluetoothService�����������Ϣ����
	public static final int MESSAGE_STATE_CHANGE = 1;
	public static final int MESSAGE_READ = 2;
	public static final int MESSAGE_WRITE = 3;
	public static final int MESSAGE_DEVICE_NAME = 4;
	public static final int MESSAGE_TOAST = 5;

	// BluetoothService�յ��Ĵ������ؼ�������
	public static final String DEVICE_NAME = "device_name";
	public static final String TOAST = "toast";

	// ����Ĵ���
	private static final int REQUEST_CONNECT_DEVICE = 1;
	private static final int REQUEST_ENABLE_BT = 2;
	private static final int REQUEST_Set = 3;

	// ���ӵ��豸������
	private String mConnectedDeviceName = null;
	// �ַ�����������������Ϣ
	private StringBuffer mOutStringBuffer;
	// ��������������
	private BluetoothAdapter mBluetoothAdapter = null;
	public static BluetoothService mService = null;

	private TextView mTitle;
	private EditText mOutEditText;
	private Button mPrintButton;
	private Button mClearButton;
	private Button mSetButton;
	MyApplication myapplication;
	TextView tvPrinterMessage;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.buy_electricity);
		ActionBar bar = getActionBar();
		bar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#f47701")));

		vars = new Vars(this);
		alerter = new Alerter(this);
		pref = PreferenceManager.getDefaultSharedPreferences(this);
		myapplication = (MyApplication) getApplication();

		dealer = pref.getString(Globals.permission, null);

		Bundle extras;
		if (savedInstanceState == null) {
			extras = getIntent().getExtras();
			if (extras != null) {
				name = extras.getString(Globals.name);
				amount = extras.getString(Globals.amount);
				meterNumber = extras.getString(Globals.meterNumber);
			}
		}

		init();

		// hide keyboard
		this.getWindow().setSoftInputMode(
				WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

		// �õ����ص�������
		mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

		if (mBluetoothAdapter == null) {
			Toast.makeText(this, "��������ʹ�ã�", Toast.LENGTH_LONG).show();
			finish();
			return;
		}

	}

	public void printClick(View v) {
		if (((CheckBox) v).isChecked()) {

			if (myapplication.getmService().getState() != BluetoothService.STATE_CONNECTED) {
				// Toast.makeText(this, R.string.not_connected,
				// Toast.LENGTH_SHORT).show();
				try {

					Intent serverIntent = new Intent(this,
							DeviceListActivity.class);
					startActivityForResult(serverIntent, REQUEST_CONNECT_DEVICE);

					return;
				} catch (Exception e) {

				}
			}

		}
	}

	@Override
	public void onStart() {
		super.onStart();

		// �ж������Ƿ������������û�п���������������
		if (!mBluetoothAdapter.isEnabled()) {

			Intent enableIntent = new Intent(
					BluetoothAdapter.ACTION_REQUEST_ENABLE);
			startActivityForResult(enableIntent, REQUEST_ENABLE_BT);
		} else {
			if (myapplication.getmService() == null) {

				setupChat();
			}

		}
	}

	@Override
	public synchronized void onResume() {
		super.onResume();

		if (myapplication.getmService() != null) {
			if (myapplication.getmService().getState() == BluetoothService.STATE_NONE) {

				myapplication.getmService().start();
			}

		}

		if (myapplication.getmService().getState() == BluetoothService.STATE_CONNECTED) {
			// ivPrinterIcon.setBackgroundResource(R.drawable.log);
			tvPrinterMessage.setText(getString(R.string.connected_txt));
			tvPrinterMessage.setTextColor(Color.parseColor("#398f14"));

		} else {
			// ivPrinterIcon.setBackgroundResource(R.drawable.not_log);
			tvPrinterMessage.setText(getString(R.string.not_connected_txt));
			tvPrinterMessage.setTextColor(Color.parseColor("#e34949"));

		}

	}

	private void setupChat() {
		Log.d(TAG, "SetupChat");

		// ��ʼ��BluetoothServiceִ����������
		myapplication.setmService(new BluetoothService(this, mHandler));

	}

	private void sendMessage(String message) {
		// ������ڳ�������

		// ���ʵ��Ҫ���͵Ķ���
		if (message.length() > 0) {
			// �õ���Ϣ�ֽڲ��Ҹ���BluetoothService
			byte[] send;
			byte[] format = { 27, 33, 0 };

			// InputStream ins = getResources().openRawResource(
			// getResources().getIdentifier("logo_transpa",
			// "drawable", getPackageName()));
			//
			//
			//
			// byte[] printbyte = {(byte)0x1B,(byte)0x2A,(byte)0x6F,(byte)0x63}
			//
			// File file = new File(Environment.getExternalStorageDirectory +
			// File.seprator()+"file_name");
			// FileInputStream fin = new FileInputStream(file);
			// byte imageContent[] = new byte[(int)file.length()];
			// ins.read(imageContent);
			//
			// byte [] width = hexToBuffer(Integer.toHexString(your_width));
			// byte [] height = hexToBuffer(Integer.toHexString(your_height));
			//
			// byte[] imageToPrint = new
			// byte[printbyte.length+imageContent.length+width.length+height.length];
			//
			//

			try {
				send = message.getBytes("GB2312");
				format[2] = ((byte) (0x1 | send[0]));
			} catch (UnsupportedEncodingException e) {
				send = message.getBytes();
			}

			myapplication.getmService().write(send, 0, send.length);
		}
	}

	public static byte[] hexToBuffer(String hexString)
			throws NumberFormatException {
		int length = hexString.length();
		byte[] buffer = new byte[(length + 1) / 2];
		boolean evenByte = true;
		byte nextByte = 0;
		int bufferOffset = 0;

		if ((length % 2) == 1) {
			evenByte = false;
		}

		for (int i = 0; i < length; i++) {
			char c = hexString.charAt(i);
			int nibble; // A "nibble" is 4 bits: a decimal 0..15

			if ((c >= '0') && (c <= '9')) {
				nibble = c - '0';
			} else if ((c >= 'A') && (c <= 'F')) {
				nibble = c - 'A' + 0x0A;
			} else if ((c >= 'a') && (c <= 'f')) {
				nibble = c - 'a' + 0x0A;
			} else {
				throw new NumberFormatException("Invalid hex digit '" + c
						+ "'.");
			}

			if (evenByte) {
				nextByte = (byte) (nibble << 4);
			} else {
				nextByte += (byte) nibble;
				buffer[bufferOffset++] = nextByte;
			}

			evenByte = !evenByte;
		}

		return buffer;
	}

	@Override
	public synchronized void onPause() {
		super.onPause();

	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		// Stop the Bluetooth services
		if (myapplication.getmService() != null)
			myapplication.getmService().stop();

	}

	// ���±������ұ�״̬�Ͷ�д״̬��Handler
	private final Handler mHandler = new Handler() {

		@Override
		public void handleMessage(Message msg) {
			switch (msg.what) {
			case MESSAGE_STATE_CHANGE:
				if (D)
					Log.i(TAG, "MESSAGE_STATE_CHANGE:" + msg.arg1);
				switch (msg.arg1) {
				case BluetoothService.STATE_CONNECTED:
					// mTitle.setText(R.string.title_connected_to);
					// mTitle.append(mConnectedDeviceName);
					// ivPrinterIcon.setBackgroundResource(R.drawable.log);

					tvPrinterMessage.setText(getString(R.string.connected_txt));
					tvPrinterMessage.setTextColor(Color.parseColor("#398f14"));

					break;
				case BluetoothService.STATE_CONNECTING:
					// mTitle.setText(R.string.title_connecting);
					tvPrinterMessage
							.setText(getString(R.string.connecting_txt));
					tvPrinterMessage.setTextColor(Color.parseColor("#000000"));

					break;
				case BluetoothService.STATE_LISTEN:
				case BluetoothService.STATE_NONE:
					// mTitle.setText(R.string.title_not_connected);
					// ivPrinterIcon.setBackgroundResource(R.drawable.not_log);
					tvPrinterMessage
							.setText(getString(R.string.not_connected_txt));
					tvPrinterMessage.setTextColor(Color.parseColor("#e34949"));

					break;
				}
				break;
			case MESSAGE_WRITE:
				break;
			case MESSAGE_READ:
				// ����Ч�ֽڻ���������һ���ַ���
				break;
			case MESSAGE_DEVICE_NAME:
				// ���������豸������
				mConnectedDeviceName = msg.getData().getString(DEVICE_NAME);
				// Toast.makeText(getApplicationContext(),
				// "���ӵ�" + mConnectedDeviceName, Toast.LENGTH_SHORT)
				// .show();
				alerter.alerterSuccessSimpleHere(mConnectedDeviceName
						+ " connected / " + mConnectedDeviceName
						+ " iracometse neza ");

				break;
			case MESSAGE_TOAST:
				// Toast.makeText(getApplicationContext(),
				// msg.getData().getString(TOAST), Toast.LENGTH_SHORT)
				// .show();

				alerter.alerterError("Error retry / ikibazo ongera ugerageze ");

				break;

			}

		}
	};

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (D)
			Log.d(TAG, "onActivityResult" + resultCode);
		switch (requestCode) {
		case REQUEST_CONNECT_DEVICE:
			// ��DeviceListActivity����һ���豸����
			if (resultCode == Activity.RESULT_OK) {

				// ��ȡ�豸��MAC��ַ
				String address = data.getExtras().getString(
						DeviceListActivity.EXTRA_DEVICE_ADDRESS);
				// �õ�BLuetoothDevice����
				if (address == null) {
					break;
				}
				// ��ȡ�豸��������
				BluetoothDevice device = mBluetoothAdapter
						.getRemoteDevice(address);
				// �������ӵ��豸
				myapplication.getmService().connect(device);
			}
			break;
		case REQUEST_ENABLE_BT:
			// �����󼤻������Ļر�
			if (resultCode == Activity.RESULT_OK) {
				// �������������õ�,���,����һ������Ự
				// ���������ɹ����������ʼ��UI
				setupChat();

			} else {
				// �û�û���������������
				Log.d(TAG, "BT not enabled");
				// Toast.makeText(this, R.string.bt_not_enabled_leaving,
				// Toast.LENGTH_SHORT).show();
				alerter.alerterError(getString(R.string.bt_not_enabled_leaving));
				finish();
			}

			/*
			 * case REQUEST_Set:
			 * 
			 * if(resultCode==Activity.RESULT_OK) {
			 * 
			 * byte a[]=data.getExtras().getByteArray("data");
			 * mService.write(a,0,a.length); Toast.makeText(this, "���óɹ���",
			 * Toast.LENGTH_SHORT).show(); } break;
			 */
		}
	}

	@Override
	public void onBackPressed() {
		android.os.Process.killProcess(android.os.Process.myPid());
	}

	private void init() {
		// TODO Auto-generated method stub
		// SET UI VARS
		agentPin = (EditText) findViewById(R.id.etAgentPin);
		customerPhone = (EditText) findViewById(R.id.etCustomerPhone);
		electricityAmount = (EditText) findViewById(R.id.etElectricityAmount);
		tName = (TextView) findViewById(R.id.tvName);
		cbPrint = (CheckBox) findViewById(R.id.cb_print);
		tMeterNumber = (TextView) findViewById(R.id.tvMeterNumber);
		tvPrinterMessage = (TextView) findViewById(R.id.tv_printer_message);
		tName.setText(name);
		tMeterNumber.setText(meterNumber);
		electricityAmount.setText(amount);
	}

	// BUY
	public void buyClick(View v) {
		// if (cd.isConnectingToInternet()) {

		if (electricityAmount.getText().toString() == null
				|| electricityAmount.getText().toString().isEmpty()) {
			alerter.alerterError(Globals.enterAmountErrorMessage);
		} else if (electricityAmount.getText().toString().length() < 1) {
			alerter.alerterError(Globals.amountLessErrorMessage);
		} else if (customerPhone.getText().toString() == null
				|| customerPhone.getText().toString().isEmpty()) {
			alerter.alerterError(Globals.phoneCheckErrorMessage);
		} else if (customerPhone.getText().toString().length() != 10) {
			alerter.alerterError(Globals.invalidNumberErrorMessage);
		} else if (agentPin.getText().toString() == null
				|| agentPin.getText().toString().isEmpty()) {
			alerter.alerterError(Globals.enterAgentPinErrorMessage);
		} else if (agentPin.getText().toString().length() < 4) {
			alerter.alerterError(Globals.invalidPinErrorMessage);
		} else {
			confirm();
		}
		// } else {
		// alerter.alerterError(Globals.noInternetDetectedErrorMessage);
		// }
	}

	// CANCEL
	public void cancel(View v) {
		alerter.alerterConfirmCancel(Globals.cancelElectricityErrorMessage);
	}

	public void layoutClick(View v) {
		InputMethodManager inputManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
		inputManager.hideSoftInputFromWindow(
				getCurrentFocus().getWindowToken(),
				InputMethodManager.HIDE_NOT_ALWAYS);
	}

	public void confirm() {
		LayoutInflater li = LayoutInflater.from(BuyElectricity.this);
		View promptsView;
		promptsView = li.inflate(R.layout.confirm_dialog_electricity, null);

		TextView tvName = (TextView) promptsView
				.findViewById(R.id.confirm_dialog_name);
		TextView tvMeterNo = (TextView) promptsView
				.findViewById(R.id.confirm_dialog_meter_no);

		TextView tvPhone = (TextView) promptsView
				.findViewById(R.id.confirm_dialog_phone);
		TextView tvAmount = (TextView) promptsView
				.findViewById(R.id.confirm_dialog_amount);

		tvName.setText(name);
		tvPhone.setText(customerPhone.getText().toString());
		tvMeterNo.setText(meterNumber);
		tvAmount.setText(electricityAmount.getText().toString());

		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
				BuyElectricity.this);

		// set prompts.xml to alertdialog builder
		alertDialogBuilder.setView(promptsView);
		// set dialog message
		alertDialogBuilder
				.setCancelable(false)
				.setPositiveButton(Globals.yes,
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {

								// Toast.makeText(
								// Airtime.this,
								// "Phone :"
								// + phoneNumber.getText()
								// .toString() + " amount :"
								// + amount.getText().toString()
								// + " agent pin :"
								// +
								// agentPin.getText().toString()+" , agent number :"+vars.prefs
								// .getString("agentPhone", null),
								// Toast.LENGTH_SHORT).show();

								new BuyElectricityAsyncTask().execute(
										meterNumber, customerPhone.getText()
												.toString(), electricityAmount
												.getText().toString(), agentPin
												.getText().toString(), name);
							}
						})
				.setNegativeButton(Globals.no,
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								dialog.cancel();
							}
						});

		// create alert dialog
		AlertDialog alertDialog = alertDialogBuilder.create();
		// show it
		alertDialog.show();
	}

	// -----------------------------------------------------------------------------------------------------
	public class BuyElectricityAsyncTask extends
			AsyncTask<String, Void, String> {

		public String theResult;
		public String action;

		String meterNo;
		String phoneNumber;
		String amount;
		String agentPin;
		String name;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			pd = new ProgressDialog(BuyElectricity.this);
			pd.setTitle(Globals.electricityPurchaceProgressMessage);
			pd.setMessage(Globals.waitProgressMessage);
			pd.setCancelable(false);
			pd.setIndeterminate(true);
			pd.show();
		}

		@Override
		// protected String doInBackground(Void... arg0) {
		protected String doInBackground(String... params) {

			meterNo = params[0];
			phoneNumber = params[1];
			amount = params[2];
			agentPin = params[3];
			name = params[4];

			StringBuilder builder = new StringBuilder();
			ConnectUtils connector = new ConnectUtils();
			HttpClient client = connector.getNewHttpClient();

			String result = null;
			String sendString = null;

//			 sendString = vars.server
//					+ "/bio-api/androidElectricity/payElectricity.php?agentNumber="
//					+ vars.prefs.getString("agentPhone", null) + "&agentPin="
//					+ agentPin + "&phoneNumber=" + phoneNumber + "&amount="
//					+ amount + "&meter_no=" + meterNo + "&dealer=" + dealer
//					+ "&version=" + Globals.version + "&dealer_name="
//					+ vars.prefs.getString(Globals.DEALERNAME, null);
			
			
			
			
			
			
			
			try {

				// DEALER
				sendString = vars.server
						+ "/bio-api/androidElectricity/payElectricity.php"
						 +"agentNumber="+ URLEncoder.encode(vars.prefs.getString("agentPhone", null), "UTF-8")
						 +"&agentPin="+ URLEncoder.encode(agentPin, "UTF-8")
						 +"&phoneNumber="+ URLEncoder.encode(phoneNumber, "UTF-8")
						  +"&amount="+ URLEncoder.encode(amount, "UTF-8")
						   +"&meter_no="+ URLEncoder.encode(meterNo, "UTF-8")
						   +"&dealer="+ URLEncoder.encode(dealer, "UTF-8")
						   +"&version="+ URLEncoder.encode(Globals.version, "UTF-8")
						   +"&dealer_name="+ URLEncoder.encode(vars.prefs.getString(Globals.DEALERNAME, null), "UTF-8")
						   
						;
						
						
			}catch(Exception e){
			
			}

			HttpGet httpGet = new HttpGet(sendString);

			try {
				HttpResponse response = client.execute(httpGet);
				StatusLine statusLine = response.getStatusLine();
				int statusCode = statusLine.getStatusCode();
				if (statusCode == 200) {
					HttpEntity entity = response.getEntity();
					InputStream content = entity.getContent();
					BufferedReader reader = new BufferedReader(
							new InputStreamReader(content));
					String line;
					while ((line = reader.readLine()) != null) {
						builder.append(line);
					}
					result = builder.toString();

				} else {

				}
			} catch (Exception e) {
				result = null;
			}

			return result;

		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);

			try {

				// REMOVE DIALOG
				if (pd != null) {
					pd.dismiss();
					// b.setEnabled(true);
				}

				payElectricityParser = new PayElectricityParser();
				payElectricityInfo = payElectricityParser.showResults(result);

				// IF OUTPUT WAS RECEIVED FROM SERVER
				if (payElectricityInfo.getResult() != null
						&& !payElectricityInfo.getResult().isEmpty()) {
					// if (result.contains("ccess")) {
					if (payElectricityInfo.getResult().contains("SUCCESS")) {

						LayoutInflater li = LayoutInflater
								.from(getApplicationContext());
						View promptsView;
						promptsView = li.inflate(
								R.layout.dialog_success_electricity, null);

						TextView tName = (TextView) promptsView
								.findViewById(R.id.tvName);
						tName.setText(payElectricityInfo.getName());

						TextView tTokenNo = (TextView) promptsView
								.findViewById(R.id.tvTokenNo);
						tTokenNo.setText(payElectricityInfo.getTokenNo());

						TextView tUnits = (TextView) promptsView
								.findViewById(R.id.tvUnits);
						tUnits.setText(payElectricityInfo.getUnits());

						TextView tMeterNo = (TextView) promptsView
								.findViewById(R.id.tvMeterNo);
						tMeterNo.setText(payElectricityInfo.getMeterNumber());

						TextView tAmount = (TextView) promptsView
								.findViewById(R.id.tvAmount);
						tAmount.setText("RW "
								+ payElectricityInfo.getTendered());

						TextView tReceitNo = (TextView) promptsView
								.findViewById(R.id.tvReceitNo);
						tReceitNo.setText(payElectricityInfo.getReceitNo());

						if (cbPrint.isChecked()
								&& myapplication.getmService().getState() == BluetoothService.STATE_CONNECTED) {

							// String title = "Electricity";

							String mobicashText = "MobiCash LTD";
							String tinNumberText = "TIN No : 101858540";
							String mobicashPhoneText = "Tel: (250)787689185/SMS:8485";
							String bodifaHouseText = "Bodifa Mercy House,5th Floor";
							String KimihururaKigaliText = "Kimihurura,Kigali";
							String receiptNumberText = "Receipt Number : "
									+ payElectricityInfo.getReceitNo();
							String dateText = "Date : "
									+ payElectricityInfo.getDate();
							String agentNameTitle = "Agent name";
							String agentName = pref
									.getString("agentName", null);

							String nameTitle = getString(R.string.client_name);
							String names = payElectricityInfo.getName();

							getString(R.string.token_no_text);
							String token = payElectricityInfo.getTokenNo();

							int width = 30;

							int padSize = width - token.length();
							int padStart = token.length() + padSize / 2;

							token = String.format("%" + padStart + "s", token);
							token = String.format("%-" + width + "s", token);

							String unitsTitle = getString(R.string.units);
							String units = payElectricityInfo.getUnits();

							String meterNoTitle = getString(R.string.meter_no_text_label);
							String meterNo = payElectricityInfo
									.getMeterNumber();

							String balanceTitle = "Balance / Amafaranga";
							String balance = "RW "
									+ payElectricityInfo.getTendered();

							String vatTitle = "TVA @ 18%";
							String vatText = "RW "
									+ payElectricityInfo.getVat();

							String totalTTCTitle = "Total TTC";
							String totalTTCText = payElectricityInfo.getTotal();

							String ArrearsTitle = "Arrears Devis EARP";
							String ArrearsText = payElectricityInfo
									.getArrears();

							String receiptNOTitle = getString(R.string.receitNo);
							String receiptNO = payElectricityInfo.getReceitNo();

							String thankUMessage = "Thank you for using MCash";

							String keepEnjoyingMessage = "Keep Enjoying !";
							String dash = "##############################";
							String star = "****************************** ";
							String line = "------------------------------";

							// int width = 30;
							//
							// int padSize = width - token.length();
							// int padStart = token.length() + padSize / 2;

							// title = String.format("%" + padStart + "s",
							// title);
							// title = String.format("%-" + width + "s", title);
							//
							// token = String.format("%" + padStart + "s",
							// token);
							// token = String.format("%-" + width + "s", token);

							String message = mobicashText + "\n"
									+ tinNumberText + "\n" + mobicashPhoneText
									+ "\n" + bodifaHouseText + "\n"
									+ KimihururaKigaliText + "\n\n"
									+ receiptNumberText + "\n" + dateText
									+ "\n" + line + "\n\n" + agentNameTitle
									+ "\n" + agentName + "\n\n" + nameTitle
									+ "\n" + names + "\n\n" + meterNoTitle
									+ "\n" + meterNo + "\n\n"

									+ balanceTitle + "\n" + balance + "\n\n"
									+ unitsTitle + "\n" + units + "\n\n" + dash
									+ "\n" + token + "\n" + dash + "\n\n"
									+ vatTitle + "\n" + vatText + "\n\n"
									+ totalTTCTitle + "\n" + totalTTCText
									+ "\n\n" + ArrearsTitle + "\n"
									+ ArrearsText + "\n\n" + line + "\n"
									+ thankUMessage + "\n"
									+ keepEnjoyingMessage
									+ "\n\n\n\n\n\n\n\n\n\n";

							sendMessage(message);
						}

						AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
								BuyElectricity.this);

						// set prompts.xml to alertdENroialog builder
						alertDialogBuilder.setView(promptsView);
						// set dialog message
						alertDialogBuilder.setCancelable(false)
								.setPositiveButton(Globals.yes,
										new DialogInterface.OnClickListener() {
											public void onClick(
													DialogInterface dialog,
													int id) {

												Intent i = new Intent();
												i.setClass(
														getApplicationContext(),
														Services.class);
												startActivity(i);
											}
										});

						// create alert dialog
						AlertDialog alertDialog = alertDialogBuilder.create();
						// show it
						alertDialog.show();

					} else {
						// alerter.alerterError(trans.getResult());
						alerter.alerterError(payElectricityInfo.getError());
					}
				} else {
					alerter.alerterError(Globals.InternetErrorMessage);
				}

			} catch (Exception e) {
				alerter.alerterError(Globals.InternetErrorMessage);
			}

		}
	}

	@Override
	protected void onStop() {
		super.onStop();
		if (pd != null)
			pd.dismiss();
	}

}
