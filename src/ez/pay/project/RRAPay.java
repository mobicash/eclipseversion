package ez.pay.project;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.conn.ConnectTimeoutException;

import services.BluetoothService;
import utils.ConnectUtils;
import utils.Transaction;
import utils.Vars;
import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class RRAPay extends BaseActivity {

	TextView phoneNumberText, amountText, nameOfBank, rraRefId, tin,
			taxPayerName, taxDescription, amountToPay, declarationDate,
			etPhone;
	EditText pinEditText;
	Button pay;
	TextView result, taxCenterNo, taxTypeNo, assessNo, rraOriginNo, decId;
	Vars vars;
	Alerter alerter;
	// ConnectionDetector cd;
	Transaction trans;

	String fone;
	String clientSMS;
	ProgressDialog pd;
	SharedPreferences pref;
	CheckBox cbPrint;
	String agentName = "Mobicash";
	String rraRefIdNumber;
	String taxPayerNameText;
	String taxTypeDescription;
	String mobicashRefId;

	String amountPaid;

	// bluetooth

	private static final String TAG = "BluetoothPrint";
	private static final boolean D = true;

	// ��BluetoothService�����������Ϣ����
	public static final int MESSAGE_STATE_CHANGE = 1;
	public static final int MESSAGE_READ = 2;
	public static final int MESSAGE_WRITE = 3;
	public static final int MESSAGE_DEVICE_NAME = 4;
	public static final int MESSAGE_TOAST = 5;

	// BluetoothService�յ��Ĵ������ؼ�������
	public static final String DEVICE_NAME = "device_name";
	public static final String TOAST = "toast";

	// ����Ĵ���
	private static final int REQUEST_CONNECT_DEVICE = 1;
	private static final int REQUEST_ENABLE_BT = 2;
	private static final int REQUEST_Set = 3;

	// ���ӵ��豸������
	private String mConnectedDeviceName = null;
	// �ַ�����������������Ϣ
	private StringBuffer mOutStringBuffer;
	// ��������������
	private BluetoothAdapter mBluetoothAdapter = null;
	public static BluetoothService mService = null;

	private TextView mTitle;
	private EditText mOutEditText;
	private Button mPrintButton;
	private Button mClearButton;
	private Button mSetButton;
	MyApplication myapplication;
	TextView tvPrinterMessage;
	String tinText;
	String action;
	TextView tvTitle;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.rra_pay_layout);
		ActionBar bar = getActionBar();
		bar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#f47701")));
		myapplication = (MyApplication) getApplication();
		initialize();

		pref = PreferenceManager.getDefaultSharedPreferences(this);
		Intent iin = getIntent();
		Bundle b = iin.getExtras();

		if (b != null) {

			action = (String) b.get("action");

			if (action.equals("RRA")) {
				this.setTitle(getString(R.string.e_tax_menu_text));
				tvTitle.setText(getString(R.string.e_tax_menu_text));
			}

			if (action.equals("Pension")) {
				this.setTitle("RSSB: Pension / Medical");
				tvTitle.setText("RSSB: Pension / Medical");
			}

			Tax tax = new Tax();
			tax.setRraRefId((String) b.get(Globals.refId));
			rraRefIdNumber = (String) b.get(Globals.refId);
			tax.setResult((String) b.get(Globals.result));
			tax.setBankName((String) b.get(Globals.bankName));
			tax.setTin((String) b.get(Globals.tin));
			tinText = (String) b.get(Globals.tin);
			taxPayerNameText = (String) b.get(Globals.taxPayerName);
			tax.setTaxPayerName((String) b.get(Globals.taxPayerName));
			tax.setTaxCenterNo((String) b.get(Globals.taxCentreNo));
			tax.setTaxTypeNo((String) b.get(Globals.taxTypeNo));
			tax.setAssessNo((String) b.get(Globals.assessNo));
			tax.setRraOriginNo((String) b.get(Globals.rraOriginNo));
			amountPaid = (String) b.get(Globals.amountToPay);
			tax.setAmountToPay((String) b.get(Globals.amountToPay));
			tax.setDecId((String) b.get(Globals.decId));
			tax.setDecDate((String) b.get(Globals.decDate));
			taxTypeDescription = (String) b.get(Globals.taxTypeDesc);
			tax.setTaxTypeDescription((String) b.get(Globals.taxTypeDesc));
			setInfoText(tax);
			setOtherInfo(tax);
		}

		// hide keyboard
		this.getWindow().setSoftInputMode(
				WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

		// �õ����ص�������
		mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

		if (mBluetoothAdapter == null) {
			Toast.makeText(this, "��������ʹ�ã�", Toast.LENGTH_LONG).show();
			finish();
			return;
		}

	}

	public void printClick(View v) {
		if (((CheckBox) v).isChecked()) {

			if (myapplication.getmService().getState() != BluetoothService.STATE_CONNECTED) {
				// Toast.makeText(this, R.string.not_connected,
				// Toast.LENGTH_SHORT).show();
				try {

					Intent serverIntent = new Intent(this,
							DeviceListActivity.class);
					startActivityForResult(serverIntent, REQUEST_CONNECT_DEVICE);

					return;
				} catch (Exception e) {

				}
			}

		}
	}

	@Override
	public void onStart() {
		super.onStart();

		// �ж������Ƿ������������û�п���������������
		if (!mBluetoothAdapter.isEnabled()) {

			Intent enableIntent = new Intent(
					BluetoothAdapter.ACTION_REQUEST_ENABLE);
			startActivityForResult(enableIntent, REQUEST_ENABLE_BT);
		} else {
			if (myapplication.getmService() == null) {

				setupChat();
			}

		}
	}

	@Override
	public synchronized void onResume() {
		super.onResume();
		if (D)

			if (myapplication.getmService() != null) {
				if (myapplication.getmService().getState() == BluetoothService.STATE_NONE) {

					myapplication.getmService().start();
				}

			}

		if (myapplication.getmService().getState() == BluetoothService.STATE_CONNECTED) {
			// ivPrinterIcon.setBackgroundResource(R.drawable.log);
			tvPrinterMessage.setText(getString(R.string.connected_txt));
			tvPrinterMessage.setTextColor(Color.parseColor("#398f14"));

		} else {
			// ivPrinterIcon.setBackgroundResource(R.drawable.not_log);
			tvPrinterMessage.setText(getString(R.string.not_connected_txt));
			tvPrinterMessage.setTextColor(Color.parseColor("#e34949"));

		}

	}

	private void setupChat() {
		Log.d(TAG, "SetupChat");

		// ��ʼ��BluetoothServiceִ����������
		myapplication.setmService(new BluetoothService(this, mHandler));

	}

	private void sendMessage(String message) {
		// ������ڳ�������

		// ���ʵ��Ҫ���͵Ķ���
		if (message.length() > 0) {
			// �õ���Ϣ�ֽڲ��Ҹ���BluetoothService
			byte[] send;
			try {
				send = message.getBytes("GB2312");
			} catch (UnsupportedEncodingException e) {
				send = message.getBytes();
			}
			// mService.write(send,10);
			myapplication.getmService().write(send, 0, send.length);
		}
	}

	@Override
	public synchronized void onPause() {
		super.onPause();

	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		// Stop the Bluetooth services
		if (myapplication.getmService() != null)
			myapplication.getmService().stop();

	}

	// ���±������ұ�״̬�Ͷ�д״̬��Handler
	private final Handler mHandler = new Handler() {

		@Override
		public void handleMessage(Message msg) {
			switch (msg.what) {
			case MESSAGE_STATE_CHANGE:
				if (D)
					Log.i(TAG, "MESSAGE_STATE_CHANGE:" + msg.arg1);
				switch (msg.arg1) {
				case BluetoothService.STATE_CONNECTED:
					// mTitle.setText(R.string.title_connected_to);
					// mTitle.append(mConnectedDeviceName);
					// ivPrinterIcon.setBackgroundResource(R.drawable.log);

					tvPrinterMessage.setText(getString(R.string.connected_txt));
					tvPrinterMessage.setTextColor(Color.parseColor("#398f14"));

					break;
				case BluetoothService.STATE_CONNECTING:
					// mTitle.setText(R.string.title_connecting);
					tvPrinterMessage
							.setText(getString(R.string.connecting_txt));
					tvPrinterMessage.setTextColor(Color.parseColor("#000000"));

					break;
				case BluetoothService.STATE_LISTEN:
				case BluetoothService.STATE_NONE:
					// mTitle.setText(R.string.title_not_connected);
					// ivPrinterIcon.setBackgroundResource(R.drawable.not_log);
					tvPrinterMessage
							.setText(getString(R.string.not_connected_txt));
					tvPrinterMessage.setTextColor(Color.parseColor("#e34949"));

					break;
				}
				break;
			case MESSAGE_WRITE:
				break;
			case MESSAGE_READ:
				// ����Ч�ֽڻ���������һ���ַ���
				break;
			case MESSAGE_DEVICE_NAME:
				// ���������豸������
				mConnectedDeviceName = msg.getData().getString(DEVICE_NAME);
				// Toast.makeText(getApplicationContext(),
				// "���ӵ�" + mConnectedDeviceName, Toast.LENGTH_SHORT)
				// .show();
				alerter.alerterSuccessSimpleHere(mConnectedDeviceName
						+ " connected / " + mConnectedDeviceName
						+ " iracometse neza ");

				break;
			case MESSAGE_TOAST:
				// Toast.makeText(getApplicationContext(),
				// msg.getData().getString(TOAST), Toast.LENGTH_SHORT)
				// .show();

				alerter.alerterError("Error retry / ikibazo ongera ugerageze ");

				break;

			}

		}
	};

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (D)
			Log.d(TAG, "onActivityResult" + resultCode);
		switch (requestCode) {
		case REQUEST_CONNECT_DEVICE:
			// ��DeviceListActivity����һ���豸����
			if (resultCode == Activity.RESULT_OK) {

				// ��ȡ�豸��MAC��ַ
				String address = data.getExtras().getString(
						DeviceListActivity.EXTRA_DEVICE_ADDRESS);
				// �õ�BLuetoothDevice����
				if (address == null) {
					break;
				}
				// ��ȡ�豸��������
				BluetoothDevice device = mBluetoothAdapter
						.getRemoteDevice(address);
				// �������ӵ��豸
				myapplication.getmService().connect(device);
			}
			break;
		case REQUEST_ENABLE_BT:
			// �����󼤻������Ļر�
			if (resultCode == Activity.RESULT_OK) {
				// �������������õ�,���,����һ������Ự
				// ���������ɹ����������ʼ��UI
				setupChat();

			} else {
				// �û�û���������������
				Log.d(TAG, "BT not enabled");
				// Toast.makeText(this, R.string.bt_not_enabled_leaving,
				// Toast.LENGTH_SHORT).show();
				alerter.alerterError(getString(R.string.bt_not_enabled_leaving));
				finish();
			}

			/*
			 * case REQUEST_Set:
			 * 
			 * if(resultCode==Activity.RESULT_OK) {
			 * 
			 * byte a[]=data.getExtras().getByteArray("data");
			 * mService.write(a,0,a.length); Toast.makeText(this, "���óɹ���",
			 * Toast.LENGTH_SHORT).show(); } break;
			 */
		}
	}

	@Override
	public void onBackPressed() {
		android.os.Process.killProcess(android.os.Process.myPid());
	}

	private void initialize() {
		// TODO Auto-generated method stub
		vars = new Vars(this);
		alerter = new Alerter(this);
		// cd = new ConnectionDetector(this);
		// phoneNumberText = (TextView) findViewById(R.id.tvPhoneNumber);
		tvPrinterMessage = (TextView) findViewById(R.id.tv_printer_message);
		cbPrint = (CheckBox) findViewById(R.id.cb_print);
		amountText = (TextView) findViewById(R.id.tvAmount);
		etPhone = (EditText) findViewById(R.id.et_phone);
		pinEditText = (EditText) findViewById(R.id.etPin);
		nameOfBank = (TextView) findViewById(R.id.tvNameOfBank);
		rraRefId = (TextView) findViewById(R.id.tvRRARefId);
		tin = (TextView) findViewById(R.id.tvTIN);
		taxPayerName = (TextView) findViewById(R.id.tvTaxPayerName);
		taxDescription = (TextView) findViewById(R.id.tvTaxDescription);
		amountToPay = (TextView) findViewById(R.id.tvAmountToPay);
		declarationDate = (TextView) findViewById(R.id.tvDeclarationDate);
		result = new TextView(this);
		taxCenterNo = new TextView(this);
		taxTypeNo = new TextView(this);
		assessNo = new TextView(this);
		rraOriginNo = new TextView(this);
		decId = new TextView(this);
		tvTitle = (TextView) findViewById(R.id.tv_title);

	}

	public void pay(View v) {

		// if (cd.isConnectingToInternet()) {

		if (etPhone.getText().toString().length() <= 0) {
			alerter.alerterError(Globals.phoneCheckErrorMessage);
		} else if (etPhone.getText().toString().length() != 10) {
			alerter.alerterError(Globals.invalidNumberErrorMessage);
		} else if (pinEditText.getText().toString().length() <= 0) {
			alerter.alerterError(Globals.enterAgentPinErrorMessage);
		} else {

			LayoutInflater li = LayoutInflater.from(RRAPay.this);
			View promptsView;
			promptsView = li.inflate(R.layout.confirm_rra_payment, null);

			TextView tvmessage = (TextView) promptsView
					.findViewById(R.id.txt_confirm_dialog_rra_payment);

			tvmessage.setText(Globals.confirmRRAPayment);

			AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
					RRAPay.this);

			// set prompts.xml to alertdialog builder
			alertDialogBuilder.setView(promptsView);
			// set dialog message
			alertDialogBuilder
					.setCancelable(false)
					.setPositiveButton(Globals.yes,
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog,
										int id) {

									clientSMS = "false";
									fone = vars.prefs.getString("agentPhone",
											null);

									new PayRRA().execute(result.getText()
											.toString(), nameOfBank.getText()
											.toString(), rraRefId.getText()
											.toString(), tin.getText()
											.toString(), taxPayerName.getText()
											.toString(), taxDescription
											.getText().toString(), taxCenterNo
											.getText().toString(), taxTypeNo
											.getText().toString(), assessNo
											.getText().toString(), rraOriginNo
											.getText().toString(), amountToPay
											.getText().toString(),
											declarationDate.getText()
													.toString(), fone,
											pinEditText.getText().toString(),
											decId.getText().toString(),
											clientSMS, etPhone.getText()
													.toString());
								}
							})
					.setNegativeButton(Globals.no,
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog,
										int id) {
									dialog.cancel();
								}
							});

			// create alert dialog
			AlertDialog alertDialog = alertDialogBuilder.create();
			// show it
			alertDialog.show();

		}

		// } else {
		// alerter.alerterError(Globals.noInternetDetectedErrorMessage);
		// }

	}

	public void setInfoText(Tax tax) {
		this.nameOfBank.setText(tax.getBankName());
		this.rraRefId.setText(tax.getRraRefId());
		this.tin.setText(tax.getTin());
		this.taxPayerName.setText(tax.getTaxPayerName());
		this.taxDescription.setText(tax.getTaxTypeDescription());
		this.amountToPay.setText(tax.getAmountToPay());
		this.declarationDate.setText(tax.getDecDate());

	}

	public void setOtherInfo(Tax tax) {
		this.result.setText(tax.getResult());
		this.taxCenterNo.setText(tax.getTaxCenterNo());
		this.taxTypeNo.setText(tax.getTaxTypeNo());
		this.assessNo.setText(tax.getAssessNo());
		this.rraOriginNo.setText(tax.getRraOriginNo());
		this.decId.setText(tax.getDecId());

	}

	public class PayRRA extends AsyncTask<String, Void, String> {

		public String theResult = null;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			pd = new ProgressDialog(RRAPay.this);
			pd.setTitle("Paying Tax / Kwishyura umusoro");
			pd.setMessage(Globals.waitProgressMessage);
			pd.setCancelable(false);
			pd.setIndeterminate(true);
			pd.show();
		}

		@Override
		protected String doInBackground(String... arg) {
			String json = "";
			String result = arg[0];
			String bankName = arg[1];
			String rraRef = arg[2];
			String tin = arg[3];
			String TaxPayerName = arg[4];
			String taxTypeDesc = arg[5];
			String taxCentreNo = arg[6];
			String taxTypeNo = arg[7];
			String assessNo = arg[8];
			String rraOriginNo = arg[9];
			String amountToPay = arg[10];
			String decDate = arg[11];
			String phoneNumber = arg[12];
			String pin = arg[13];
			String decId = arg[14];
			String clientSMS = arg[15];

			String customerphone = arg[16];

			StringBuilder builder = new StringBuilder();
			ConnectUtils connector = new ConnectUtils();
			HttpClient client = connector.getNewHttpClient();

			String urlString = null;

			// LOAD SERVER PREF

			try {

				// DEALER
				urlString = vars.server
						+ "/bio-api/androidTax/payTax.php?result="
						+ URLEncoder.encode(result, "UTF-8")
						+ "&bankName="
						+ URLEncoder.encode(bankName, "UTF-8")

						+ "&rraRef="
						+ URLEncoder.encode(rraRef, "UTF-8")

						+ "&tin="
						+ URLEncoder.encode(tin, "UTF-8")

						+ "&taxPayerName="
						+ URLEncoder.encode(TaxPayerName, "UTF-8")

						+ "&taxTypeDesc="
						+ URLEncoder.encode(taxTypeDesc, "UTF-8")

						+ "&taxCentreNo="
						+ URLEncoder.encode(taxCentreNo, "UTF-8")

						+ "&taxTypeNo="
						+ URLEncoder.encode(taxTypeNo, "UTF-8")

						+ "&assessNo="
						+ URLEncoder.encode(assessNo, "UTF-8")

						+ "&dealer="
						+ URLEncoder.encode(
								pref.getString(Globals.permission, null),
								"UTF-8")

						+ "&rraOriginNo="
						+ URLEncoder.encode(rraOriginNo, "UTF-8")

						+ "&amountToPay="
						+ URLEncoder.encode(amountToPay, "UTF-8")

						+ "&decDate="
						+ URLEncoder.encode(decDate, "UTF-8")

						+ "&phoneNumber="
						+ URLEncoder.encode(phoneNumber, "UTF-8")

						+ "&pin="
						+ URLEncoder.encode(pin, "UTF-8")

						+ "&dec_id="
						+ URLEncoder.encode(decId, "UTF-8")

						+ "&client_sms="
						+ URLEncoder.encode(clientSMS, "UTF-8")

						+ "&customerphone="
						+ URLEncoder.encode(customerphone, "UTF-8")
						+ "&version="
						+ URLEncoder.encode(Globals.version, "UTF-8")
						+ "&dealer_name="
						+ URLEncoder.encode(
								vars.prefs.getString(Globals.DEALERNAME, null),
								"UTF-8");

			} catch (UnsupportedEncodingException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

			HttpGet httpGet = new HttpGet(urlString);

			try {
				HttpResponse response = client.execute(httpGet);
				StatusLine statusLine = response.getStatusLine();
				int statusCode = statusLine.getStatusCode();
				if (statusCode == 200) {
					HttpEntity entity = response.getEntity();
					InputStream content = entity.getContent();
					BufferedReader reader = new BufferedReader(
							new InputStreamReader(content));
					String line;
					while ((line = reader.readLine()) != null) {
						builder.append(line);
					}
					json = builder.toString();

				} else {

				}
			} catch (ConnectTimeoutException e) {

				e.printStackTrace();
			} catch (ClientProtocolException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}

			return json;
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			// REMOVE DIALOG
			if (pd != null) {
				pd.dismiss();
				// b.setEnabled(true);
			}

			trans = new Transaction(result);

			// IF OUTPUT WAS RECEIVED FROM SERVER
			if (trans.getMessage() != null) {

				if (trans.getResult().equals("Success")) {

					LayoutInflater li = LayoutInflater
							.from(getApplicationContext());
					View promptsView;
					promptsView = li.inflate(R.layout.dialog_success_simple,
							null);

					TextView msgTxt = (TextView) promptsView
							.findViewById(R.id.success_simple_message);

					msgTxt.setText(trans.getMessage());

					// print here

					if (cbPrint.isChecked()
							&& myapplication.getmService().getState() == BluetoothService.STATE_CONNECTED) {
						// String title = "E-Tax";
						String mobicashText = "MobiCash LTD";
						String tinNumberText = "TIN No : 101858540";
						String mobicashPhoneText = "Tel: (250)787689185/SMS:8485";
						String bodifaHouseText = "Bodifa Mercy House,5th Floor";
						String KimihururaKigaliText = "Kimihurura,Kigali";
						String receiptNumberText = "Receipt Number : "
								+ trans.getTransactionid();
						String dateText = "Date : " + trans.getDateTime();
						String agentNameTitle = "Agent name";
						String agentName = pref.getString("agentName", null);
						String mobicashReferenceIdTitle = "Mobicash Reference ID";
						String clientChargesTitle = "Client Charges ";
						String totalAmountTitle = "Total Amount";

						String rraRefIdTitle = "RRA reference ID";

						String taxPayerNameTitle = "Tax payer Name";

						String taxTypeDescriptionTitle = "Tax type description";

						String mobicashRefIdTitle = "Mobicash reference ID";
						String clientTinTitle = "Client tin";

						mobicashRefId = trans.getExtra1();

						String amountPaidTitle = "Total Amount";

						String thankUMessage = "Thank you for using MCash ";
						String keepEnjoyingMessage = "Keep Enjoying !";
						String line = "------------------------------";
						String dash = "##############################";
						String star = "******************************";
						double amount = Double.parseDouble(amountPaid);
						double commission = Double.parseDouble(trans
								.getCommissions());
						double totalAmount = amount + commission;

						String message = mobicashText + "\n" + tinNumberText
								+ "\n" + mobicashPhoneText + "\n"
								+ bodifaHouseText + "\n" + KimihururaKigaliText
								+ "\n\n" + receiptNumberText + "\n" + dateText
								+ "\n" + line + "\n\n" + agentNameTitle + "\n"
								+ agentName + "\n\n" + mobicashRefIdTitle
								+ "\n" + mobicashRefId + "\n\n"
								+ clientTinTitle + "\n" + tinText + "\n\n"
								+ rraRefIdTitle + "\n" + rraRefIdNumber
								+ "\n\n" + taxPayerNameTitle + "\n"
								+ taxPayerNameText + "\n\n"
								+ taxTypeDescriptionTitle + "\n"
								+ taxTypeDescription + "\n\n" + amountPaidTitle
								+ "\n" + amount + " RW \n\n"
								+ clientChargesTitle + "\n" + commission
								+ " RW \n\n" + totalAmountTitle + "\n"
								+ totalAmount + " RW" + "\n\n" + line + "\n"
								+ thankUMessage + "\n" + keepEnjoyingMessage
								+ "\n\n\n\n\n\n\n\n\n\n";
						sendMessage(message);
					}

					AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
							RRAPay.this);

					// set prompts.xml to alertdENroialog builder
					alertDialogBuilder.setView(promptsView);
					// set dialog message
					alertDialogBuilder.setCancelable(false).setPositiveButton(
							"OK", new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog,
										int id) {

									Intent i = new Intent(
											getApplicationContext(),
											Services.class);
									startActivity(i);

								}
							});

					// create alert dialog
					AlertDialog alertDialog = alertDialogBuilder.create();
					// show it
					alertDialog.show();

				} else {
					// alerter.alerterError(trans.getResult());
					alerter.alerterError(trans.getError());
				}
			} else {
				alerter.alerterError(Globals.InternetErrorMessage);
			}

		}
	}

	public void layoutClick(View v) {
		InputMethodManager inputManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
		inputManager.hideSoftInputFromWindow(
				getCurrentFocus().getWindowToken(),
				InputMethodManager.HIDE_NOT_ALWAYS);
	}

	// CANCEL
	public void cancel(View v) {
		alerter.alerterConfirmCancelEtax(Globals.cancelEtaxErrorMessage);
	}

	@Override
	protected void onStop() {
		super.onStop();
		if (pd != null)
			pd.dismiss();
	}

}
