package ez.pay.project;

import android.app.ActionBar;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class AdminActivity extends BaseActivity {

	EditText etAdminPin;
	String adminPin;
	Alerter alerter;
	SharedPreferences pref;
	Editor edit;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.admin_activity);

		ActionBar bar = getActionBar();
		bar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#f47701")));

		init();

	}

	public void layoutClick(View v) {
		InputMethodManager inputManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
		inputManager.hideSoftInputFromWindow(
				getCurrentFocus().getWindowToken(),
				InputMethodManager.HIDE_NOT_ALWAYS);
	}

	private void init() {
		// TODO Auto-generated method stub
		etAdminPin = (EditText) findViewById(R.id.et_admin_pin_number);
		adminPin = etAdminPin.getText().toString();
		alerter = new Alerter(this);
		pref = PreferenceManager.getDefaultSharedPreferences(this);
		edit = pref.edit();

	}

	public void adminLick(View v) {
		if (etAdminPin.getText().toString().length() <= 0) {
			alerter.alerterError(Globals.enterPSPIDErrorMessage);
		} else if (etAdminPin.getText().toString().length() != 8) {
			alerter.alerterError(Globals.invalidPSPIDErrorMessage);
		} else {

			edit.putString("PSPID", etAdminPin.getText().toString());
			edit.commit();

			customToast("PSP Id  saved successfully");

			Intent intent = new Intent(getApplicationContext(),
					GetClientTag.class);
			startActivity(intent);
		}

	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
	}

	@Override
	protected void onStop() {
		super.onStop();
	}

	@Override
	public void onBackPressed() {
		Intent intent = new Intent(getApplicationContext(), First_time.class);
		startActivity(intent);
	}

	public void customToast(String message) {
		LayoutInflater inflater = getLayoutInflater();
		View toastLayout = inflater.inflate(R.layout.custom_toast,
				(ViewGroup) findViewById(R.id.custom_toast_layout));
		TextView tvCustomToastMessage = (TextView) toastLayout
				.findViewById(R.id.tv_custom_toast_message);
		tvCustomToastMessage.setText(message);

		Toast toast = new Toast(getApplicationContext());
		toast.setDuration(Toast.LENGTH_LONG);
		toast.setView(toastLayout);
		toast.setGravity(Gravity.CENTER, 0, 0);
		toast.show();

		Intent iGoHome = new Intent(getApplicationContext(), GetClientTag.class);
		startActivity(iGoHome);
	}

}
