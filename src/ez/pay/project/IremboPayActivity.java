package ez.pay.project;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.conn.ConnectTimeoutException;

import services.BluetoothService;
import utils.ConnectUtils;
import utils.Transaction;
import utils.Vars;
import android.app.ActionBar;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;

public class IremboPayActivity extends BaseActivity {

	TextView tvCustomerName, tvCustomerIdNumber, tvBillNumber, tvBillerCode,
			tvRraAccountName, tvRraAccountNumber, tvAmount, tvDate,
			tvDescription, tvCustomerPhone;

	String customerName, customerIdNumber, billNumber, billerCode,
			rraAccountName, rraAccountNumber, amount, date, description,
			mobile;

	EditText etPin, etMobile;
	Button pay;
	Vars vars;
	Alerter alerter;
	// ConnectionDetector cd;
	Transaction trans;

	ProgressDialog pd;
	SharedPreferences pref;
	CheckBox cbPrint;

	// bluetooth

	private static final String TAG = "BluetoothPrint";
	private static final boolean D = true;

	// ��BluetoothService�����������Ϣ����
	public static final int MESSAGE_STATE_CHANGE = 1;
	public static final int MESSAGE_READ = 2;
	public static final int MESSAGE_WRITE = 3;
	public static final int MESSAGE_DEVICE_NAME = 4;
	public static final int MESSAGE_TOAST = 5;

	// BluetoothService�յ��Ĵ������ؼ�������
	public static final String DEVICE_NAME = "device_name";
	public static final String TOAST = "toast";

	// ����Ĵ���
	private static final int REQUEST_CONNECT_DEVICE = 1;
	private static final int REQUEST_ENABLE_BT = 2;
	private static final int REQUEST_Set = 3;

	// ���ӵ��豸������
	private String mConnectedDeviceName = null;
	// �ַ�����������������Ϣ
	private StringBuffer mOutStringBuffer;
	// ��������������
	private BluetoothAdapter mBluetoothAdapter = null;
	public static BluetoothService mService = null;

	private TextView mTitle;
	private EditText mOutEditText;
	private Button mPrintButton;
	private Button mClearButton;
	private Button mSetButton;
	MyApplication myapplication;
	TextView tvPrinterMessage;
	String tinText;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.irembo_pay_activity);
		ActionBar bar = getActionBar();
		bar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#f47701")));
		myapplication = (MyApplication) getApplication();
		initialize();

		pref = PreferenceManager.getDefaultSharedPreferences(this);
		Intent iin = getIntent();
		Bundle b = iin.getExtras();

		if (b != null) {

			customerName = (String) b.get(Globals.customerName);
			customerIdNumber = (String) b.get(Globals.customerIdNumber);
			billNumber = (String) b.get(Globals.billNumber);
			billerCode = (String) b.get(Globals.billerCode);
			rraAccountName = (String) b.get(Globals.rraAccountName);
			rraAccountNumber = (String) b.get(Globals.rraAccountNumber);
			amount = (String) b.get(Globals.amount);
			date = (String) b.get(Globals.date);
			description = (String) b.get(Globals.description);
			mobile = (String) b.get(Globals.mobile);

			setValues(customerName, customerIdNumber, billNumber, billerCode,
					rraAccountName, rraAccountNumber, amount, date,
					description, mobile);

		}

		// hide keyboard
		this.getWindow().setSoftInputMode(
				WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

		// bluetooth
		//
		// // �õ����ص�������
		// mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
		//
		// if (mBluetoothAdapter == null) {
		// Toast.makeText(this, "��������ʹ�ã�", Toast.LENGTH_LONG).show();
		// finish();
		// return;
		// }

	}

	public void setValues(String customerName, String customerIdNumber,
			String billNumber, String billerCode, String rraAccountName,
			String rraAccountNumber, String amount, String date,
			String description, String mobile) {

		tvCustomerName.setText(customerName);
		tvCustomerIdNumber.setText(customerIdNumber);
		tvBillNumber.setText(billNumber);
		tvBillerCode.setText(billerCode);
		tvRraAccountName.setText(rraAccountName);
		tvRraAccountNumber.setText(rraAccountNumber);
		tvAmount.setText(amount);
		tvDate.setText(date);
		tvDescription.setText(description);
		tvCustomerPhone.setText(mobile);

	}

	public void printClick(View v) {
		if (((CheckBox) v).isChecked()) {

			if (myapplication.getmService().getState() != BluetoothService.STATE_CONNECTED) {
				// Toast.makeText(this, R.string.not_connected,
				// Toast.LENGTH_SHORT).show();
				try {

					Intent serverIntent = new Intent(this,
							DeviceListActivity.class);
					startActivityForResult(serverIntent, REQUEST_CONNECT_DEVICE);

					return;
				} catch (Exception e) {

				}
			}

		}
	}

	// @Override
	// public void onStart() {
	// super.onStart();
	//
	// // �ж������Ƿ������������û�п���������������
	// if (!mBluetoothAdapter.isEnabled()) {
	//
	// Intent enableIntent = new Intent(
	// BluetoothAdapter.ACTION_REQUEST_ENABLE);
	// startActivityForResult(enableIntent, REQUEST_ENABLE_BT);
	// } else {
	// if (myapplication.getmService() == null) {
	//
	// setupChat();
	// }
	//
	// }
	// }

	// @Override
	// public synchronized void onResume() {
	// super.onResume();
	// if (D)
	//
	// if (myapplication.getmService() != null) {
	// if (myapplication.getmService().getState() ==
	// BluetoothService.STATE_NONE) {
	//
	// myapplication.getmService().start();
	// }
	//
	// }
	//
	// if (myapplication.getmService().getState() ==
	// BluetoothService.STATE_CONNECTED) {
	// // ivPrinterIcon.setBackgroundResource(R.drawable.log);
	// tvPrinterMessage.setText(getString(R.string.connected_txt));
	// tvPrinterMessage.setTextColor(Color.parseColor("#398f14"));
	//
	// } else {
	// // ivPrinterIcon.setBackgroundResource(R.drawable.not_log);
	// tvPrinterMessage.setText(getString(R.string.not_connected_txt));
	// tvPrinterMessage.setTextColor(Color.parseColor("#e34949"));
	//
	// }
	//
	// }

	// private void setupChat() {
	// Log.d(TAG, "SetupChat");
	//
	// // ��ʼ��BluetoothServiceִ����������
	// myapplication.setmService(new BluetoothService(this, mHandler));
	//
	// }

	// private void sendMessage(String message) {
	// // ������ڳ�������
	//
	// // ���ʵ��Ҫ���͵Ķ���
	// if (message.length() > 0) {
	// // �õ���Ϣ�ֽڲ��Ҹ���BluetoothService
	// byte[] send;
	// try {
	// send = message.getBytes("GB2312");
	// } catch (UnsupportedEncodingException e) {
	// send = message.getBytes();
	// }
	// // mService.write(send,10);
	// myapplication.getmService().write(send, 0, send.length);
	// }
	// }

	@Override
	public synchronized void onPause() {
		super.onPause();

	}

	// @Override
	// public void onDestroy() {
	// super.onDestroy();
	// // Stop the Bluetooth services
	// if (myapplication.getmService() != null)
	// myapplication.getmService().stop();
	//
	// }

	// ���±������ұ�״̬�Ͷ�д״̬��Handler
	// private final Handler mHandler = new Handler() {
	//
	// @Override
	// public void handleMessage(Message msg) {
	// switch (msg.what) {
	// case MESSAGE_STATE_CHANGE:
	// if (D)
	// Log.i(TAG, "MESSAGE_STATE_CHANGE:" + msg.arg1);
	// switch (msg.arg1) {
	// case BluetoothService.STATE_CONNECTED:
	// // mTitle.setText(R.string.title_connected_to);
	// // mTitle.append(mConnectedDeviceName);
	// // ivPrinterIcon.setBackgroundResource(R.drawable.log);
	//
	// tvPrinterMessage.setText(getString(R.string.connected_txt));
	// tvPrinterMessage.setTextColor(Color.parseColor("#398f14"));
	//
	// break;
	// case BluetoothService.STATE_CONNECTING:
	// // mTitle.setText(R.string.title_connecting);
	// tvPrinterMessage
	// .setText(getString(R.string.connecting_txt));
	// tvPrinterMessage.setTextColor(Color.parseColor("#000000"));
	//
	// break;
	// case BluetoothService.STATE_LISTEN:
	// case BluetoothService.STATE_NONE:
	// // mTitle.setText(R.string.title_not_connected);
	// // ivPrinterIcon.setBackgroundResource(R.drawable.not_log);
	// tvPrinterMessage
	// .setText(getString(R.string.not_connected_txt));
	// tvPrinterMessage.setTextColor(Color.parseColor("#e34949"));
	//
	// break;
	// }
	// break;
	// case MESSAGE_WRITE:
	// break;
	// case MESSAGE_READ:
	// // ����Ч�ֽڻ���������һ���ַ���
	// break;
	// case MESSAGE_DEVICE_NAME:
	// // ���������豸������
	// mConnectedDeviceName = msg.getData().getString(DEVICE_NAME);
	// // Toast.makeText(getApplicationContext(),
	// // "���ӵ�" + mConnectedDeviceName, Toast.LENGTH_SHORT)
	// // .show();
	// alerter.alerterSuccessSimpleHere(mConnectedDeviceName
	// + " connected / " + mConnectedDeviceName
	// + " iracometse neza ");
	//
	// break;
	// case MESSAGE_TOAST:
	// // Toast.makeText(getApplicationContext(),
	// // msg.getData().getString(TOAST), Toast.LENGTH_SHORT)
	// // .show();
	//
	// alerter.alerterError("Error retry / ikibazo ongera ugerageze ");
	//
	// break;
	//
	// }
	//
	// }
	// };

	// @Override
	// public void onActivityResult(int requestCode, int resultCode, Intent
	// data) {
	// if (D)
	// Log.d(TAG, "onActivityResult" + resultCode);
	// switch (requestCode) {
	// case REQUEST_CONNECT_DEVICE:
	// // ��DeviceListActivity����һ���豸����
	// if (resultCode == Activity.RESULT_OK) {
	//
	// // ��ȡ�豸��MAC��ַ
	// String address = data.getExtras().getString(
	// DeviceListActivity.EXTRA_DEVICE_ADDRESS);
	// // �õ�BLuetoothDevice����
	// if (address == null) {
	// break;
	// }
	// // ��ȡ�豸��������
	// BluetoothDevice device = mBluetoothAdapter
	// .getRemoteDevice(address);
	// // �������ӵ��豸
	// myapplication.getmService().connect(device);
	// }
	// break;
	// case REQUEST_ENABLE_BT:
	// // �����󼤻������Ļر�
	// if (resultCode == Activity.RESULT_OK) {
	// // �������������õ�,���,����һ������Ự
	// // ���������ɹ����������ʼ��UI
	// setupChat();
	//
	// } else {
	// // �û�û���������������
	// Log.d(TAG, "BT not enabled");
	// // Toast.makeText(this, R.string.bt_not_enabled_leaving,
	// // Toast.LENGTH_SHORT).show();
	// alerter.alerterError(getString(R.string.bt_not_enabled_leaving));
	// finish();
	// }
	//
	// }
	// }

	@Override
	public void onBackPressed() {
		android.os.Process.killProcess(android.os.Process.myPid());
	}

	private void initialize() {
		// TODO Auto-generated method stub
		vars = new Vars(this);
		alerter = new Alerter(this);
		// cd = new ConnectionDetector(this);
		// phoneNumberText = (TextView) findViewById(R.id.tvPhoneNumber);
		tvPrinterMessage = (TextView) findViewById(R.id.tv_printer_message);
		cbPrint = (CheckBox) findViewById(R.id.cb_print);

		tvCustomerName = (TextView) findViewById(R.id.tv_customer_name);
		tvCustomerIdNumber = (TextView) findViewById(R.id.tv_customer_id_number);
		tvBillNumber = (TextView) findViewById(R.id.tv_bill_number);
		tvBillerCode = (TextView) findViewById(R.id.tv_biller_code);
		tvRraAccountName = (TextView) findViewById(R.id.tv_rra_account_name);
		tvRraAccountNumber = (TextView) findViewById(R.id.tv_rra_account_number);
		tvAmount = (TextView) findViewById(R.id.tv_amount);
		tvDate = (TextView) findViewById(R.id.tv_date);
		tvDescription = (TextView) findViewById(R.id.tv_description);
		tvCustomerPhone = (TextView) findViewById(R.id.tv_customer_phone);
		etPin = (EditText) findViewById(R.id.et_pin);
		etMobile = (EditText) findViewById(R.id.et_mobile);

	}

	public void pay(View v) {

		if (etMobile.getText().toString().length() <= 0) {
			alerter.alerterError(Globals.phoneCheckErrorMessage);
		} else if (etMobile.getText().toString().length() != 10) {
			alerter.alerterError(Globals.invalidNumberErrorMessage);
		} else if (etPin.getText().toString().length() <= 0) {
			alerter.alerterError(Globals.enterAgentPinErrorMessage);
		} else {

			LayoutInflater li = LayoutInflater.from(IremboPayActivity.this);
			View promptsView;
			promptsView = li.inflate(R.layout.confirm_rra_payment, null);

			TextView tvmessage = (TextView) promptsView
					.findViewById(R.id.txt_confirm_dialog_rra_payment);

			tvmessage.setText(Globals.confirmRRAPayment);

			AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
					IremboPayActivity.this);

			// set prompts.xml to alertdialog builder
			alertDialogBuilder.setView(promptsView);
			// set dialog message
			alertDialogBuilder
					.setCancelable(false)
					.setPositiveButton(Globals.yes,
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog,
										int id) {

									new PayIrembo().execute();
								}
							})
					.setNegativeButton(Globals.no,
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog,
										int id) {
									dialog.cancel();
								}
							});

			// create alert dialog
			AlertDialog alertDialog = alertDialogBuilder.create();
			// show it
			alertDialog.show();

		}

		// } else {
		// alerter.alerterError(Globals.noInternetDetectedErrorMessage);
		// }

	}

	public class PayIrembo extends AsyncTask<String, Void, String> {
		String json;
		public String theResult = null;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			pd = new ProgressDialog(IremboPayActivity.this);
			pd.setTitle("Paying Tax / Kwishyura umusoro");
			pd.setMessage(Globals.waitProgressMessage);
			pd.setCancelable(false);
			pd.setIndeterminate(true);
			pd.show();
		}

		@Override
		protected String doInBackground(String... arg) {

			StringBuilder builder = new StringBuilder();
			ConnectUtils connector = new ConnectUtils();
			HttpClient client = connector.getNewHttpClient();

			String urlString = null;

			// LOAD SERVER PREF

			try {

				// DEALER
				urlString = vars.server
						+ "/bio-api/androidIrembo/iremboPay.php?agentNumber="
						+ URLEncoder.encode(
								vars.prefs.getString("agentPhone", null),
								"UTF-8")
						+ "&accountNumber="
						+ URLEncoder.encode(rraAccountNumber, "UTF-8")

						+ "&pin="
						+ URLEncoder
								.encode(etPin.getText().toString(), "UTF-8")

						+ "&billNumber="
						+ URLEncoder.encode(billNumber, "UTF-8")

						+ "&paidAmount="
						+ URLEncoder.encode(amount, "UTF-8")

						+ "&paymentStatus="
						+ URLEncoder.encode("approved", "UTF-8")

						+ "&paymentType="
						+ URLEncoder.encode("cash", "UTF-8")

						+ "&payerName="
						+ URLEncoder.encode(customerName, "UTF-8")
						+ "&payerPhone="
						+ URLEncoder.encode(etMobile.getText().toString(),
								"UTF-8")
						+ "&dealer="
						+ URLEncoder.encode(
								pref.getString(Globals.permission, null),
								"UTF-8") + "&version="
						+ URLEncoder.encode(Globals.version, "UTF-8")

						+ "&dealer_name="
						+ URLEncoder.encode(
								vars.prefs.getString(Globals.DEALERNAME, null),
								"UTF-8")

				;
			} catch (UnsupportedEncodingException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

			HttpGet httpGet = new HttpGet(urlString);

			try {
				HttpResponse response = client.execute(httpGet);
				StatusLine statusLine = response.getStatusLine();
				int statusCode = statusLine.getStatusCode();
				if (statusCode == 200) {
					HttpEntity entity = response.getEntity();
					InputStream content = entity.getContent();
					BufferedReader reader = new BufferedReader(
							new InputStreamReader(content));
					String line;
					while ((line = reader.readLine()) != null) {
						builder.append(line);
					}
					json = builder.toString();

				} else {

				}
			} catch (ConnectTimeoutException e) {

				e.printStackTrace();
			} catch (ClientProtocolException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}

			return json;
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			// REMOVE DIALOG
			if (pd != null) {
				pd.dismiss();
				// b.setEnabled(true);
			}

			trans = new Transaction(result);

			// IF OUTPUT WAS RECEIVED FROM SERVER
			if (trans.getMessage() != null) {

				if (trans.getResult().equals("Success")) {

					LayoutInflater li = LayoutInflater
							.from(getApplicationContext());
					View promptsView;
					promptsView = li.inflate(R.layout.dialog_success_simple,
							null);

					TextView msgTxt = (TextView) promptsView
							.findViewById(R.id.success_simple_message);

					msgTxt.setText(trans.getMessage());

					// print here

					// if (cbPrint.isChecked()
					// && myapplication.getmService().getState() ==
					// BluetoothService.STATE_CONNECTED) {
					// // String title = "E-Tax";
					// String mobicashText = "MobiCash LTD";
					// String tinNumberText = "TIN No : 101858540";
					// String mobicashPhoneText =
					// "Tel: (250)787797979/SMS:8485";
					// String bodifaHouseText = "Bodifa Mercy House,5th Floor";
					// String KimihururaKigaliText = "Kimihurura,Kigali";
					// String receiptNumberText = "Receipt Number : "
					// + trans.getTransactionid();
					// String dateText = "Date : " + trans.getDateTime();
					// String agentNameTitle = "Agent name";
					// String agentName = pref.getString("agentName", null);
					// String mobicashReferenceIdTitle =
					// "Mobicash Reference ID";
					// String clientChargesTitle = "Client Charges ";
					// String totalAmountTitle = "Total Amount";
					//
					// String rraRefIdTitle = "RRA reference ID";
					//
					// String taxPayerNameTitle = "Tax payer Name";
					//
					// String taxTypeDescriptionTitle = "Tax type description";
					//
					// String mobicashRefIdTitle = "Mobicash reference ID";
					// String clientTinTitle = "Client tin";
					//
					// String mobicashRefId = trans.getExtra1();
					//
					// String amountPaidTitle = "Total Amount";
					//
					// String thankUMessage = "Thank you for using MCash ";
					// String keepEnjoyingMessage = "Keep Enjoying !";
					// String line = "------------------------------";
					// String dash = "##############################";
					// String star = "******************************";
					// double amountDouble = Double.parseDouble(amount);
					// double commission = Double.parseDouble(trans
					// .getCommissions());
					// double totalAmount = amountDouble + commission;
					//
					// String message = mobicashText + "\n"
					// +tinNumberText+ "\n"
					// +mobicashPhoneText + "\n"
					// + bodifaHouseText + "\n"
					// +KimihururaKigaliText+ "\n\n"
					// + receiptNumberText + "\n"
					// + dateText+ "\n"
					// + line + "\n\n"
					// + agentNameTitle + "\n" + agentName + "\n\n"
					// + mobicashRefIdTitle + "\n" + mobicashRefId+ "\n\n"
					// + getString(R.string.customer_name) + "\n"+ customerName
					// + "\n\n"
					// +getString(R.string.customer_id_number) + "\n" +
					// customerIdNumber + "\n\n"
					// + getString(R.string.bill_number) + "\n" + billNumber+
					// "\n\n"
					// // + getString(R.string.biller_code) + "\n" + billerCode
					// + "\n\n"
					// + getString(R.string.rra_account_name) + "\n" +
					// rraAccountName + "\n\n"
					// + getString(R.string.description) + "\n" + description+
					// "\n\n"
					// // + getString(R.string.rra_account_number) + "\n" +
					// rraAccountNumber + "\n\n"
					// + amountPaidTitle + "\n" + amount+ " RW \n\n"
					// + clientChargesTitle + "\n" + commission + " RW \n\n"
					// +totalAmountTitle + "\n"+ totalAmount + " RW \n\n"
					// // + getString(R.string.creation_date) + "\n" + date +
					// "\n\n"
					// // + getString(R.string.mobile) + "\n" +
					// etMobile.getText().toString()+ "\n\n"
					//
					// + line + "\n" + thankUMessage + "\n"
					// + keepEnjoyingMessage + "\n\n\n\n";
					//
					// sendMessage(message);
					// }

					AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
							IremboPayActivity.this);

					// set prompts.xml to alertdENroialog builder
					alertDialogBuilder.setView(promptsView);
					// set dialog message
					alertDialogBuilder.setCancelable(false).setPositiveButton(
							"OK", new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog,
										int id) {

									Intent i = new Intent(
											getApplicationContext(),
											Services.class);
									startActivity(i);

								}
							});

					// create alert dialog
					AlertDialog alertDialog = alertDialogBuilder.create();
					// show it
					alertDialog.show();

				} else {
					// alerter.alerterError(trans.getResult());
					alerter.alerterError(trans.getError());
				}
			} else {
				alerter.alerterError(Globals.InternetErrorMessage);
			}

		}
	}

	public void layoutClick(View v) {
		InputMethodManager inputManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
		inputManager.hideSoftInputFromWindow(
				getCurrentFocus().getWindowToken(),
				InputMethodManager.HIDE_NOT_ALWAYS);
	}

	// CANCEL
	public void cancel(View v) {
		alerter.alerterConfirmCancelEtax(Globals.cancelIremboPaymentMessage);
	}

	@Override
	protected void onStop() {
		super.onStop();
		if (pd != null)
			pd.dismiss();
	}

}
