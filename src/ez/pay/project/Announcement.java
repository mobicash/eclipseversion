package ez.pay.project;

import android.app.ActionBar;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.Html;
import android.widget.TextView;

public class Announcement extends BaseActivity {

	TextView tvAnnouncement;
	String announcement;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.announcement);
		ActionBar bar = getActionBar();
		bar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#f47701")));
		init();

		Intent iin = getIntent();
		Bundle b = iin.getExtras();

		if (b != null) {

			announcement = (String) b.get("ANN");
			tvAnnouncement.setText(Html.fromHtml(announcement));

		}

	}

	public void init() {
		tvAnnouncement = (TextView) findViewById(R.id.tv_announcement);
	}

}
