package ez.pay.project;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.conn.ConnectTimeoutException;

import services.BluetoothService;
import utils.ConnectUtils;
import utils.Transaction;
import utils.Vars;
import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class RssbPayActivity extends BaseActivity {

	TextView tvNationalId, tvName, tvTatalPremium, tvRemainingAmount,
			tvEligibility, tvHouseHoldCategory, tvNumberOfMembers, tvInvoice,
			tvProvince, tvDistrict, tvFinancialYear;

	EditText etPin, etMobile, etAmount;
	Button pay;
	Vars vars;
	Alerter alerter;
	// ConnectionDetector cd;
	Transaction trans;

	ProgressDialog pd;
	SharedPreferences pref;
	CheckBox cbPrint;

	// bluetooth

	private static final String TAG = "BluetoothPrint";
	private static final boolean D = true;

	// ��BluetoothService�����������Ϣ����
	public static final int MESSAGE_STATE_CHANGE = 1;
	public static final int MESSAGE_READ = 2;
	public static final int MESSAGE_WRITE = 3;
	public static final int MESSAGE_DEVICE_NAME = 4;
	public static final int MESSAGE_TOAST = 5;

	// BluetoothService�յ��Ĵ������ؼ�������
	public static final String DEVICE_NAME = "device_name";
	public static final String TOAST = "toast";

	// ����Ĵ���
	private static final int REQUEST_CONNECT_DEVICE = 1;
	private static final int REQUEST_ENABLE_BT = 2;
	private static final int REQUEST_Set = 3;

	// ���ӵ��豸������
	private String mConnectedDeviceName = null;
	// �ַ�����������������Ϣ
	private StringBuffer mOutStringBuffer;
	// ��������������
	private BluetoothAdapter mBluetoothAdapter = null;
	public static BluetoothService mService = null;

	private TextView mTitle;
	private EditText mOutEditText;
	private Button mPrintButton;
	private Button mClearButton;
	private Button mSetButton;
	MyApplication myapplication;
	TextView tvPrinterMessage;
	String tinText, financialYear, province, district;
	int remainingAmount;
	int amountPaid;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.rssb_pay_activity);
		ActionBar bar = getActionBar();
		bar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#f47701")));
		myapplication = (MyApplication) getApplication();
		initialize();

		pref = PreferenceManager.getDefaultSharedPreferences(this);
		Intent iin = getIntent();
		Bundle b = iin.getExtras();

		if (b != null) {

			tvNationalId.setText((String) b.get("householdNID"));
			tvName.setText((String) b.get("name"));
			tvTatalPremium.setText((String) b.get("totalPremium"));

			int total = Integer.parseInt((String) b.get("totalPremium"));
			amountPaid = Integer.parseInt((String) b.get("alreadyPaid"));
			remainingAmount = total - amountPaid;

			tvRemainingAmount.setText(Integer.toString(remainingAmount));
			tvEligibility.setText((String) b.get("eligibility"));
			tvHouseHoldCategory.setText((String) b.get("houseHoldCategory"));
			tvNumberOfMembers.setText((String) b.get("numberOfMembers"));
			tvInvoice.setText((String) b.get("invoice"));
			tvFinancialYear.setText((String) b.get("financialYear"));

			tvProvince.setText((String) b.get("province"));
			tvDistrict.setText((String) b.get("district"));

			financialYear = (String) b.get("financialYear");
			province = (String) b.get("province");
			district = (String) b.get("district");

		}

		// hide keyboard
		this.getWindow().setSoftInputMode(
				WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

		// bluetooth
		//
		// �õ����ص�������
		mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

	}

	public void printClick(View v) {
		if (((CheckBox) v).isChecked()) {

			if (myapplication.getmService().getState() != BluetoothService.STATE_CONNECTED) {
				// Toast.makeText(this, R.string.not_connected,
				// Toast.LENGTH_SHORT).show();
				try {

					Intent serverIntent = new Intent(this,
							DeviceListActivity.class);
					startActivityForResult(serverIntent, REQUEST_CONNECT_DEVICE);

					return;
				} catch (Exception e) {

				}
			}

		}
	}

	@Override
	public void onStart() {
		super.onStart();

		// �ж������Ƿ������������û�п���������������
		if (!mBluetoothAdapter.isEnabled()) {

			Intent enableIntent = new Intent(
					BluetoothAdapter.ACTION_REQUEST_ENABLE);
			startActivityForResult(enableIntent, REQUEST_ENABLE_BT);
		} else {
			if (myapplication.getmService() == null) {

				setupChat();
			}

		}
	}

	@Override
	public synchronized void onResume() {
		super.onResume();
		if (D)

			if (myapplication.getmService() != null) {
				if (myapplication.getmService().getState() == BluetoothService.STATE_NONE) {

					myapplication.getmService().start();
				}

			}

		if (myapplication.getmService().getState() == BluetoothService.STATE_CONNECTED) {
			// ivPrinterIcon.setBackgroundResource(R.drawable.log);
			tvPrinterMessage.setText(getString(R.string.connected_txt));
			tvPrinterMessage.setTextColor(Color.parseColor("#398f14"));

		} else {
			// ivPrinterIcon.setBackgroundResource(R.drawable.not_log);
			tvPrinterMessage.setText(getString(R.string.not_connected_txt));
			tvPrinterMessage.setTextColor(Color.parseColor("#e34949"));

		}

	}

	private void setupChat() {
		Log.d(TAG, "SetupChat");

		// ��ʼ��BluetoothServiceִ����������
		myapplication.setmService(new BluetoothService(this, mHandler));

	}

	private void sendMessage(String message) {
		// ������ڳ�������

		// ���ʵ��Ҫ���͵Ķ���
		if (message.length() > 0) {
			// �õ���Ϣ�ֽڲ��Ҹ���BluetoothService
			byte[] send;
			try {
				send = message.getBytes("GB2312");
			} catch (UnsupportedEncodingException e) {
				send = message.getBytes();
			}
			// mService.write(send,10);
			myapplication.getmService().write(send, 0, send.length);
		}
	}

	@Override
	public synchronized void onPause() {
		super.onPause();

	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		// Stop the Bluetooth services
		if (myapplication.getmService() != null)
			myapplication.getmService().stop();

	}

	// ���±������ұ�״̬�Ͷ�д״̬��Handler
	private final Handler mHandler = new Handler() {

		@Override
		public void handleMessage(Message msg) {
			switch (msg.what) {
			case MESSAGE_STATE_CHANGE:
				if (D)
					Log.i(TAG, "MESSAGE_STATE_CHANGE:" + msg.arg1);
				switch (msg.arg1) {
				case BluetoothService.STATE_CONNECTED:
					// mTitle.setText(R.string.title_connected_to);
					// mTitle.append(mConnectedDeviceName);
					// ivPrinterIcon.setBackgroundResource(R.drawable.log);

					tvPrinterMessage.setText(getString(R.string.connected_txt));
					tvPrinterMessage.setTextColor(Color.parseColor("#398f14"));

					break;
				case BluetoothService.STATE_CONNECTING:
					// mTitle.setText(R.string.title_connecting);
					tvPrinterMessage
							.setText(getString(R.string.connecting_txt));
					tvPrinterMessage.setTextColor(Color.parseColor("#000000"));

					break;
				case BluetoothService.STATE_LISTEN:
				case BluetoothService.STATE_NONE:
					// mTitle.setText(R.string.title_not_connected);
					// ivPrinterIcon.setBackgroundResource(R.drawable.not_log);
					tvPrinterMessage
							.setText(getString(R.string.not_connected_txt));
					tvPrinterMessage.setTextColor(Color.parseColor("#e34949"));

					break;
				}
				break;
			case MESSAGE_WRITE:
				break;
			case MESSAGE_READ:
				// ����Ч�ֽڻ���������һ���ַ���
				break;
			case MESSAGE_DEVICE_NAME:
				// ���������豸������
				mConnectedDeviceName = msg.getData().getString(DEVICE_NAME);
				// Toast.makeText(getApplicationContext(),
				// "���ӵ�" + mConnectedDeviceName, Toast.LENGTH_SHORT)
				// .show();
				alerter.alerterSuccessSimpleHere(mConnectedDeviceName
						+ " connected / " + mConnectedDeviceName
						+ " iracometse neza ");

				break;
			case MESSAGE_TOAST:
				// Toast.makeText(getApplicationContext(),
				// msg.getData().getString(TOAST), Toast.LENGTH_SHORT)
				// .show();

				alerter.alerterError("Error retry / ikibazo ongera ugerageze ");

				break;

			}

		}
	};

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (D)
			Log.d(TAG, "onActivityResult" + resultCode);
		switch (requestCode) {
		case REQUEST_CONNECT_DEVICE:
			// ��DeviceListActivity����һ���豸����
			if (resultCode == Activity.RESULT_OK) {

				// ��ȡ�豸��MAC��ַ
				String address = data.getExtras().getString(
						DeviceListActivity.EXTRA_DEVICE_ADDRESS);
				// �õ�BLuetoothDevice����
				if (address == null) {
					break;
				}
				// ��ȡ�豸��������
				BluetoothDevice device = mBluetoothAdapter
						.getRemoteDevice(address);
				// �������ӵ��豸
				myapplication.getmService().connect(device);
			}
			break;
		case REQUEST_ENABLE_BT:
			// �����󼤻������Ļر�
			if (resultCode == Activity.RESULT_OK) {
				// �������������õ�,���,����һ������Ự
				// ���������ɹ����������ʼ��UI
				setupChat();

			} else {
				// �û�û���������������
				Log.d(TAG, "BT not enabled");
				// Toast.makeText(this, R.string.bt_not_enabled_leaving,
				// Toast.LENGTH_SHORT).show();
				alerter.alerterError(getString(R.string.bt_not_enabled_leaving));
				finish();
			}

		}
	}

	@Override
	public void onBackPressed() {
		android.os.Process.killProcess(android.os.Process.myPid());
	}

	private void initialize() {
		// TODO Auto-generated method stub
		vars = new Vars(this);
		alerter = new Alerter(this);
		// cd = new ConnectionDetector(this);
		// phoneNumberText = (TextView) findViewById(R.id.tvPhoneNumber);
		tvPrinterMessage = (TextView) findViewById(R.id.tv_printer_message);
		cbPrint = (CheckBox) findViewById(R.id.cb_print);

		tvNationalId = (TextView) findViewById(R.id.tv_national_id);
		tvName = (TextView) findViewById(R.id.tv_name);
		tvTatalPremium = (TextView) findViewById(R.id.tv_tatal_premium);
		tvRemainingAmount = (TextView) findViewById(R.id.tv_remaining_amount);
		tvEligibility = (TextView) findViewById(R.id.tv_eligibility);
		tvHouseHoldCategory = (TextView) findViewById(R.id.tv_houseHold_category);
		tvNumberOfMembers = (TextView) findViewById(R.id.tv_number_of_members);
		tvProvince = (TextView) findViewById(R.id.tv_province);
		tvDistrict = (TextView) findViewById(R.id.tv_district);
		tvFinancialYear = (TextView) findViewById(R.id.tv_financial_year);

		etAmount = (EditText) findViewById(R.id.et_amount);
		tvInvoice = (TextView) findViewById(R.id.tv_invoice);
		etPin = (EditText) findViewById(R.id.et_pin);
		etMobile = (EditText) findViewById(R.id.et_mobile);

	}

	public void pay(View v) {

		int mod1 = 3000;
		int mod2 = 7000;

		if (etAmount.getText().toString().length() <= 0) {
			alerter.alerterError(Globals.enterAmountErrorMessage);
		} else if (etMobile.getText().toString().length() <= 0) {
			alerter.alerterError(Globals.phoneCheckErrorMessage);
		} else if (etMobile.getText().toString().length() != 10) {
			alerter.alerterError(Globals.invalidNumberErrorMessage);
		} else if (etPin.getText().toString().length() <= 0) {
			alerter.alerterError(Globals.enterAgentPinErrorMessage);
		} else {

			//

			if (canIPay(Integer.parseInt(etAmount.getText().toString()),
					Integer.parseInt(this.tvTatalPremium.getText().toString()),
					amountPaid)) {

				if (Integer.parseInt(etAmount.getText().toString()) % mod1 == 0
						|| Integer.parseInt(etAmount.getText().toString())
								% mod2 == 0) {
					LayoutInflater li = LayoutInflater
							.from(RssbPayActivity.this);
					View promptsView;
					promptsView = li.inflate(R.layout.confirm_rssb_payment,
							null);

					TextView tvClientName = (TextView) promptsView
							.findViewById(R.id.tv_client_name);
					tvClientName.setText(tvName.getText().toString());

					TextView tvNationalId = (TextView) promptsView
							.findViewById(R.id.tv_national_id);
					tvNationalId
							.setText(this.tvNationalId.getText().toString());

					TextView tvTataPremium = (TextView) promptsView
							.findViewById(R.id.tv_tatal_premium);
					tvTataPremium.setText(this.tvTatalPremium.getText()
							.toString());

					TextView tvRemainingAmount = (TextView) promptsView
							.findViewById(R.id.tv_remaining_amount);

					// int amount =
					// Integer.parseInt(etAmount.getText().toString());
					// int remainingAmount = this.remainingAmount - amount ;
					tvRemainingAmount.setText(this.tvRemainingAmount.getText()
							.toString());

					TextView tvAmount = (TextView) promptsView
							.findViewById(R.id.tv_amount);
					tvAmount.setText(etAmount.getText().toString());

					TextView tvHouseHoldCategory = (TextView) promptsView
							.findViewById(R.id.tv_houseHold_category);
					tvHouseHoldCategory.setText(this.tvHouseHoldCategory
							.getText().toString());

					TextView tvNumberOfMembers = (TextView) promptsView
							.findViewById(R.id.tv_number_of_members);
					tvNumberOfMembers.setText(this.tvNumberOfMembers.getText()
							.toString());

					TextView tvInvoice = (TextView) promptsView
							.findViewById(R.id.tv_invoice);
					tvInvoice.setText(this.tvInvoice.getText().toString());

					TextView tvClientMobile = (TextView) promptsView
							.findViewById(R.id.tv_client_mobile);
					tvClientMobile.setText(etMobile.getText().toString());

					TextView tvFinancialYear = (TextView) promptsView
							.findViewById(R.id.tv_financial_year);
					tvFinancialYear.setText(financialYear);

					TextView tvProvince = (TextView) promptsView
							.findViewById(R.id.tv_province);
					tvProvince.setText(province);

					TextView tvDistrict = (TextView) promptsView
							.findViewById(R.id.tv_district);
					tvDistrict.setText(district);

					AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
							RssbPayActivity.this);

					// set prompts.xml to alertdialog builder
					alertDialogBuilder.setView(promptsView);
					// set dialog message
					alertDialogBuilder
							.setCancelable(false)
							.setPositiveButton(Globals.yes,
									new DialogInterface.OnClickListener() {
										public void onClick(
												DialogInterface dialog, int id) {

											new RssbPayAsyncTask().execute();
										}
									})
							.setNegativeButton(Globals.no,
									new DialogInterface.OnClickListener() {
										public void onClick(
												DialogInterface dialog, int id) {
											dialog.cancel();
										}
									});

					// create alert dialog
					AlertDialog alertDialog = alertDialogBuilder.create();
					// show it
					alertDialog.show();
				} else {
					alerter.alerterError(Globals.enterAmountModuloErrorMessage);
				}

				//
			} else {
				Toast.makeText(getApplicationContext(), "not possible ",
						Toast.LENGTH_LONG).show();
			}

		}

		// } else {
		// alerter.alerterError(Globals.noInternetDetectedErrorMessage);
		// }

	}

	public class RssbPayAsyncTask extends AsyncTask<String, Void, String> {
		String json;
		public String theResult = null;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			pd = new ProgressDialog(RssbPayActivity.this);
			pd.setMessage(Globals.waitProgressMessage);
			pd.setCancelable(false);
			pd.setIndeterminate(true);
			pd.show();
		}

		@Override
		protected String doInBackground(String... arg) {

			StringBuilder builder = new StringBuilder();
			ConnectUtils connector = new ConnectUtils();
			HttpClient client = connector.getNewHttpClient();

			String urlString = null;

			// LOAD SERVER PREF

			try {

				// DEALER
				urlString = vars.server
						+ "/bio-api/androidRssb/rssbPay.php?agentNumber="
						+ URLEncoder.encode(
								vars.prefs.getString("agentPhone", null),
								"UTF-8")
						+ "&pin="
						+ URLEncoder
								.encode(etPin.getText().toString(), "UTF-8")
						+ "&householdNID="
						+ URLEncoder.encode(tvNationalId.getText().toString(),
								"UTF-8")
						+ "&payerPhone="
						+ URLEncoder.encode(etMobile.getText().toString(),
								"UTF-8")
						+ "&amount="
						+ URLEncoder.encode(etAmount.getText().toString(),
								"UTF-8")
						+ "&payerName="
						+ URLEncoder.encode(tvName.getText().toString(),
								"UTF-8")
						+ "&dealer="
						+ URLEncoder.encode(
								pref.getString(Globals.permission, null),
								"UTF-8") + "&version="
						+ URLEncoder.encode(Globals.version, "UTF-8")

						+ "&dealer_name="
						+ URLEncoder.encode(
								vars.prefs.getString(Globals.DEALERNAME, null),
								"UTF-8");

			} catch (UnsupportedEncodingException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

			HttpGet httpGet = new HttpGet(urlString);

			try {
				HttpResponse response = client.execute(httpGet);
				StatusLine statusLine = response.getStatusLine();
				int statusCode = statusLine.getStatusCode();
				if (statusCode == 200) {
					HttpEntity entity = response.getEntity();
					InputStream content = entity.getContent();
					BufferedReader reader = new BufferedReader(
							new InputStreamReader(content));
					String line;
					while ((line = reader.readLine()) != null) {
						builder.append(line);
					}
					json = builder.toString();

				} else {

				}
			} catch (ConnectTimeoutException e) {

				e.printStackTrace();
			} catch (ClientProtocolException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}

			return json;
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);

			try {
				// REMOVE DIALOG
				if (pd != null) {
					pd.dismiss();
					// b.setEnabled(true);
				}

				trans = new Transaction(result);

				// IF OUTPUT WAS RECEIVED FROM SERVER
				if (trans.getMessage() != null) {

					if (trans.getResult().equals("Success")) {

						LayoutInflater li = LayoutInflater
								.from(getApplicationContext());
						View promptsView;
						promptsView = li.inflate(
								R.layout.dialog_success_simple, null);

						TextView msgTxt = (TextView) promptsView
								.findViewById(R.id.success_simple_message);

						msgTxt.setText(trans.getMessage());

						// print here

						if (cbPrint.isChecked()
								&& myapplication.getmService().getState() == BluetoothService.STATE_CONNECTED) {
							// String title = "E-Tax";
							String mobicashText = "MobiCash LTD";
							String tinNumberText = "TIN No : 101858540";
							String mobicashPhoneText = "Tel: (250)787797979/SMS:8485";
							String bodifaHouseText = "Bodifa Mercy House,1st Floor";
							String KimihururaKigaliText = "Kimihurura,Kigali";
							String receiptNumberText = "Receipt Number : "
									+ trans.getTransactionid();
							String dateText = "Date : " + trans.getDateTime();
							String agentNameTitle = "Agent name";
							String agentName = pref
									.getString("agentName", null);
							String mobicashReferenceIdTitle = "Mobicash Reference ID";
							String clientChargesTitle = "Client Charges ";
							String totalAmountTitle = "Total Amount";

							String rraRefIdTitle = "RRA reference ID";

							String taxPayerNameTitle = "Tax payer Name";

							String taxTypeDescriptionTitle = "Tax type description";

							String mobicashRefIdTitle = "Mobicash reference ID";
							String clientTinTitle = "Client tin";

							String mobicashRefId = trans.getExtra1();

							String amountPaidTitle = "Total Amount";

							String thankUMessage = "Thank you for using MCash ";
							String keepEnjoyingMessage = "Keep Enjoying !";
							String line = "------------------------------";
							String dash = "##############################";
							String star = "******************************";

							String message = mobicashText + "\n"
									+ tinNumberText + "\n" + mobicashPhoneText
									+ "\n" + bodifaHouseText + "\n"
									+ KimihururaKigaliText + "\n\n"
									+ receiptNumberText + "\n" + dateText
									+ "\n" + line + "\n\n" + agentNameTitle
									+ "\n" + agentName + "\n\n"
									+ mobicashRefIdTitle + "\n" + mobicashRefId
									+ "\n\n" + "House hold Name" + "\n"
									+ tvName.getText().toString() + "\n\n"
									+ "House hold NID" + "\n"
									+ tvNationalId.getText().toString()
									+ "\n\n" + "Financial year" + "\n"
									+ financialYear + "\n\n" + "District"
									+ "\n" + district + "\n\n"
									+ "House hold category" + "\n"
									+ tvHouseHoldCategory.getText().toString()
									+ "\n\n" + "Number Of Members" + "\n"
									+ tvNumberOfMembers.getText().toString()
									+ "\n\n" + "Amount paid" + "\n"
									+ etAmount.getText().toString() + "\n\n"
									+ line + "\n" + thankUMessage + "\n"
									+ keepEnjoyingMessage
									+ "\n\n\n\n\n\n\n\n\n\n";

							sendMessage(message);
						}

						AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
								RssbPayActivity.this);

						// set prompts.xml to alertdENroialog builder
						alertDialogBuilder.setView(promptsView);
						// set dialog message
						alertDialogBuilder.setCancelable(false)
								.setPositiveButton("OK",
										new DialogInterface.OnClickListener() {
											public void onClick(
													DialogInterface dialog,
													int id) {

												Intent i = new Intent(
														getApplicationContext(),
														Services.class);
												startActivity(i);

											}
										});

						// create alert dialog
						AlertDialog alertDialog = alertDialogBuilder.create();
						// show it
						alertDialog.show();

					} else {
						// alerter.alerterError(trans.getResult());
						alerter.alerterError(trans.getError());
					}
				} else {
					alerter.alerterError(Globals.InternetErrorMessage);
				}

			} catch (Exception e) {

				if (pd != null) {
					pd.dismiss();
					// b.setEnabled(true);
				}
				alerter.alerterError(Globals.InternetErrorMessage);
			}

		}
	}

	public void layoutClick(View v) {
		InputMethodManager inputManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
		inputManager.hideSoftInputFromWindow(
				getCurrentFocus().getWindowToken(),
				InputMethodManager.HIDE_NOT_ALWAYS);
	}

	// CANCEL
	public void cancel(View v) {
		alerter.alerterConfirmCancelEtax(Globals.cancelRssbPaymentMessage);
	}

	@Override
	protected void onStop() {
		super.onStop();
		if (pd != null)
			pd.dismiss();
	}

	public boolean canIPay(int amount, int tatalPremium, int alreadyPaid) {

		if (amount % 3000 == 0) {
			tatalPremium += 9000;
		}

		if (amount % 7000 == 0 ) {
			tatalPremium += 21000;
		}

		int totalAmountPaid = amount + alreadyPaid;

		if (totalAmountPaid <= tatalPremium) {
			return true;
		} else {
			return false;
		}

	}

}
