package ez.pay.project;

import android.app.ActionBar;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.widget.TextView;

public class ShowTag extends BaseActivity {

	TextView tvCustomerType, tvPspId, tvDateTime, tvNfcTagWerial;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		ActionBar bar = getActionBar();
		bar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#f47701")));
		setContentView(R.layout.show_tag);
		initialize();

		Bundle extras = getIntent().getExtras();
		String userName;

		if (extras != null) {
			tvCustomerType.setText(extras.getString("customerType"));
			tvPspId.setText(extras.getString("PspId"));
			tvDateTime.setText(extras.getString("dateTime"));
			tvNfcTagWerial.setText(extras.getString("TagSerial"));
		}
	}

	private void initialize() {
		// TODO Auto-generated method stub
		tvCustomerType = (TextView) findViewById(R.id.tv_customer_type);
		tvPspId = (TextView) findViewById(R.id.tv_psp_id);
		tvDateTime = (TextView) findViewById(R.id.tv_date_time);
		tvNfcTagWerial = (TextView) findViewById(R.id.tv_nfc_tag_serial);

	}

	@Override
	public void onBackPressed() {

		Intent intent = new Intent(getApplicationContext(), GetClientTag.class);
		startActivity(intent);
	}

}
