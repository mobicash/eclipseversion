package ez.pay.project;

import java.util.Calendar;

import utils.Transaction;
import utils.Vars;
import android.app.ActionBar;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;

//import android.support.v4.app.Fragment;

public class AgentLogMenu extends BaseActivity {

	Vars vars;
	Transaction trans;
	Alerter alerter;
	SharedPreferences pref;

	boolean dayClick = false;
	boolean periodClick = false;
	String agentPin;
	LinearLayout llPerPeriode, llPerDay;
	EditText etSingleDate, etFromdate, etTodate;

	private Calendar cal;
	private static final int DATE_PICKER = 0;
	private static final int DATE_PICKER_TO = 1;
	private static final int DATE_PICKER_FROM = 2;

	private int fromYear, fromMonth, fromDay, toYear, toMonth, today, day,
			month, year;

	String date, fromDate, toDate, savedMonth, savedDay, savedToMonth,
			savedToDay, savedFromMonth, savedFromDay;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.agentl_log_menu);

		// action bar styling
		ActionBar bar = getActionBar();
		bar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#f47701")));

		init();

		Bundle extras;
		if (savedInstanceState == null) {
			extras = getIntent().getExtras();
			if (extras != null) {
				agentPin = extras.getString("agentPin");
			}
		}

	}

	public void onRadioClick(View view) {

		// llSearch.setVisibility(View.VISIBLE);

		switch (view.getId()) {

		case R.id.rb_per_day:
			dayClick = true;
			periodClick = false;
			llPerDay.setVisibility(View.VISIBLE);
			llPerPeriode.setVisibility(View.GONE);
			break;

		case R.id.rb_per_periode:
			dayClick = false;
			periodClick = true;
			llPerPeriode.setVisibility(View.VISIBLE);
			llPerDay.setVisibility(View.GONE);

			break;

		}
	}

	private void init() {
		// TODO Auto-generated method stub
		vars = new Vars(this);
		alerter = new Alerter(this);
		pref = PreferenceManager.getDefaultSharedPreferences(this);
		llPerPeriode = (LinearLayout) findViewById(R.id.ll_per_periode);
		llPerDay = (LinearLayout) findViewById(R.id.ll_per_day);

		etSingleDate = (EditText) findViewById(R.id.et_single_date);
		etFromdate = (EditText) findViewById(R.id.et_from_date);
		etTodate = (EditText) findViewById(R.id.et_to_date);

	}

	// CANCEL
	public void cancel(View v) {
		alerter.alerterConfirmCancel(Globals.cancelHistoricErrorMessage);
	}

	private DatePickerDialog.OnDateSetListener dateListener = new DatePickerDialog.OnDateSetListener() {
		public void onDateSet(DatePicker view, int year, int month, int day) {

			month = month + 1;

			if (month < 10) {
				savedMonth = "0" + month;
			} else {
				savedMonth = Integer.toString(month);
			}

			if (day < 10) {
				savedDay = "0" + Integer.toString(day);
			} else {
				savedDay = Integer.toString(day);
			}

			date = year + "-" + savedMonth + "-" + savedDay;
			etSingleDate.setText(savedMonth + "-" + savedDay + "-" + year);

		}
	};

	private DatePickerDialog.OnDateSetListener from_dateListener = new DatePickerDialog.OnDateSetListener() {
		public void onDateSet(DatePicker view, int year, int month, int day) {

			month = month + 1;

			if (month < 10) {
				savedFromMonth = "0" + month;
			} else {
				savedFromMonth = Integer.toString(month);
			}

			if (day < 10) {
				savedFromDay = "0" + Integer.toString(day);
			} else {
				savedFromDay = Integer.toString(day);
			}

			fromDate = year + "-" + savedFromMonth + "-" + savedFromDay;
			etFromdate
					.setText(savedFromMonth + "-" + savedFromDay + "-" + year);

		}
	};

	private DatePickerDialog.OnDateSetListener to_dateListener = new DatePickerDialog.OnDateSetListener() {
		public void onDateSet(DatePicker view, int year, int month, int day) {

			month = month + 1;

			if (month < 10) {
				savedToMonth = "0" + month;
			} else {
				savedToMonth = Integer.toString(month);
			}

			if (day < 10) {
				savedToDay = "0" + Integer.toString(day);
			} else {
				savedToDay = Integer.toString(day);
			}

			toDate = year + "-" + savedToMonth + "-" + savedToDay;
			etTodate.setText(savedToMonth + "-" + savedToDay + "-" + year);

		}
	};

	@Override
	protected Dialog onCreateDialog(int id) {
		switch (id) {

		case DATE_PICKER:

			Calendar c = Calendar.getInstance();
			year = c.get(Calendar.YEAR);
			month = c.get(Calendar.MONTH);
			day = c.get(Calendar.DAY_OF_MONTH);

			return new DatePickerDialog(this, dateListener, year, month, day);

		case DATE_PICKER_TO:

			final Calendar c1 = Calendar.getInstance();
			toYear = c1.get(Calendar.YEAR);
			toMonth = c1.get(Calendar.MONTH);
			today = c1.get(Calendar.DAY_OF_MONTH);

			return new DatePickerDialog(this, to_dateListener, toYear, toMonth,
					today);

		case DATE_PICKER_FROM:

			final Calendar c2 = Calendar.getInstance();
			fromYear = c2.get(Calendar.YEAR);
			fromMonth = c2.get(Calendar.MONTH);
			fromDay = c2.get(Calendar.DAY_OF_MONTH);

			return new DatePickerDialog(this, from_dateListener, fromYear,
					fromMonth, fromDay);
		}
		return null;
	}

	public void singleDateClick(View view) {
		showDialog(DATE_PICKER);
	}

	public void fromDateClick(View view) {
		showDialog(DATE_PICKER_FROM);
	}

	public void toDateClick(View view) {
		showDialog(DATE_PICKER_TO);
	}

	public void singleClick(View v) {

		Intent intent = new Intent(getApplicationContext(),
				CommissionActivity.class);
		intent.putExtra("listName", "Historic");
		intent.putExtra("buttonClick", "singleClick");
		intent.putExtra("date", date);
		intent.putExtra("agentPin", agentPin);
		startActivity(intent);

	}

	public void doubleClick(View v) {

		Intent intent = new Intent(getApplicationContext(),
				CommissionActivity.class);
		intent.putExtra("listName", "Historic");
		intent.putExtra("buttonClick", "doubleClick");
		intent.putExtra("fromDate", fromDate);
		intent.putExtra("toDate", toDate);
		intent.putExtra("agentPin", agentPin);
		startActivity(intent);

	}

}
