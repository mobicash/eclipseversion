package ez.pay.project;

import org.acra.ACRA;
import org.acra.annotation.ReportsCrashes;

import android.app.Application;

//http://mcash.ug:5984/
//http://mcash.ug:5984/acralyzer/_design/acralyzer/index.html#/dashboard/

@ReportsCrashes(
        formKey = "",
        		 formUri = "http://mcash.ug:5984/acra-mcash/_design/acra-storage/_update/report",
        		 //formUri = "https://mcash.cloudant.com/acra-mcash/_design/acra-storage/_update/report",
        reportType = org.acra.sender.HttpSender.Type.JSON,
        httpMethod = org.acra.sender.HttpSender.Method.PUT,
        		 formUriBasicAuthLogin = "mcash",
        	        formUriBasicAuthPassword = "mcash123"
        // Your usual ACRA configuration
        
        )
public class Mcash extends Application {
	
	 @Override
	  public final void onCreate() {
	    super.onCreate();
	    ACRA.init(this);
	  }

}
