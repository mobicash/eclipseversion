package ez.pay.project;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.conn.ConnectTimeoutException;

import services.BluetoothService;
import utils.ConnectUtils;
import utils.Transaction;
import utils.Vars;
import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class Airtime extends BaseActivity {

	public Vars vars;
	Alerter alerter;
	Transaction trans;

	// UI VARS
	EditText phoneNumber;
	EditText agentPin;
	EditText amount;
	protected String imei;

	Button bContinue;
	ProgressDialog pd;
	SharedPreferences pref;
	// Connection detector class
	// ConnectionDetector cd;
	Context context;

	String dealer;
	CheckBox cbPrint;

	// bluetooth

	private static final String TAG = "BluetoothPrint";
	private static final boolean D = true;

	// ��BluetoothService�����������Ϣ����
	public static final int MESSAGE_STATE_CHANGE = 1;
	public static final int MESSAGE_READ = 2;
	public static final int MESSAGE_WRITE = 3;
	public static final int MESSAGE_DEVICE_NAME = 4;
	public static final int MESSAGE_TOAST = 5;

	// BluetoothService�յ��Ĵ������ؼ�������
	public static final String DEVICE_NAME = "device_name";
	public static final String TOAST = "toast";

	// ����Ĵ���
	private static final int REQUEST_CONNECT_DEVICE = 1;
	private static final int REQUEST_ENABLE_BT = 2;
	private static final int REQUEST_Set = 3;

	// ���ӵ��豸������
	private String mConnectedDeviceName = null;
	// �ַ�����������������Ϣ
	private StringBuffer mOutStringBuffer;
	// ��������������
	private BluetoothAdapter mBluetoothAdapter = null;
	public static BluetoothService mService = null;

	private TextView mTitle;
	private EditText mOutEditText;
	private Button mPrintButton;
	private Button mClearButton;
	private Button mSetButton;
	MyApplication myapplication;
	TextView tvPrinterMessage;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.airtime);
		ActionBar bar = getActionBar();
		bar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#f47701")));
		vars = new Vars(this);
		alerter = new Alerter(this);
		pref = PreferenceManager.getDefaultSharedPreferences(this);
		dealer = pref.getString(Globals.permission, null);
		myapplication = (MyApplication) getApplication();
		// cd = new ConnectionDetector(this);
		context = getApplicationContext();
		init();
		this.getWindow().setSoftInputMode(
				WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

		// �õ����ص�������
		mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

		if (mBluetoothAdapter == null) {
			Toast.makeText(this, "��������ʹ�ã�", Toast.LENGTH_LONG).show();
			finish();
			return;
		}

	}

	public void printClick(View v) {
		if (((CheckBox) v).isChecked()) {

			if (myapplication.getmService().getState() != BluetoothService.STATE_CONNECTED) {
				// Toast.makeText(this, R.string.not_connected,
				// Toast.LENGTH_SHORT).show();
				try {

					Log.e("printClick ",
							"********printClick mService.getState() != BluetoothService.STATE_CONNECTED *******");

					Intent serverIntent = new Intent(this,
							DeviceListActivity.class);
					startActivityForResult(serverIntent, REQUEST_CONNECT_DEVICE);

					return;
				} catch (Exception e) {

				}
			}

		}
	}

	@Override
	public void onStart() {
		super.onStart();
		Log.e("Log ", "********onStart*******");
		if (D)
			// Log.e(TAG, "++ ON START ++");
			// �ж������Ƿ������������û�п���������������
			if (!mBluetoothAdapter.isEnabled()) {
				Log.e("printClick ",
						"********onStart !mBluetoothAdapter.isEnabled() *******");
				Intent enableIntent = new Intent(
						BluetoothAdapter.ACTION_REQUEST_ENABLE);
				startActivityForResult(enableIntent, REQUEST_ENABLE_BT);
			} else {
				if (myapplication.getmService() == null) {
					Log.e("printClick ", "********setupChat *******");
					setupChat();
				}

			}
	}

	@Override
	public synchronized void onResume() {
		super.onResume();
		Log.e("Log ", "********onResume*******");
		if (D)
			// Log.e(TAG, "+ ON RESUME +");
			if (myapplication.getmService() != null) {
				if (myapplication.getmService().getState() == BluetoothService.STATE_NONE) {
					Log.e("printClick ",
							"********onResume mService.getState() == BluetoothService.STATE_NONE *******");
					myapplication.getmService().start();
				} else {

					try {

						if (myapplication.getmService().getState() == BluetoothService.STATE_CONNECTED) {
							// ivPrinterIcon.setBackgroundResource(R.drawable.log);
							tvPrinterMessage
									.setText(getString(R.string.connected_txt));
							tvPrinterMessage.setTextColor(Color
									.parseColor("#398f14"));
							Log.e("printClick ",
									"********onResume mService.getState() == BluetoothService.STATE_CONNECTED *******");
						} else {
							// ivPrinterIcon.setBackgroundResource(R.drawable.not_log);
							tvPrinterMessage
									.setText(getString(R.string.not_connected_txt));
							tvPrinterMessage.setTextColor(Color
									.parseColor("#e34949"));
							Log.e("printClick ",
									"********onResume mService.getState() != BluetoothService.STATE_CONNECTED *******");
						}

					} catch (Exception e) {

						alerter.alerterError("Error open bluetooth in settings ");

					}

				}

			}

	}

	private void setupChat() {
		Log.d(TAG, "SetupChat");

		// ��ʼ��BluetoothServiceִ����������
		myapplication.setmService(new BluetoothService(this, mHandler));

	}

	private void sendMessage(String message) {
		// ������ڳ�������

		// ���ʵ��Ҫ���͵Ķ���
		if (message.length() > 0) {
			// �õ���Ϣ�ֽڲ��Ҹ���BluetoothService
			byte[] send;
			try {
				send = message.getBytes("GB2312");
			} catch (UnsupportedEncodingException e) {
				send = message.getBytes();
			}
			// mService.write(send,10);
			myapplication.getmService().write(send, 0, send.length);
		}
	}

	@Override
	public synchronized void onPause() {
		super.onPause();
		if (D)
			Log.e(TAG, "- ON PAUSE -");
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		// Stop the Bluetooth services
		if (myapplication.getmService() != null)
			myapplication.getmService().stop();
		if (D)
			Log.e(TAG, "--- ON DESTROY ---");
	}

	// ���±������ұ�״̬�Ͷ�д״̬��Handler
	private final Handler mHandler = new Handler() {

		@Override
		public void handleMessage(Message msg) {
			switch (msg.what) {
			case MESSAGE_STATE_CHANGE:
				if (D)
					Log.i(TAG, "MESSAGE_STATE_CHANGE:" + msg.arg1);
				switch (msg.arg1) {
				case BluetoothService.STATE_CONNECTED:
					// mTitle.setText(R.string.title_connected_to);
					// mTitle.append(mConnectedDeviceName);
					// ivPrinterIcon.setBackgroundResource(R.drawable.log);

					tvPrinterMessage.setText(getString(R.string.connected_txt));
					tvPrinterMessage.setTextColor(Color.parseColor("#398f14"));
					Log.e("printClick ",
							"********handleMessage STATE_CONNECTED *******");

					break;
				case BluetoothService.STATE_CONNECTING:
					// mTitle.setText(R.string.title_connecting);
					tvPrinterMessage
							.setText(getString(R.string.connecting_txt));
					tvPrinterMessage.setTextColor(Color.parseColor("#000000"));
					Log.e("printClick ",
							"********handleMessage CONNECTING *******");

					break;
				case BluetoothService.STATE_LISTEN:
				case BluetoothService.STATE_NONE:
					// mTitle.setText(R.string.title_not_connected);
					// ivPrinterIcon.setBackgroundResource(R.drawable.not_log);
					tvPrinterMessage
							.setText(getString(R.string.not_connected_txt));
					tvPrinterMessage.setTextColor(Color.parseColor("#e34949"));

					Log.e("printClick ",
							"********handleMessage NOT_CONNECTED *******");

					break;
				}
				break;
			case MESSAGE_WRITE:
				break;
			case MESSAGE_READ:
				// ����Ч�ֽڻ���������һ���ַ���
				break;
			case MESSAGE_DEVICE_NAME:
				// ���������豸������
				mConnectedDeviceName = msg.getData().getString(DEVICE_NAME);
				// Toast.makeText(getApplicationContext(),
				// "���ӵ�" + mConnectedDeviceName, Toast.LENGTH_SHORT)
				// .show();
				alerter.alerterSuccessSimpleHere(mConnectedDeviceName
						+ " connected / " + mConnectedDeviceName
						+ " iracometse neza ");

				break;
			case MESSAGE_TOAST:
				// Toast.makeText(getApplicationContext(),
				// msg.getData().getString(TOAST), Toast.LENGTH_SHORT)
				// .show();

				alerter.alerterError("Error retry / ikibazo ongera ugerageze ");

				break;

			}

		}
	};

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (D)
			Log.d(TAG, "onActivityResult" + resultCode);
		switch (requestCode) {
		case REQUEST_CONNECT_DEVICE:
			// ��DeviceListActivity����һ���豸����
			if (resultCode == Activity.RESULT_OK) {

				// ��ȡ�豸��MAC��ַ
				String address = data.getExtras().getString(
						DeviceListActivity.EXTRA_DEVICE_ADDRESS);
				// �õ�BLuetoothDevice����
				if (address == null) {
					break;
				}
				// ��ȡ�豸��������
				BluetoothDevice device = mBluetoothAdapter
						.getRemoteDevice(address);
				// �������ӵ��豸
				myapplication.getmService().connect(device);
			}
			break;
		case REQUEST_ENABLE_BT:
			// �����󼤻������Ļر�
			if (resultCode == Activity.RESULT_OK) {
				// �������������õ�,���,����һ������Ự
				// ���������ɹ����������ʼ��UI
				setupChat();

			} else {
				// �û�û���������������
				Log.d(TAG, "BT not enabled");
				// Toast.makeText(this, R.string.bt_not_enabled_leaving,
				// Toast.LENGTH_SHORT).show();
				alerter.alerterError(getString(R.string.bt_not_enabled_leaving));
				finish();
			}

			/*
			 * case REQUEST_Set:
			 * 
			 * if(resultCode==Activity.RESULT_OK) {
			 * 
			 * byte a[]=data.getExtras().getByteArray("data");
			 * mService.write(a,0,a.length); Toast.makeText(this, "���óɹ���",
			 * Toast.LENGTH_SHORT).show(); } break;
			 */
		}
	}

	@Override
	public void onBackPressed() {
		android.os.Process.killProcess(android.os.Process.myPid());
	}

	private void init() {
		// TODO Auto-generated method stub
		// SET UI VARS
		phoneNumber = (EditText) findViewById(R.id.airtime_phoneNumber);
		agentPin = (EditText) findViewById(R.id.airtime_agentPin);
		amount = (EditText) findViewById(R.id.airtime_amount);
		bContinue = (Button) findViewById(R.id.btContinue);

		tvPrinterMessage = (TextView) findViewById(R.id.tv_printer_message);
		cbPrint = (CheckBox) findViewById(R.id.cb_print);

	}

	// BUY
	public void pay(View v) {
		// if (cd.isConnectingToInternet()) {
		if (phoneNumber.getText().toString() == null
				|| phoneNumber.getText().toString().isEmpty()) {
			alerter.alerterError(Globals.phoneCheckErrorMessage);
		} else if (phoneNumber.getText().toString().length() != 10) {
			alerter.alerterError(Globals.invalidNumberErrorMessage);
		} else if (amount.getText().toString() == null
				|| amount.getText().toString().isEmpty()) {
			alerter.alerterError(Globals.enterAmountErrorMessage);
		} else if (agentPin.getText().toString() == null
				|| agentPin.getText().toString().isEmpty()) {
			alerter.alerterError(Globals.enterAgentPinErrorMessage);
		} else if (agentPin.getText().toString().length() < 4) {
			alerter.alerterError(Globals.invalidPinErrorMessage);
		} else {
			confirmAirtime();
		}
		// } else {
		// alerter.alerterError(Globals.noInternetDetectedErrorMessage);
		// }

	}

	// CANCEL
	public void cancel(View v) {
		alerter.alerterConfirmCancel(Globals.cancelAirtimeErrorMessage);
	}

	public void layoutClick(View v) {
		InputMethodManager inputManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
		inputManager.hideSoftInputFromWindow(
				getCurrentFocus().getWindowToken(),
				InputMethodManager.HIDE_NOT_ALWAYS);
	}

	public void confirmAirtime() {
		LayoutInflater li = LayoutInflater.from(Airtime.this);
		View promptsView;
		promptsView = li.inflate(R.layout.confirm_dialog_airtime, null);

		TextView tvPhone = (TextView) promptsView
				.findViewById(R.id.confirm_dialog_phone);
		TextView tvAmount = (TextView) promptsView
				.findViewById(R.id.confirm_dialog_amount);

		tvPhone.setText(phoneNumber.getText().toString());
		tvAmount.setText(amount.getText().toString());

		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
				Airtime.this);

		// set prompts.xml to alertdialog builder
		alertDialogBuilder.setView(promptsView);
		// set dialog message
		alertDialogBuilder
				.setCancelable(false)
				.setPositiveButton(Globals.yes,
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {

								// Toast.makeText(
								// Airtime.this,
								// "Phone :"
								// + phoneNumber.getText()
								// .toString() + " amount :"
								// + amount.getText().toString()
								// + " agent pin :"
								// +
								// agentPin.getText().toString()+" , agent number :"+vars.prefs
								// .getString("agentPhone", null),
								// Toast.LENGTH_SHORT).show();

								new BuyAirtimeAsyncTask().execute(phoneNumber
										.getText().toString(), amount.getText()
										.toString(), agentPin.getText()
										.toString());
							}
						})
				.setNegativeButton(Globals.no,
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								dialog.cancel();
							}
						});

		// create alert dialog
		AlertDialog alertDialog = alertDialogBuilder.create();
		// show it
		alertDialog.show();
	}

	// -----------------------------------------------------------------------------------------------------
	public class BuyAirtimeAsyncTask extends AsyncTask<String, Void, String> {

		public String theResult;
		public String action;

		String topToNumber;
		String topAmount;
		String agentPin;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			pd = new ProgressDialog(Airtime.this);
			pd.setTitle(Globals.airtimePurchaceProgressMessage);
			pd.setMessage("please wait / ihangane ...");
			pd.setCancelable(false);
			pd.setIndeterminate(true);
			pd.show();
		}

		@Override
		// protected String doInBackground(Void... arg0) {
		protected String doInBackground(String... params) {

			topToNumber = params[0];
			topAmount = params[1];
			agentPin = params[2];

			//

			StringBuilder builder = new StringBuilder();
			ConnectUtils connector = new ConnectUtils();
			HttpClient client = connector.getNewHttpClient();

			String result = "FAILED";

			String sendString = null;

			try {

				// DEALER
				sendString = vars.server
						+ "/bio-api/androidAirtime/androidAirtimeFinal.php?"
						+ "agentNumber="
						+ URLEncoder.encode(
								vars.prefs.getString("agentPhone", null),
								"UTF-8")
						+ "&agentPin="
						+ URLEncoder.encode(agentPin, "UTF-8")
						+ "&numberToLoad="
						+ URLEncoder.encode(topToNumber, "UTF-8")
						+ "&amount="
						+ URLEncoder.encode(topAmount, "UTF-8")
						+ "&dealer="
						+ URLEncoder.encode(dealer, "UTF-8")
						+ "&version="
						+ URLEncoder.encode(Globals.version, "UTF-8")
						+ "&dealer_name="
						+ URLEncoder.encode(
								vars.prefs.getString(Globals.DEALERNAME, null),
								"UTF-8");

			} catch (Exception e) {

			}

			HttpGet httpGet = new HttpGet(sendString);

			try {
				HttpResponse response = client.execute(httpGet);
				StatusLine statusLine = response.getStatusLine();
				int statusCode = statusLine.getStatusCode();
				if (statusCode == 200) {
					HttpEntity entity = response.getEntity();
					InputStream content = entity.getContent();
					BufferedReader reader = new BufferedReader(
							new InputStreamReader(content));
					String line;
					while ((line = reader.readLine()) != null) {
						builder.append(line);
					}
					result = builder.toString();

				} else {

				}
			} catch (ConnectTimeoutException e) {
				theResult = "timeout";
				// theResult = "timeout";
				e.printStackTrace();
			} catch (ClientProtocolException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}

			return result;

			//

		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			// REMOVE DIALOG
			if (pd != null) {
				pd.dismiss();
				// b.setEnabled(true);
			}

			trans = new Transaction(result);

			// IF OUTPUT WAS RECEIVED FROM SERVER
			if (trans.getResult() != null && !trans.getResult().isEmpty()) {
				// if (result.contains("ccess")) {
				if (trans.getResult().contains("ccess")) {

					LayoutInflater li = LayoutInflater
							.from(getApplicationContext());
					View promptsView;
					promptsView = li.inflate(R.layout.dialog_success_simple,
							null);

					TextView msgTxt = (TextView) promptsView
							.findViewById(R.id.success_simple_message);

					msgTxt.setText(trans.getResult());

					if (cbPrint.isChecked()
							&& myapplication.getmService().getState() == BluetoothService.STATE_CONNECTED) {

						String title = "Airtime";

						String mobicashText = "MobiCash LTD";
						String tinNumberText = "TIN No : 101858540";
						String mobicashPhoneText = "Tel: (250)787689185/SMS:8485";
						String bodifaHouseText = "Bodifa Mercy House,5th Floor";
						String KimihururaKigaliText = "Kimihurura,Kigali";
						String receiptNumberText = "Receipt Number : ";
						String dateText = "Date : ";
						String agentNameTitle = "Agent name";
						String agentName = pref.getString("agentName", null);

						String mobileTitle = getString(R.string.mobile_no_text);
						String mobile = topToNumber;

						String amountTitle = getString(R.string.units);
						String amount = topAmount;

						String thankUMessage = "Thank you for using MCash ";
						String keepEnjoyingMessage = "Keep Enjoying !";
						String dash = "##############################";
						String star = "******************************";
						String line = "------------------------------";

						int width = 30;

						int padSize = width - title.length();
						int padStart = title.length() + padSize / 2;

						title = String.format("%" + padStart + "s", title);
						title = String.format("%-" + width + "s", title);

						String message = mobicashText + "\n" + tinNumberText
								+ "\n" + mobicashPhoneText + "\n"
								+ bodifaHouseText + "\n" + KimihururaKigaliText
								+ "\n\n" + receiptNumberText + "\n" + dateText
								+ "\n" + line + "\n\n\n" + mobileTitle + "\n"
								+ mobile + "\n\n" + amountTitle + "\n" + amount
								+ "\n\n" + line + "\n" + thankUMessage + "\n"
								+ keepEnjoyingMessage + "\n\n\n\n\n\n\n\n\n\n";

						sendMessage(message);
					}

					AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
							Airtime.this);

					// set prompts.xml to alertdENroialog builder
					alertDialogBuilder.setView(promptsView);
					// set dialog message
					alertDialogBuilder.setCancelable(false).setPositiveButton(
							"OK", new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog,
										int id) {
									Intent i = new Intent();
									i.setClass(getApplicationContext(),
											Services.class);
									startActivity(i);
								}
							});

					// create alert dialog
					AlertDialog alertDialog = alertDialogBuilder.create();
					// show it
					alertDialog.show();

				} else {
					// alerter.alerterError(trans.getResult());
					alerter.alerterError(trans.getError());
				}
			} else {
				alerter.alerterError(Globals.InternetErrorMessage);
			}
		}
	}

	@Override
	protected void onStop() {
		super.onStop();
		if (pd != null)
			pd.dismiss();
	}

}
