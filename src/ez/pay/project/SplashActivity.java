package ez.pay.project;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

public class SplashActivity extends Activity {
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.splash);
		ActionBar bar = getActionBar();
		bar.hide();
		int secondsDelayed = 2;
		new Handler().postDelayed(new Runnable() {
			public void run() {
				startActivity(new Intent(SplashActivity.this, First_time.class));
				finish();
			}
		}, secondsDelayed * 1000);
	}
}
