package ez.pay.project;

//Bouquet model

public class ElectricityInfo {
	private String name;
	private String meterNumber;
	private String amount;
	private String error;
	private String result;
	
	public ElectricityInfo(String name, String meterNumber, String amount,
			String error, String result) {
		super();
		this.name = name;
		this.meterNumber = meterNumber;
		this.amount = amount;
		this.error = error;
		this.result = result;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getMeterNumber() {
		return meterNumber;
	}
	public void setMeterNumber(String meterNumber) {
		this.meterNumber = meterNumber;
	}
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	public String getError() {
		return error;
	}
	public void setError(String error) {
		this.error = error;
	}
	public String getResult() {
		return result;
	}
	public void setResult(String result) {
		this.result = result;
	}

}
