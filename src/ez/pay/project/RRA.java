package ez.pay.project;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.conn.ConnectTimeoutException;

import utils.ConnectUtils;
import utils.TaxParser;
import utils.Vars;
import android.app.ActionBar;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;

public class RRA extends BaseActivity {

	EditText rraRefId;
	Alerter alerter;
	// ConnectionDetector cd;
	Vars vars;
	ProgressDialog pd;
	SharedPreferences pref;
	TextView tvTitle;
	String action;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.rra_layout);
		ActionBar bar = getActionBar();
		bar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#f47701")));
		pref = PreferenceManager.getDefaultSharedPreferences(this);
		initialize();
		// hide keyboard
		this.getWindow().setSoftInputMode(
				WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

		Intent iin = getIntent();
		Bundle b = iin.getExtras();

		if (b != null) {

			action = (String) b.get("action");

			if (action.equals("RRA")) {
				this.setTitle(getString(R.string.e_tax_menu_text));
				tvTitle.setText(getString(R.string.e_tax_menu_text));
			}

			if (action.equals("Pension")) {
				this.setTitle("RSSB: Pension / Medical");
				tvTitle.setText("RSSB: Pension / Medical");
			}

		}

	}

	private void initialize() {
		// TODO Auto-generated method stub
		rraRefId = (EditText) findViewById(R.id.etRefId);
		alerter = new Alerter(this);
		// cd = new ConnectionDetector(this);
		vars = new Vars(this);
		tvTitle = (TextView) findViewById(R.id.tv_title);

	}

	public void check(View v) {
		String refId = rraRefId.getText().toString();

		// if (cd.isConnectingToInternet()) {
		if (refId.length() <= 0) {
			alerter.alerterError(Globals.rraRefIdErrorMessage);
		} else {
			new LoadInfo().execute(refId);
		}

		// } else {
		// alerter.alerterError(Globals.noInternetDetectedErrorMessage);
		// }

	}

	private class LoadInfo extends AsyncTask<String, Void, String> {
		private static final String TAG = "PostFetcher";
		String theResult = "NO CONNECTION MADE";
		Tax tax = new Tax();
		String refId;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			pd = new ProgressDialog(RRA.this);
			pd.setTitle(Globals.refIdCheckProgressMessage);
			pd.setMessage(Globals.waitProgressMessage);
			pd.setCancelable(false);
			pd.setIndeterminate(true);
			pd.show();
		}

		@Override
		protected String doInBackground(String... params) {

			// Create an HTTP client
			refId = params[0];

			StringBuilder builder = new StringBuilder();
			ConnectUtils connector = new ConnectUtils();
			HttpClient client = connector.getNewHttpClient();

			String result = "FAILED";
			String urlString = null;

			// LOAD SERVER PREF

			try {
				urlString = vars.server
						+ "/bio-api/androidTax/getRefDetails.php?ref_number="
						+ URLEncoder.encode(refId, "UTF-8")
						+ "&dealer="
						+ URLEncoder.encode(
								pref.getString(Globals.permission, null),
								"UTF-8") + "&version="
						+ URLEncoder.encode(Globals.version, "UTF-8")

						+ "&dealer_name="
						+ URLEncoder.encode(
								vars.prefs.getString(Globals.DEALERNAME, null),
								"UTF-8");

				

			} catch (UnsupportedEncodingException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

			HttpGet httpGet = new HttpGet(urlString);

			try {
				HttpResponse response = client.execute(httpGet);
				StatusLine statusLine = response.getStatusLine();
				int statusCode = statusLine.getStatusCode();
				if (statusCode == 200) {
					HttpEntity entity = response.getEntity();
					InputStream content = entity.getContent();
					BufferedReader reader = new BufferedReader(
							new InputStreamReader(content));
					String line;
					while ((line = reader.readLine()) != null) {
						builder.append(line);
					}
					result = builder.toString();

				} else {

				}
			} catch (ConnectTimeoutException e) {

				e.printStackTrace();
			} catch (ClientProtocolException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}

			return result;
		}

		@Override
		protected void onPostExecute(String json) {

			try {

				TaxParser taxParser = new TaxParser();

				Tax tax = taxParser.getTax(json);

				if (tax.getResult().equals("Success")
						&& tax.getError().equals("No error")) {
					// displayed info
					Intent intent = new Intent(RRA.this, RRAPay.class);
					intent.putExtra(Globals.result, tax.getResult());
					intent.putExtra(Globals.bankName, tax.getBankName());
					intent.putExtra(Globals.rraRef, tax.getRraRefId());
					intent.putExtra(Globals.tin, tax.getTin());
					intent.putExtra(Globals.taxPayerName, tax.getTaxPayerName());
					intent.putExtra(Globals.taxTypeDesc,
							tax.getTaxTypeDescription());
					intent.putExtra(Globals.taxCentreNo, tax.getTaxCenterNo());
					intent.putExtra(Globals.taxTypeNo, tax.getTaxTypeNo());
					intent.putExtra(Globals.assessNo, tax.getAssessNo());
					intent.putExtra(Globals.rraOriginNo, tax.getRraOriginNo());
					intent.putExtra(Globals.amountToPay, tax.getAmountToPay());
					intent.putExtra(Globals.decId, tax.getDecId());
					intent.putExtra(Globals.decDate, tax.getDecDate());
					intent.putExtra(Globals.refId, refId);
					intent.putExtra("action", action);

					startActivity(intent);

				} else {
					alerter.alerterError(tax.getError());
				}

				pd.dismiss();

			} catch (Exception e) {

				if (pd != null)
					pd.dismiss();

				alerter.alerterError(Globals.InternetErrorMessage);

			}

		}
	}

	public void layoutClick(View v) {
		InputMethodManager inputManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
		inputManager.hideSoftInputFromWindow(
				getCurrentFocus().getWindowToken(),
				InputMethodManager.HIDE_NOT_ALWAYS);
	}

	// CANCEL
	public void cancel(View v) {
		alerter.alerterConfirmCancelEtax(Globals.cancelEtaxErrorMessage);
	}

	public void airtimeClick(View v) {

		if (rraRefId.getText().length() > 0) {
			alerter.alerterToAirtime(Globals.cancelEtaxErrorMessage);
		} else {
			Intent intent = new Intent(RRA.this, Airtime.class);
			startActivity(intent);
		}

	}

	public void electricityClick(View v) {

		if (rraRefId.getText().length() > 0) {
			alerter.alerterToElectricity(Globals.cancelEtaxErrorMessage);
		} else {
			Intent intent = new Intent(RRA.this, Electricity.class);
			startActivity(intent);
		}

	}

	public void dstvClick(View v) {

		if (rraRefId.getText().length() > 0) {
			alerter.alerterToDstv(Globals.cancelEtaxErrorMessage);
		} else {
			Intent intent = new Intent(RRA.this, Dstv.class);
			startActivity(intent);
		}

	}

	public void canalClick(View v) {

		if (rraRefId.getText().length() > 0) {
			alerter.alerterToCanal(Globals.cancelEtaxErrorMessage);
		} else {
			Intent intent = new Intent(RRA.this, Canal.class);
			startActivity(intent);
		}

	}

	public void startimesClick(View v) {

		if (rraRefId.getText().length() > 0) {
			alerter.alerterToStartimes(Globals.cancelEtaxErrorMessage);
		} else {
			Intent intent = new Intent(RRA.this, Startimes.class);
			startActivity(intent);
		}

	}

	public void checkBalanceClick(View v) {

		if (rraRefId.getText().length() > 0) {
			alerter.alerterToCheckBalance(Globals.cancelEtaxErrorMessage);
		} else {
			Intent intent = new Intent(RRA.this, AgentFloatCheck.class);
			startActivity(intent);
		}

	}

	@Override
	protected void onStop() {
		super.onStop();
		if (pd != null)
			pd.dismiss();
	}

}
