package ez.pay.project;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.conn.ConnectTimeoutException;
import org.json.JSONObject;

import utils.AlarmManagerBroadcastReceiver;
import utils.ClientInfoParser;
import utils.ConnectUtils;
import utils.Transaction;
import utils.Vars;
import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.DownloadManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

public class First_time extends Activity {
	ProgressDialog pd;
	EditText agent;
	EditText agentPin;
	TextView txt;
	String imei, activityIdentify;
	private ProgressBar progressBar;
	SharedPreferences pref;
	Editor edit;
	JSONObject json;
	String server;
	// Connection detector class
	// ConnectionDetector cd;

	EditText settings_pwd;

	Alerter alerter;
	Context context;
	public Vars vars;
	Transaction trans;
	String clientType;
	private AlarmManagerBroadcastReceiver alarmManagerBroadcastReceiver;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.first_time_layout);

		ActionBar bar = getActionBar();
		bar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#f47701")));

		pref = PreferenceManager.getDefaultSharedPreferences(this);
		context = getApplicationContext();
		activityIdentify = pref.getString(Globals.permission, null);
		alerter = new Alerter(this);

		if (activityIdentify != null) {
			Intent intent = new Intent(First_time.this, GetClientTag.class);
			startActivity(intent);
		}

		TelephonyManager tm = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
		// CHECKS INTERNET CONNECTION
		// cd = new ConnectionDetector(getApplicationContext());
		vars = new Vars(this);
		agent = (EditText) findViewById(R.id.first_time_agtNum);
		// agent.setText("0722000801");
		agentPin = (EditText) findViewById(R.id.first_time_agtPin);
		// agentPin.setText("898989");
		// get IMEI and
		imei = tm.getDeviceId();
		// get The Phone Number

		edit = pref.edit();
		edit.putString("server", Globals.rwanda);
		edit.commit();
		server = pref.getString("server", null);
		clientType = pref.getString(Globals.permission, null);

		// hide keyboard
		this.getWindow().setSoftInputMode(
				WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

		// new CheckVersionAsyncTask().execute(Globals.version);
		alarmManagerBroadcastReceiver = new AlarmManagerBroadcastReceiver();
		alarmManagerBroadcastReceiver.setAlarm(getApplicationContext());

	}

	public void check(View v) {

		// if (cd.isConnectingToInternet()) {
		if (agent.getText().toString() == null
				|| agent.getText().toString().isEmpty()) {
			alerter.alerterError(Globals.phoneCheckErrorMessage);
		} else if (agentPin.getText().toString() == null
				|| agentPin.getText().toString().isEmpty()) {
			alerter.alerterError(Globals.enterAgentPinErrorMessage);
		} else {

			CheckInstall check = new CheckInstall(imei, agent.getText()
					.toString(), agentPin.getText().toString());
			check.execute();

		}

	}

	private void showAppVersion() {
		// TODO Auto-generated method stub

		LayoutInflater li = LayoutInflater.from(getApplicationContext());
		View promptsView;
		promptsView = li.inflate(R.layout.dialog_success_simple, null);

		TextView msgTxt = (TextView) promptsView
				.findViewById(R.id.success_simple_message);

		TextView tvSuccessSimpleTitle = (TextView) promptsView
				.findViewById(R.id.tvSuccessSimpleTitle);
		tvSuccessSimpleTitle.setText("Version / Veriziyo");

		msgTxt.setText("This app is version " + Globals.version
				+ " / iyi sisitemu ni viriziyo " + Globals.version);

		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
				First_time.this);

		// set prompts.xml to alertdENroialog builder
		alertDialogBuilder.setView(promptsView);
		// set dialog message
		alertDialogBuilder.setCancelable(false).setPositiveButton("OK",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						dialog.cancel();
					}
				});

		// create alert dialog
		AlertDialog alertDialog = alertDialogBuilder.create();
		// show it
		alertDialog.show();

	}

	// // ALERT GENERATOR
	// public void alerter(String title, String message,
	// final Class<Agent_home> class1) {
	//
	// // public void onClick(View arg0) {
	// // loadEm();
	// AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
	//
	// // set title
	// alertDialogBuilder.setTitle(title);
	//
	// // set dialog message
	// alertDialogBuilder.setMessage(message).setCancelable(false)
	// .setPositiveButton("OK", new DialogInterface.OnClickListener() {
	// public void onClick(DialogInterface dialog, int id) {
	// // if this button is clicked, close
	// // current activity
	// // MainActivity.this.finish();
	//
	// Intent i = new Intent();
	// i.setClass(getApplicationContext(), class1);
	// startActivity(i);
	//
	// // dialog.cancel();
	// }
	// });
	//
	// // create alert dialog
	// AlertDialog alertDialog = alertDialogBuilder.create();
	//
	// // show it
	// alertDialog.show();
	// // }
	// }

	// ALERT GENERATOR
	public void alerter(String title, String message) {

		// public void onClick(View arg0) {
		// loadEm();
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);

		// set title
		alertDialogBuilder.setTitle(title);

		// set dialog message
		alertDialogBuilder.setMessage(message).setCancelable(false)
				.setPositiveButton("OK", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						// if this button is clicked, close
						// current activity
						// MainActivity.this.finish();
						dialog.cancel();
					}
				});

		// create alert dialog
		AlertDialog alertDialog = alertDialogBuilder.create();

		// show it
		alertDialog.show();
		// }
	}

	// ------------------------------------------------------------------
	public class CheckInstall extends AsyncTask<Void, Integer, String> {

		String imei;
		String agent;
		String pin;
		String theResult;

		CheckInstall(String imei, String agent, String agentPin) {

			this.imei = imei;
			this.agent = agent;
			this.pin = agentPin;
		}

		@Override
		protected void onPreExecute() {
			pd = new ProgressDialog(First_time.this);
			pd.setMessage("please wait");
			pd.setCancelable(false);
			pd.setIndeterminate(true);
			pd.show();
			super.onPreExecute();
		}

		@Override
		protected String doInBackground(Void... arg0) {

			StringBuilder builder = new StringBuilder();
			ConnectUtils connector = new ConnectUtils();
			HttpClient client = connector.getNewHttpClient();

			String result = "FAILED";

			// LOAD SERVER PREF
			pref = PreferenceManager
					.getDefaultSharedPreferences(First_time.this);
			// edit = pref.edit();
			server = pref.getString("server", null);

			String sendString = server + "checkAgent.php?mobile=" + agent
					+ "&action=checkInstall" + "&imei=" + imei + "&clientPin="
					+ pin + "&firstUse=now&version=" + Globals.version;

			// Log.e("link", sendString);

			HttpGet httpGet = new HttpGet(sendString);

			try {
				HttpResponse response = client.execute(httpGet);
				StatusLine statusLine = response.getStatusLine();
				int statusCode = statusLine.getStatusCode();
				if (statusCode == 200) {
					HttpEntity entity = response.getEntity();
					InputStream content = entity.getContent();
					BufferedReader reader = new BufferedReader(
							new InputStreamReader(content));
					String line;
					while ((line = reader.readLine()) != null) {
						builder.append(line);
					}
					result = builder.toString();

				} else {

				}
			} catch (ConnectTimeoutException e) {
				result = "timeout";
				// theResult = "timeout";
				e.printStackTrace();
			} catch (ClientProtocolException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}

			return result;
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			// REMOVE DIALOG
			if (pd != null) {
				pd.dismiss();
				// b.setEnabled(true);
			}

			trans = new Transaction(result);

			if (trans.getResult() != null) {
				if (trans.getResult().contains("Success")) {

					edit.putString("agentPhone", agent.toString());
					edit.putString("imei", imei);
					edit.putString("agentName", trans.getExtra1());
					edit.putString(Globals.permission, trans.getExtra2());
					edit.putString(Globals.DEALERNAME, trans.getExtra3());
					edit.putString("AGENTPIN", agentPin.getText().toString());
					edit.putString("PSPID", trans.getExtra4());
					edit.putString("Details", trans.getDetails());

					edit.commit();

					Intent i = new Intent();
					i.setClass(getApplicationContext(), GetClientTag.class);
					startActivity(i);

				} else {
					alerter.alerterError(trans.getMessage());
				}

			} else {

				alerter.alerterError("Error Connecting to Server");
			}
		}

		@Override
		protected void onProgressUpdate(Integer... values) {
			super.onProgressUpdate(values);
		}

	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
	}

	public void layoutClick(View v) {
		InputMethodManager inputManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
		inputManager.hideSoftInputFromWindow(
				getCurrentFocus().getWindowToken(),
				InputMethodManager.HIDE_NOT_ALWAYS);
	}

	@Override
	protected void onStop() {
		super.onStop();
		if (pd != null)
			pd.dismiss();
	}

	@Override
	public void onBackPressed() {
		Intent intent = new Intent(Intent.ACTION_MAIN);
		intent.addCategory(Intent.CATEGORY_HOME);
		intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		startActivity(intent);
	}

	// ------------------------------------------------------------------
	public class CheckVersionAsyncTask extends
			AsyncTask<String, Integer, String> {

		String appVersion;
		String stingUrl;

		@Override
		protected void onPreExecute() {

			super.onPreExecute();

		}

		@Override
		protected String doInBackground(String... parms) {

			StringBuilder builder = new StringBuilder();
			ConnectUtils connector = new ConnectUtils();
			HttpClient client = connector.getNewHttpClient();

			String result = "FAILED";

			appVersion = parms[0];

			try {
				stingUrl = server
						+ "/bio-api/android/androidCheckVersion.php?version_number="
						+ URLEncoder.encode(appVersion, "UTF-8");

			} catch (UnsupportedEncodingException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

			HttpGet httpGet = new HttpGet(stingUrl);

			try {
				HttpResponse response = client.execute(httpGet);
				StatusLine statusLine = response.getStatusLine();
				int statusCode = statusLine.getStatusCode();
				if (statusCode == 200) {
					HttpEntity entity = response.getEntity();
					InputStream content = entity.getContent();
					BufferedReader reader = new BufferedReader(
							new InputStreamReader(content));
					String line;
					while ((line = reader.readLine()) != null) {
						builder.append(line);
					}
					result = builder.toString();

				} else {

				}
			} catch (ConnectTimeoutException e) {

				e.printStackTrace();
			} catch (ClientProtocolException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}

			return result;
		}

		@Override
		protected void onPostExecute(String result) {

			try {

				ClientInfoParser clientInfoParser = new ClientInfoParser();

				ClientInfo clientInfo = clientInfoParser
						.getParsedResults(result);

				if (clientInfo.getResult() != null
						&& !clientInfo.getResult().isEmpty()) {

					if (clientInfo.getResult().equals("Failed")) {
						alerterUpdate(getString(R.string.out_dated_error_message));
					}

				}

			} catch (Exception e) {

			}

		}

	}

	public void alerterUpdate(String message) {
		LayoutInflater li = LayoutInflater.from(First_time.this);
		View promptsView;
		promptsView = li.inflate(R.layout.update_dialog, null);

		// TextView accountTxt = (TextView)
		// promptsView.findViewById(R.id.confirm_dialog_acct);
		TextView errorTxt = (TextView) promptsView
				.findViewById(R.id.tv_update_message);

		errorTxt.setText(message);

		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
				First_time.this);

		// set prompts.xml to alertdialog builder
		alertDialogBuilder.setView(promptsView);
		// set dialog message
		alertDialogBuilder
				.setCancelable(false)
				.setPositiveButton(getString(R.string.yes),
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {

								update();

								dialog.cancel();
							}
						})
				.setNegativeButton(getString(R.string.no),
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								// dialog.cancel();
								dialog.cancel();
							}
						});

		// create alert dialog
		AlertDialog alertDialog = alertDialogBuilder.create();

		// show it
		alertDialog.show();

	}

	public void update() {

		// String url = "http://searchgurbani.com/audio/sggs/1.mp3";

		DownloadManager.Request request = new DownloadManager.Request(
				Uri.parse(Globals.DOWNLOAD_APK_LINK));
		// request.setMimeType("application/vnd.android.package-archive");
		request.setTitle(getString(R.string.application_downlaoding_title));
		request.setDescription(getString(R.string.application_downlaoding_description));
		request.allowScanningByMediaScanner();
		request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
		request.setDestinationInExternalPublicDir(
				Environment.DIRECTORY_DOWNLOADS, "file name");
		DownloadManager manager = (DownloadManager) getSystemService(Context.DOWNLOAD_SERVICE);
		manager.enqueue(request);

	}

}
