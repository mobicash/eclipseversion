package ez.pay.project;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import models.Area;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.conn.ConnectTimeoutException;

import utils.AESHelper;
import utils.AreaParser;
import utils.ConnectUtils;
import utils.MifareUltralightTagTester;
import utils.NFCManager;
import utils.Vars;
import android.app.ActionBar;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.media.MediaPlayer;
import android.nfc.NdefMessage;
import android.nfc.NdefRecord;
import android.nfc.NfcAdapter;
import android.nfc.Tag;
import android.nfc.TagLostException;
import android.nfc.tech.MifareUltralight;
import android.nfc.tech.Ndef;
import android.nfc.tech.NdefFormatable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class ShowClientTag extends BaseActivity {
	private NfcAdapter mNfcAdapter;
	private PendingIntent mPendingIntent;
	NdefMessage mNdefPushMessage;
	Vars vars;

	Alerter alerter;
	OnDataPass dataPasser;
	String nfcValue;
	MyApplication myApplication;
	List<Area> areaList = new ArrayList<Area>();
	SharedPreferences pref;

	private NdefMessage message = null;
	private NFCManager nfcMger;

	// private ProgressDialog dialog;
	Tag currentTag;

	String saveddata;
	String tagInfo;
	NfcAdapter nfcAdpt;

	private static int blockOffset = 4;
	private static int blockNumber = 48;
	private static int blockSize = 4;

	private static final String TAG = MifareUltralightTagTester.class
			.getSimpleName();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		ActionBar bar = getActionBar();
		bar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#f47701")));

		vars = new Vars(this);

		nfcAdpt = NfcAdapter.getDefaultAdapter(this);

		nfcMger = new NFCManager(this);

		alerter = new Alerter(this);
		myApplication = (MyApplication) getApplication();

		setContentView(R.layout.show_client_tag);
		blink();
		new CustomerTypeAsyncTask().execute();

		if ((getIntent().getFlags() & Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT) != 0) {
			finish();
			return;
		}

	}

	public void showTagInfo(Tag tag) {

		nfcValue = readTag(tag);
		nfcValue = nfcValue.trim();
		Log.e("nfcValue", nfcValue);

		// Toast.makeText(getApplicationContext(), "value : " + nfcValue,
		// Toast.LENGTH_LONG).show();

		if (nfcValue != null) {

			if (nfcValue.length() > 23) {

				String pspId = nfcValue.substring(2, 10);

				String encyptedUUID = nfcValue.substring(23, nfcValue.length());

				String deCyptedUUID = decryption(encyptedUUID, "@AA0KUTS=!");

				if (deCyptedUUID != null && !deCyptedUUID.isEmpty()) {

					String customerType = nfcValue.substring(0, 2);
					String customerTypeName = null;

					if (areaList.size() > 0) {

						for (Area area : areaList) {
							if (area.getValue().substring(0, 2)
									.equals(customerType)) {
								customerTypeName = area.getValue();
							}
						}
					}

					String dateTime = nfcValue.substring(10, 23);
					Long dataTimeLong = Long.valueOf(dateTime).longValue();

					Calendar calendar = Calendar.getInstance();
					calendar.setTimeInMillis(dataTimeLong);

					int mYear = calendar.get(Calendar.YEAR);
					int mMonth = calendar.get(Calendar.MONTH) + 1;
					int mDay = calendar.get(Calendar.DAY_OF_MONTH);
					int mHour = calendar.get(Calendar.HOUR);
					int mMinutes = calendar.get(Calendar.MINUTE);
					int mSeonds = calendar.get(Calendar.SECOND);

					String dateFormatted = mYear + "-" + mMonth + "-" + mDay
							+ "  " + mHour + ":" + mMinutes + ":" + mSeonds;

					Log.e("deCyptedUUID", deCyptedUUID);

					if (areaList.size() > 0) {

						// customToast("customer type : " + customerTypeName
						// + " |" + "PSP ID : " + pspId
						// + " | date time :" + dateFormatted + " |"
						// + "NFC Tag Serial: " + deCyptedUUID);

						Intent intent = new Intent(getApplicationContext(),
								ShowTag.class);
						intent.putExtra("customerType", customerTypeName);
						intent.putExtra("PspId", pspId);
						intent.putExtra("dateTime", dateFormatted);
						intent.putExtra("TagSerial", deCyptedUUID);
						startActivity(intent);
						playNotification(getApplicationContext());

					} else {
						customToast("Please tap again");
					}

					// Intent intent = new Intent(ShowClientTag.this,
					// ShowTag.class);
					// startActivity(intent);

				} else {
					customToast("unrecognized tag");
				}
				// <end if>
			} else {
				customToast("unrecognized tag");
			}
			// <end if>
		} else {
			customToast("unrecognized tag");
		}
		// Toast toast = Toast.makeText(ShowClientTag.this, decryptedData,
		// Toast.LENGTH_LONG);
		// toast.setGravity(Gravity.CENTER, 0, 0);
		// toast.show();

		// Log.e("message", "Code=====++++" + parse(record));

		// tvEncryptedData.setText(decryption(nfcValue, "@AA0KUTS=!"));

	}

	@Override
	protected void onPause() {
		super.onPause();
		if (nfcAdpt.isEnabled()) {
			try {
				nfcMger.disableDispatch();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	protected void onResume() {
		super.onResume();
		if (nfcAdpt == null) {
			// Stop here, we definitely need NFC
			alerter.alerterError("This device doesn't support NFC.");
			// showerrors("This device doesn't support NFC.", "ERROR");

		} else {
			if (!nfcAdpt.isEnabled()) {
				// showerrors("Please turn on your nfc", "ERROR");
				alerter.alerterError("Please turn on your nfc.");

			} else {
				try {
					nfcMger.verifyNFC();
					// nfcMger.enableDispatch();

					Intent nfcIntent = new Intent(this, getClass());
					nfcIntent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
					PendingIntent pendingIntent = PendingIntent.getActivity(
							this, 0, nfcIntent, 0);
					IntentFilter[] intentFiltersArray = new IntentFilter[] {};
					String[][] techList = new String[][] {
							{ android.nfc.tech.Ndef.class.getName() },
							{ android.nfc.tech.NdefFormatable.class.getName() } };

					nfcAdpt.enableForegroundDispatch(this, pendingIntent,
							intentFiltersArray, techList);
				} catch (NFCManager.NFCNotSupported nfcnsup) {
					// Toast.makeText(getApplicationContext(),
					// "NFC not supported", Toast.LENGTH_LONG).show();

					alerter.alerterError("NFC not supported.");
				} catch (NFCManager.NFCNotEnabled nfcnEn) {

					// Toast.makeText(getApplicationContext(),
					// "NFC Not enabled",
					// Toast.LENGTH_LONG).show();
					alerter.alerterError("NFC Not enabled.");
				}
			}
		}

	}

	public static String parse(NdefRecord record) {
		// Preconditions.checkArgument(record.getTnf() ==
		// NdefRecord.TNF_WELL_KNOWN);
		// Preconditions.checkArgument(Arrays.equals(record.getType(),
		// NdefRecord.RTD_TEXT));
		try {
			byte[] payload = record.getPayload();
			/*
			 * payload[0] contains the "Status Byte Encodings" field, per the
			 * NFC Forum "Text Record Type Definition" section 3.2.1.
			 * 
			 * bit7 is the Text Encoding Field.
			 * 
			 * if (Bit_7 == 0): The text is encoded in UTF-8 if (Bit_7 == 1):
			 * The text is encoded in UTF16
			 * 
			 * Bit_6 is reserved for future use and must be set to zero.
			 * 
			 * Bits 5 to 0 are the length of the IANA language code.
			 */
			String textEncoding = ((payload[0] & 0200) == 0) ? "UTF-8"
					: "UTF-16";
			int languageCodeLength = payload[0] & 0077;
			String languageCode = new String(payload, 1, languageCodeLength,
					"US-ASCII");
			String text = new String(payload, languageCodeLength + 1,
					payload.length - languageCodeLength - 1, textEncoding);
			return text;
			// return new TextRecord(languageCode, text);
		} catch (UnsupportedEncodingException e) {
			// should never happen unless we get a malformed tag.
			throw new IllegalArgumentException(e);
		}
	}

	public static boolean isText(NdefRecord record) {
		try {
			parse(record);
			return true;
		} catch (IllegalArgumentException e) {
			return false;
		}
	}

	@Override
	public void onNewIntent(Intent intent) {

		Tag tag = intent.getParcelableExtra(NfcAdapter.EXTRA_TAG);

		try {
			byte[] id = tag.getId();

			if (id.length != 0) {
				showTagInfo(tag);
			}

		} catch (NullPointerException e) {

		} catch (Exception e) {
			// if (!myApplication.isTrue()) {
			customToast(e.toString());

			// } else {
			// myApplication.setTrue(false);
			// }

		}

	}

	public interface OnDataPass {
		public void onDataPass(String data);
	}

	public static void playNotification(Context context) {
		MediaPlayer mp = MediaPlayer.create(context, R.raw.beep);
		mp.start();
	}

	public String encryption(String strNormalText, String seedValue) {

		String normalTextEnc = "";
		try {
			normalTextEnc = AESHelper.encrypt(seedValue, strNormalText);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return normalTextEnc;
	}

	public String decryption(String strEncryptedText, String seedValue) {

		String strDecryptedText = "";
		try {
			strDecryptedText = AESHelper.decrypt(seedValue, strEncryptedText);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return strDecryptedText;
	}

	public void customToast(String message) {
		LayoutInflater inflater = getLayoutInflater();
		View toastLayout = inflater.inflate(R.layout.custom_toast,
				(ViewGroup) findViewById(R.id.custom_toast_layout));
		TextView tvCustomToastMessage = (TextView) toastLayout
				.findViewById(R.id.tv_custom_toast_message);
		tvCustomToastMessage.setText(message);

		Toast toast = new Toast(getApplicationContext());
		toast.setDuration(Toast.LENGTH_LONG);
		toast.setView(toastLayout);
		toast.setGravity(Gravity.CENTER, 0, 0);
		toast.show();
	}

	public class CustomerTypeAsyncTask extends AsyncTask<String, Void, String> {

		public String theResult;
		public String action;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();

		}

		@Override
		// protected String doInBackground(Void... arg0) {
		protected String doInBackground(String... params) {

			StringBuilder builder = new StringBuilder();
			ConnectUtils connector = new ConnectUtils();
			HttpClient client = connector.getNewHttpClient();

			String result = "FAILED";

			String sendString = null;

			try {

				// DEALER
				sendString = server + "getCustomerType.php";

			} catch (Exception e) {

			}

			HttpGet httpGet = new HttpGet(sendString);

			try {
				HttpResponse response = client.execute(httpGet);
				StatusLine statusLine = response.getStatusLine();
				int statusCode = statusLine.getStatusCode();
				if (statusCode == 200) {
					HttpEntity entity = response.getEntity();
					InputStream content = entity.getContent();
					BufferedReader reader = new BufferedReader(
							new InputStreamReader(content));
					String line;
					while ((line = reader.readLine()) != null) {
						builder.append(line);
					}
					result = builder.toString();

				} else {

				}
			} catch (ConnectTimeoutException e) {
				theResult = "timeout";
				// theResult = "timeout";
				e.printStackTrace();
			} catch (ClientProtocolException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}

			return result;

			//

		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);

			try {

				AreaParser areaParser = new AreaParser();
				areaList = areaParser.getAreaList(result);

				if (areaList.get(0).getValue() != null) {

				} else {
					// alerter.alerterError(Globals.InternetErrorMessage);
				}

			} catch (Exception e) {
				// alerter.alerterError(Globals.InternetErrorMessage);
			}

		}
	}

	public void writeTag(Tag tag, NdefMessage message) {
		if (tag != null) {
			try {
				Ndef ndefTag = Ndef.get(tag);

				if (ndefTag == null) {
					// Let's try to format the Tag in NDEF
					NdefFormatable nForm = NdefFormatable.get(tag);
					if (nForm != null) {
						nForm.connect();
						nForm.format(message);
						nForm.close();
					}
				} else {
					ndefTag.connect();
					ndefTag.writeNdefMessage(message);
					ndefTag.close();
				}

			} catch (TagLostException e) {
				// customToast("Unsupported Tag");
			} catch (Exception e) {

				// customToast("Unsupported Tag");
				e.printStackTrace();
			}
		}
	}

	public void blink() {
		ImageView image = (ImageView) findViewById(R.id.iv_icon_nfc);
		Animation animation1 = AnimationUtils.loadAnimation(
				getApplicationContext(), R.anim.blink);
		image.startAnimation(animation1);
	}

	public String readTag(Tag tag) {
		MifareUltralight mifare = MifareUltralight.get(tag);
		try {
			mifare.connect();
			byte[] payload1 = mifare.readPages(4);
			String part1 = new String(payload1, Charset.forName("US-ASCII"));
			byte[] payload2 = mifare.readPages(8);
			String part2 = new String(payload2, Charset.forName("US-ASCII"));

			byte[] payload3 = mifare.readPages(12);
			String part3 = new String(payload3, Charset.forName("US-ASCII"));

			byte[] payload4 = mifare.readPages(14);
			String part4 = new String(payload4, Charset.forName("US-ASCII"));
			part4 = part4.substring(8);

			return part1 + part2 + part3 + part4;
		} catch (IOException e) {
			Log.e(TAG, "IOException while writing MifareUltralight message...",
					e);
			customToast("unrecognized tag");
		} catch (Exception e) {
			customToast("unrecognized tag");
		} finally {
			if (mifare != null) {
				try {
					mifare.close();
				} catch (IOException e) {
					Log.e(TAG, "Error closing tag...", e);
					customToast("unrecognized tag");
				}
			}
		}
		return null;
	}

}
