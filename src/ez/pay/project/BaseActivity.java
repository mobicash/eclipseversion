package ez.pay.project;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.conn.ConnectTimeoutException;

import utils.ClientInfoParser;
import utils.ConnectUtils;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.DownloadManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

public class BaseActivity extends Activity {

	SharedPreferences preferences;
	Editor editor;
	String server;
	ProgressDialog pd;
	Alerter alerter;
	MyApplication myApply;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		init();

	}

	private void init() {
		// TODO Auto-generated method stub
		preferences = PreferenceManager.getDefaultSharedPreferences(this);
		editor = preferences.edit();
		server = preferences.getString("server", null);
		alerter = new Alerter(this);
		myApply = (MyApplication) getApplication();
	}

	public void update() {

		// String url = "http://searchgurbani.com/audio/sggs/1.mp3";

		DownloadManager.Request request = new DownloadManager.Request(
				Uri.parse(Globals.DOWNLOAD_APK_LINK));
		// request.setMimeType("application/vnd.android.package-archive");
		request.setTitle(getString(R.string.application_downlaoding_title));
		request.setDescription(getString(R.string.application_downlaoding_description));
		request.allowScanningByMediaScanner();
		request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
		request.setDestinationInExternalPublicDir(
				Environment.DIRECTORY_DOWNLOADS, "file name");
		DownloadManager manager = (DownloadManager) getSystemService(Context.DOWNLOAD_SERVICE);
		manager.enqueue(request);

	}

	public void alerterUpdate(String message) {
		LayoutInflater li = LayoutInflater.from(BaseActivity.this);
		View promptsView;
		promptsView = li.inflate(R.layout.update_dialog, null);

		// TextView accountTxt = (TextView)
		// promptsView.findViewById(R.id.confirm_dialog_acct);
		TextView errorTxt = (TextView) promptsView
				.findViewById(R.id.tv_update_message);

		errorTxt.setText(message);

		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
				BaseActivity.this);

		// set prompts.xml to alertdialog builder
		alertDialogBuilder.setView(promptsView);
		// set dialog message
		alertDialogBuilder
				.setCancelable(false)
				.setPositiveButton(getString(R.string.yes),
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {

								update();

								dialog.cancel();
							}
						})
				.setNegativeButton(getString(R.string.no),
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								// dialog.cancel();
								dialog.cancel();
							}
						});

		// create alert dialog
		AlertDialog alertDialog = alertDialogBuilder.create();

		// show it
		alertDialog.show();

	}

	public void alerterSuccess(String message) {
		LayoutInflater li = LayoutInflater.from(BaseActivity.this);
		View promptsView;
		promptsView = li.inflate(R.layout.success_dialog, null);

		// TextView accountTxt = (TextView)
		// promptsView.findViewById(R.id.confirm_dialog_acct);
		TextView errorTxt = (TextView) promptsView
				.findViewById(R.id.success_dialog);

		errorTxt.setText(message);

		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
				BaseActivity.this);

		// set prompts.xml to alertdialog builder
		alertDialogBuilder.setView(promptsView);
		// set dialog message
		alertDialogBuilder.setCancelable(false).setPositiveButton("OK",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {

						dialog.cancel();
					}
				});

		// create alert dialog
		AlertDialog alertDialog = alertDialogBuilder.create();

		// show it
		alertDialog.show();

	}

	// ------------------------------------------------------------------
	// ------------------------------------------------------------------
	public class CheckVersionAsyncTask extends
			AsyncTask<String, Integer, String> {

		String appVersion;
		String stingUrl;

		@Override
		protected void onPreExecute() {

			super.onPreExecute();
			pd = new ProgressDialog(BaseActivity.this);
			pd.setMessage("please wait / ihangane ...");
			pd.setCancelable(false);
			pd.setIndeterminate(true);
			pd.show();

		}

		@Override
		protected String doInBackground(String... parms) {

			StringBuilder builder = new StringBuilder();
			ConnectUtils connector = new ConnectUtils();
			HttpClient client = connector.getNewHttpClient();

			String result = "FAILED";

			appVersion = parms[0];

			try {
				stingUrl = server
						+ "/bio-api/android/androidCheckVersion.php?version_number="
						+ URLEncoder.encode(appVersion, "UTF-8") + "&version="
						+ URLEncoder.encode(Globals.version, "UTF-8");

			} catch (UnsupportedEncodingException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

			HttpGet httpGet = new HttpGet(stingUrl);

			try {
				HttpResponse response = client.execute(httpGet);
				StatusLine statusLine = response.getStatusLine();
				int statusCode = statusLine.getStatusCode();
				if (statusCode == 200) {
					HttpEntity entity = response.getEntity();
					InputStream content = entity.getContent();
					BufferedReader reader = new BufferedReader(
							new InputStreamReader(content));
					String line;
					while ((line = reader.readLine()) != null) {
						builder.append(line);
					}
					result = builder.toString();

				} else {

				}
			} catch (ConnectTimeoutException e) {

				e.printStackTrace();
			} catch (ClientProtocolException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}

			return result;
		}

		@Override
		protected void onPostExecute(String result) {

			try {
				// REMOVE DIALOG
				if (pd != null) {
					pd.dismiss();
					// b.setEnabled(true);
				}

				ClientInfoParser clientInfoParser = new ClientInfoParser();

				ClientInfo clientInfo = clientInfoParser
						.getParsedResults(result);

				if (clientInfo.getResult() != null
						&& !clientInfo.getResult().isEmpty()) {

					if (clientInfo.getResult().equals("Failed")) {
						alerterUpdate(getString(R.string.out_dated_error_message));
					} else {
						alerterSuccess(getString(R.string.updated_version_message));
					}

				} else {
					alerter.alerterError(Globals.InternetErrorMessage);
				}

			} catch (Exception e) {
				if (pd != null) {
					pd.dismiss();
					// b.setEnabled(true);
				}
				alerter.alerterError(Globals.InternetErrorMessage);

			}

		}

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		// getMenuInflater().inflate(R.menu.first_time, menu);
		getMenuInflater().inflate(R.menu.base_menu, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {

		case R.id.i_nfc_tag_info:

			myApply.setTrue(true);
			Intent iShowNfcInfo = new Intent(getApplicationContext(),
					ShowClientTag.class);
			startActivity(iShowNfcInfo);

			break;

		// case R.id.i_settings:
		//
		// Intent iSettings = new Intent(getApplicationContext(),
		// SettingLogin.class);
		// startActivity(iSettings);
		//
		// break;

		case R.id.logout:

			Editor edit = preferences.edit();
			edit.remove("agentPhone");
			edit.remove("agentName");
			edit.remove("imei");
			edit.remove(Globals.permission);
			edit.commit();

			Intent i = new Intent(getApplicationContext(), First_time.class);
			startActivity(i);

			break;
		// case R.id.exit:
		// Intent intent = new Intent(Intent.ACTION_MAIN);
		// intent.addCategory(Intent.CATEGORY_HOME);
		// intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		// startActivity(intent);
		// break;

		}
		return true;
	}

}
