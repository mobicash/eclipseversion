package ez.pay.project;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;

public class AlertClass {
	
	Context context;
	
	public AlertClass(Context context){
		this.context = context;
	}

	public void alerter(String title, String message) {

		// public void onClick(View arg0) {
	
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);

		// set title
		alertDialogBuilder.setTitle(title);

		// set dialog message
		alertDialogBuilder
				.setMessage(message)
				.setCancelable(false)
				.setPositiveButton("OK",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								// if this button is clicked, close
								// current activity
								// MainActivity.this.finish();
								dialog.cancel();
							}
						});

		// create alert dialog
		AlertDialog alertDialog = alertDialogBuilder.create();

		// show it
		alertDialog.show();
		// }
	}
}
