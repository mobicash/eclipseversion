package ez.pay.project;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.conn.ConnectTimeoutException;

import services.BluetoothService;
import utils.ConnectUtils;
import utils.Transaction;
import utils.Vars;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;

public class PaySchoolFeesActivity extends BaseActivity {

	TextView tvStudentRegNumber, tvStudentName, tvSchoolName, tvSchoolFees,
			tvSchoolCode, tvPrinterMessage;

	EditText etAmount, etPayerName, etPhone, etPin;

	String studentRegNumber, studentName, schoolName, schoolFees, schoolCode;

	Alerter alerter;

	CheckBox cbPrint;
	ProgressDialog pd;
	SharedPreferences pref;
	Vars vars;

	// printer

	// bluetooth

	private static final String TAG = "BluetoothPrint";
	private static final boolean D = true;

	// ��BluetoothService�����������Ϣ����
	public static final int MESSAGE_STATE_CHANGE = 1;
	public static final int MESSAGE_READ = 2;
	public static final int MESSAGE_WRITE = 3;
	public static final int MESSAGE_DEVICE_NAME = 4;
	public static final int MESSAGE_TOAST = 5;

	// BluetoothService�յ��Ĵ������ؼ�������
	public static final String DEVICE_NAME = "device_name";
	public static final String TOAST = "toast";

	// ����Ĵ���
	private static final int REQUEST_CONNECT_DEVICE = 1;
	private static final int REQUEST_ENABLE_BT = 2;
	private static final int REQUEST_Set = 3;

	// ���ӵ��豸������
	private String mConnectedDeviceName = null;
	// �ַ�����������������Ϣ
	private StringBuffer mOutStringBuffer;
	// ��������������
	private BluetoothAdapter mBluetoothAdapter = null;
	public static BluetoothService mService = null;

	private TextView mTitle;
	private EditText mOutEditText;
	private Button mPrintButton;
	private Button mClearButton;
	private Button mSetButton;
	MyApplication myapplication;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_pay_school_fees);
		init();
		Intent iin = getIntent();
		Bundle b = iin.getExtras();

		if (b != null) {
			studentRegNumber = (String) b.get(Globals.regNumber);
			studentName = (String) b.get(Globals.studentName);
			schoolName = (String) b.get(Globals.schoolName);
			schoolFees = (String) b.get(Globals.schoolFees);
			schoolCode = (String) b.get(Globals.schoolCode);

			setValues(studentRegNumber, studentName, schoolName, schoolFees,
					schoolCode);

		}

		// �õ����ص�������
		mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

		if (mBluetoothAdapter == null) {
			finish();
			return;
		}
	}

	public void init() {
		tvStudentRegNumber = (TextView) findViewById(R.id.tv_student_reg_number);
		tvStudentName = (TextView) findViewById(R.id.tv_student_name);
		tvSchoolName = (TextView) findViewById(R.id.tv_school_name);
		tvSchoolFees = (TextView) findViewById(R.id.tv_school_fees);
		tvSchoolCode = (TextView) findViewById(R.id.tv_school_code);

		tvPrinterMessage = (TextView) findViewById(R.id.tv_printer_message);
		cbPrint = (CheckBox) findViewById(R.id.cb_print);

		etAmount = (EditText) findViewById(R.id.et_amount);
		etPhone = (EditText) findViewById(R.id.et_phone);
		etPayerName = (EditText) findViewById(R.id.et_payer_name);
		etPin = (EditText) findViewById(R.id.et_pin);
		alerter = new Alerter(this);
		myapplication = (MyApplication) getApplication();
		pref = PreferenceManager.getDefaultSharedPreferences(this);
		vars = new Vars(this);

	}

	public void printClick(View v) {
		if (((CheckBox) v).isChecked()) {

			if (myapplication.getmService().getState() != BluetoothService.STATE_CONNECTED) {
				// Toast.makeText(this, R.string.not_connected,
				// Toast.LENGTH_SHORT).show();
				try {

					Intent serverIntent = new Intent(this,
							DeviceListActivity.class);
					startActivityForResult(serverIntent, REQUEST_CONNECT_DEVICE);

					return;
				} catch (Exception e) {

				}
			}

		}
	}

	@Override
	public void onStart() {
		super.onStart();

		// �ж������Ƿ������������û�п���������������
		if (!mBluetoothAdapter.isEnabled()) {

			Intent enableIntent = new Intent(
					BluetoothAdapter.ACTION_REQUEST_ENABLE);
			startActivityForResult(enableIntent, REQUEST_ENABLE_BT);
		} else {
			if (myapplication.getmService() == null) {

				setupChat();
			}

		}
	}

	@Override
	public synchronized void onResume() {
		super.onResume();
		if (D)

			if (myapplication.getmService() != null) {
				if (myapplication.getmService().getState() == BluetoothService.STATE_NONE) {

					myapplication.getmService().start();
				}

			}

		if (myapplication.getmService().getState() == BluetoothService.STATE_CONNECTED) {
			// ivPrinterIcon.setBackgroundResource(R.drawable.log);
			tvPrinterMessage.setText(getString(R.string.connected_txt));
			tvPrinterMessage.setTextColor(Color.parseColor("#398f14"));

		} else {
			// ivPrinterIcon.setBackgroundResource(R.drawable.not_log);
			tvPrinterMessage.setText(getString(R.string.not_connected_txt));
			tvPrinterMessage.setTextColor(Color.parseColor("#e34949"));

		}

	}

	private void setupChat() {
		Log.d(TAG, "SetupChat");

		// ��ʼ��BluetoothServiceִ����������
		myapplication.setmService(new BluetoothService(this, mHandler));

	}

	private void sendMessage(String message) {
		// ������ڳ�������

		// ���ʵ��Ҫ���͵Ķ���
		if (message.length() > 0) {
			// �õ���Ϣ�ֽڲ��Ҹ���BluetoothService
			byte[] send;
			try {
				send = message.getBytes("GB2312");
			} catch (UnsupportedEncodingException e) {
				send = message.getBytes();
			}
			// mService.write(send,10);
			myapplication.getmService().write(send, 0, send.length);
		}
	}

	@Override
	public synchronized void onPause() {
		super.onPause();

	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		// Stop the Bluetooth services
		if (myapplication.getmService() != null)
			myapplication.getmService().stop();

	}

	// ���±������ұ�״̬�Ͷ�д״̬��Handler
	private final Handler mHandler = new Handler() {

		@Override
		public void handleMessage(Message msg) {
			switch (msg.what) {
			case MESSAGE_STATE_CHANGE:
				if (D)
					Log.i(TAG, "MESSAGE_STATE_CHANGE:" + msg.arg1);
				switch (msg.arg1) {
				case BluetoothService.STATE_CONNECTED:
					// mTitle.setText(R.string.title_connected_to);
					// mTitle.append(mConnectedDeviceName);
					// ivPrinterIcon.setBackgroundResource(R.drawable.log);

					tvPrinterMessage.setText(getString(R.string.connected_txt));
					tvPrinterMessage.setTextColor(Color.parseColor("#398f14"));

					break;
				case BluetoothService.STATE_CONNECTING:
					// mTitle.setText(R.string.title_connecting);
					tvPrinterMessage
							.setText(getString(R.string.connecting_txt));
					tvPrinterMessage.setTextColor(Color.parseColor("#000000"));

					break;
				case BluetoothService.STATE_LISTEN:
				case BluetoothService.STATE_NONE:
					// mTitle.setText(R.string.title_not_connected);
					// ivPrinterIcon.setBackgroundResource(R.drawable.not_log);
					tvPrinterMessage
							.setText(getString(R.string.not_connected_txt));
					tvPrinterMessage.setTextColor(Color.parseColor("#e34949"));

					break;
				}
				break;
			case MESSAGE_WRITE:
				break;
			case MESSAGE_READ:
				// ����Ч�ֽڻ���������һ���ַ���
				break;
			case MESSAGE_DEVICE_NAME:
				// ���������豸������
				mConnectedDeviceName = msg.getData().getString(DEVICE_NAME);
				// Toast.makeText(getApplicationContext(),
				// "���ӵ�" + mConnectedDeviceName, Toast.LENGTH_SHORT)
				// .show();
				alerter.alerterSuccessSimpleHere(mConnectedDeviceName
						+ " connected / " + mConnectedDeviceName
						+ " iracometse neza ");

				break;
			case MESSAGE_TOAST:
				// Toast.makeText(getApplicationContext(),
				// msg.getData().getString(TOAST), Toast.LENGTH_SHORT)
				// .show();

				alerter.alerterError("Error retry / ikibazo ongera ugerageze ");

				break;

			}

		}
	};

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (D)
			Log.d(TAG, "onActivityResult" + resultCode);
		switch (requestCode) {
		case REQUEST_CONNECT_DEVICE:
			// ��DeviceListActivity����һ���豸����
			if (resultCode == Activity.RESULT_OK) {

				// ��ȡ�豸��MAC��ַ
				String address = data.getExtras().getString(
						DeviceListActivity.EXTRA_DEVICE_ADDRESS);
				// �õ�BLuetoothDevice����
				if (address == null) {
					break;
				}
				// ��ȡ�豸��������
				BluetoothDevice device = mBluetoothAdapter
						.getRemoteDevice(address);
				// �������ӵ��豸
				myapplication.getmService().connect(device);
			}
			break;
		case REQUEST_ENABLE_BT:
			// �����󼤻������Ļر�
			if (resultCode == Activity.RESULT_OK) {
				// �������������õ�,���,����һ������Ự
				// ���������ɹ����������ʼ��UI
				setupChat();

			} else {
				// �û�û���������������
				Log.d(TAG, "BT not enabled");
				// Toast.makeText(this, R.string.bt_not_enabled_leaving,
				// Toast.LENGTH_SHORT).show();
				alerter.alerterError(getString(R.string.bt_not_enabled_leaving));
				finish();
			}

			/*
			 * case REQUEST_Set:
			 * 
			 * if(resultCode==Activity.RESULT_OK) {
			 * 
			 * byte a[]=data.getExtras().getByteArray("data");
			 * mService.write(a,0,a.length); Toast.makeText(this, "���óɹ���",
			 * Toast.LENGTH_SHORT).show(); } break;
			 */
		}
	}

	@Override
	public void onBackPressed() {
		android.os.Process.killProcess(android.os.Process.myPid());
	}

	public void setValues(String studentRegNumber, String studentName,
			String schoolName, String schoolFees, String schoolCode) {

		tvStudentRegNumber.setText(studentRegNumber);
		tvStudentName.setText(studentName);
		tvSchoolName.setText(schoolName);
		tvSchoolFees.setText(schoolFees);
		tvSchoolCode.setText(schoolCode);

	}

	public void pay(View v) {

		if (etAmount.getText().length() <= 0) {

			alerter.alerterError(Globals.amountErrorMessage);

		} else if (etPayerName.getText().length() <= 0) {

			alerter.alerterError(Globals.payerCheckErrorMessage);

		} else if (etPhone.getText().length() <= 0) {

			alerter.alerterError(Globals.phoneCheckErrorMessage);

		} else if (etPhone.getText().length() != 10) {

			alerter.alerterError(Globals.invalidNumberErrorMessage);

		} else if (etPin.getText().length() <= 0) {

			alerter.alerterError(Globals.enterAgentPinErrorMessage);

		} else {

			confirmPayment();

		}

	}

	public void layoutClick(View v) {
		InputMethodManager inputManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
		inputManager.hideSoftInputFromWindow(
				getCurrentFocus().getWindowToken(),
				InputMethodManager.HIDE_NOT_ALWAYS);
	}

	// CANCEL
	public void cancel(View v) {
		alerter.alerterConfirmCancelEtax(Globals.cancelPaySchoolFeesMessage);
	}

	@Override
	protected void onStop() {
		super.onStop();
		if (pd != null)
			pd.dismiss();
	}

	public void confirmPayment() {
		LayoutInflater li = LayoutInflater.from(PaySchoolFeesActivity.this);
		View promptsView;
		promptsView = li.inflate(R.layout.confirm_dialog_school_fees_payment,
				null);

		TextView tvStudentRegNumber, tvStudentName, tvSchoolName, tvSchoolFees, tvSchoolCode, tvPrinterMessage, tvAmount, tvPayerName, tvPhone;

		tvStudentRegNumber = (TextView) promptsView
				.findViewById(R.id.tv_student_reg_number);
		tvStudentRegNumber.setText(studentRegNumber);
		tvStudentName = (TextView) promptsView
				.findViewById(R.id.tv_student_name);
		tvStudentName.setText(studentName);
		tvSchoolName = (TextView) promptsView.findViewById(R.id.tv_school_name);
		tvSchoolName.setText(schoolName);
		tvSchoolFees = (TextView) promptsView.findViewById(R.id.tv_school_fees);
		tvSchoolFees.setText(schoolFees);
		tvSchoolCode = (TextView) promptsView.findViewById(R.id.tv_school_code);
		tvSchoolCode.setText(schoolCode);
		tvAmount = (TextView) promptsView.findViewById(R.id.tv_amount);
		tvAmount.setText(etAmount.getText().toString());
		tvPhone = (TextView) promptsView.findViewById(R.id.tv_client_phone);
		tvPhone.setText(etPhone.getText().toString());
		tvPayerName = (TextView) promptsView.findViewById(R.id.tv_payer_name);
		tvPayerName.setText(etPayerName.getText().toString());

		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
				PaySchoolFeesActivity.this);

		// set prompts.xml to alertdialog builder
		alertDialogBuilder.setView(promptsView);
		// set dialog message
		alertDialogBuilder
				.setCancelable(false)
				.setPositiveButton(Globals.yes,
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {

								// send here
								new PaySchoolFees().execute(studentRegNumber,
										studentName, schoolName, schoolCode,
										schoolFees, etAmount.getText()
												.toString(), etPayerName
												.getText().toString(), etPhone
												.getText().toString(),
										vars.prefs
												.getString("agentPhone", null),
										etPin.getText().toString());
							}
						})
				.setNegativeButton(Globals.no,
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								dialog.cancel();
							}
						});

		// create alert dialog
		AlertDialog alertDialog = alertDialogBuilder.create();
		// show it
		alertDialog.show();
	}

	public class PaySchoolFees extends AsyncTask<String, Void, String> {

		public String theResult = null;

		String regNumber, studentName, schoolName, schoolCode, amountToPay,
				amountPaid, payerName, customerPhone, phoneNumber, pin;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			pd = new ProgressDialog(PaySchoolFeesActivity.this);
			pd.setTitle("Paying School fees / Kwishyura ishuri");
			pd.setMessage(Globals.waitProgressMessage);
			pd.setCancelable(false);
			pd.setIndeterminate(true);
			pd.show();
		}

		@Override
		protected String doInBackground(String... arg) {
			String json = "";
			regNumber = arg[0];
			studentName = arg[1];
			schoolName = arg[2];
			schoolCode = arg[3];
			amountToPay = arg[4];
			amountPaid = arg[5];
			payerName = arg[6];
			customerPhone = arg[7];
			phoneNumber = arg[8];
			pin = arg[9];

			StringBuilder builder = new StringBuilder();
			ConnectUtils connector = new ConnectUtils();
			HttpClient client = connector.getNewHttpClient();

			String urlString = null;

			// LOAD SERVER PREF

			try {

				// DEALER
				urlString = vars.server
						+ "/bio-api/androidSchool/paySchoolFees.php?regNumber="
						+ URLEncoder.encode(regNumber, "UTF-8")
						+ "&studentName="
						+ URLEncoder.encode(studentName, "UTF-8")

						+ "&schoolName="
						+ URLEncoder.encode(schoolName, "UTF-8")

						+ "&schoolCode="
						+ URLEncoder.encode(schoolCode, "UTF-8")

						+ "&amountToPay="
						+ URLEncoder.encode(amountToPay, "UTF-8")

						+ "&amountPaid="
						+ URLEncoder.encode(amountPaid, "UTF-8")

						+ "&payername="
						+ URLEncoder.encode(payerName, "UTF-8")

						+ "&customerphone="
						+ URLEncoder.encode(customerPhone, "UTF-8")

						+ "&phoneNumber="
						+ URLEncoder.encode(phoneNumber, "UTF-8")

						+ "&dealer="
						+ URLEncoder.encode(
								pref.getString(Globals.permission, null),
								"UTF-8")

						+ "&pin="
						+ URLEncoder.encode(pin, "UTF-8")
						+ "&version="
						+ URLEncoder.encode(Globals.version, "UTF-8")

						+ "&dealer_name="
						+ URLEncoder.encode(
								vars.prefs.getString(Globals.DEALERNAME, null),
								"UTF-8")

				;

			} catch (UnsupportedEncodingException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

			HttpGet httpGet = new HttpGet(urlString);

			try {
				HttpResponse response = client.execute(httpGet);
				StatusLine statusLine = response.getStatusLine();
				int statusCode = statusLine.getStatusCode();
				if (statusCode == 200) {
					HttpEntity entity = response.getEntity();
					InputStream content = entity.getContent();
					BufferedReader reader = new BufferedReader(
							new InputStreamReader(content));
					String line;
					while ((line = reader.readLine()) != null) {
						builder.append(line);
					}
					json = builder.toString();

				} else {

				}
			} catch (ConnectTimeoutException e) {

				e.printStackTrace();
			} catch (ClientProtocolException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}

			return json;
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			// REMOVE DIALOG
			if (pd != null) {
				pd.dismiss();
				// b.setEnabled(true);
			}

			Transaction trans = new Transaction(result);

			// IF OUTPUT WAS RECEIVED FROM SERVER
			if (trans.getMessage() != null) {

				if (trans.getResult().equals("Success")) {

					LayoutInflater li = LayoutInflater
							.from(getApplicationContext());
					View promptsView;
					promptsView = li.inflate(R.layout.dialog_success_simple,
							null);

					TextView msgTxt = (TextView) promptsView
							.findViewById(R.id.success_simple_message);

					msgTxt.setText(trans.getMessage());

					// print here

					if (cbPrint.isChecked()
							&& myapplication.getmService().getState() == BluetoothService.STATE_CONNECTED) {
						// String title = "E-Tax";
						String mobicashText = "MobiCash LTD";
						String tinNumberText = "TIN No : 101858540";
						String mobicashPhoneText = "Tel: (250)787689185/SMS:8485";
						String bodifaHouseText = "Bodifa Mercy House,5th Floor";
						String KimihururaKigaliText = "Kimihurura,Kigali";
						String receiptNumberText = "Receipt Number : "
								+ trans.getTransactionid();
						String dateText = "Date : " + trans.getDateTime();
						String agentNameTitle = "Agent name";
						String agentName = pref.getString("agentName", null);
						String mobicashReferenceIdTitle = "Mobicash Reference ID";
						String clientChargesTitle = "Client Charges ";

						String mobicashRefIdTitle = "Mobicash reference ID";
						String mobicashRefId = trans.getExtra1();
						String studentRegNoTitle = "Student reg No";
						String studentNameTitle = "Student Name";
						String schoolNameTitle = "School Name";
						String schoolFeesTitle = "School fees";
						String amountPaidTitle = "Amount Paid";
						String schoolCodeTitle = "School code";
						String payerNameTitle = "Payer name";
						String totalAmountToPayTitle = "Total amount to pay ";

						String thankUMessage = "Thank you for using MCash ";
						String keepEnjoyingMessage = "Keep Enjoying !";
						String line = "------------------------------";
						String dash = "##############################";
						String star = "******************************";

						double amountToPay = Double.parseDouble(amountPaid);
						double commission = Double.parseDouble(trans
								.getCommissions());
						double totalToPay = amountToPay + commission;

						String message = mobicashText + "\n" + tinNumberText
								+ "\n" + mobicashPhoneText + "\n"
								+ bodifaHouseText + "\n" + KimihururaKigaliText
								+ "\n\n" + receiptNumberText + "\n" + dateText
								+ "\n" + line + "\n\n" + studentNameTitle
								+ "\n" + studentName + "\n\n"
								+ studentRegNoTitle + "\n" + regNumber + "\n\n"
								+ schoolNameTitle + "\n" + schoolName + "\n\n"
								+ amountPaidTitle + "\n"
								+ etAmount.getText().toString() + " RW \n\n"

								+ clientChargesTitle + "\n"
								+ String.valueOf(commission) + " RW \n\n"

								+ totalAmountToPayTitle + "\n"
								+ String.valueOf(totalToPay) + " RW \n\n"

								+ payerNameTitle + "\n" + payerName + "\n\n"
								+ agentNameTitle + "\n" + agentName + "\n\n"
								+ mobicashRefIdTitle + "\n" + mobicashRefId
								+ "\n\n" + line + "\n" + thankUMessage + "\n"
								+ keepEnjoyingMessage + "\n\n\n\n";

						sendMessage(message);
					}

					AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
							PaySchoolFeesActivity.this);

					// set prompts.xml to alertdENroialog builder
					alertDialogBuilder.setView(promptsView);
					// set dialog message
					alertDialogBuilder.setCancelable(false).setPositiveButton(
							"OK", new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog,
										int id) {

									Intent i = new Intent(
											getApplicationContext(),
											Services.class);
									startActivity(i);

								}
							});

					// create alert dialog
					AlertDialog alertDialog = alertDialogBuilder.create();
					// show it
					alertDialog.show();

				} else {
					// alerter.alerterError(trans.getResult());
					alerter.alerterError(trans.getError());
				}
			} else {
				alerter.alerterError(Globals.InternetErrorMessage);
			}

		}
	}

}
