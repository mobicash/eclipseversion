package ez.pay.project;

import java.io.IOException;
import java.math.BigInteger;
import java.nio.charset.Charset;
import java.util.ArrayList;

import utils.AESHelper;
import utils.MifareUltralightTagTester;
import utils.NFCManager;
import android.app.ActionBar;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.nfc.NdefMessage;
import android.nfc.NfcAdapter;
import android.nfc.Tag;
import android.nfc.tech.MifareUltralight;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class WriteClientInfo extends BaseActivity {

	private NFCManager nfcMger;

	private NdefMessage message = null;
	// private ProgressDialog dialog;
	Tag currentTag;

	String saveddata;
	NfcAdapter nfcAdpt;
	MyApplication myApplication;
	Alerter alerter;
	String tagInfo;
	SharedPreferences pref;
	static ProgressDialog pd;

	// String uniqueID;

	private static int blockOffset = 4;
	private static int blockNumber = 48;
	private static int blockSize = 4;

	private static final String TAG = MifareUltralightTagTester.class.getSimpleName();

	static Context context;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.write_client_info);

		ActionBar bar = getActionBar();
		bar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#f47701")));

		nfcAdpt = NfcAdapter.getDefaultAdapter(this);

		nfcMger = new NFCManager(this);
		pd = new ProgressDialog(WriteClientInfo.this);
		context = getApplicationContext();

		// final Spinner sp = (Spinner) findViewById(R.id.tagType);
		/*
		 * ArrayAdapter<CharSequence> aa = ArrayAdapter.createFromResource(this,
		 * R.array.tagContentType, android.R.layout.simple_spinner_dropdown_item);
		 * aa.setDropDownViewResource (android.R.layout.simple_spinner_dropdown_item);
		 * sp.setAdapter(aa);
		 */

		alerter = new Alerter(this);
		myApplication = (MyApplication) getApplication();
		pref = PreferenceManager.getDefaultSharedPreferences(this);

		// uniqueID = UUID.randomUUID().toString();
		//
		// String uniqueIDArray[] = uniqueID.split("-");
		// int size = uniqueIDArray.length;
		// String uniqueIDLastPart = uniqueIDArray[size - 1];

		String encyptedUUID = encryption(myApplication.getNfcTagSerial(), "@AA0KUTS=!");

		tagInfo = myApplication.getCustomerType() + pref.getString("PSPID", null) + myApplication.getDateTime()
				+ encyptedUUID;

		// Log.e("UID", myApplication.getNfcTagSerial());
		// Log.e("UID last number", uniqueIDLastPart);
		// Log.e("encyptedUUID", encyptedUUID);
		// Log.e("tagInfo", tagInfo);

		// Nfc nfc = new Nfc();
		//
		// nfc.setCustomerType(myApplication.getCustomerType());
		// nfc.setNfcTagSerial(myApplication.getNfcTagSerial());
		// nfc.setPspId(Globals.PSP_ID);
		// nfc.setDateTime(myApplication.getDateTime());
		// nfc.setCustomerWalletId(myApplication.getCustomerWalletId());
		//
		// nfcTagJson = nfc.toString();
		// phone.setText(nfcTagJson);
		writeOnTag();
		blink();

		// Intent intent = new Intent(getApplicationContext(),
		// WriteClientInfo.class);
		// startActivity(intent);

	}

	@Override
	protected void onResume() {
		super.onResume();
		if (nfcAdpt == null) {
			// Stop here, we definitely need NFC
			alerter.alerterError("This device doesn't support NFC.");
			// showerrors("This device doesn't support NFC.", "ERROR");

		} else {
			if (!nfcAdpt.isEnabled()) {
				// showerrors("Please turn on your nfc", "ERROR");
				alerter.alerterError("Please turn on your nfc.");

			} else {
				try {
					nfcMger.verifyNFC();
					// nfcMger.enableDispatch();

					Intent nfcIntent = new Intent(this, getClass());
					nfcIntent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
					PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, nfcIntent, 0);
					IntentFilter[] intentFiltersArray = new IntentFilter[] {};
					String[][] techList = new String[][] { { android.nfc.tech.Ndef.class.getName() },
							{ android.nfc.tech.NdefFormatable.class.getName() } };

					nfcAdpt.enableForegroundDispatch(this, pendingIntent, intentFiltersArray, techList);
				} catch (NFCManager.NFCNotSupported nfcnsup) {
					// Toast.makeText(getApplicationContext(),
					// "NFC not supported", Toast.LENGTH_LONG).show();

					alerter.alerterError("NFC not supported.");
				} catch (NFCManager.NFCNotEnabled nfcnEn) {

					// Toast.makeText(getApplicationContext(),
					// "NFC Not enabled",
					// Toast.LENGTH_LONG).show();
					alerter.alerterError("NFC Not enabled.");
				}
			}
		}

	}

	@Override
	protected void onPause() {
		super.onPause();
		if (nfcAdpt.isEnabled()) {
			try {
				nfcMger.disableDispatch();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

	}

	@Override
	protected void onStop() {
		// TODO Auto-generated method stub
		super.onStop();

		// REMOVE DIALOG
		if (pd != null) {
			pd.dismiss();
			// b.setEnabled(true);
		}
	}

	@Override
	public void onNewIntent(Intent intent) {
		Log.d("Nfc", "New intent");
		// It is the time to write the tag
		currentTag = intent.getParcelableExtra(NfcAdapter.EXTRA_TAG);

		if (!myApplication.getNfdTagUid().equals(bin2hex(currentTag.getId()))) {

			alerter.alerterError("Please put the same tag .");

		} else {

			if (message != null) {
				// writeTag(message, currentTag);
				// MifareUltralightTagTester mifareUltralightTagTester = new
				// MifareUltralightTagTester();
				writeTag(currentTag, tagInfo + " ");
				// dialog.dismiss();

				// Log.e("tagInfo", tagInfo + " ");

				// Toast.makeText(getApplicationContext(), tagInfo,
				// Toast.LENGTH_LONG)
				// .show();

				// alerterSuccess("Tag written");

				// write.setText("Writen data: " + saveddata);

			} else {
				// Handle intent

			}

		}

	}

	public void writeOnTag() {
		if (!nfcAdpt.isEnabled()) {

			// showerrors("Please turn on your nfc", "ERROR");

			alerter.alerterError("Please turn on your nfc.");

		} else {
			saveddata = tagInfo;
			// Log.e("decri===", decryption(saveddata, "@AA0KUTS=!"));
			// Log.e("encrypted===", saveddata);
			// Log.e("encrypted : ", encryption(tagInfo, "@AA0KUTS=!"));
			message = nfcMger.createTextMessage(saveddata);

			/*
			 * switch (pos) { case 0: message = nfcMger.createUriMessage(content,
			 * "http://"); break; case 1: message = nfcMger.createUriMessage(content,
			 * "tel:"); break; case 2:
			 * 
			 * break; }
			 */

			if (message != null) {

				// dialog = new ProgressDialog(WriteClientInfo.this);
				customToastException("Tap NFC Tag please");
				// dialog.show();
			}
		}
	}

	public String encryption(String strNormalText, String seedValue) {

		String normalTextEnc = "";
		try {
			normalTextEnc = AESHelper.encrypt(seedValue, strNormalText);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return normalTextEnc;
	}

	public String decryption(String strEncryptedText, String seedValue) {

		String strDecryptedText = "";
		try {
			strDecryptedText = AESHelper.decrypt(seedValue, strEncryptedText);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return strDecryptedText;
	}

	public void showerrors(String message, String title) {
		AlertDialog.Builder downloadDialog = new AlertDialog.Builder(this);
		downloadDialog.setTitle(title);
		downloadDialog.setMessage(message);
		downloadDialog.setCancelable(false);
		downloadDialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialogInterface, int i) {
				dialogInterface.dismiss();

			}
		});

		downloadDialog.show();
	}

	public void alerterSuccess(String message) {
		LayoutInflater li = LayoutInflater.from(WriteClientInfo.this);
		View promptsView;
		promptsView = li.inflate(R.layout.dialog_success_simple, null);

		// TextView accountTxt = (TextView)
		// promptsView.findViewById(R.id.confirm_dialog_acct);
		TextView messageTxt = (TextView) promptsView.findViewById(R.id.success_simple_message);

		messageTxt.setText(message);

		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(WriteClientInfo.this);

		// set prompts.xml to alertdialog builder
		alertDialogBuilder.setView(promptsView);
		// set dialog message
		alertDialogBuilder.setCancelable(false).setPositiveButton("Ok", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {

				myApplication.cleanNfcInfo();
				Intent intent = new Intent(WriteClientInfo.this, GetClientTag.class);
				startActivity(intent);

			}
		});

		// create alert dialog
		AlertDialog alertDialog = alertDialogBuilder.create();

		// show it
		alertDialog.show();
	}

	public void customToast(String message) {
		LayoutInflater inflater = getLayoutInflater();
		View toastLayout = inflater.inflate(R.layout.custom_toast, (ViewGroup) findViewById(R.id.custom_toast_layout));
		TextView tvCustomToastMessage = (TextView) toastLayout.findViewById(R.id.tv_custom_toast_message);
		tvCustomToastMessage.setText(message);

		Toast toast = new Toast(getApplicationContext());
		toast.setDuration(Toast.LENGTH_LONG);
		toast.setView(toastLayout);
		toast.setGravity(Gravity.CENTER, 0, 0);
		toast.show();

		Intent iGoHome = new Intent(getApplicationContext(), GetClientTag.class);
		startActivity(iGoHome);
	}

	public void customToastException(String message) {
		LayoutInflater inflater = getLayoutInflater();
		View toastLayout = inflater.inflate(R.layout.custom_toast, (ViewGroup) findViewById(R.id.custom_toast_layout));
		TextView tvCustomToastMessage = (TextView) toastLayout.findViewById(R.id.tv_custom_toast_message);
		tvCustomToastMessage.setText(message);

		Toast toast = new Toast(getApplicationContext());
		toast.setDuration(Toast.LENGTH_LONG);
		toast.setView(toastLayout);
		toast.setGravity(Gravity.CENTER, 0, 0);
		toast.show();

	}

	public String getDateWellFormated(String input) {

		String splitDate[] = input.split(" ");
		String date = splitDate[0];
		String dateArray[] = date.split("-");
		String year = dateArray[0];
		String month = dateArray[1];
		String day = dateArray[2];

		String time = splitDate[1];
		String timeArray[] = time.split(":");
		String hour = timeArray[0];
		String minutes = timeArray[1];
		String seconds = timeArray[2];

		// String formattedDate = year + month + day + hour + minutes + seconds;
		String formattedDate = year + month + day;

		return formattedDate;
	}

	// public void writeTag(Tag tag, NdefMessage message) {
	// if (tag != null) {
	// try {
	// Ndef ndefTag = Ndef.get(tag);
	//
	// if (ndefTag == null) {
	// // Let's try to format the Tag in NDEF
	// NdefFormatable nForm = NdefFormatable.get(tag);
	// if (nForm != null) {
	// nForm.connect();
	// nForm.format(message);
	// nForm.close();
	// }
	// } else {
	// ndefTag.connect();
	// ndefTag.writeNdefMessage(message);
	// ndefTag.close();
	// }
	//
	// customToast("Tag is ready to be used");
	//
	// } catch (TagLostException e) {
	// customToastException("Unable to write , please try again .");
	// writeOnTag();
	// } catch (Exception e) {
	//
	// customToastException("Unable to write , please try again .");
	// writeOnTag();
	// e.printStackTrace();
	// }
	// }
	// }

	public void blink() {
		ImageView image = (ImageView) findViewById(R.id.iv_icon_nfc);
		Animation animation1 = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.blink);
		image.startAnimation(animation1);
	}

	// boolean writeTag(NdefMessage message, Tag tag) {
	//
	// int size = message.toByteArray().length;
	//
	// try {
	//
	// if (tag != null) {
	// Ndef ndef = Ndef.get(tag);
	// if (ndef != null) {
	// ndef.connect();
	//
	// if (!ndef.isWritable()) {
	// customToastException("Cannot write to this tag. This tag is read-only.");
	// return false;
	// }
	//
	// if (ndef.getMaxSize() < size) {
	// customToastException("Cannot write to this tag. Message size ("
	// + size
	// + " bytes) exceeds this tag's capacity of "
	// + ndef.getMaxSize() + " bytes.");
	// return false;
	// }
	//
	// ndef.writeNdefMessage(message);
	// ndef.close();
	// // setReadOnly(tag);
	//
	// customToast("Tag is ready to be used.");
	// return true;
	// } else {
	//
	// NdefFormatable formatable = NdefFormatable.get(tag);
	// if (formatable != null) {
	// formatable.connect();
	// formatable.format(message);
	// customToast("Tag is ready to be used.");
	// formatable.close();
	// // setReadOnly(tag);
	// return true;
	//
	// } else {
	// customToastException("Tag doesn't support NDEF , use another Tag.");
	// return false;
	// }
	//
	// }
	//
	// } else {
	// customToastException("Unrecognized tag");
	// return false;
	// }
	//
	// } catch (Exception e) {
	// Toast.makeText(this, e.toString(), Toast.LENGTH_LONG).show();
	// }
	//
	// return false;
	// }

	/**
	 * Write text to NFC Tag! Text size should be less than size of NFC tag memory!
	 * 46 bytes
	 * 
	 * @param tag
	 * @param tagText
	 * @return
	 */
	public boolean writeTag(Tag tag, String tagText) {

		pd.setMessage("please don't remove the tag ...");
		pd.setCancelable(false);
		pd.setIndeterminate(true);
		pd.show();

		boolean result = true;
		MifareUltralight ultralight = MifareUltralight.get(tag);
		if (tagText.length() < (blockNumber - blockOffset) * blockSize) {
			ArrayList<String> list = new ArrayList<String>();
			int start = 0;
			int end = blockSize;

			while (start < tagText.length()) {
				String text;
				try {

					text = tagText.substring(start, end);
				} catch (Exception e) {
					text = tagText.substring(start, tagText.length());

					// REMOVE DIALOG
					if (pd != null) {
						pd.dismiss();
						// b.setEnabled(true);
					}

					customToastException("Unable to write , please try again .");
				}
				Log.e(TAG, " - [" + text + "]");
				list.add(text);
				start += blockSize;
				end += blockSize;
			}
			try {
				ultralight.connect();

				int page = blockOffset;
				for (String text : list) {
					Log.e(TAG, "page=" + page + " [" + text + "]");
					ultralight.writePage(page, text.getBytes(Charset.forName("US-ASCII")));
					page++;
				}

				// set read only

				// byte[] makeReadOnly;
				// makeReadOnly = ultralight.transceive(new byte[] { (byte) 0xA2,
				// (byte) 0x02, (byte) 0x00, (byte) 0x00, (byte) 0xFF,
				// (byte) 0xFF });

				customToast("Tag is ready to be used.");

			} catch (IOException e) {
				Log.e(TAG, "IOException while closing MifareUltralight...", e);
				customToastException("Unable to write , please try again .");
				result = false;
				// REMOVE DIALOG
				if (pd != null) {
					pd.dismiss();
					// b.setEnabled(true);
				}
			} finally {
				try {
					ultralight.close();
					// REMOVE DIALOG
					if (pd != null) {
						pd.dismiss();
						// b.setEnabled(true);
					}
				} catch (IOException e) {
					Log.e(TAG, "IOException while closing MifareUltralight...", e);
					result = false;

					// REMOVE DIALOG
					if (pd != null) {
						pd.dismiss();
						// b.setEnabled(true);
					}

					customToastException("Unable to write , please try again .");
				}
			}
		} else {
			Log.e(TAG, "Text size bigger than MilfareUltralight size (46 byte)");
			result = false;
			// REMOVE DIALOG
			if (pd != null) {
				pd.dismiss();
				// b.setEnabled(true);
			}

			customToastException("Text size bigger than MilfareUltralight C tag .");

		}
		return result;
	}

	public void setReadOnly(Tag tag) {

		try {

			MifareUltralight ultralight = MifareUltralight.get(tag);
			ultralight.connect();
			byte[] result;
			result = ultralight.transceive(
					new byte[] { (byte) 0xA2, (byte) 0x02, (byte) 0x00, (byte) 0x00, (byte) 0xFF, (byte) 0xFF });
			ultralight.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			customToastException(e.toString());
		}
	}

	static String bin2hex(byte[] data) {
		return String.format("%0" + (data.length * 2) + "X", new BigInteger(1, data));
	}

}