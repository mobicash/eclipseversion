package ez.pay.project;

import utils.ConnectionDetector;
import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.RadioButton;
import android.widget.Toast;

public class Settings extends Activity {

	String server;
	// Connection detector class
	ConnectionDetector cd;

	SharedPreferences prefs;
	Editor edit;
	String permission;
	String activityIdentify;
	RadioButton radioBroker, radioIndependent;
	String clientType;

	// connectoin params

	// Transaction object

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.settings_layout);

		prefs = PreferenceManager.getDefaultSharedPreferences(this);
		clientType = prefs.getString(Globals.permission, null);
		edit = prefs.edit();

		// action bar styling
		ActionBar bar = getActionBar();
		bar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#f47701")));

		activityIdentify = prefs.getString("first_use", null);

		if (activityIdentify != null) {
			Intent intent = new Intent(Settings.this, Services.class);
			startActivity(intent);
		}

		// grid info

		init();

	}

	// ------------------------------------

	private void init() {
		// TODO Auto-generated method stub
		radioBroker = (RadioButton) findViewById(R.id.radio_broker);
		radioIndependent = (RadioButton) findViewById(R.id.radio_independent);

		if (clientType != null && !clientType.isEmpty()) {

			if (clientType.equals(Globals.broker)) {
				radioBroker.setChecked(true);
				radioIndependent.setChecked(false);

			}

			if (clientType.equals(Globals.independent)) {
				radioBroker.setChecked(false);
				radioIndependent.setChecked(true);

			}

		}

	}

	@Override
	protected void onDestroy() {

		super.onDestroy();
	}

	public void onChooseClick(View view) {

		switch (view.getId()) {

		case R.id.radio_broker:
			permission = Globals.broker;
			break;

		case R.id.radio_independent:
			permission = Globals.independent;
			break;
		}
	}

	public void permissionClick(View view) {

		if (permission == Globals.independent) {
			edit.putString(Globals.permission, Globals.independent);
			edit.commit();

			Intent intent = new Intent(Settings.this, First_time.class);
			startActivity(intent);
		} else if (permission == Globals.broker) {
			edit.putString(Globals.permission, Globals.broker);
			edit.commit();

			Intent intenti = new Intent(Settings.this, First_time.class);
			startActivity(intenti);

		} else if (clientType != null && !clientType.isEmpty()) {
			Intent intenti = new Intent(Settings.this, First_time.class);
			startActivity(intenti);
		} else {
			Toast.makeText(getApplicationContext(), "Select permission",
					Toast.LENGTH_SHORT).show();
		}

	}

}
