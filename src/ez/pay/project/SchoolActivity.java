package ez.pay.project;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import models.Student;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.conn.ConnectTimeoutException;

import utils.ConnectUtils;
import utils.StudentParser;
import utils.Vars;
import android.app.ActionBar;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

public class SchoolActivity extends Activity {

	EditText etStudentRegistrationNumber;
	Alerter alerter;
	// ConnectionDetector cd;
	Vars vars;
	ProgressDialog pd;
	SharedPreferences pref;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_school);

		ActionBar bar = getActionBar();
		bar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#f47701")));
		pref = PreferenceManager.getDefaultSharedPreferences(this);
		initialize();
		// hide keyboard
		this.getWindow().setSoftInputMode(
				WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
	}

	private void initialize() {
		etStudentRegistrationNumber = (EditText) findViewById(R.id.et_student_reg_number);
		alerter = new Alerter(this);
		vars = new Vars(this);
	}

	public void check(View v) {

		if (etStudentRegistrationNumber.getText().length() <= 0) {
			alerter.alerterError(Globals.studentRegNumberErrorMessage);
		} else {
			new LoadInfo().execute(etStudentRegistrationNumber.getText()
					.toString());
		}

	}

	private class LoadInfo extends AsyncTask<String, Void, String> {
		private static final String TAG = "PostFetcher";
		String theResult = "NO CONNECTION MADE";
		Student student = new Student();
		String studentReg;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			pd = new ProgressDialog(SchoolActivity.this);
			pd.setTitle(Globals.refIdCheckProgressMessage);
			pd.setMessage(Globals.waitProgressMessage);
			pd.setCancelable(false);
			pd.setIndeterminate(true);
			pd.show();
		}

		@Override
		protected String doInBackground(String... params) {

			// Create an HTTP client
			studentReg = params[0];

			StringBuilder builder = new StringBuilder();
			ConnectUtils connector = new ConnectUtils();
			HttpClient client = connector.getNewHttpClient();

			String result = "FAILED";
			String urlString = null;

			// LOAD SERVER PREF

			try {
				urlString = vars.server
						+ "/bio-api/androidSchool/getStudentInfo.php?reg_number="
						+ URLEncoder.encode(studentReg, "UTF-8")
						+ "&dealer="
						+ URLEncoder.encode(
								pref.getString(Globals.permission, null),
								"UTF-8") + "&version="
						+ URLEncoder.encode(Globals.version, "UTF-8")

						+ "&dealer_name="
						+ URLEncoder.encode(
								vars.prefs.getString(Globals.DEALERNAME, null),
								"UTF-8")

				;

			} catch (UnsupportedEncodingException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

			HttpGet httpGet = new HttpGet(urlString);

			try {
				HttpResponse response = client.execute(httpGet);
				StatusLine statusLine = response.getStatusLine();
				int statusCode = statusLine.getStatusCode();
				if (statusCode == 200) {
					HttpEntity entity = response.getEntity();
					InputStream content = entity.getContent();
					BufferedReader reader = new BufferedReader(
							new InputStreamReader(content));
					String line;
					while ((line = reader.readLine()) != null) {
						builder.append(line);
					}
					result = builder.toString();

				} else {

				}
			} catch (ConnectTimeoutException e) {

				e.printStackTrace();
			} catch (ClientProtocolException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}

			return result;
		}

		@Override
		protected void onPostExecute(String json) {

			try {

				StudentParser studentParser = new StudentParser();

				Student student = studentParser.getStudent(json);

				if (student.getResult().equals("Success")) {
					// displayed info
					Intent intent = new Intent(SchoolActivity.this,
							PaySchoolFeesActivity.class);
					intent.putExtra(Globals.message, student.getMessage());
					intent.putExtra(Globals.result, student.getResult());
					intent.putExtra(Globals.error, student.getError());
					intent.putExtra(Globals.regNumber, student.getRegNumber());
					intent.putExtra(Globals.studentName,
							student.getStudentName());
					intent.putExtra(Globals.schoolName, student.getSchoolName());
					intent.putExtra(Globals.schoolCode, student.getSchoolCode());
					intent.putExtra(Globals.schoolFees, student.getSchoolFees());

					startActivity(intent);

				} else if (student.getResult().equals("FAILED")) {
					alerter.alerterError(student.getError());
				} else {
					alerter.alerterError(Globals.InternetErrorMessage);
				}

				pd.dismiss();

			} catch (Exception e) {

				if (pd != null)
					pd.dismiss();

			}

		}
	}

	public void layoutClick(View v) {
		InputMethodManager inputManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
		inputManager.hideSoftInputFromWindow(
				getCurrentFocus().getWindowToken(),
				InputMethodManager.HIDE_NOT_ALWAYS);
	}

	// CANCEL
	public void cancel(View v) {
		alerter.alerterConfirmCancelEtax(Globals.cancelPaySchoolFeesMessage);
	}

	@Override
	protected void onStop() {
		super.onStop();
		if (pd != null)
			pd.dismiss();
	}

}
