package ez.pay.project;

import org.json.JSONObject;

import utils.Vars;
import android.app.ActionBar;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;

public class SettingLogin extends BaseActivity {
	ProgressDialog pd;
	EditText agentPin;
	TextView txt;
	String imei, activityIdentify;
	SharedPreferences pref;
	Editor edit;
	JSONObject json;
	String server;
	// Connection detector class
	// ConnectionDetector cd;

	EditText settings_pwd;

	Alerter alerter;
	Context context;
	public Vars vars;
	String agentPinSaved;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.setting_login);

		ActionBar bar = getActionBar();
		bar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#f47701")));

		pref = PreferenceManager.getDefaultSharedPreferences(this);

		agentPinSaved = pref.getString("AGENTPIN", null);

		context = getApplicationContext();
		alerter = new Alerter(this);

		// CHECKS INTERNET CONNECTION
		// cd = new ConnectionDetector(getApplicationContext());
		vars = new Vars(this);
		// agent.setText("0722000801");
		agentPin = (EditText) findViewById(R.id.first_time_agtPin);

		// hide keyboard
		this.getWindow().setSoftInputMode(
				WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

	}

	public void check(View v) {

		// if (cd.isConnectingToInternet()) {
		if (agentPin.getText().toString() == null
				|| agentPin.getText().toString().isEmpty()) {
			alerter.alerterError(Globals.enterAgentPinErrorMessage);
		} else if (!agentPinSaved.equals(agentPin.getText().toString())) {
			alerter.alerterError(Globals.invalidPasswordErrorMessage);
		} else {
			Intent intent = new Intent(getApplication(), AdminActivity.class);
			startActivity(intent);
		}

	}

	// ALERT GENERATOR
	public void alerter(String title, String message) {

		// public void onClick(View arg0) {
		// loadEm();
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);

		// set title
		alertDialogBuilder.setTitle(title);

		// set dialog message
		alertDialogBuilder.setMessage(message).setCancelable(false)
				.setPositiveButton("OK", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						// if this button is clicked, close
						// current activity
						// MainActivity.this.finish();
						dialog.cancel();
					}
				});

		// create alert dialog
		AlertDialog alertDialog = alertDialogBuilder.create();

		// show it
		alertDialog.show();
		// }
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
	}

	public void layoutClick(View v) {
		InputMethodManager inputManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
		inputManager.hideSoftInputFromWindow(
				getCurrentFocus().getWindowToken(),
				InputMethodManager.HIDE_NOT_ALWAYS);
	}

	@Override
	public void onBackPressed() {
		Intent intent = new Intent(Intent.ACTION_MAIN);
		intent.addCategory(Intent.CATEGORY_HOME);
		intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		startActivity(intent);
	}

}
