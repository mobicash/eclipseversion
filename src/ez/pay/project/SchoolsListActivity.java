package ez.pay.project;

import java.util.ArrayList;

import utils.Item;
import adapters.GridViewAdapter;
import android.app.ActionBar;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

public class SchoolsListActivity extends BaseActivity implements
		OnItemClickListener {

	// grid
	ListView gridView;
	ArrayList<Item> gridArray = new ArrayList<Item>();
	GridViewAdapter customGridAdapter;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_schools_list);

		// action bar styling
		ActionBar bar = getActionBar();
		bar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#f47701")));

		gridArray.add(new Item(BitmapFactory.decodeResource(
				this.getResources(), R.drawable.academic_bridge),
				"Academic bridge"));

		gridView = (ListView) findViewById(android.R.id.list);
		customGridAdapter = new GridViewAdapter(this, R.layout.row_services,
				gridArray);
		gridView.setAdapter(customGridAdapter);

		gridView.setOnItemClickListener(this);
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		// TODO Auto-generated method stub

		// Toast.makeText(getApplicationContext(),
		// "Click ListItem Number " + position, Toast.LENGTH_LONG).show();

		switch (position) {
		case 0:

			Intent intent0 = new Intent(SchoolsListActivity.this,
					SchoolActivity.class);
			startActivity(intent0);

			break;

		}

	}
}
