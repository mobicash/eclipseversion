package ez.pay.project;

//Bouquet model

public class ClientInfo {
	private String message;
	private String result;
	private String clientNumber;
	private String clientID;
	private String clientName;
	private String clientPhoto;

	public ClientInfo() {
	}

	public ClientInfo(String message, String result, String clientNumber,
			String clientID, String clientName, String clientPhoto) {
		super();
		this.message = message;
		this.result = result;
		this.clientNumber = clientNumber;
		this.clientID = clientID;
		this.clientName = clientName;
		this.clientPhoto = clientPhoto;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}

	public String getClientNumber() {
		return clientNumber;
	}

	public void setClientNumber(String clientNumber) {
		this.clientNumber = clientNumber;
	}

	public String getClientID() {
		return clientID;
	}

	public void setClientID(String clientID) {
		this.clientID = clientID;
	}

	public String getClientName() {
		return clientName;
	}

	public void setClientName(String clientName) {
		this.clientName = clientName;
	}

	public String getClientPhoto() {
		return clientPhoto;
	}

	public void setClientPhoto(String clientPhoto) {
		this.clientPhoto = clientPhoto;
	}
	
	

	

}
