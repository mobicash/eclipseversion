package ez.pay.project;

import java.io.UnsupportedEncodingException;

import org.json.JSONObject;

import services.BluetoothService;
import utils.ConnectionDetector;
import android.app.ActionBar;
import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

public class SingleAgentLog extends BaseActivity {

	EditText agent;
	EditText agentPin;
	TextView txt;
	String imei;
	private ProgressBar progressBar;
	SharedPreferences pref;
	Editor edit;
	JSONObject json;
	String server, listName;
	// Connection detector class
	ConnectionDetector cd;

	EditText settings_pwd;

	Alerter alerter;

	TextView txtTransactionId, txtMember, txtTransactionType, txtAmount,
			txtStatus, txtDate, txtDescription;

	CheckBox cbPrint;

	// bluetooth

	private static final String TAG = "BluetoothPrint";
	private static final boolean D = true;

	// ��BluetoothService�����������Ϣ����
	public static final int MESSAGE_STATE_CHANGE = 1;
	public static final int MESSAGE_READ = 2;
	public static final int MESSAGE_WRITE = 3;
	public static final int MESSAGE_DEVICE_NAME = 4;
	public static final int MESSAGE_TOAST = 5;

	// BluetoothService�յ��Ĵ������ؼ�������
	public static final String DEVICE_NAME = "device_name";
	public static final String TOAST = "toast";

	// ����Ĵ���
	private static final int REQUEST_CONNECT_DEVICE = 1;
	private static final int REQUEST_ENABLE_BT = 2;
	// ���ӵ��豸������
	private String mConnectedDeviceName = null;
	// ��������������
	private BluetoothAdapter mBluetoothAdapter = null;
	public static BluetoothService mService = null;

	MyApplication myapplication;
	TextView tvPrinterMessage;

	String transactionId, member, transactionType, amount, status, date,
			description;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.layout_single_agent_log);
		ActionBar bar = getActionBar();
		bar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#f47701")));

		init();

		Bundle extras = getIntent().getExtras();
		if (extras != null) {

			listName = extras.getString("listName");

			if (listName.contains("Commission")) {
				this.setTitle("Commission");
			} else {
				this.setTitle("Historic");
			}

			setFields(extras.getString(Globals.agentLogTransactionID),
					extras.getString(Globals.agentLogMember),
					extras.getString(Globals.agentLogtransactionType),
					extras.getString(Globals.agentLogamount),
					extras.getString(Globals.agentLogStatus),
					extras.getString(Globals.agentLogDate),
					extras.getString(Globals.agentLogDescription));
		}

		// �õ����ص�������
		mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

		if (mBluetoothAdapter == null) {
			Toast.makeText(this, "��������ʹ�ã�", Toast.LENGTH_LONG).show();
			finish();
			return;
		}

	}

	// printer method

	public void printClick(View v) {
		if (((CheckBox) v).isChecked()) {

			if (myapplication.getmService().getState() != BluetoothService.STATE_CONNECTED) {
				// Toast.makeText(this, R.string.not_connected,
				// Toast.LENGTH_SHORT).show();
				try {

					Log.e("printClick ",
							"********printClick mService.getState() != BluetoothService.STATE_CONNECTED *******");

					Intent serverIntent = new Intent(this,
							DeviceListActivity.class);
					startActivityForResult(serverIntent, REQUEST_CONNECT_DEVICE);

					return;
				} catch (Exception e) {

				}
			}

		}
	}

	@Override
	public void onStart() {
		super.onStart();
		Log.e("Log ", "********onStart*******");
		if (D)
			// Log.e(TAG, "++ ON START ++");
			// �ж������Ƿ������������û�п���������������
			if (!mBluetoothAdapter.isEnabled()) {
				Log.e("printClick ",
						"********onStart !mBluetoothAdapter.isEnabled() *******");
				Intent enableIntent = new Intent(
						BluetoothAdapter.ACTION_REQUEST_ENABLE);
				startActivityForResult(enableIntent, REQUEST_ENABLE_BT);
			} else {
				if (myapplication.getmService() == null) {
					Log.e("printClick ", "********setupChat *******");
					setupChat();
				}

			}
	}

	@Override
	public synchronized void onResume() {
		super.onResume();
		Log.e("Log ", "********onResume*******");
		if (D)
			// Log.e(TAG, "+ ON RESUME +");
			if (myapplication.getmService() != null) {
				if (myapplication.getmService().getState() == BluetoothService.STATE_NONE) {
					Log.e("printClick ",
							"********onResume mService.getState() == BluetoothService.STATE_NONE *******");
					myapplication.getmService().start();
				}

			}

		if (myapplication.getmService().getState() == BluetoothService.STATE_CONNECTED) {
			// ivPrinterIcon.setBackgroundResource(R.drawable.log);
			tvPrinterMessage.setText(getString(R.string.connected_txt));
			tvPrinterMessage.setTextColor(Color.parseColor("#398f14"));
			Log.e("printClick ",
					"********onResume mService.getState() == BluetoothService.STATE_CONNECTED *******");
		} else {
			// ivPrinterIcon.setBackgroundResource(R.drawable.not_log);
			tvPrinterMessage.setText(getString(R.string.not_connected_txt));
			tvPrinterMessage.setTextColor(Color.parseColor("#e34949"));
			Log.e("printClick ",
					"********onResume mService.getState() != BluetoothService.STATE_CONNECTED *******");
		}

	}

	private void setupChat() {
		Log.d(TAG, "SetupChat");

		// ��ʼ��BluetoothServiceִ����������
		myapplication.setmService(new BluetoothService(this, mHandler));

	}

	private void sendMessage(String message) {
		// ������ڳ�������

		// ���ʵ��Ҫ���͵Ķ���
		if (message.length() > 0) {
			// �õ���Ϣ�ֽڲ��Ҹ���BluetoothService
			byte[] send;
			try {
				send = message.getBytes("GB2312");
			} catch (UnsupportedEncodingException e) {
				send = message.getBytes();
			}
			// mService.write(send,10);
			myapplication.getmService().write(send, 0, send.length);
		}
	}

	@Override
	public synchronized void onPause() {
		super.onPause();
		if (D)
			Log.e(TAG, "- ON PAUSE -");
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		// Stop the Bluetooth services
		if (myapplication.getmService() != null)
			myapplication.getmService().stop();
		if (D)
			Log.e(TAG, "--- ON DESTROY ---");
	}

	// ���±������ұ�״̬�Ͷ�д״̬��Handler
	private final Handler mHandler = new Handler() {

		@Override
		public void handleMessage(Message msg) {
			switch (msg.what) {
			case MESSAGE_STATE_CHANGE:
				if (D)
					Log.i(TAG, "MESSAGE_STATE_CHANGE:" + msg.arg1);
				switch (msg.arg1) {
				case BluetoothService.STATE_CONNECTED:
					// mTitle.setText(R.string.title_connected_to);
					// mTitle.append(mConnectedDeviceName);
					// ivPrinterIcon.setBackgroundResource(R.drawable.log);

					tvPrinterMessage.setText(getString(R.string.connected_txt));
					tvPrinterMessage.setTextColor(Color.parseColor("#398f14"));
					Log.e("printClick ",
							"********handleMessage STATE_CONNECTED *******");

					break;
				case BluetoothService.STATE_CONNECTING:
					// mTitle.setText(R.string.title_connecting);
					tvPrinterMessage
							.setText(getString(R.string.connecting_txt));
					tvPrinterMessage.setTextColor(Color.parseColor("#000000"));
					Log.e("printClick ",
							"********handleMessage CONNECTING *******");

					break;
				case BluetoothService.STATE_LISTEN:
				case BluetoothService.STATE_NONE:
					// mTitle.setText(R.string.title_not_connected);
					// ivPrinterIcon.setBackgroundResource(R.drawable.not_log);
					tvPrinterMessage
							.setText(getString(R.string.not_connected_txt));
					tvPrinterMessage.setTextColor(Color.parseColor("#e34949"));

					Log.e("printClick ",
							"********handleMessage NOT_CONNECTED *******");

					break;
				}
				break;
			case MESSAGE_WRITE:
				break;
			case MESSAGE_READ:
				// ����Ч�ֽڻ���������һ���ַ���
				break;
			case MESSAGE_DEVICE_NAME:
				// ���������豸������
				mConnectedDeviceName = msg.getData().getString(DEVICE_NAME);
				// Toast.makeText(getApplicationContext(),
				// "���ӵ�" + mConnectedDeviceName, Toast.LENGTH_SHORT)
				// .show();
				alerter.alerterSuccessSimpleHere(mConnectedDeviceName
						+ " connected / " + mConnectedDeviceName
						+ " iracometse neza ");

				break;
			case MESSAGE_TOAST:
				// Toast.makeText(getApplicationContext(),
				// msg.getData().getString(TOAST), Toast.LENGTH_SHORT)
				// .show();

				alerter.alerterError("Error retry / ikibazo ongera ugerageze ");

				break;

			}

		}
	};

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (D)
			Log.d(TAG, "onActivityResult" + resultCode);
		switch (requestCode) {
		case REQUEST_CONNECT_DEVICE:
			// ��DeviceListActivity����һ���豸����
			if (resultCode == Activity.RESULT_OK) {

				// ��ȡ�豸��MAC��ַ
				String address = data.getExtras().getString(
						DeviceListActivity.EXTRA_DEVICE_ADDRESS);
				// �õ�BLuetoothDevice����
				if (address == null) {
					break;
				}
				// ��ȡ�豸��������
				BluetoothDevice device = mBluetoothAdapter
						.getRemoteDevice(address);
				// �������ӵ��豸
				myapplication.getmService().connect(device);
			}
			break;
		case REQUEST_ENABLE_BT:
			// �����󼤻������Ļر�
			if (resultCode == Activity.RESULT_OK) {
				// �������������õ�,���,����һ������Ự
				// ���������ɹ����������ʼ��UI
				setupChat();

			} else {
				// �û�û���������������
				Log.d(TAG, "BT not enabled");
				// Toast.makeText(this, R.string.bt_not_enabled_leaving,
				// Toast.LENGTH_SHORT).show();
				alerter.alerterError(getString(R.string.bt_not_enabled_leaving));
				finish();
			}

			/*
			 * case REQUEST_Set:
			 * 
			 * if(resultCode==Activity.RESULT_OK) {
			 * 
			 * byte a[]=data.getExtras().getByteArray("data");
			 * mService.write(a,0,a.length); Toast.makeText(this, "���óɹ���",
			 * Toast.LENGTH_SHORT).show(); } break;
			 */
		}
	}

	@Override
	public void onBackPressed() {
		android.os.Process.killProcess(android.os.Process.myPid());
	}

	private void init() {
		// TODO Auto-generated method stub
		alerter = new Alerter(this);
		txtTransactionId = (TextView) findViewById(R.id.txt_transaction_id);
		txtMember = (TextView) findViewById(R.id.txt_member);
		txtTransactionType = (TextView) findViewById(R.id.txt_transaction_type);
		txtAmount = (TextView) findViewById(R.id.txt_amount);
		txtStatus = (TextView) findViewById(R.id.txt_status);
		txtDate = (TextView) findViewById(R.id.txt_date);
		txtDescription = (TextView) findViewById(R.id.txt_description);
		pref = PreferenceManager.getDefaultSharedPreferences(this);
		myapplication = (MyApplication) getApplication();
		tvPrinterMessage = (TextView) findViewById(R.id.tv_printer_message);
		cbPrint = (CheckBox) findViewById(R.id.cb_print);

	}

	private void setFields(String transactionId, String member,
			String transactionType, String amount, String status, String date,
			String description) {
		// TODO Auto-generated method stub

		txtTransactionId.setText(transactionId);
		this.transactionId = transactionId;

		txtMember.setText(member);
		this.member = member;

		txtTransactionType.setText(transactionType);
		this.transactionType = transactionType;

		txtAmount.setText(amount);
		this.amount = amount;

		txtStatus.setText(status);
		this.status = status;

		txtDate.setText(date);
		this.date = date;

		txtDescription.setText(description);
		this.description = description;

	}

	public void print(View v) {

		if (cbPrint.isChecked()
				&& myapplication.getmService().getState() == BluetoothService.STATE_CONNECTED) {

			String title = getString(R.string.log);
			String thankUMessage = "Thank you for using MCash ";
			String keepEnjoyingMessage = "Keep Enjoying !";
			String star = "******************************";
			String line = "------------------------------";

			int width = 30;

			int padSize = width - title.length();
			int padStart = title.length() + padSize / 2;

			title = String.format("%" + padStart + "s", title);
			title = String.format("%-" + width + "s", title);

			String mobicashText = "MobiCash LTD";
			String tinNumberText = "TIN No : 101858540";
			String mobicashPhoneText = "Tel: (250)787689185/SMS:8485";
			String bodifaHouseText = "Bodifa Mercy House,5th Floor";
			String KimihururaKigaliText = "Kimihurura,Kigali";

			String message = mobicashText + "\n" + tinNumberText + "\n"
					+ mobicashPhoneText + "\n" + bodifaHouseText + "\n"
					+ KimihururaKigaliText + "\n" + line + "\n\n"
					+ getString(R.string.transaction_id) + "\n" + transactionId
					+ "\n\n" + getString(R.string.member) + "\n" + member
					+ "\n\n" + getString(R.string.transaction_type) + "\n"
					+ transactionType + "\n\n"
					+ getString(R.string.upper_description) + "\n"
					+ description + "\n\n" + getString(R.string.amount_print)
					+ "\n" + amount + "\n\n" + getString(R.string.status)
					+ "\n" + status + "\n\n" + getString(R.string.upper_date)
					+ "\n" + date + "\n\n" + line + "\n" + thankUMessage + "\n"
					+ keepEnjoyingMessage + "\n\n\n\n";
			sendMessage(message);

		} else {
			alerter.alerterError("Printer not connected !!!");
		}

	}

}
