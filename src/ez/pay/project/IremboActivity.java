package ez.pay.project;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import models.Irembo;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.conn.ConnectTimeoutException;

import utils.ConnectUtils;
import utils.IremboParser;
import utils.Vars;
import android.app.ActionBar;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

public class IremboActivity extends BaseActivity {

	EditText etBillNumber;
	Alerter alerter;
	// ConnectionDetector cd;
	Vars vars;
	ProgressDialog pd;
	SharedPreferences pref;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.irembo_activity);
		ActionBar bar = getActionBar();
		bar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#f47701")));
		pref = PreferenceManager.getDefaultSharedPreferences(this);
		initialize();
		// hide keyboard
		this.getWindow().setSoftInputMode(
				WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
	}

	private void initialize() {
		// TODO Auto-generated method stub
		etBillNumber = (EditText) findViewById(R.id.et_bill_number);
		alerter = new Alerter(this);
		// cd = new ConnectionDetector(this);
		vars = new Vars(this);
	}

	public void check(View v) {
		String billNumber = etBillNumber.getText().toString();

		// if (cd.isConnectingToInternet()) {
		if (billNumber.length() <= 0) {
			alerter.alerterError(Globals.billNumberErrorMessage);
		} else {
			new LoadInfo().execute(billNumber);
		}

	}

	private class LoadInfo extends AsyncTask<String, Void, String> {
		String billNumber;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			pd = new ProgressDialog(IremboActivity.this);
			pd.setTitle(Globals.refIdCheckProgressMessage);
			pd.setMessage(Globals.waitProgressMessage);
			pd.setCancelable(false);
			pd.setIndeterminate(true);
			pd.show();
		}

		@Override
		protected String doInBackground(String... params) {

			// Create an HTTP client
			billNumber = params[0];

			StringBuilder builder = new StringBuilder();
			ConnectUtils connector = new ConnectUtils();
			HttpClient client = connector.getNewHttpClient();

			String result = "FAILED";
			String urlString = null;

			// LOAD SERVER PREF

			try {
				urlString = vars.server
						+ "/bio-api/androidIrembo/iremboGetRefDetails.php?bill_number="
						+ URLEncoder.encode(billNumber, "UTF-8")
						+ "&dealer="
						+ URLEncoder.encode(
								pref.getString(Globals.permission, null),
								"UTF-8")
						+ "&version="
						+ URLEncoder.encode(Globals.version, "UTF-8")

						+ "&dealer_name="
						+ URLEncoder.encode(
								vars.prefs.getString(Globals.DEALERNAME, null),
								"UTF-8")

				;

			} catch (UnsupportedEncodingException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

			HttpGet httpGet = new HttpGet(urlString);

			try {
				HttpResponse response = client.execute(httpGet);
				StatusLine statusLine = response.getStatusLine();
				int statusCode = statusLine.getStatusCode();
				if (statusCode == 200) {
					HttpEntity entity = response.getEntity();
					InputStream content = entity.getContent();
					BufferedReader reader = new BufferedReader(
							new InputStreamReader(content));
					String line;
					while ((line = reader.readLine()) != null) {
						builder.append(line);
					}
					result = builder.toString();

				} else {

				}
			} catch (ConnectTimeoutException e) {

				e.printStackTrace();
			} catch (ClientProtocolException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}

			return result;
		}

		@Override
		protected void onPostExecute(String json) {

			try {

				IremboParser iremboParser = new IremboParser();

				Irembo irembo = iremboParser.getIrembo(json);

				if (irembo.getResult().equals("Success")) {
					// displayed info
					Intent intent = new Intent(IremboActivity.this,
							IremboPayActivity.class);

					intent.putExtra(Globals.billerCode, irembo.getBiller_code());
					intent.putExtra(Globals.billNumber, irembo.getBill_number());
					intent.putExtra(Globals.description,
							irembo.getDescription());
					intent.putExtra(Globals.customerName,
							irembo.getCustome_r_name());
					intent.putExtra(Globals.date, irembo.getCreation_date());
					intent.putExtra(Globals.amount, irembo.getAmount());
					intent.putExtra(Globals.customerIdNumber,
							irembo.getCustomer_id_number());
					intent.putExtra(Globals.mobile, irembo.getMobile());
					intent.putExtra(Globals.rraAccountName,
							irembo.getRra_account_name());
					intent.putExtra(Globals.rraAccountNumber,
							irembo.getRra_account_number());

					startActivity(intent);

				} else {
					alerter.alerterError(irembo.getMessage());
				}

				pd.dismiss();

			} catch (Exception e) {

				if (pd != null)
					pd.dismiss();

			}

		}
	}

	public void layoutClick(View v) {
		InputMethodManager inputManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
		inputManager.hideSoftInputFromWindow(
				getCurrentFocus().getWindowToken(),
				InputMethodManager.HIDE_NOT_ALWAYS);
	}

	// CANCEL
	public void cancel(View v) {
		alerter.alerterConfirmCancelEtax(Globals.cancelIremboPaymentMessage);
	}

	@Override
	protected void onStop() {
		super.onStop();
		if (pd != null)
			pd.dismiss();
	}

}
