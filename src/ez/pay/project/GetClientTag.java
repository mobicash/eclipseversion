package ez.pay.project;

import java.io.IOException;
import java.math.BigInteger;
import java.nio.charset.Charset;
import java.util.Calendar;

import models.Area;
import utils.AESHelper;
import utils.MifareUltralightTagTester;
import utils.NFCManager;
import utils.Vars;
import android.app.ActionBar;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.media.MediaPlayer;
import android.nfc.NdefMessage;
import android.nfc.NfcAdapter;
import android.nfc.Tag;
import android.nfc.tech.MifareUltralight;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class GetClientTag extends BaseActivity {
	private NfcAdapter mNfcAdapter;
	private PendingIntent mPendingIntent;
	NdefMessage mNdefPushMessage;
	Vars vars;

	Alerter alerter;
	String nfcValue;
	MyApplication myApplication;
	SharedPreferences pref;
	private NdefMessage message = null;
	private NFCManager nfcMger;

	private static int blockOffset = 4;
	private static int blockNumber = 48;
	private static int blockSize = 4;

	private static final String TAG = MifareUltralightTagTester.class
			.getSimpleName();

	NfcAdapter nfcAdpt;
	Tag tag;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		ActionBar bar = getActionBar();
		bar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#f47701")));

		vars = new Vars(this);
		alerter = new Alerter(this);
		myApplication = (MyApplication) getApplication();
		pref = PreferenceManager.getDefaultSharedPreferences(this);

		nfcAdpt = NfcAdapter.getDefaultAdapter(this);

		nfcMger = new NFCManager(this);

		if (!pref.contains("PSPID")) {
			// customToast("Please set PSP Id");
			Intent intent = new Intent(getApplicationContext(),
					SettingLogin.class);
			startActivity(intent);
		}

		setContentView(R.layout.get_client_tag);
		blink();

	}

	@Override
	public void onNewIntent(Intent intent) {

		try {

			tag = intent.getParcelableExtra(NfcAdapter.EXTRA_TAG);
			myApplication.setNfdTagUid(bin2hex(tag.getId()));

			byte[] id = tag.getId();

			if (id.length != 0) {
				// do it here

				checkIfTagExist(tag);

			}

		} catch (Exception ioException) {
			myApplication.setNfdTagUid(bin2hex(tag.getId()));

			Intent intent1 = new Intent(getApplicationContext(),
					SendClientInfo.class);
			startActivity(intent1);

			// setIntent(intent);
			// resolveIntent(intent);

		}

	}

	public static void playNotification(Context context) {
		MediaPlayer mp = MediaPlayer.create(context, R.raw.beep);
		mp.start();
	}

	public void customToast(String message) {
		LayoutInflater inflater = getLayoutInflater();
		View toastLayout = inflater.inflate(R.layout.custom_toast,
				(ViewGroup) findViewById(R.id.custom_toast_layout));
		TextView tvCustomToastMessage = (TextView) toastLayout
				.findViewById(R.id.tv_custom_toast_message);
		tvCustomToastMessage.setText(message);

		Toast toast = new Toast(getApplicationContext());
		toast.setDuration(Toast.LENGTH_LONG);
		toast.setView(toastLayout);
		toast.setGravity(Gravity.CENTER, 0, 0);
		toast.show();
	}

	static String bin2hex(byte[] data) {
		return String.format("%0" + (data.length * 2) + "X", new BigInteger(1,
				data));
	}

	public String decryption(String strEncryptedText, String seedValue) {

		String strDecryptedText = "";
		try {
			strDecryptedText = AESHelper.decrypt(seedValue, strEncryptedText);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return strDecryptedText;
	}

	@Override
	public void onBackPressed() {
		Intent intent = new Intent(Intent.ACTION_MAIN);
		intent.addCategory(Intent.CATEGORY_HOME);
		intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		startActivity(intent);
	}

	public void blink() {
		ImageView image = (ImageView) findViewById(R.id.iv_icon_nfc);
		Animation animation1 = AnimationUtils.loadAnimation(
				getApplicationContext(), R.anim.blink);
		image.startAnimation(animation1);
	}

	public String readTag(Tag tag) {
		MifareUltralight mifare = MifareUltralight.get(tag);
		try {
			mifare.connect();
			byte[] payload1 = mifare.readPages(4);
			String part1 = new String(payload1, Charset.forName("US-ASCII"));
			byte[] payload2 = mifare.readPages(8);
			String part2 = new String(payload2, Charset.forName("US-ASCII"));

			byte[] payload3 = mifare.readPages(12);
			String part3 = new String(payload3, Charset.forName("US-ASCII"));

			byte[] payload4 = mifare.readPages(14);
			String part4 = new String(payload4, Charset.forName("US-ASCII"));
			part4 = part4.substring(8);

			return part1 + part2 + part3 + part4;
		} catch (IOException e) {

			Intent intent = new Intent(getApplicationContext(),
					SendClientInfo.class);
			startActivity(intent);
			Log.e(TAG, "IOException while writing MifareUltralight message...",
					e);
			// customToast("unrecognized tag");
		} catch (Exception e) {

			Intent intent = new Intent(getApplicationContext(),
					SendClientInfo.class);
			startActivity(intent);
			// customToast("unrecognized tag");
		} finally {
			if (mifare != null) {
				try {
					mifare.close();
				} catch (IOException e) {
					Log.e(TAG, "Error closing tag...", e);
					// customToast("unrecognized tag");

					Intent intent = new Intent(getApplicationContext(),
							SendClientInfo.class);
					startActivity(intent);
				}
			}
		}
		return null;
	}

	@Override
	protected void onPause() {
		super.onPause();
		if (nfcAdpt.isEnabled()) {
			try {
				nfcMger.disableDispatch();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	protected void onResume() {
		super.onResume();
		if (nfcAdpt == null) {
			// Stop here, we definitely need NFC
			alerter.alerterError("This device doesn't support NFC.");
			// showerrors("This device doesn't support NFC.", "ERROR");

		} else {
			if (!nfcAdpt.isEnabled()) {
				// showerrors("Please turn on your nfc", "ERROR");
				alerter.alerterError("Please turn on your nfc.");

			} else {
				try {
					nfcMger.verifyNFC();
					// nfcMger.enableDispatch();

					Intent nfcIntent = new Intent(this, getClass());
					nfcIntent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
					PendingIntent pendingIntent = PendingIntent.getActivity(
							this, 0, nfcIntent, 0);
					IntentFilter[] intentFiltersArray = new IntentFilter[] {};
					String[][] techList = new String[][] {
							{ android.nfc.tech.Ndef.class.getName() },
							{ android.nfc.tech.NdefFormatable.class.getName() } };

					nfcAdpt.enableForegroundDispatch(this, pendingIntent,
							intentFiltersArray, techList);
				} catch (NFCManager.NFCNotSupported nfcnsup) {
					// Toast.makeText(getApplicationContext(),
					// "NFC not supported", Toast.LENGTH_LONG).show();

					alerter.alerterError("NFC not supported.");
				} catch (NFCManager.NFCNotEnabled nfcnEn) {

					// Toast.makeText(getApplicationContext(),
					// "NFC Not enabled",
					// Toast.LENGTH_LONG).show();
					alerter.alerterError("NFC Not enabled.");
				}
			}
		}

	}

	public void checkIfTagExist(Tag tag) {

		nfcValue = readTag(tag);
		nfcValue = nfcValue.trim();

		if (nfcValue != null) {

			if (nfcValue.length() > 23) {

				String encyptedUUID = nfcValue.substring(23, nfcValue.length());

				String deCyptedUUID = decryption(encyptedUUID, "@AA0KUTS=!");

				if (deCyptedUUID != null && !deCyptedUUID.isEmpty()) {

					customToast("Data Already Exist on the TAG");

				} else {
					Intent intent = new Intent(getApplicationContext(),
							SendClientInfo.class);
					startActivity(intent);
				}
				// <end if>
			} else {
				Intent intent = new Intent(getApplicationContext(),
						SendClientInfo.class);
				startActivity(intent);
			}
			// <end if>
		} else {
			Intent intent = new Intent(getApplicationContext(),
					SendClientInfo.class);
			startActivity(intent);
		}

	}

}
