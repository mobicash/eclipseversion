package com.authentication.asynctask;


import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android_serialport_api.BarCodeAPI;

public class AsyncBarCode extends Handler {
	
	private final int PREPARE = 1;
	private final int START_DECODE = 2;
	private final int STOP_DECODE = 3;
	private final int CLOSE = 4;
	private final int PDF417 = 5;
	private final int MICRO_PDF417 = 6;
	private final int DATA_MATRIX = 7;
	
	private final byte[] buffer = new byte[1024];
	
	
	private Handler mWorkerThreadHandler;
	
	private BarCodeAPI api;
	
	public AsyncBarCode(Looper looper) {
		createHandler(looper);
		api = new BarCodeAPI();
	}
	
	private Handler createHandler(Looper looper) {
		return mWorkerThreadHandler = new WorkerHandler(looper);
	}
	
	private OnPrepareListener onPrepareListener;
	private OnStartDecodeListener onStartDecodeListener;
	private OnStopDecodeListener onStopDecodeListener;
	
	
	
	public void setOnPrepareListener(OnPrepareListener onPrepareListener) {
		this.onPrepareListener = onPrepareListener;
	}

	public void setOnStartDecodeListener(OnStartDecodeListener onStartDecodeListener) {
		this.onStartDecodeListener = onStartDecodeListener;
	}

	public void setOnStopDecodeListener(OnStopDecodeListener onStopDecodeListener) {
		this.onStopDecodeListener = onStopDecodeListener;
	}



	public interface OnPrepareListener{
		void OnPrepare();
	};
	
	public interface OnStartDecodeListener{
		void OnDecodeSuccess(byte[] data);
		void OnDecodeFail();
	};
	
	public interface OnStopDecodeListener{
		void OnStopDecodeSuccess();
		void OnStopDecodeFail();
	};
	
	
	public void prepareDecode(){
		mWorkerThreadHandler.sendEmptyMessage(PREPARE);
	}
	
	public void startDecode(){
		mWorkerThreadHandler.sendEmptyMessage(START_DECODE);
	}
	
	public void stopDecode(){
		mWorkerThreadHandler.sendEmptyMessage(STOP_DECODE);
	}
	
	public void close(){
		mWorkerThreadHandler.sendEmptyMessage(CLOSE);
	}
	
	public void setEnablePDF417(int enable){
		mWorkerThreadHandler.obtainMessage(PDF417, enable).sendToTarget();
	}
	
	public void setEnableMicroPDF417(int enable){
		mWorkerThreadHandler.obtainMessage(MICRO_PDF417, enable).sendToTarget();
	}
	
	public void setEnableDataMatrix(int enable){
		mWorkerThreadHandler.obtainMessage(DATA_MATRIX, enable).sendToTarget();
	}
	
	private void prepare(){
		api.open();
		api.setHost();
		api.decodeDataPacketFormat();
        sendEmptyMessage(PREPARE);
	}
	
	private void start(){
		int length = api.startDecode(buffer);
		api.stopDecode();
		obtainMessage(START_DECODE, length, -1).sendToTarget();
	}
	
	private void stop(){
		obtainMessage(STOP_DECODE, api.stopDecode()).sendToTarget();
	}
	

	
	
	
	@Override
	public void handleMessage(Message msg) {
		super.handleMessage(msg);
		switch (msg.what) {
		case PREPARE:
			if(onPrepareListener != null){
				onPrepareListener.OnPrepare();
			}
			break;
		case START_DECODE:
			if(onStartDecodeListener != null){
				int length = msg.arg1;
				if(length > 0){
					byte[] data = new byte[length];
					System.arraycopy(buffer, 0, data, 0, data.length);
					onStartDecodeListener.OnDecodeSuccess(data);
				}else{
					onStartDecodeListener.OnDecodeFail();
				}
			}
			break;
		case STOP_DECODE:
			if(onStopDecodeListener != null){
				if((Boolean)msg.obj){
					onStopDecodeListener.OnStopDecodeSuccess();
				}else{
					onStopDecodeListener.OnStopDecodeFail();
				}
			}
			break;
		case CLOSE:
			api.close();
			break;

		default:
			break;
		}
	}



	protected class WorkerHandler extends Handler {
		public WorkerHandler(Looper looper) {
			super(looper);
			
		}

		@Override
		public void handleMessage(Message msg) {
			switch (msg.what) {
			case PREPARE:
				prepare();
				break;
			case START_DECODE:
				start();
				break;
			case STOP_DECODE:
				stop();
				break;
			case CLOSE:
				api.close();
				break;
			case PDF417:
				api.setEnablePDF417((Integer)msg.obj);           
				break;
			case MICRO_PDF417:
				api.setEnableMicroPDF417((Integer)msg.obj);           
				break;
			case DATA_MATRIX:
				api.setEnableDataMatrix((Integer)msg.obj);
				break;
			default:
				break;
			}
		}
	}
}
