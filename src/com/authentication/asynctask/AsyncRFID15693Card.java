package com.authentication.asynctask;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android_serialport_api.RFID15693API;

public class AsyncRFID15693Card extends Handler {
	private static final int INIT = 0;
    private static final int FIND_CARD = 1;
    private static final int READ = 2;
    private static final int WRITE = 3;


	private Handler mWorkerThreadHandler;

	private RFID15693API reader;

	public AsyncRFID15693Card(Looper looper) {
		mWorkerThreadHandler = createHandler(looper);
		reader = new RFID15693API();
	}
	

	protected Handler createHandler(Looper looper) {
		return new WorkerHandler(looper);
	}
	
	private OnInitListener onInitListener;
	private OnFindCardListener onFindCardListener;
	private OnWriteListener onWriteListener;
	private OnReadListener onReadListener;
	
	
	
	public void setOnInitListener(OnInitListener onInitListener) {
		this.onInitListener = onInitListener;
	}


	public void setOnFindCardListener(OnFindCardListener onFindCardListener) {
		this.onFindCardListener = onFindCardListener;
	}


	public void setOnWriteListener(OnWriteListener onWriteListener) {
		this.onWriteListener = onWriteListener;
	}


	public void setOnReadListener(OnReadListener onReadListener) {
		this.onReadListener = onReadListener;
	}

	public interface OnInitListener{
		void initSuccess();
		void initFail();
	}
	
	public interface OnFindCardListener{
		void findSuccess(byte[] data);
		void findFail();
	}
	
	public interface OnWriteListener{
		void writeSuccess();
		void writeFail();
	}
	
	public interface OnReadListener{
		void readSuccess(byte[] data);
		void readFail();
	}
	
	public void init(){
		mWorkerThreadHandler.sendEmptyMessage(INIT);
	}
	
	public void findCard(){
		mWorkerThreadHandler.sendEmptyMessage(FIND_CARD);
	}
	
	public void write(int position,byte[] data){
		mWorkerThreadHandler.obtainMessage(WRITE, position, -1, data).sendToTarget();
	}
	
	public void read(int position){
		mWorkerThreadHandler.obtainMessage(READ, position, -1).sendToTarget();
	}

	protected class WorkerHandler extends Handler {
		public WorkerHandler(Looper looper) {
			super(looper);
		}

		@Override
		public void handleMessage(Message msg) {
			switch (msg.what) {
			case INIT:
                AsyncRFID15693Card.this.obtainMessage(INIT, initReader()).sendToTarget();
				break;
			case FIND_CARD:
				AsyncRFID15693Card.this.obtainMessage(FIND_CARD, reader.findCard()).sendToTarget();
				break;
			case READ:
				AsyncRFID15693Card.this.obtainMessage(READ, readData(msg.arg1)).sendToTarget();
				break;
			case WRITE:
				AsyncRFID15693Card.this.obtainMessage(WRITE, writeData(msg.arg1, (byte[])msg.obj)).sendToTarget();
				break;
			default:
				break;
			}
		}
	}



	@Override
	public void handleMessage(Message msg) {
		super.handleMessage(msg);
		switch (msg.what) {
		case INIT:
			if((Boolean)msg.obj){
				if(onInitListener != null){
					onInitListener.initSuccess();
				}
			}else{
				if(onInitListener != null){
					onInitListener.initFail();
				}
			}
			break;
		case FIND_CARD:
			byte[] findData = (byte[])msg.obj;
			if(findData != null){
				if(onFindCardListener != null){
					onFindCardListener.findSuccess(findData);
				}
			}else{
				if(onFindCardListener != null){
					onFindCardListener.findFail();
				}
			}
			break;
		case READ:
			byte[] readData = (byte[])msg.obj;
			if(readData != null){
				if(onReadListener != null){
					onReadListener.readSuccess(readData);
				}
			}else{
				if(onReadListener != null){
					onReadListener.readFail();
				}
			}
			break;
		case WRITE:
			if((Boolean)msg.obj){
				if(onWriteListener != null){
					onWriteListener.writeSuccess();
				}
			}else{
				if(onWriteListener != null){
					onWriteListener.writeFail();
				}
			}
			break;
		default:
			break;
		}
	}
	
	private boolean initReader(){
		if(reader.configurationReaderMode()){
			if(reader.configurationProtocolMode()){
				if(reader.setCheckCode()){
					return true;
				}
			}
		}
		return false;
	}
	
	private byte[] readData(int position){
		byte[] findData = reader.findCard();
		if(findData != null){
			byte[] uid = new byte[8];
			System.arraycopy(findData, 1, uid, 0, uid.length);
			if(reader.selectCard(uid)){
				return reader.readOne(position);
			}
		}
		return null;
	}
	
	private boolean writeData(int position,byte[] data){
		byte[] findData = reader.findCard();
		if(findData != null){
			byte[] uid = new byte[8];
			System.arraycopy(findData, 1, uid, 0, uid.length);
			if(reader.selectCard(uid)){
				return reader.writeOne(position,data);
			}
		}
		return false;
	}
}
