package imageStuff;

import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;

import android.graphics.Bitmap;
import android.util.Log;

public class MemoryCache {
	
	String tag = "MEMORY CACHE";

	// TOGGLE TO TURN LOGGING ON AN OFF
	Boolean log = false;

	private static final String TAG = "MemoryCache";
	private Map<String, Bitmap> cache = Collections
			.synchronizedMap(new LinkedHashMap<String, Bitmap>(10, 1.5f, true));// Last
																				// argument
																				// true
																				// for
																				// LRU
																				// ordering
	private long size = 0;// current allocated size
	private long limit = 1000000;// max memory in bytes

	public MemoryCache() {
		Log.v(tag,"++++++++++++++++++++++++: MEMCACHE CREATED");
		
		// use 25% of available heap size
		// Logg.logAct("MemoryCache");
		setLimit(Runtime.getRuntime().maxMemory() / 4);
	}

	public void setLimit(long new_limit) {
		limit = new_limit;
		Log.i(TAG, "MemoryCache will use up to " + limit / 1024. / 1024. + "MB");
	}

	public Bitmap get(String id) {
		Log.v(tag,"+++++++++++++++++: GET STRING IMAGE");
		Log.v(tag,"checking memcache for id:" + id);
		try {
			if (!cache.containsKey(id)) {
				Log.e(tag,"cache does not contain:" + id);
				return null;
			}

			// NullPointerException sometimes happen here
			// http://code.google.com/p/osmdroid/issues/detail?id=78
			Log.e(tag,"memcache: id found! :" + id);
			return cache.get(id);
		} catch (NullPointerException ex) {
			ex.printStackTrace();
			return null;
		}
	}

	public void put(String id, Bitmap bitmap) {
		Log.v(tag,"+++++++++++++++++ put function:");
		Log.v(tag,": putting id bitmap:" + id);
		try {
			if (cache.containsKey(id)) {
				Log.v(tag,"memcache: cache contains id:, subtracting size");
				size -= getSizeInBytes(cache.get(id));
				Log.v(tag,"memcache: size:" + size);
			}

			cache.put(id, bitmap);
			size += getSizeInBytes(bitmap);
			Log.v(tag,": cache DOES NOT CONTAIN id:, subtracting size");
			Log.v(tag,": size:" + size);

			checkSize();
		} catch (Throwable th) {
			th.printStackTrace();
		}
	}

	private void checkSize() {
		Log.v(tag,"+++++++++++++++++:CHECK SIZE FUNCTION +++++++++++");
		Log.i(TAG, "cache size=" + size + " length=" + cache.size());
		if (size > limit) {
			Log.v(tag,"CHECK SIZE: size is greater than limit");
			Iterator<Entry<String, Bitmap>> iter = cache.entrySet().iterator();// least
																				// recently
																				// accessed
																				// item
																				// will
																				// be
																				// the
																				// first
																				// one
																				// iterated
			while (iter.hasNext()) {
				Log.v(tag,":CHECK SIZE: iterating through cache entry set");
				Entry<String, Bitmap> entry = iter.next();
				Log.v(tag,"memcache: CHECK SIZE: current entry:" + entry.getKey());

				Log.v(tag,"memcache: CHECK SIZE: reducing size, entry size:"
						+ getSizeInBytes(entry.getValue()));
				size -= getSizeInBytes(entry.getValue());
				Log.v(tag,"memcache: CHECK SIZE: size, size:" + size);

				iter.remove();
				if (size <= limit)
					break;
			}
			Log.i(TAG, "Clean cache. New size " + cache.size());
		}
	}

	public void clear() {
		try {
			// NullPointerException sometimes happen here
			// http://code.google.com/p/osmdroid/issues/detail?id=78
			cache.clear();
			size = 0;
		} catch (NullPointerException ex) {
			ex.printStackTrace();
		}
	}

	long getSizeInBytes(Bitmap bitmap) {
		if (bitmap == null)
			return 0;
		return bitmap.getRowBytes() * bitmap.getHeight();
	}

	void log(String msg) {
		if (log == true)
			System.out.println(msg);
	}
}