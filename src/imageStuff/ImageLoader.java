package imageStuff;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Collections;
import java.util.Map;
import java.util.WeakHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Handler;
import android.util.Log;
import android.widget.ImageView;
import ez.pay.project.R;

public class ImageLoader {

	String tag = "IMAGE LOADER";

	// MEM CASH
	MemoryCache memoryCache = new MemoryCache();

	// FILE CACHE
	FileCache fileCache;

	// SNCY MAP
	private Map<ImageView, String> imageViews = Collections
			.synchronizedMap(new WeakHashMap<ImageView, String>());

	// EXECUTOR
	ExecutorService executorService;

	Boolean log = true;

	// HANDLER
	Handler handler = new Handler();// handler to display images in UI thread

	public ImageLoader(Context context) {
		Log.v(tag, "+++++++++++++++++:ImageLoader: IMAGE LOADER CREATED");
		// Log.v(tag, msg);
		fileCache = new FileCache(context);
		executorService = Executors.newFixedThreadPool(5);
	}

	// final int stub_id = R.drawable.stub;
	final int stub_id = R.drawable.act;

	public void DisplayImage(String url, ImageView imageView) {
		Log.v(tag, "+++++++++++++++++:ImageLoader:DisplayImage CALLED");

		imageViews.put(imageView, url);
		Log.v(tag, "i: added to imageView synchronized bitmap:" + url);

		// CHECKING BITMAP FOR URL:
		Log.v(tag,
				"i:ImageLoader:DisplayImage: attempting to fetch bitmap from memcachhe");
		Bitmap bitmap = memoryCache.get(url);
		// log("iiiiiiiiiiiiiiiiiiiiiiiiiiiiii: memoryCache.get(url) ---- getting bitmap from memCache");

		if (bitmap != null) {
			Log.v(tag, "image FOUND found in memcache:" + url);
			imageView.setImageBitmap(bitmap);
		} else {
			Log.e(tag,
					"i:ImageLoader:DisplayImage: bitmpa NOT found in memcache");
			queuePhoto(url, imageView);

			imageView.setImageResource(stub_id);
		}
	}

	private void queuePhoto(String url, ImageView imageView) {
		// log("iiiiiiiiiiiiiiiiiiiiiiiiiiiiii: queuePhoto(String url, ImageView imageView):"
		// + url);
		Log.v(tag, "++++++++++++++++++++++i::queuePhoto: queing phot:" + url);
		PhotoToLoad p = new PhotoToLoad(url, imageView);

		executorService.submit(new PhotosLoader(p));
	}

	private Bitmap getBitmap(String url) {
		Log.v(tag, "+++++++++++++i::queuePhoto: get BIT MAP");
		File f = fileCache.getFile(url);

		// from SD cache
		Bitmap b = decodeFile(f);
		if (b != null)
			return b;

		// from web
		try {
			Bitmap bitmap = null;
			URL imageUrl = new URL(url);
			HttpURLConnection conn = (HttpURLConnection) imageUrl
					.openConnection();
			conn.setConnectTimeout(30000);
			conn.setReadTimeout(30000);
			conn.setInstanceFollowRedirects(true);
			InputStream is = conn.getInputStream();
			OutputStream os = new FileOutputStream(f);
			Utils.CopyStream(is, os);
			os.close();
			conn.disconnect();
			// IVAN
			// bitmap = decodeFile(f);
			bitmap = decodeFile(f);
			return bitmap;
		} catch (Throwable ex) {
			ex.printStackTrace();
			if (ex instanceof OutOfMemoryError)
				memoryCache.clear();
			return null;

		}
	}

	// decodes image and scales it to reduce memory consumption
	private Bitmap decodeFile(File f) {
		try {
			// decode image size
			BitmapFactory.Options o = new BitmapFactory.Options();
			o.inJustDecodeBounds = true;
			FileInputStream stream1 = new FileInputStream(f);
			BitmapFactory.decodeStream(stream1, null, o);
			stream1.close();
			// Bitmap bitmap=BitmapFactory.decodeStream(stream1,null,o);
			// stream1.close();

			// Find the correct scale value. It should be the power of 2.
			final int REQUIRED_SIZE = 170;
			int width_tmp = o.outWidth, height_tmp = o.outHeight;
			int scale = 1;

			// Utils.log("**************************************************************");
			// Utils.log("_____WWWWWW_____WIDTH:" + width_tmp);
			// Utils.log("_____HHHHHH_____HEIGHT:" + height_tmp);
			// Utils.log("**************************************************************");
			// while(true){
			// if(width_tmp/2<REQUIRED_SIZE || height_tmp/2<REQUIRED_SIZE)
			// break;
			// width_tmp/=2;
			// height_tmp/=2;
			// scale*=2;
			// }

			// decode with inSampleSize
			BitmapFactory.Options o2 = new BitmapFactory.Options();
			o2.inSampleSize = scale;
			FileInputStream stream2 = new FileInputStream(f);
			Bitmap bitmap = BitmapFactory.decodeStream(stream2, null, o2);
			stream2.close();
			return bitmap;
		} catch (FileNotFoundException e) {

		} catch (OutOfMemoryError e) {
			e.printStackTrace();
			// POSSIBLE LOCATION OF OUT OF MEMORY ERROR
			log("MMMMMMMMMMMMMMMM:OutOfMemoryError out of memory error:");

		} catch (IOException e) {
			log("MMMMMMMMMMMMMMMM:IOException e out of memory error:");
			e.printStackTrace();
			// POSSIBLE LOCATION OF OUT OF MEMORY ERROR

		}
		return null;
	}

	// Task for the queue
	private class PhotoToLoad {
		public String url;
		public ImageView imageView;

		public PhotoToLoad(String u, ImageView i) {
			Log.v(tag, "i:ImageLoader:PhotoToLoad: NEW PHOTOLOAD ITEM:" + u);
			url = u;
			imageView = i;
		}
	}

	class PhotosLoader implements Runnable {
		PhotoToLoad photoToLoad;

		PhotosLoader(PhotoToLoad photoToLoad) {
			Log.v(tag, "++++++++++++++++++++++++++++++: PHOTOSLOADER CLASS");
			this.photoToLoad = photoToLoad;
		}

		@Override
		public void run() {
			Log.v(tag, "++++++++++: PHOTOSLOADER run function");
			try {
				if (imageViewReused(photoToLoad))
					return;
				Bitmap bmp = getBitmap(photoToLoad.url);
				memoryCache.put(photoToLoad.url, bmp);
				if (imageViewReused(photoToLoad))
					return;
				BitmapDisplayer bd = new BitmapDisplayer(bmp, photoToLoad);
				handler.post(bd);
			} catch (Throwable th) {
				th.printStackTrace();
			}
		}
	}

	boolean imageViewReused(PhotoToLoad photoToLoad) {
		// log("++++++++++++++++++++++++++++++:  boolean imageViewReused ");
		String tag = imageViews.get(photoToLoad.imageView);
		// log("iiiiiiiiiiiiiiiiiiiiiiiiiiiiii: tag string" + tag);
		if (tag == null || !tag.equals(photoToLoad.url)) {
			// log("iiiiiiiiiiiiiiiiiiiiiiiiiiiiii: tag==null || !tag.equals(photoToLoad.url)");
			return true;
		} else {
			// log("iiiiiiiiiiiiiiiiiiiiiiiiiiiiii: image NOT RE USED");
			return false;
		}

	}

	// Used to display bitmap in the UI thread
	class BitmapDisplayer implements Runnable {
		Bitmap bitmap;
		PhotoToLoad photoToLoad;

		public BitmapDisplayer(Bitmap b, PhotoToLoad p) {
			bitmap = b;
			photoToLoad = p;
		}

		public void run() {
			if (imageViewReused(photoToLoad))
				return;
			if (bitmap != null)
				photoToLoad.imageView.setImageBitmap(bitmap);
			else
				photoToLoad.imageView.setImageResource(stub_id);
		}
	}

	public void clearCache() {
		memoryCache.clear();
		fileCache.clear();
	}

	void log(String msg) {
		if (log == true)
			System.out.println(msg);
	}

}
