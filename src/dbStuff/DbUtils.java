package dbStuff;

import java.util.List;

import android.util.Log;

public class DbUtils {
	
	//DELETE PERSON
	public static void removeEnrollment(String phone){
		Boolean there = false;
		log("------------: check phone:"+phone);
		
		List<Personsugarorm> personThere = Personsugarorm.find(Personsugarorm.class, "phone_num = ?", phone);
		log("personThere size:"+personThere.size());
		
		log("deleting person");
		personThere.get(0).delete();
		
		
		//return there;
	}
	
	
	//CHECK IF PHONE NUMBER IS IN LOCAL DB
	public static boolean checkPhone(String phone){
		Boolean there = false;
		log("------------: check phone:"+phone);
		
		List<Personsugarorm> personThere = Personsugarorm.find(Personsugarorm.class, "phone_num = ?", phone);
		log("personThere size:"+personThere.size());
		
		if(personThere.size()>0){
			there = true;
			log("size greater than 0: there is:"+there);
		}else if(personThere.size()<1){
		    there = false;	
		    log("size less than 1: there is:"+there);
		}
		
		return there;
	}
	
	static void log(String msg){
		Log.v("OFFLINE FINGER", msg);
	}

}
