package dbStuff;


import android.content.Context;
import android.util.Log;

import com.orm.SugarRecord;

public class Personsugarorm  extends SugarRecord<Personsugarorm>  {

	public String firstName;
	public String lastName;
	public String phoneNum;
	public String pin;
	public String idNo;
	public String gender;
	public String dob;
	public String email;
	public String city;
	public String nokName;
	public String nokPhone;
	public String finger;
	public String fingerChoice;
	
	//EXTRA FEILDS FOR FUTURE
	public String extraone = "none"; //USED TO HOLD SENT TO SERVER RESULT
	public String extratwo;// USED TO THE TIME OF ENROLLMENT IN MILLISECS
	public String extrathree;// USED TO SAVE THE DATE TIME ENROLLMENT WAS SENT.
	public int extraintone; 
	public int extraintwo; 
	public int extraintthree;
	
	
	public String saved = "no";
	
	public Personsugarorm(Context arg0, PersonNonOrm person) {
		super(arg0);
		
		log("Personsugarorm CONSTRUCTOR");
		
	    firstName = person.firstName;
	    lastName = person.lastName;
	    phoneNum = person.phoneNum;
	    pin = person.pin;
	    idNo = person.idNo;
	    gender = person.gender;
	    dob = person.dob;
	    email = person.email;
	    city = person.city;
	    nokName = person.nokName;
	    nokPhone = person.nokPhone;
	    finger = person.finger;
	    fingerChoice = person.fingerChoice;
	    
	    //SET DATE AND TIME
	    //extraintone = Integer.valueOf(String.valueOf(System.currentTimeMillis()));
//	    extraintone = -1;
//	    extra
	   // log("TIME CREATED SET TO:"+extraintone);
	    		
		// TODO Auto-generated constructor stub
	}
	
	public Personsugarorm(Context arg0) {
		super(arg0);
		// TODO Auto-generated constructor stub
	}
	
	void log(String message) {
		Log.v("Personsugarorm", message);
	}
	
}
