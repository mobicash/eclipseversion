package dbStuff;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import utils.GenUtils;
import utils.Transaction;
import utils.UrlConnector;
import utils.Vars;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;
import ez.pay.project.Alerter;
import ez.pay.project.Globals;
import ez.pay.project.R;

public class SendOfflineEnrollment {

	List<Personsugarorm> personList = new ArrayList();

	Personsugarorm person;
	Alerter alerter;
	Vars vars;

	public boolean sendEnrollment(String phoneNum, Context context) {

		log("phoneNum:");

		boolean success = false;

		vars = new Vars(context);
		alerter = new Alerter(context);

		// GET PERSON OBJECT
		personList = Personsugarorm.find(Personsugarorm.class, "phone_num = ?",
				phoneNum);

		// IF PERSON IS NOT FOUND
		if (personList.size() <= 1) {
			success = false;
		} else if (personList.size() > 0) {
			// GET PERSON

			person = personList.get(0);
			log("loaded person:" + person.firstName);

			if (vars.cd.isConnectingToInternet()) {
				sendEnrollmentAsync sac = new sendEnrollmentAsync();
				sac.execute();
			} else {
				alerter.alerterError("NO INTERNET CONNECTION FOUND");
			}

		}

		return success;
	}

	// -----------------------------------------------------------------------------------------------------
	public class sendEnrollmentAsync extends AsyncTask<Void, Void, String> {

		public String theResult = "NO CONNECTION MADE";
		public String action;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			System.out.println("STARTING OFLINE ENROLMENT ASYCN");
			vars.pd = new ProgressDialog(vars.context);
			vars.pd.setTitle("Sending Enrollment...");
			vars.pd.setMessage("Please wait.");
			vars.pd.setCancelable(false);
			vars.pd.setIndeterminate(true);
			vars.pd.show();
		}

		@Override
		// protected String doInBackground(Void... arg0) {
		protected String doInBackground(Void... arg0) {

			// VARS TO BE SENT TO SERVER
			List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(8);

			// AGENT NUMBER
			nameValuePairs.add(new BasicNameValuePair("agentNumber", vars.prefs
					.getString("agentPhone", null)));
			log("SENT TO SERVER: AGENT PHONE:"
					+ vars.prefs.getString("agentPhone", null));

			// CLIENT PHONE NUMBER
			nameValuePairs.add(new BasicNameValuePair("clientnumber",
					person.phoneNum));

			// FIRST NAME
			nameValuePairs.add(new BasicNameValuePair("firstname",
					person.firstName));

			// LAST NAME
			nameValuePairs.add(new BasicNameValuePair("lastname",
					person.lastName));

			// ID NO
			nameValuePairs.add(new BasicNameValuePair("idnumber", person.idNo));

			// peen
			nameValuePairs
					.add(new BasicNameValuePair("clientcode", person.pin));

			// NOK NAME
			nameValuePairs.add(new BasicNameValuePair("cli_nok_name",
					person.nokName));

			// NOK PHONE
			nameValuePairs.add(new BasicNameValuePair("cli_nok_mobile",
					person.nokPhone));

			// GENDER
			nameValuePairs.add(new BasicNameValuePair("cli_gender",
					person.gender));

			// DOB
			nameValuePairs
					.add(new BasicNameValuePair("cli_day_dob", person.dob));

			// CITY

			log("city:" + person.city);
			// FINGER CHOICE
			nameValuePairs.add(new BasicNameValuePair("fingerChoice",
					person.fingerChoice));

			// FINGER TEMPLATE
			nameValuePairs.add(new BasicNameValuePair("fingertemplate",
					person.finger));

			// //FACE PHOTO
			nameValuePairs.add(new BasicNameValuePair("facePic", GenUtils
					.getPicString(person, "face", vars)));

			//
			// //ID PHOTO
			nameValuePairs.add(new BasicNameValuePair("idPic", GenUtils
					.getPicString(person, "id", vars)));

			// IMAGE STUFF

			// CREATE CONNECT OBJECT
			UrlConnector conn = new UrlConnector(vars.server
					+ "/bio-api/android_offline_enroll/offlineEnroll.php",
					nameValuePairs, vars);

			theResult = conn.connectt();
			log("THE RESULT:" + theResult);
			return theResult;
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			System.out.println("its done...................");
			// REMOVE DIALOG
			if (vars.pd != null) {
				vars.pd.dismiss();
				// b.setEnabled(true);
			}

			vars.trans = new Transaction(result);

			if (result != null) {

				vars.trans = new Transaction(result);

				// IF OUTPUT WAS RECEIVED FROM SERVER
				if (vars.trans.getResult() != null) {

					log("TRANS RESULT:" + vars.trans.getResult());

					// if (result.contains("ccess")) {
					if (vars.trans.getResult().contains("ccess")) {

						LayoutInflater li = LayoutInflater.from(vars.context
								.getApplicationContext());
						View promptsView;
						promptsView = li.inflate(
								R.layout.dialog_success_simple, null);

						TextView msgTxt = (TextView) promptsView
								.findViewById(R.id.success_simple_message);

						msgTxt.setText(vars.trans.getResult());

						AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
								vars.context);

						// UPDATE PERSON OBJECT
						person.saved = "yes";
						person.extraone = "";
						person.save();

						// SENT BROADCAST THAT ENROLLMENT DONE
						Intent broadcastIntent = new Intent();
						broadcastIntent.setAction(Globals.ENROLLMENT_SENT);
						broadcastIntent.addCategory(Intent.CATEGORY_DEFAULT);
						broadcastIntent.putExtra("personNum", person.phoneNum);
						System.out
								.println("sending sent to server broadcast......................");
						vars.context.sendBroadcast(broadcastIntent);

						// set prompts.xml to alertdENroialog builder
						alertDialogBuilder.setView(promptsView);
						// set dialog message
						alertDialogBuilder
								.setCancelable(false)
								.setPositiveButton("OK",
										new DialogInterface.OnClickListener() {
											public void onClick(
													DialogInterface dialog,
													int id) {
												// Intent i = new Intent();
												// i.setClass(vars.context.getApplicationContext(),
												// Mcash_main_activity.class);
												// vars.context.startActivity(i);

											}
										})
								.setNegativeButton("Cancel",
										new DialogInterface.OnClickListener() {
											public void onClick(
													DialogInterface dialog,
													int id) {
												dialog.cancel();
											}
										});

						// create alert dialog
						AlertDialog alertDialog = alertDialogBuilder.create();
						// show it
						alertDialog.show();

					} else {
						// alerter.alerterError(trans.getResult());
						alerter.alerterError(vars.trans.getError());
						// SAVE ERROR
						person.extraone = vars.trans.getError();
						person.save();

						// SENT BROADCAST THAT ENROLLMENT DONE
						Intent broadcastIntent = new Intent();
						broadcastIntent.setAction(Globals.ENROLLMENT_SENT);
						broadcastIntent.addCategory(Intent.CATEGORY_DEFAULT);
						broadcastIntent.putExtra("personNum", person.phoneNum);
						System.out
								.println("sending sent to server broadcast......................");
						vars.context.sendBroadcast(broadcastIntent);
					}
				} else {

					alerter.alerterError("INCORRECT REPLY FROM SERVER - enrollment not sent");

					person.extraone = "INCORRECT REPLY FROM SERVER";
					person.save();

					// SENT BROADCAST THAT ENROLLMENT DONE
					Intent broadcastIntent = new Intent();
					broadcastIntent.setAction(Globals.ENROLLMENT_SENT);
					broadcastIntent.addCategory(Intent.CATEGORY_DEFAULT);
					broadcastIntent.putExtra("personNum", person.phoneNum);
					System.out
							.println("sending sent to server broadcast......................");
					vars.context.sendBroadcast(broadcastIntent);

				}

			} else {

				alerter.alerterError("EMPTY REPLY FROM SERVER");
				person.extraone = "EMPTY REPLY FROM SERVER";
				person.save();

				// SENT BROADCAST THAT ENROLLMENT DONE
				Intent broadcastIntent = new Intent();
				broadcastIntent.setAction(Globals.ENROLLMENT_SENT);
				broadcastIntent.addCategory(Intent.CATEGORY_DEFAULT);
				broadcastIntent.putExtra("personNum", person.phoneNum);
				System.out
						.println("sending sent to server broadcast......................");
				vars.context.sendBroadcast(broadcastIntent);

			}
		}

	}

	void log(String msg) {
		Log.e("SendOfflineEnrollment", msg);
	}

	// decodes image and scales it to reduce memory consumption
	private Bitmap decodeFile(File f) {
		try {
			// decode image size
			BitmapFactory.Options o = new BitmapFactory.Options();
			o.inJustDecodeBounds = true;
			FileInputStream stream1 = new FileInputStream(f);
			BitmapFactory.decodeStream(stream1, null, o);
			stream1.close();
			// Bitmap bitmap=BitmapFactory.decodeStream(stream1,null,o);
			// stream1.close();

			// Find the correct scale value. It should be the power of 2.
			final int REQUIRED_SIZE = 170;
			int width_tmp = o.outWidth, height_tmp = o.outHeight;
			int scale = 1;

			// Utils.log("**************************************************************");
			// Utils.log("_____WWWWWW_____WIDTH:" + width_tmp);
			// Utils.log("_____HHHHHH_____HEIGHT:" + height_tmp);
			// Utils.log("**************************************************************");
			// while(true){
			// if(width_tmp/2<REQUIRED_SIZE || height_tmp/2<REQUIRED_SIZE)
			// break;
			// width_tmp/=2;
			// height_tmp/=2;
			// scale*=2;
			// }

			// decode with inSampleSize
			BitmapFactory.Options o2 = new BitmapFactory.Options();
			o2.inSampleSize = scale;
			FileInputStream stream2 = new FileInputStream(f);
			Bitmap bitmap = BitmapFactory.decodeStream(stream2, null, o2);
			stream2.close();
			return bitmap;
		} catch (FileNotFoundException e) {

		} catch (OutOfMemoryError e) {
			e.printStackTrace();
			// POSSIBLE LOCATION OF OUT OF MEMORY ERROR
			log("MMMMMMMMMMMMMMMM:OutOfMemoryError out of memory error:");

		} catch (IOException e) {
			log("MMMMMMMMMMMMMMMM:IOException e out of memory error:");
			e.printStackTrace();
			// POSSIBLE LOCATION OF OUT OF MEMORY ERROR

		}
		return null;
	}

}
