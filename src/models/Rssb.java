package models;

public class Rssb {

	private String result;
	private String message;
	private String errorCode;
	private String reply;
	private String name;
	private String householdNID;
	private String totalPremium;
	private String alreadyPaid;
	private String eligibility;
	private String houseHoldCategory;
	private String numberOfMembers;
	private String invoice;
	private String district;
	private String province;

	public Rssb() {

	}

	public String getDistrict() {
		return district;
	}

	public void setDistrict(String district) {
		this.district = district;
	}

	public String getProvince() {
		return province;
	}

	public void setProvince(String province) {
		this.province = province;
	}

	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}

	public String getReply() {
		return reply;
	}

	public void setReply(String reply) {
		this.reply = reply;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getHouseholdNID() {
		return householdNID;
	}

	public void setHouseholdNID(String householdNID) {
		this.householdNID = householdNID;
	}

	public String getTotalPremium() {
		return totalPremium;
	}

	public void setTotalPremium(String totalPremium) {
		this.totalPremium = totalPremium;
	}

	public String getAlreadyPaid() {
		return alreadyPaid;
	}

	public void setAlreadyPaid(String alreadyPaid) {
		this.alreadyPaid = alreadyPaid;
	}

	public String getEligibility() {
		return eligibility;
	}

	public void setEligibility(String eligibility) {
		this.eligibility = eligibility;
	}

	public String getHouseHoldCategory() {
		return houseHoldCategory;
	}

	public void setHouseHoldCategory(String houseHoldCategory) {
		this.houseHoldCategory = houseHoldCategory;
	}

	public String getNumberOfMembers() {
		return numberOfMembers;
	}

	public void setNumberOfMembers(String numberOfMembers) {
		this.numberOfMembers = numberOfMembers;
	}

	public String getInvoice() {
		return invoice;
	}

	public void setInvoice(String invoice) {
		this.invoice = invoice;
	}

}
