package models;

public class Nfc {

	private String customerType;
	private String nfcTagSerial;
	private String pspId;
	private String dateTime;
	private String customerWalletId;

	public Nfc() {

	}

	public String getCustomerType() {
		return customerType;
	}

	public void setCustomerType(String customerType) {
		this.customerType = customerType;
	}

	public String getNfcTagSerial() {
		return nfcTagSerial;
	}

	public void setNfcTagSerial(String nfcTagSerial) {
		this.nfcTagSerial = nfcTagSerial;
	}

	public String getPspId() {
		return pspId;
	}

	public void setPspId(String pspId) {
		this.pspId = pspId;
	}

	public String getDateTime() {
		return dateTime;
	}

	public void setDateTime(String dateTime) {
		this.dateTime = dateTime;
	}

	public String getCustomerWalletId() {
		return customerWalletId;
	}

	public void setCustomerWalletId(String customerWalletId) {
		this.customerWalletId = customerWalletId;
	}

	@Override
	public String toString() {
		return "{\"customerType\": " + "\"" + customerType + "\""
				+ ", \"nfcTagSerial\": " + "\"" + nfcTagSerial + "\""
				+ ", \"pspId\": " + "\"" + pspId + "\"" + ", \"dateTime\": "
				+ "\"" + dateTime + "\"" + ", \"customerWalletId\": " + "\""
				+ customerWalletId + "\"" + "}";
	}

}
