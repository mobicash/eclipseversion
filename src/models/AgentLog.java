package models;

public class AgentLog {

	private String result;
	private String error;
	private String date;
	private String amount;
	private String status;
	private String transferId;
	private String transferName;
	private String description;
	private String name;

	public AgentLog() {

	}

	public AgentLog(String result, String error, String date, String amount,
			String status, String transferId, String transferName,
			String description, String name) {
		super();
		this.result = result;
		this.error = error;
		this.date = date;
		this.amount = amount;
		this.status = status;
		this.transferId = transferId;
		this.transferName = transferName;
		this.description = description;
		this.name = name;
	}

	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getTransferId() {
		return transferId;
	}

	public void setTransferId(String transferId) {
		this.transferId = transferId;
	}

	public String getTransferName() {
		return transferName;
	}

	public void setTransferName(String transferName) {
		this.transferName = transferName;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
