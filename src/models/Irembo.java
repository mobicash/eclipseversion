package models;

public class Irembo {

	private String message;
	private String result;
	private String error;
	private String biller_code;
	private String bill_number;
	private String description;
	private String custome_r_name;
	private String creation_date;
	private String amount;
	private String customer_id_number;
	private String mobile;
	private String rra_account_name;
	private String rra_account_number;

	public Irembo() {

	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}

	public String getBiller_code() {
		return biller_code;
	}

	public void setBiller_code(String biller_code) {
		this.biller_code = biller_code;
	}

	public String getBill_number() {
		return bill_number;
	}

	public void setBill_number(String bill_number) {
		this.bill_number = bill_number;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getCustome_r_name() {
		return custome_r_name;
	}

	public void setCustome_r_name(String custome_r_name) {
		this.custome_r_name = custome_r_name;
	}

	public String getCreation_date() {
		return creation_date;
	}

	public void setCreation_date(String creation_date) {
		this.creation_date = creation_date;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getCustomer_id_number() {
		return customer_id_number;
	}

	public void setCustomer_id_number(String customer_id_number) {
		this.customer_id_number = customer_id_number;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getRra_account_name() {
		return rra_account_name;
	}

	public void setRra_account_name(String rra_account_name) {
		this.rra_account_name = rra_account_name;
	}

	public String getRra_account_number() {
		return rra_account_number;
	}

	public void setRra_account_number(String rra_account_number) {
		this.rra_account_number = rra_account_number;
	}

}
