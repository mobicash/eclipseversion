package adapters;


import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import ez.pay.project.R;

public class FingerChoiceListAdapter extends ArrayAdapter<String> {

	Context mContext;
	int layoutResourceId;
	String[] data;

	public FingerChoiceListAdapter(Context mContext, int layoutResourceId,
			String[] data) {

		super(mContext, layoutResourceId, data);

		this.layoutResourceId = layoutResourceId;
		this.mContext = mContext;
		this.data = data;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		if (convertView == null) {
			// inflate the layout
			LayoutInflater inflater = ((Activity) mContext).getLayoutInflater();
			convertView = inflater.inflate(layoutResourceId, parent, false);
		}

		// object item based on the position
		String objectItem = data[position];

		// finger text
		TextView tvFingerText = (TextView) convertView
				.findViewById(R.id.tvFingerText);
		tvFingerText.setText(objectItem);

		return convertView;

	}

}
