package adapters;

import java.util.List;

import models.Area;
import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import ez.pay.project.R;

public class AreaAdapter extends ArrayAdapter<Area> {

	Context mContext;
	int layoutResourceId;
	List<Area> areaList = null;
	LayoutInflater inflater;
	int defaultValue;

	public AreaAdapter(Context mContext, int layoutResourceId,
			List<Area> areaList, int defaultValue) {

		super(mContext, layoutResourceId, areaList);

		this.layoutResourceId = layoutResourceId;
		this.mContext = mContext;
		this.areaList = areaList;
		this.defaultValue = defaultValue;
		inflater = ((Activity) mContext).getLayoutInflater();
	}

	@Override
	public View getDropDownView(int position, View convertView, ViewGroup parent) {
		return getCustomView(position, convertView, parent);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		return getCustomView(position, convertView, parent);
	}

	public View getCustomView(int position, View convertView, ViewGroup parent) {

		/********** Inflate spinner_rows.xml file for each row ( Defined below ) ************/
		View row = inflater.inflate(R.layout.spinner_rows, parent, false);

		/***** Get each Model object from Arraylist ********/

		Area area = areaList.get(position);
		
		// if (area.getDefaultValue() == "true") {
		//
		// }

		TextView label = (TextView) row.findViewById(R.id.tv_area);

		// Set values for spinner each row
		label.setText(area.getValue());
		label.setTag(area.getId());

		return row;

	}

}
