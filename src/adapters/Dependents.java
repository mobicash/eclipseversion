package adapters;

public class Dependents {
	
	private String name;
	private String relationship;
	
	
	public Dependents(String name, String relationship){
		System.out.println("+++++++++++++++++: DEPENDENTS CREATE");
		log("name:"+name);
		this.name = name;
		log("relationship:"+relationship);
		this.relationship = relationship;
	}

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getRelationship() {
		return relationship;
	}
	public void setRelationship(String relationship) {
		this.relationship = relationship;
	}
	

	void log(String msg) {
		System.out.println(msg);
	}


}
