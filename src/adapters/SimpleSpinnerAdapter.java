package adapters;

import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import ez.pay.project.R;

public class SimpleSpinnerAdapter extends ArrayAdapter<String> {

	Context mContext;
	int layoutResourceId;
	List<String> stringList = null;
	LayoutInflater inflater;
	int defaultValue;

	public SimpleSpinnerAdapter(Context mContext, int layoutResourceId,
			List<String> stringList, int defaultValue) {

		super(mContext, layoutResourceId, stringList);

		this.layoutResourceId = layoutResourceId;
		this.mContext = mContext;
		this.stringList = stringList;
		this.defaultValue = defaultValue;
		inflater = ((Activity) mContext).getLayoutInflater();
	}

	@Override
	public View getDropDownView(int position, View convertView, ViewGroup parent) {
		return getCustomView(position, convertView, parent);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		return getCustomView(position, convertView, parent);
	}

	public View getCustomView(int position, View convertView, ViewGroup parent) {

		/********** Inflate spinner_rows.xml file for each row ( Defined below ) ************/
		View row = inflater.inflate(R.layout.simple_spinner_row, parent, false);

		/***** Get each Model object from Arraylist ********/

		// if (area.getDefaultValue() == "true") {
		//
		// }

		TextView label = (TextView) row.findViewById(R.id.tv_name);
		// Set values for spinner each row
		label.setText(stringList.get(position));

		label.setGravity(Gravity.CENTER);
		label.setPadding(5, 0, 5, 16);
		// int left, int top, int right, int bottom
		label.setTextSize(16);

		return row;

	}

	public static int getPosition(List<String> stringList, String value) {
		int position = 0;

		for (String string : stringList) {

			if (stringList.get(position).equals(value)) {
				break;
			} else {
				position++;
			}

		}

		return position;
	}

}
