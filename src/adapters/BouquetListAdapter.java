package adapters;

import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import ez.pay.project.Bouquet;
import ez.pay.project.R;

public class BouquetListAdapter extends ArrayAdapter<Bouquet> {
	

    Context mContext;
    int layoutResourceId;
    List<Bouquet> data = null;

    public BouquetListAdapter(Context mContext, int layoutResourceId, List<Bouquet> data) {

        super(mContext, layoutResourceId, data);

        this.layoutResourceId = layoutResourceId;
        this.mContext = mContext;
        this.data = data;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {


        if (convertView == null) {
            // inflate the layout
            LayoutInflater inflater = ((Activity) mContext).getLayoutInflater();
            convertView = inflater.inflate(layoutResourceId, parent, false);
        }

        // object item based on the position
        Bouquet objectItem = data.get(position);

        // get the TextView and then set the text (item name) and tag (item ID) values
        
        
        //price
        TextView tvPrice = (TextView) convertView.findViewById(R.id.tvPrice);
        tvPrice.setTag(objectItem.getPrice());
        tvPrice.setText(objectItem.getName());
        tvPrice.setVisibility(View.GONE);
        
        // bouquet
        TextView tvBouquet = (TextView) convertView.findViewById(R.id.tvText);
        tvBouquet.setTag(objectItem.getId());
        tvBouquet.setText(objectItem.getName()+" ("+objectItem.getPrice()+")");


        return convertView;

    }

}
