package adapters;

import java.util.List;

import models.AgentLog;
import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import ez.pay.project.R;

public class AgentLogsAdapter extends ArrayAdapter<AgentLog> {

	Context mContext;
	int layoutResourceId;
	List<AgentLog> data = null;

	public AgentLogsAdapter(Context mContext, int layoutResourceId,
			List<AgentLog> data) {

		super(mContext, layoutResourceId, data);

		this.layoutResourceId = layoutResourceId;
		this.mContext = mContext;
		this.data = data;
	}

	static class ViewHolder {
		TextView names;
		TextView amount;
		TextView transactionName;

	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		if (convertView == null) {
			// inflate the layout
			LayoutInflater inflater = ((Activity) mContext).getLayoutInflater();
			convertView = inflater.inflate(layoutResourceId, parent, false);

			ViewHolder viewHolder = new ViewHolder();
			viewHolder.names = (TextView) convertView
					.findViewById(R.id.txt_name);

			viewHolder.amount = (TextView) convertView
					.findViewById(R.id.txt_amount);

			viewHolder.transactionName = (TextView) convertView
					.findViewById(R.id.text_transaction_name);

			convertView.setTag(viewHolder);

		}
		// object item based on the position
		AgentLog objectItem = data.get(position);

		ViewHolder holder = (ViewHolder) convertView.getTag();
		holder.names.setText(objectItem.getName());
		holder.amount.setText(objectItem.getAmount());
		holder.transactionName.setText(objectItem.getTransferName());

		// fill data

		return convertView;

	}

}
